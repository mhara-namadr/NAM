#!/bin/bash

PROJ=$1

cp $PROJ/"appsettings.$BRANCH_NAME.json" $PROJ/"appsettings.json" -v


REPO="$ACR_URL/$(echo $PROJ | awk '{print tolower($0)}')"

TAG="$2"

ENV_STR="$3"

ENV="${ENV_STR:=dev}"



echo "Branch: $BRANCH_NAME"
echo "Deploy $PROJ under environment $ENV_STR with image $REPO:$TAG"



docker build -t $(echo $PROJ | awk '{print tolower($0)}'):$TAG --build-arg PROJ=$PROJ -f .devops/Dockerfile ../

docker tag $(echo $PROJ | awk '{print tolower($0)}'):$TAG $REPO:$TAG
#docker tag $(echo $PROJ | awk '{print tolower($0)}') $REPO:latest

#docker login $ACR_URL -u $ACR_LOGIN -p $ACR_KEY

docker push $REPO:$TAG

#NAMESPACE=$(echo $PROJ | awk '{gsub("_","-",$0); print tolower($0)}') \

NAMESPACE="$ENV-"$(echo $PROJ | awk '{gsub("_","-",$0); print tolower($0)}') \
      && ADDR=$(echo $PROJ | awk '{gsub("_","-",$0); print tolower($0)}')".$ENV."$(jq -r '."ingress-dns"' .devops/project.json) \
      && NAME=$(jq -r '."deployment-name"' .devops/project.json)

helm upgrade --install \
      -n $NAMESPACE \
      --create-namespace \
      $NAME .devops/fulcrumone/ \
      --set image.repository="$REPO" \
      --set image.tag="$TAG" \
      --set ingress.hosts[0].host="$ADDR" \
      --set ingress.hosts[0].path="$ADDR" \
      --set ingress.tls[0].hosts[0]="$ADDR" \
      --set ingress.tls[0].secretName="tls-secret" \
      --values .devops/fulcrumone/values.yaml
