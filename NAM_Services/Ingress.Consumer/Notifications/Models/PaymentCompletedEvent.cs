using Ingress.Consumer.Models;
 

namespace Ingress.Consumer.Notifications.Models
{
    public class PaymentCompletedEvent : Event
    {
        public long OrderId { get; set; }
        public long PaymentId { get; set; }
    }
}