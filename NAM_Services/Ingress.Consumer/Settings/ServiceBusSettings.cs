namespace Ingress.Consumer.Settings
{
    public class ServiceBusSettings
    {
        public string ConnectionString { get; set; }
        public string TopicName { get; set; }
    }
}