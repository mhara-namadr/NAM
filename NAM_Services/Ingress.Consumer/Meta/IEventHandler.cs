using Ingress.Consumer.Models;
using System.Threading.Tasks; 

namespace Ingress.Consumer.Meta
{
    public interface IEventHandler<in TEvent> : IEventHandler where TEvent : Event
    {
        Task Handle(TEvent @event);
    }

    public interface IEventHandler
    {
    }
}