

using Ingress.Producer.Models;

namespace Ingress.Producer.Notifications.Models
{
    public class PaymentCompletedEvent : Event
    {
        public long OrderId { get; set; }
        public long PaymentId { get; set; }
    }
}