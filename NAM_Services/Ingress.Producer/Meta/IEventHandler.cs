using System.Threading.Tasks;
using Ingress.Producer.Models; 

namespace Ingress.Producer.Meta
{
    public interface IEventHandler<in TEvent> : IEventHandler where TEvent : Event
    {
        Task Handle(TEvent @event);
    }

    public interface IEventHandler
    {
    }
}