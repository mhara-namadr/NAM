using Ingress.Producer.Models;
using System.Collections.Generic;
using System.Threading.Tasks; 

namespace Ingress.Producer.Meta
{
    public interface IEventDispatcher<TEvent> where TEvent : Event
    {
        void Publish(TEvent @event);
        Task PublishAsync(TEvent @event);
        Task PublishManyAsync(ICollection<TEvent> @events);
    }
}