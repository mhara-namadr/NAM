using Ingress.Producer.Models;
using System; 

namespace Ingress.Producer.Meta
{
    public interface IEventBus : IDisposable
    { 
        void Subscribe<TEvent, TEventHandler>()
            where TEvent : Event
            where TEventHandler : IEventHandler<TEvent>;
        
        void Unsubscribe<TEvent, TEventHandler>()
            where TEvent : Event
            where TEventHandler : IEventHandler<TEvent>;
    }
}