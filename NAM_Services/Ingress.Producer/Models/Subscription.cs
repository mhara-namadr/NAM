using System;

namespace Ingress.Producer.Models
{
    public class Subscription
    {
        public Type HandlerType { get; private set; }

        public Subscription(Type handlerType)
        {
            HandlerType = handlerType;
        }
    }
}