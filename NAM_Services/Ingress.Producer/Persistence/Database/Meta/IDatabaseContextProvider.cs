using Microsoft.EntityFrameworkCore;
using System; 

namespace Ingress.Producer.Persistence.Database.Meta
{
    public interface IDatabaseContextProvider<TContext> : IDisposable where TContext : DbContext
    {
        TContext GetContext();
    }
}