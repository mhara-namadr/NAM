using System;
using Microsoft.Azure.ServiceBus;

namespace Ingress.Producer.AzureServiceBus.Meta
{
    public interface IServiceBusConnectionManager : IDisposable
    {
        ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder { get; }
        ITopicClient CreateTopicClient();
    }
}