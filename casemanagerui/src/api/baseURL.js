
//export const BASE_URL_JWT = 'http://localhost:645XX';


 export const BASE_URL_JWT = 'https://ssoservice.fulcrumone.net';
 export const GENERATE_TOKEN = BASE_URL_JWT+'/api/generateToken';
 export const REFRESH_TOKEN = BASE_URL_JWT+'/api/token/refresh';
 export const USER_ROLES=BASE_URL_JWT+'/api/GetUserRole?email=';
 export const CREATE_COMMON_USER = BASE_URL_JWT+'/api/Users';
 export const CHANGE_PASSWORD=BASE_URL_JWT+'/api/ChangePassword';
 export const GET_COLLABORATOR=BASE_URL_JWT+'/api/GetAllUser?role=';
export const REVOKE=BASE_URL_JWT+'/api​/token​/revoke';


