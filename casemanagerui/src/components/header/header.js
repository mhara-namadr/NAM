import React from "react";
import './header.scss'
import F1HeaderNav from 'f1-header-navigation-react'
import logo from '../../assets/img/logoSqr.jpg'

class Header extends React.Component {
  constructor() {
    super();
    this.state = {
      timeout:1000 * 1800 * 1,
      showModal: false,
      userLoggedIn: false,
      isTimedOut: false,
      selectedProduct: 1,
    }

  }

  
  ////////////////////
  logout = () => {  
    this.props.history.push("/login");
    window.localStorage.clear();
    sessionStorage.clear();
  }

  logoClick = () => {
    this.props.history.push("/home");
  }

  CustomBrand = ({}) => <div className="my-custom-brand">
  {/* <img src={logo} alt="" style={{width: '40px'}} />                 */}
</div>

onRedirect=()=>{
  this.props.history.push("/home")
}
onItemClick = (item, child_item) =>{
//console.log("Item Clicked!", item, child_item)

 if(child_item && child_item.id==="logout"){
  window.onbeforeunload = function() {
    sessionStorage.clear();
    localStorage.clear();
 }
 this.props.history.push('/logout')
}else if(child_item&&child_item.id==="changePassword"){
  this.props.history.push('/changePassword')
}else{
  this.props.history.push('/home')
}
}

 brandImgStyle = {width: '90px'}
 navBarStyle = {backgroundColor: '#225cb2'}
 navBarSimpleStyle = {backgroundColor: '#A9A9A9'}
 
  headerConfig = {
    navBarStyle : this.navBarStyle,
    navBarClassName : 'my-custom-nav-bar',
    brandType : 'text', // text/image/component, required
    brandText: <span className="branding"  onClick={this.onRedirect}><img src={logo} style={{width:"35px", height:"35px"}}/><b>NAM Neutrals Portal</b></span>,
    brandLink: '',
    brandImgUrl: "", // in case of type === image
    brandClass: 'my-brand', // custom class for brand styling
    brandStyle: this.brandImgStyle, // style to be apply on brand component
    disableHambergerIcon: true, // default true
    onItemClick: this.onItemClick,
    menus:{
        right:[
       
         
          {
            type: 'link', // link/dropdown
            label: 'Home',
            icon: '',
            href: '',
             // in case of dropdowns
            is_active: false,
            className: 'home',
            id:"home"
          },
          {
            type: 'dropdown', // link/dropdown
            label: '',
            icon: 'user',
            className: 'custom-dropdown',         
            childs: [
              {
                label:' '+ localStorage.getItem("email"),
                href: '',
                className: 'custom-option',
                icon: 'user',
                id:"email"
              },
              {
                label: ' Change Password',
                href: '',
                className: 'custom-option',
                icon: 'lock',
                id:"changePassword"
              },
              {
                label: ' Logout',
                href: '',
                className: 'custom-option',
                icon: 'sign-out',
                id:'logout'
              }
            ], // in case of dropdowns
            is_active: false,
          },
          
        ]
      }
  }
 
  render() {
    return (

      <div>
           <F1HeaderNav
            config={this.headerConfig}
            brandComponent={<this.CustomBrand />} // in case of type === component
          />
      
       </div>
    );
  }
}


export default Header;
