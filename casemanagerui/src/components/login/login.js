import React from "react";
 import './login.scss';
import '../../App.css';
import DynamicForm from 'f1-dynamic-form-react';
import Header from '../header/loginHeader';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {Link} from 'react-router-dom';
import {post,get} from '../../api/api';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';

import Signin from "f1-signin-form-react";

import {GENERATE_TOKEN,USER_ROLES,REVOKE} from '../../api/baseURL';
 import namlogo from '../../assets/img/namlogo.png';

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      show: false,
      email: '',
      password: '',
      current: { id: 1, email: "EstimatorAdmin", password: "123" },

      formData: [
        { "key": "email", "type": "email", "disabled": false, "id": "username", "icon": "user", "label": "User Name", "validators": [{ "name": "required", "value": true }] },
        { "key": "password", "type": "password", "disabled": false, "id": "password", "icon": "user", "label": "Password", "validators": [{ "name": "required", "value": true }, { "name": "minLength", "value": 2 }, { "name": "maxLength", "value": 10, "validationMessage": "Maximum 10 digits allowed." }] },
      ]

    };
  }

 

  onChangeHandler = (event, key) => {
    console.log("On change triggered!!!!!!!", event.target.value, key);     
  }

  submitForm = (event, data) => {  
    post(GENERATE_TOKEN,
      {
        "email": data.formData.email,
        "password": data.formData.password,
        "clientID":"F1"
      }
      ).then(res=>{
        if(res.status===200){
         // console.log("res",res)
         sessionStorage.setItem("token", res.data.accessToken);
         sessionStorage.setItem("refreshToken", res.data.refreshToken);
          localStorage.setItem("email", data.formData.email);
         get(USER_ROLES+data.formData.email).then(resp=>{
          // console.log("resp",resp)
          localStorage.setItem("role",resp.data.name)
         })

        // this.setState({show:true})

         this.props.history.push("/home")
        }
      }).catch(error=>{
        //console.log(error,"nnn","Invalid credentials.")
        if(error.status==400){
          this.setState({show:true})
        }
      
      })    
  };      

  rememberMe = (value) => {     
    //console.log(value.formData.rememberme);       
  };      

  forgotPass = (value) => {     
   // console.log(value.formData); 
  //  console.log('Forgot pass')     
    this.props.history.push('/forgotPassword')
  };      

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }
    this.setState({ show: false })
};

componentDidMount(){
  localStorage.clear()
}
  
  render() {
    return (
      <div className="main-container loginPage">
        {/* <Header history={this.props.history}/> */}
          
        <Grid container >
     
          <Grid item sm={12} md={12} className="loginBox" >
          
          <Snackbar open={this.state.show} autoHideDuration={1000} onClose={() => this.handleClose({ vertical: 'bottom', horizontal: 'right' })}><Alert severity="error" >Invalid credentials. !</Alert></Snackbar>

          

          <Paper className={""} style={{textAlign:"left"}}>
          <div className="loginheadbox">
            <div><img src={namlogo} /></div>Welcome to the Neutrals Portal
            </div>
          <Signin     
            Email={true}    
            UserName={false}
            RememberMe={true}
            ForgotPassword={true}
            titleAlign="left"
            SubmitButtonText="Log In"
            ForgotPasswordText="Forgot Password?"
            LableUsername="Username"
            PlaceholderUsername="Enter your Username"
            onSubmit={this.submitForm}
            onChange={this.rememberMe}
            onClick={this.forgotPass}
            design="material" // bootstrap
            materialOutline={true}
           // additionalHeading={<AdditionalHeading />}
            registerLink=""
            registerText={ 
              <p></p>
            // <p style={{textAlign:"center",padding:"5px"}}><Link to="/register">NEW USER? REGISTER HERE!</Link></p>
          }
            registerLinkAlign="left"
            registerClick={this.registerClick}
            extErrors={this.state.extErrors} // pass external errors here
            validationEvent="blur" // onchange/blur
          />
          
             {/* <p className="copyRight">Copyright © 2020 | All Rights Reserved. Powered by FulcrumOne</p> */}
            </Paper>  
            
          </Grid>
          {/* <Grid item sm={6} md={6} direction="column" className="contentBox">
            
            <h2>F1 CLI Login Page.</h2>
            <span>An easy and smart <br/> solution for quicker<br/> & POC and project setup.</span>
          </Grid> */}
          
        </Grid>
        
      </div>
    );
  }
}

export default Login;
