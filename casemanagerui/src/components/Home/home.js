import React from "react";
import './home.scss';
import Header from '../header/header';
import TabPanel from 'f1-tab-panel-react';
import FormJson from "./normalform.json";
import DynamicForm from "f1-dynamic-form-react";
import { formSetConfig } from "./formGroup.js";
import MatDatagrid from "f1-mat-data-grid-react";
import Checkbox from '@material-ui/core/Checkbox';

//for CSV 

import { CsvBuilder } from "filefy";
import "jspdf-autotable";
const jsPDF = typeof window !== "undefined" ? require("jspdf").jsPDF : null;



class Home extends React.Component {
  constructor() {
    super();
    this.dyFormRef = React.createRef(); 
    this.state = {key:"materialTable", 
    tabShowOnCondition:false,
    current: {},
    extErrors: {} ,

    defaultSort: false,   ///asc
      orderBy:3,
      displayProducts: [
        {
         casecode: 1011011,
         casename: 'Peter vs John',
         hdate:'12/10/2022',
         status:'Hold',
         location:'Denver'
        },
        {
          casecode: 1011012,
          casename: 'Dona vs Dolly',
          hdate:'13/10/2022',
          status:'In Progress',
          location:'Paris'
         }
      ],


      columns: [
        { title: 'Profile', field: 'imageUrl', render: rowData => <img src={rowData.imageUrl} style={{ width: 40, borderRadius: '50%' }} /> },
        { title: 'Name', field: 'name', sorting: false },
        // sorting false above
        { title: 'Surname', field: 'surname', sorting: true},
        { title: 'Sex', field: 'sex' },
            {
          title: 'Check1',
          field: 'Check1',        
          //render: selectCheck1,
          width: '80px',
          type: "boolean",
          editComponent: (props)=>{
              return(
                  <Checkbox
                  value = {props.value}
                  checked = {props.value}
                  disabled={props.rowData.Check2 ||props.rowData.Check3}
                  id = {"Check1"+props.rowData.id}
                  onChange = {(e)=>{
                      props.onChange(!props.value)
                      let data = {...props.rowData}
                      data.Check1 =!props.value;
                      props.onRowDataChange(data)

                  }}
                  />
              )
          }

      },
      {
        title: 'Check2',
        field: 'Check2',        
        //render: selectCheck1,
        width: '80px',
        type: "boolean",
        editComponent: (props)=>{
            return(
                <Checkbox
                value = {props.value}
                checked = {props.value}
                disabled={props.rowData.Check1 ||props.rowData.Check3}
                id = {"Check2"+props.rowData.id}
                onChange = {(e)=>{
                    props.onChange(!props.value)
                    let data = {...props.rowData}
                    data.Check2 =!props.value;
                    props.onRowDataChange(data)

                }}
                />
            )
        }

    },
    {
      title: 'Check3',
      field: 'Check3',        
      //render: selectCheck1,
      width: '80px',
      type: "boolean",
      editComponent: (props)=>{
          return(
              <Checkbox
              value = {props.value}
              checked = {props.value}
             // disabled={props.rowData.Check1 ||props.rowData.Check2}
              id = {"Check3"+props.rowData.id}
              onChange = {(e)=>{
                  props.onChange(!props.value)
                  let data = {...props.rowData}
                  data.Check3 =!props.value;
                  props.onRowDataChange(data)                  
              }}
              />
          )
      }

  },
        { title: 'Type', field: 'type', removable: false, hidden: false },
        // hidden true or false above
        { title: 'Birth Year', field: 'birthYear', type: 'date' },
        {
          title: 'Birth City',
          field: 'birthCity',
          lookup: { 34: 'Seattle', 63: 'Washington', 55: 'Chicago' },
        },
      ]
    
  };

  //Dynamic form functions
  this.customValidators = [
    {
      key: "name", // field name
      validator: this.customValidationFunction, // function to be called
      message: "This is custom validtion function message", // message to show
    },
  ];
  }

  onSelect =(k)=>{
    this.setState({key: k })
    console.log(this.state.key,"key",k)
  }

  
  onSignup = (model) => {
    console.log(model);
    // you can set error messages at any event for example when form submitted
    // you can general key to show general error messsage for whole form for e.g. "Something went wrong!"
    // for showing error messages for a field user field key as show below
    this.setState({
      extErrors: {
        'general': 'This is general external error.',
        'name': 'this is external error for name',
        'email': 'this is external error for email'
      }
    })
  };

  customValidationFunction = (key, value) => {
    console.log("Custome Validator:", key, value);
    if (value) return true;
    return false; // returns true means valid and false means invalid
  };

  onAutocompleteSelect = (key, value, item) =>{
    console.log("Autocompleted Selected", key, value, item);
  }

  autoCompleteItems = {
    country:[
            { id: 'IN', label: 'India' },
            { id: 'PAK', label: 'Pakistan' },
            { id: 'EN', label: 'England' },
            { id: 'US', label: 'United States' }
          ]
  }

  //mat data grid 

   //Data type: 'boolean', 'numeric', 'date', 'datetime', 'time', 'currency'

   addRow = (newData) => {
    console.log("addRow", newData)
    this.setState({ displayProducts: [...this.state.displayProducts, newData] });
    console.log(this.state.displayProducts)
  }

  rowUpdate = (newData, oldData) => {
    console.log(newData, oldData)
    const index = this.state.displayProducts.indexOf(oldData);
    this.setState({ displayProducts: this.state.displayProducts.map(item => item === oldData ? item = newData : item) });
  }

  rowDelete = (oldData) => {
    console.log("rowDelete", oldData)
    this.setState({ displayProducts: this.state.displayProducts.filter((item, i) => item != oldData) });
  }

  onSelectionChange = (evt, data) => {
    console.log(evt, data)
  }

  rowSelectDel = (evt, data) => {
    console.log(evt, data)
  }

  onChangePage = (page) => {
    console.log("onChangePage", page)
  }

  onChangeRowsPerPage = (pageSize) => {
    console.log("onChangeRowsPerPage", pageSize)
  }
  onFilterChange = (event) => {
    console.log(event)
  }

  onOrderChange = (column) => {
    console.log("onOrderChange using function", column)

    this.setState({ defaultSort: !this.state.defaultSort })
    let sort = this.state.defaultSort ? 'asc' : 'desc';
    this.setState({orderBy:column})

  }

  onSearchChange = (event) => {
    console.log(event)
  }

  onTreeExpandChange = (event)=>{
    console.log('onTreeExpandChange', event)
  }


//Generate CSV and PDF
  byString = (o, s) => {
    if (!s) {
      return;
    }

    s = s.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
    s = s.replace(/^\./, ""); // strip a leading dot
    var a = s.split(".");
    for (var i = 0, n = a.length; i < n; ++i) {
      var x = a[i];
      if (o && x in o) {
        o = o[x];
      } else {
        return;
      }
    }
    return o;
  }
  getFieldValue = (rowData, columnDef, lookup = true) => {
    let value =
      typeof rowData[columnDef.field] !== "undefined"
        ? rowData[columnDef.field]
        : this.byString(rowData, columnDef.field);
    if (columnDef.lookup && lookup) {
      value = columnDef.lookup[value];
    }

    return value;
  };

  isDataType(type) {
    let dataType = "normal";

    if (this.parentFunc) {
      dataType = "tree";
    } else if (this.columns.find((a) => a.tableData.groupOrder > -1)) {
      dataType = "group";
    }

    return type === dataType;
  }

  sort(a, b, type) {
    if (type === "numeric") {
      return a - b;
    } else {
      if (a !== b) {
        // to find nulls
        if (!a) return -1;
        if (!b) return 1;
      }
      return a < b ? -1 : a > b ? 1 : 0;
    }
  }

  getTableData = () => {
    const columns = this.state.columns
      .filter(
        (columnDef) =>
          (!columnDef.hidden || columnDef.export === true) &&
          columnDef.export !== false &&
          columnDef.field
      )
      // .sort((a, b) =>
      //   a.tableData.columnOrder > b.tableData.columnOrder ? 1 : -1
      // );
    const data = (this.state.displayProducts  //data for pdf generate this.state.displayProducts.filter((el)=>el.name=="XYZ")
    ).map((rowData) =>
      columns.map((columnDef) => this.getFieldValue(rowData, columnDef))
    );

    return [columns, data];
  }
  csvExp=()=>{
    //let fileName ="Demo" || "data";

   const [columns, data] = this.getTableData();
    const builder = new CsvBuilder("demo" + ".csv");
    builder
      .setDelimeter(",")
      .setColumns(columns.map((columnDef) => columnDef.title))
      .addRows(data)
      .exportFile();
  }
  pdfExp=()=>{
    let exportFileName="demo";
    let title="Mat table"
    if (jsPDF !== null) {
      const [columns, data] = this.getTableData();

      let content = {
        startY: 50,
        head: [columns.map((columnDef) => columnDef.title)],
        body: data,
      };

      const unit = "pt";
      const size = "A4";
      const orientation = "landscape";

      const doc = new jsPDF(orientation, unit, size);
      doc.setFontSize(15);
      doc.text(exportFileName || title, 40, 40);
      doc.autoTable(content);
      doc.save(
        (exportFileName || title || "data") + ".pdf"
      );
    }
  }

//   removeFile = (e, file, index) =>{
//     console.log(e, file, index);
// }
removeFile = (e, file, index,key) =>{
    console.log("dyFormRef",this.dyFormRef)
   this.dyFormRef.removeFile(e, file, index,key);
}


renderUploadedFilesItem = (file, index, key) =>{
    return (
      <li style={{borderColor:'#ccc'}} key={index}>
        <span className="file-name">{file.name}</span>
        <span className="action" onClick={(e)=>this.removeFile(e, file, index, key)}>
          <i className="fa fa-trash"></i>
          <button className="btn btn-primary">Upload</button>
        </span>
      </li>
    );
}

  //End PDF and CSV

  render(){
    const fileUploaderFields = ['file', 'file1']
    return  <div className="main-container homePage">
    <Header history={this.props.history} />
    <div className="analytics-section">
      </div>

      
      <div className="tabPanelDiv">
      
      <TabPanel
        onSelect={this.onSelect}
        activeTab={this.state.key}
        tabData =  {[
                    {eventKey:'materialTable',
                    title:'Cases',
                    
                    content:
                    <MatDatagrid
                    rows={this.state.displayProducts}
                    columns={
                      [
                        { title: 'Case code', field: 'casecode',width:'100px' },
                        { title: 'Case Name', field: 'casename', sorting: false, width:'500px' },
                        // sorting false above
                        { title: 'Hearing Date', field: 'hdate', sorting: true,width:'100px'},
                        { title: 'Status', field: 'status',width:'100px' },
                            
                        { title: 'Location', field: 'location', removable: false, hidden: false,width:'100px' },
                        // hidden true or false above
                        
                      ]
                    }
                    search={false}
                    grouping={false}                       //to show or hide grouping
                    addRow={this.addRow}                  //to Add row
                    addRowPosition={"first"}              //"last"
                    rowUpdate={this.rowUpdate}
                    rowDelete={this.rowDelete}
                    onSelectionChange={this.onSelectionChange}
                    rowSelectDel={this.rowSelectDel}
                    treeView={false}                      //to show or hide tree view
                    selection={false}                      //to show checkboxes
                    filtering={true}                      //to show column filters
                    title={"Case List"}              //Title
                    paging={true}                         // false -to show all records on single page
                    pageSize={5}                         //to set rows per page
                    showFirstLastPageButtons={false}       // show first and last page button in pagination
                    pageSizeOptions={[5, 10, 20, 50]}     //*false to hide* to show rows per page steps  
                    paginationType={"stepped"}             //"normal" or "stepped" 
                    exportButton={false}                   //Export to csv and pdf
                    exportAllData={true}
                    exportCsv={this.csvExp}             //custom csv
                    exportPdf={this.pdfExp}             //custom pdf
                    actionsColumnIndex={-1}               //Remove this prop to get action column at first position
                    onChangePage={this.onChangePage}
                    onChangeRowsPerPage={this.onChangeRowsPerPage}
                    onOrderChange={this.onOrderChange}         //on change of column sort
                    onFilterChange={this.onFilterChange}      //on change of filter
                    customPagination={{
                      isDisplay: false,
                      totalCount: 100,
                    }}
                    onSearchChange={this.onSearchChange}      //on search change
                    onTreeExpandChange={this.onTreeExpandChange}    //on tree expand click
                    sorting={false}     
          
                    //if using custom header
                    isCustomHeader={true}
          
                    actionColumnStyle={{width:"200px",textAlign:"center"}}  // Applicable only if using customHeader
                    orderDirection={this.state.defaultSort ? 'asc' : 'desc'}
                    orderBy ={this.state.orderBy }
                    stickyHeader = {{show:true,maxBodyHeight:'650px'}} 
                    // fixColumns = {{show:true,fixedColumns:{
                    //   left: 2,
                    //   right: 0
                    // } }}
                             //inline add edit
                      editable={{
                      editable:true,
                      showEdit:true,
                      showDelete:true,
                      showAdd:false
                    }}
          
                              // Custom Icons in grid
                    // customIcon= {{
                    //               Add: () => 'Add Row',Clear: () =>  <ThreeSixtyIcon />,
                    //               Edit: ()=> <img src= {EditImage} heigth="20px" width="20px"/>
                    //             }}
                                                          /* for customIcon[Check,Clear,Delete,DetailPanel,Edit,Export,Filter,FirstPage,LastPage,
                                                          NextPage,PreviousPage,ResetSearch,Search,SortArrow]*/
                  />
                  },
                  {eventKey:'Calendar',
                  title:'Calendar',
                  content:<h1>Calendar</h1>
  
                },
                  {eventKey:'dynamicForm',
                  title:'Pending Tasks',
                  content:<h1>Pending tasks</h1>
  
                },
                {eventKey:'timesheet',
                  title:'Timesheet',
                  content:<h1>Timesheet</h1>
  
                },
                {eventKey:'invoices',
                  title:'Invoices',
                  content:<h1>Invoices</h1>
  
                },
                    this.state.tabShowOnCondition?{eventKey:'contact',title:'Contact',content:'cccc3'}:'']}
        tabDesign = {"material"}              
        />
        </div>
      </div>
  }
}
export default Home;
