import React from "react";

import Switch from '@material-ui/core/Switch';

const fieldSetLabel = () =>{
	return (
		<div>
			<h1>Fieldset Lable 1</h1>
			<p>Sub label</p>
		</div>
		);
}

export const formSetConfig = [
	{
		"fieldSetClassName": "custom-fieldset",
		"fieldSetLabelType": "string", // string/component//accordian
		"fieldSetLabel": "",
		"columns":[
		
		  {
			"colmd":"6",
			"colxs":"6",
			"childType":"fieldset",
			"fieldsets":[
			  {
				"fieldSetClassName": "chilld-fieldset accordianDiv",
				"fieldSetLabelType": "accordian", // string/component/accordian
				"fielsetId":"fieldset1",
				"fieldSetLabel": "This is child1 with Accordian",
				"rows":[[	{
					"colmd":"12",
					"colxs":"12",
					"key": "date", 
					"label": "date bb", 
					"type":"date",
					"id":"dateSelect",
					"disabled": false,
					
					format:"DD/MM/YY",
					placeholder:"DD/MM/YY",
					"validators":[
						{"name":"required", "value":true}, 						
						]
					}]],
			  }
			,  {
				"fieldSetClassName": "chilld-fieldset accordianDiv",
				"fieldSetLabelType": "accordian", // string/component/accordian
				"fielsetId":"fieldset2",
				"fieldSetLabel": "This is child1 with Accordian",
				"rows":[[	{
					"colmd":"12",
					"colxs":"12",
					"key": "date", 
					"label": "date bb", 
					"type":"date",
					"id":"dateSelect",
					"disabled": false,
					
					format:"DD/MM/YY",
					placeholder:"DD/MM/YY",
					"validators":[
						{"name":"required", "value":true}, 						
						]
					}]]
			  }
			
			
			]
			}]
	},
{
	"fieldSetClassName": "custom-fieldset",
	"fieldSetLabelType": "component", // string/component
	"fieldSetLabel": fieldSetLabel,
	
	"columns":[
	
		{
			"colmd":"6",
			"colxs":"12",
			"rows":[
				[ 	{"colmd": "6",
				"key": "ImageQ5",
				"type":"icon",
				"name": "ImageQ520210513", 
				"path":"https://fulcrumdigital.com/images/fd-logo-2021.svg", 
				"position":"Left",
				"mimetype":"" },	{
					"colmd":"12",
					"colxs":"12",
					"key": "date", 
					"label": "date bb", 
					"type":"date",
					"id":"dateSelect",
					"disabled": false,
					
					format:"DD/MM/YY",
					placeholder:"DD/MM/YY",
					"validators":[
						{"name":"required", "value":true}, 						
						]
					},{
						key:"materialSwitch",
						type:"materialSwitch",
						name:"materialSwitch",
						color:"Primary",
						"label": "material Switch", 
						disabled:false,
						
						showExtraLabel:{labelOn:"Yes",labelOff:"No"}
					},
					{
						key:"customSwitch",
						type:"CustomSwitch",
						name:"customSwitch",
						"label": "customSwitch",
						className:"switchCust",
						handleDiameter:28,
						offColor:"#08f",
						onColor:"#0ff",
						offHandleColor:"#0ff",
						onHandleColor:"#08f",
						height:40,
						width:70,
						borderRadius:6,
						activeBoxShadow:"0px 0px 1px 2px #fffc35",
						uncheckedIcon:<div>Off</div>,
						checkedIcon:<div>On</div>
					
					}
					,{
						
					"colmd":"12",
					"colxs":"12",
					"key": "materialDate",
					"type":"materialDate",
					readOnly:true,
					inputVariant:"outlined",
					disableToolbar:true,
					variant:"inline",
					defaultValue:"",
					format:"dd/MM/yyyy",
					margin:"normal", 
					"label": "Material Date", 
					"fieldLabel":"Material Date",
					minDate:"10/10/2020",
					maxDate:new Date(),
					minDateMessage:"m.minDateMessage",
					maxDateMessage:"m.maxDateMessage",
					"id":"materialDate",
					"disabled": false,
					autoOk:true,
					okLabel:true,
					//"format":"DD/MM/YY",
					"placeholder":"DD/MM/YY",
					"validators":[
						{"name":"required", "value":true}, 						
						]
					}
				],
				[
					{
					"colmd":"6",
					"colxs":"12",
					"key": "sname",
					"label": "Last Name", 
					"validators":[
						{"name":"required", "value":true}, 						
						]
					},
					{ 
						"colmd":"6",
						"colxs":"12",
						"key": "fname", 
						"label": "First Name", 
						"validators":[
							{"name":"required", "value":true},							
						]
					}
				]
			]
		},
		{
			"colmd":"6",
			"colxs":"12",
			"rows":[
				[
					{
					"id":"name3",
					"colmd":"12",
					"colxs":"12",
					"key": "name3", 
					"icon":"user", 
					"label": "Name", 
					"validators":[
						{"name":"required", "value":true}, 
						{"name":"minLength", "value":2},
						{"name":"maxLength", "value":10, "validationMessage": "Maximum 10 digits allowed."}
						]
					}					
				],
				[
					{
					"colmd":"12",
					"colxs":"12",
					"key": "address",
					"label": "Address", 
					"type": "textarea",
					"rows": "3",
					"validators":[
						{"name":"required", "value":true}
						]
					}
				],
				[
					{ 
							"colmd":"12",
							"colxs":"12",
							"key": "file",
							"label": "Files", 
							"type":"file",
							"componentType":"DRAG_DROP_BROWSE", //DRAG_DROP_BROWSE/BROWSE
							"acceptFileTypes":['image/jpeg', 'image/png'],
      						"dropTitle":"Drag and drop or Browse",
      						"maxFileSize":100000000,
							"validators":[
								{"name":"required", "value":true}								
							],
							"layout":"2column"
						}
				]
			]
		}
	]	
},
{
	"fieldSetClassName": "custom-fieldgroup",
	"fieldSetLabelType": "string", // string/component
	"fieldSetLabel": "This is field set2",
	"columns":[
		{
			"colmd":"12",
			"colxs":"12",
				"rows":[
					[
						{ 
							"colmd":"12",
							"colxs":"12",
						    "key": "content",
						    "label": "Content",
						    "type": "texteditor",
						    "wrapperClassName":"editor-wrapper",
						    "editorClassName":"my-editor",
						    "toolbarClassName":"my-editor-toolbar",    
						    "validators":[{"name":"required", "value":true}],
						    "toolbar":{
						       "options":[
						          "inline",
						          "blockType",
						          "fontSize",
						          "fontFamily",
						          "list",
						          "textAlign",
						          "link",
						          "embedded",
						          "image",
						          "remove"
						       ],
						      "inline":{
						         "inDropdown":false,
						         "className":"undefined",
						         "component":"undefined",
						         "dropdownClassName":"undefined",
						         "options":[
						            "bold",
						            "italic",
						            "underline",
						            "strikethrough",
						            "monospace",
						            "superscript",
						            "subscript"
						         ],
						         "bold":{
						            "icon":"bold",
						            "className":"undefined"
						         },
						         "italic":{
						            "icon":"italic",
						            "className":"undefined"
						         },
						         "underline":{
						            "icon":"underline",
						            "className":"undefined"
						         },
						         "strikethrough":{
						            "icon":"strikethrough",
						            "className":"undefined"
						         },
						         "monospace":{
						            "icon":"monospace",
						            "className":"undefined"
						         },
						         "superscript":{
						            "icon":"superscript",
						            "className":"undefined"
						         },
						         "subscript":{
						            "icon":"subscript",
						            "className":"undefined"
						         }
						      },
						      "image":{
						         "icon":"image",
						         "className":"undefined",
						         "component":"undefined",
						         "popupClassName":"undefined",
						         "urlEnabled":true,
						         "uploadEnabled":false,
						         "alignmentEnabled":true,
						         "uploadCallback":"undefined",
						         "previewImage":false,
						         "inputAccept":"image/gif,image/jpeg,image/jpg,image/png,image/svg",
						         "alt":{
						            "present":false,
						            "mandatory":false
						         },
						         "defaultSize":{
						            "height":"auto",
						            "width":"auto"
						         }
						      }
						    }
						  }
					],
					[
						// { 
						// 	"colmd":"6",
						// 	"key": "file",
						// 	"label": "Files", 
						// 	"type":"file",
						// 	"componentType":"DRAG_DROP_BROWSE", //DRAG_DROP_BROWSE/BROWSE
						// 	"acceptFileTypes":['image/jpeg', 'image/png'],
      					// 	"dropTitle":"Drag and drop files here to upload!",
      					// 	//"maxFileSize":1000000,
						// 	"validators":[
						// 		{"name":"required", "value":true}								
						// 	]
						// },
						// {
						//     "key": "country",
						//     "label": "Country",
						//     "type": "autocomplete",
						//     "disabled": true,
						//     "validators":[{"name":"required", "value":true}]
						//  }

					]
				]
		}
	]

}
]	