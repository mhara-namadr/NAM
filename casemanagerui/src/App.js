import React from 'react';
import './App.css';
import { Route, Switch, Redirect,BrowserRouter as Router } from "react-router-dom";

import Login from './components/login/login'
import Home from './components/Home/home'
import Logout from './components/login/logout'
import Contacts from './components/contacts/contacts'
import Register from './components/register/register'
import ChangePassword from './components/changePassword/changePassword'

import ForgotPassword from './components/login/forgotPassword';


export const PrivateRoute = ({component: Component, ...rest}) => {
  return (

      // Show the component only when the user is logged in
      // Otherwise, redirect the user to /signin page.
      <Route {...rest} render={props => (
       
              <Component {...props} />
         
      )} />
  );
};

export const PublicRoute = ({component: Component, restricted, ...rest}) => {
return (
    // restricted = false meaning public route
    // restricted = true meaning restricted route
    <Route {...rest} render={props => (
    restricted ?
            <Redirect to="/home" />
        : <Component {...props} />
    )} />
);
};

class App extends React.Component{
  constructor(){
super()
  }


  render(){
  return (
    <div className="App">       
          <Switch>
            
          <PublicRoute restricted={false} path="/" component={Login} exact />
          <PublicRoute restricted={false} path="/logout" component={Logout} exact />
     
            <PublicRoute restricted={false} path="/forgotPassword" component={ForgotPassword} exact/> 
            <PublicRoute restricted={false} path="/contact" component={Contacts} exact/>
            <PublicRoute restricted={false} path="/register" component={Register} exact/> 
    
            <PrivateRoute path="/home" component={Home} exact/>
            <PrivateRoute path="/changePassword" component={ChangePassword} exact/>
         
          </Switch>
       
    </div>
  );
  }
}

export default App;