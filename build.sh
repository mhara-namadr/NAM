#!/bin/bash
PROJ=$1
REPO="$ACR_URL/$(echo $PROJ | awk '{print tolower($0)}')"
TAG="$2"
ENV_STR="$3"
ENV="${ENV_STR:=dev}"
DIR="$4"
CONTEXT="$5"
CONTEXT="${CONTEXT:=.}"

echo "Branch: $BRANCH_NAME"
echo "Deploy $PROJ from dir '$DIR' under environment $ENV_STR with image $REPO:$TAG"

if [ -d $DIR ]
then
      cd $DIR
      pwd
fi

## Dotnet Config Files
if [ -f $PROJ/"appsettings.$BRANCH_NAME.json" ] 
then 
      echo "...using appsettings.$BRANCH_NAME.json as default setting"
      cp $PROJ/"appsettings.$BRANCH_NAME.json" $PROJ/"appsettings.json" -v
      cat $PROJ/"appsettings.json"
fi

## JS Config Files
if [ -f "src/api/baseURL.$BRANCH_NAME.js" ] 
then 
      echo "...using src/api/baseURL.$BRANCH_NAME.js as default setting"
      cp src/api/baseURL.$BRANCH_NAME.js src/api/baseURL.js -v 
      cat src/api/baseURL.js
fi

## Helm values
if [ -f $PROJ/"values.$BRANCH_NAME.yaml" ] 
then 
      echo "...using values.$BRANCH_NAME.yaml as default setting"
      cp "./deploy/values.default.yaml" $PROJ/"values.$BRANCH_NAME.yaml" -v
fi

docker build -t $(echo $PROJ | awk '{print tolower($0)}'):$TAG --build-arg PROJ=$PROJ -f .devops/Dockerfile $CONTEXT
docker tag $(echo $PROJ | awk '{print tolower($0)}'):$TAG $REPO:$TAG
docker login $ACR_URL -u $ACR_LOGIN -p $ACR_KEY
docker push $REPO:$TAG

if ["$PROJ" == "UI"]
then
  NAMESPACE="$ENV-"$(echo $PROJ | awk '{gsub("_","-",$0); print tolower($0)}') \
        && ADDR=$ENV.$(jq -r '."ingress-dns"' .devops/project.json) \
        && NAME=$(jq -r '."deployment-name"' .devops/project.json)
else
  NAMESPACE="$ENV-"$(echo $PROJ | awk '{gsub("_","-",$0); print tolower($0)}') \
        && ADDR=$(echo $PROJ | awk '{gsub("_","-",$0); print tolower($0)}')".$ENV."$(jq -r '."ingress-dns"' .devops/project.json) \
        && NAME=$(jq -r '."deployment-name"' .devops/project.json)
fi


helm upgrade --install \
      -n $ENV \
      --create-namespace \
      $NAME .devops/fulcrumone/ \
      --set image.repository="$REPO" \
      --set image.tag="$TAG" \
      --set ingress.hosts[0].host="$ADDR" \
      --set ingress.hosts[0].path="$ADDR" \
      --set ingress.tls[0].hosts[0]="$ADDR" \
      --set ingress.tls[0].secretName="tls-$NAME-secret" \
      --values .devops/values.yaml \
      --set fullnameOverride=$NAME
