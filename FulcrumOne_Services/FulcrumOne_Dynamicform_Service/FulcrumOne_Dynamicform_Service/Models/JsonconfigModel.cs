﻿using Newtonsoft.Json;

namespace FulcrumOne_Dynamicform_Service.Models
{
    public class JsonconfigModel
    {
        public string[]? Jsondata { get; set; }
        public string? Jsonfor { get; set; }
        public string? formid { get; set; }
        public string? id { get; set; }
        public string? Isdeleted { get; set; }
        public string? Responsemessage { get; set; }

    }

}
