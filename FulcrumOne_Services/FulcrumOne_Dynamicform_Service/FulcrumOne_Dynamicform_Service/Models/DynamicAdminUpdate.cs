﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Dynamicform_Service.Models
{
    public class DynamicAdminUpdate
    {
        public List<Data>? Data { get; set; }
        ////public List<Row> Rows { get; set; }

        public string? ProductLine { get; set; }
        public string? Product { get; set; }
        public string? FormName { get; set; }
        public string? Color { get; set; }
        public string? Font { get; set; }
        public string? FontStyle { get; set; }
        public string? Design { get; set; }
        public string? Language { get; set; }
        [Required]
        [RegularExpression("^[0-9]*$", ErrorMessage = "formid must be numeric")]
        public string? formid { get; set; }

        [Required]
        [StringRanges]
        public string? id { get; set; }

        [Required]
        [StringRanges]
        public string? clientid { get; set; }
        public string? Isdeleted { get; set; }
        public DateTime Createddate { get; set; }

        [StringRanges]
        public string? Createdby { get; set; }
        public string? Responsemessage { get; set; } 
    }
    public class StringRangesAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if ((value.ToString() == "") || (value.ToString() == "string") || (value.ToString() == "String") || (value.ToString() == "STRING") || (value.ToString() == null))
            {
                return new ValidationResult("Please enter a correct value");
            }
            return ValidationResult.Success;
        }
    }





}
