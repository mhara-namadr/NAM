﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Dynamicform_Service.Models
{
    public class Dynamicadminmodel
    {

        /// <summary>
        /// Dynamicadminmodel
        /// </summary>
        public Dynamicadminmodel()
        {
            Createdby = "Admin";
            Createddate = DateTime.UtcNow;
            Isdeleted = "0";
        }

        public List<Data> Data { get; set; } 

        public string? ProductLine { get; set; }
        public string? Product { get; set; }
        public string? FormName { get; set; }
        public string? Color { get; set; }
        public string? Font { get; set; }
        public string? FontStyle { get; set; }

        public string? Design { get; set; }
        public string? Language { get; set; }
        public string? formid { get; set; }
        public string? id { get; set; }

        [Required]
        [StringRange]
        public string clientid { get; set; }
        public string Isdeleted { get; set; }
        public DateTime Createddate { get; set; }

        [StringRange]
        public string? Createdby { get; set; }
        public string? Responsemessage { get; set; }
    }

    public class StringRangeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if ((value.ToString() == "") || (value.ToString() == "string") || (value.ToString() == "String") || (value.ToString() == "STRING") || (value.ToString() == null))
            {
                return new ValidationResult("Please enter a correct value");
            }
            return ValidationResult.Success;
        }
    }


    public class Option
    {
        public string? id { get; set; }
        public string? key { get; set; }
        public string? label { get; set; }
        public string? name { get; set; }
        public string? value { get; set; }
    }

    public class Validator
    {
        public string? name { get; set; }
        public bool value { get; set; }
        public string? validationMessage { get; set; }
    }

    public class Row
    {
        public string? colmd { get; set; }
        public string? key { get; set; }
        public string? label { get; set; }
        public string? type { get; set; }
        public string? Effectivedate { get; set; }
        public List<Option>? options { get; set; }
        public List<Validator>? validators { get; set; }
        public Imagedetail? Imagedetail { get; set; }
    }

    public class Data
    {
        public List<Row>? rows { get; set; }
    }

    public class Imagedetail
    {
        public string? Colmd { get; set; }
        public string? Key { get; set; }
        public string? Type { get; set; }
        public string? Name { get; set; }
        public string? Path { get; set; }
        public string? Position { get; set; }
        public string? mimetype { get; set; }
    }

    ////public class Root
    ////{
    ////    public List<Data> Data { get; set; }
    ////}
}
