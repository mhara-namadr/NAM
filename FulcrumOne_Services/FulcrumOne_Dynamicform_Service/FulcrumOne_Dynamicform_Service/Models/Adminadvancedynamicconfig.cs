﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Dynamicform_Service.Models
{
    public class Adminadvancedynamicconfig
    {
        /// <summary>
        /// Adminadvancedynamicconfig
        /// </summary>
        public Adminadvancedynamicconfig()
        {
            Createdby = "Admiin";
            Createddate = DateTime.UtcNow;
            Isdeleted = "0";
        }

        public List<dynamic> Data { get; set; }
        public string Color { get; set; }
        public string Font { get; set; }
        public string FontStyle { get; set; }
        public string Language { get; set; }
        public string Design { get; set; }
        public string formid { get; set; }
        public string id { get; set; }

        [Required]
        [StringRange]
        public string clientid { get; set; }
        public string Isdeleted { get; set; }
        public DateTime Createddate { get; set; }

        [StringRange]
        public string Createdby { get; set; }
        public string Responsemessage { get; set; }

    }
}
