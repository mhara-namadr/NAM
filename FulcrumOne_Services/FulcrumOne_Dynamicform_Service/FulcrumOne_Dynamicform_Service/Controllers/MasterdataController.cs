﻿using FulcrumOne_Dynamicform_Service.Core.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using NLog;

namespace FulcrumOne_Dynamicform_Service.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class MasterdataController : ControllerBase
    {
        private readonly IMasterdataService _Servc;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        private readonly List<string>? Validtablename;
        public MasterdataController(IMasterdataService Service)
        {
            _Servc = Service;
            Validtablename = new List<string> { "LNK_LeadAction_OptionMapping" };
        }


        [Route("/api/getdropdowndata/{mastername}")]
        [HttpGet]
        public async Task<IActionResult> Getdropdowndata(string mastername)
        {
            try
            {
                string tablename = "Mst_" + mastername;
                var result = await _Servc.Getdatafordropdownlist(tablename);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [Route("/api/getmultipledropdowndata/{mastername}")]
        [HttpGet]
        public async Task<IActionResult> Getmultipledropdowndata(string mastername)
        {
            try
            {
                dynamic? dropdownlist;
                string tablesname = string.Empty;
                int count = 0;
                foreach (string value in mastername.Split(','))
                {
                    if (count == 0)
                    {
                        tablesname = "Mst_" + value;
                    }
                    else
                    {
                        tablesname = tablesname + "," + "Mst_" + value;
                    }
                    count++;
                }
                dropdownlist = await _Servc.Getmultipledropdownlist(tablesname);
                return Ok(dropdownlist);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [Route("/api/getsingledropdowndatabychannel/{mastername}")]
        [HttpGet]
        public async Task<IActionResult> Getsingledropdowndatabychannel(string mastername, int Channelid)
        {
            try
            {
                string tablename = "Mst_" + mastername;
                var result = await _Servc.Getsingledropdowndatabychannel(tablename, Channelid);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [Route("/api/getmultipledropdowndatabychannel/{mastername}")]
        [HttpGet]
        public async Task<IActionResult> Getmultipledropdowndatabychannel(string mastername, int Channelid)
        {
            try
            {
                dynamic? dropdownlist;
                string tablesname = string.Empty;
                int count = 0;
                foreach (string value in mastername.Split(','))
                {
                    if (count == 0)
                    {
                        tablesname = "Mst_" + value;
                    }
                    else
                    {
                        tablesname = tablesname + "," + "Mst_" + value;
                    }
                    count++;
                }
                dropdownlist = await _Servc.Getmultipledropdowndatabychannel(tablesname, Channelid);
                return Ok(dropdownlist);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [Route("/api/gettransactiontabledropdown/{mastername}")]
        [HttpGet]
        public async Task<IActionResult> Gettransactiontabledropdown(string mastername)
        {
            try
            {
                bool tableexist = Validtablename!.Exists(e => e.EndsWith(mastername));
                if (!tableexist)
                {
                    return BadRequest("Invalid Mastername");
                }
                var result = await _Servc.Gettransactiontabledropdown(mastername);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
   

    }
}
