﻿using System.ComponentModel.DataAnnotations;
using System.Globalization;
using FulcrumOne_Dynamicform_Service.Core.IServices;
using Microsoft.AspNetCore.Mvc;
using NLog;
using Newtonsoft.Json;
using FulcrumOne_Dynamicform_Service.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace F1_Dynamicforms_Crudoperation.Controllers
{

    /// <summary>
    /// CrudoperationController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    ////[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CrudoperationController : ControllerBase
    {
        private readonly ICrudoperationServices _Services;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// CrudoperationController
        /// </summary>
        /// <param name="Services"></param>
        public CrudoperationController(ICrudoperationServices Services)
        {
            _Services = Services;
        }

        /// <summary>
        /// AddnewDynamicdata
        /// </summary>
        /// <returns></returns>
        [Route("/api/addnewdynamicdata")]
        [HttpPost]
        public async Task<IActionResult> AddnewDynamicdata([FromBody] Dictionary<string, string> Jsondata)
        {
            try
            {
                if (Jsondata.Count == 0)
                {
                    return BadRequest("Json data cannot be null");
                }
                bool blankkey = (Jsondata.ContainsKey("") || Jsondata.ContainsKey(" "));
                if (blankkey)
                {
                    return BadRequest("Invalid JSON data");
                }
                ////bool Clientidexists = (Jsondata.ContainsKey("clientid"));
                ////if (!Clientidexists)
                ////{
                ////    return BadRequest("clientid is required");
                ////}
                ////if (Jsondata.ContainsPair("clientid", "") || Jsondata.ContainsPair("clientid", " "))
                ////{
                ////    return BadRequest("clientid is required");
                ////}

                var result = await _Services.AddnewDynamicdata(Jsondata);
                if (result.Responsemessage == "Record inserted successfully")
                    return Ok(result);
                else return BadRequest("Something went wrong while adding the data");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// GetDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        [Route("/api/getdyanmicdatabyformid/{loggerid}")]
        [HttpGet]
        public async Task<IActionResult> GetDyanmicdatabyformid([Required] int loggerid)
        {
            try
            {
                var result = await _Services.GetDyanmicdatabyformid(loggerid);
                if (result == null || result == "")
                    return NotFound("Record not found for the loggerid  " + loggerid);
                else
                    return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        [Route("/api/getdynamicdatabydate")]
        [HttpGet]
        public async Task<IActionResult> Getdynamicdatabydate([Required] string Fromdate, [Required] string Todate)
        {
            string[] formats = { "yyyy.MM.dd", "yyyy-MM-dd", "yyyy/MM/dd" };
            DateTime parsedDate;
            var isValidFormat = DateTime.TryParseExact(Fromdate, formats, new CultureInfo("en-US"), DateTimeStyles.None, out parsedDate);

            try
            {
                if (!isValidFormat)
                {
                    return NotFound("Invalid date format.Expected date yyyy-MM-dd");
                }
                var result = await _Services.Getdynamicdatabydate(Fromdate, Todate);
                if (result == null || result.Count == 0)
                    return NotFound("No record found for the selected date range.");
                else
                    return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        [Route("/api/getdynamicdatabydynamicfields")]
        [HttpPost]
        public async Task<IActionResult> Getdynamicdatabydynamicfields([FromBody] Dictionary<string, string> Jsondata)
        {
            try
            {
                if (Jsondata.Count == 0)
                {
                    return BadRequest("Json data cannot be null");
                }
                bool blankkey = (Jsondata.ContainsKey("") || Jsondata.ContainsKey(" "));
                if (blankkey)
                {
                    return BadRequest("Invalid JSON data");
                }
                var result = await _Services.Getdynamicdatabydynamicfields(Jsondata);
                if (result == null || result.Count == 0)
                    return NotFound("No records found.");
                else
                    return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        [Route("/api/getdynamicdatabyclientid/{clientid}")]
        [HttpGet]
        public async Task<IActionResult> Getdynamicdatabyclientid(string clientid)
        {
            try
            {
                var result = await _Services.Getdynamicdatabyclientid(clientid);
                if (result == null || result.Count == 0)
                    return NotFound("Record not found for the clientid " + clientid);
                else
                    return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// GetAllDynamicdata
        /// </summary>
        /// <returns></returns>
        [Route("/api/getalldynamicdata")]
        [HttpGet]
        public async Task<IActionResult> GetAllDynamicdata()
        {
            try
            {
                var result = await _Services.GetAllDyanmicdata();
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }



        /// <summary>
        /// GetAllDynamicdatabyStatus
        /// </summary>
        /// <param name="IsDeleted"></param>
        /// <returns></returns>
        [Route("/api/getalldynamicdatabystatus")]
        [HttpGet]
        public async Task<IActionResult> GetAllDynamicdatabyStatus(Boolean IsDeleted = false)
        {
            try
            {
                var result = await _Services.GetAlldynamicdatabystatus(IsDeleted);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// UpdateDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param> 
        /// <returns></returns>
        [Route("/api/updatedynamicdata")]
        [HttpPut]
        public async Task<IActionResult> UpdateDynamicdata([FromBody] Dictionary<string, string> Jsondata)
        {
            try
            {
                if (Jsondata.Count == 0)
                {
                    return BadRequest("Json data cannot be null");
                }
                bool blankkey = (Jsondata.ContainsKey("") || Jsondata.ContainsKey(" "));
                if (blankkey)
                {
                    return BadRequest("Invalid JSON data");
                }
                ////if (Jsondata.ContainsPair("clientid", "") || Jsondata.ContainsPair("clientid", " "))
                ////{
                ////    return BadRequest("clientid is required");
                ////}
                bool formidExists = Jsondata.ContainsKey("loggerid");
                bool idExists = Jsondata.ContainsKey("id");
                ////bool clientidExists = (Jsondata.ContainsKey("clientid"));
                if (formidExists && idExists)////&& clientidExists)
                {
                    var result = await _Services.UpdateDynamicdata(Jsondata);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Input JSON data must have loggerid and id.");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Deletedynamicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        [Route("/api/deletedynamicdatabyformid/{loggerid}")]
        [HttpDelete]
        public async Task<IActionResult> Deletedynamicdatabyformid([Required] int loggerid)
        {
            try
            {
                var formidexists = await _Services.GetDyanmicdatabyformid(Convert.ToInt32(loggerid));
                if (formidexists == null || formidexists == "")
                    return NotFound("Record not found for the loggerid  " + loggerid);

                dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(formidexists);
                jsonObj["Isdeleted"] = "1";

                Dictionary<string, string> Jsondata = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonObj.ToString());
                var result = await _Services.UpdateDynamicdata(Jsondata);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

    }
}
