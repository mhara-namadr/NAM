﻿using FulcrumOne_Dynamicform_Service.Core.IServices;
using FulcrumOne_Dynamicform_Service.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FulcrumOne_Dynamicform_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    ////[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdmindynamicformconfigController : ControllerBase
    {
        private readonly IAdmindynamicformServices _Services;
        private readonly ICrudoperationServices _Crudservice;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// AdmindynamicformconfigController
        /// </summary>
        /// <param name="Services"></param>
        public AdmindynamicformconfigController(IAdmindynamicformServices Services, ICrudoperationServices srvc)
        {
            _Services = Services;
            _Crudservice = srvc;
        }


        /// <summary>
        /// Adminadddynamicdata
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [Route("/api/adminadddynamicdata")]
        [HttpPost]
        public async Task<IActionResult> Adminadddynamicdata([FromBody] Dynamicadminmodel Json)
        {
            try
            {
                ////Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(Json);

                ////var data = JsonConvert.DeserializeObject<Dynamicadminmodel>(model.Jsondata.ToString());

                ////Model.Jsondata = model.Replace(" ", "");  ////model;
                if (Json.clientid == "")
                {
                    return BadRequest("clientid is required.");
                }
                 
                ////Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(model.Root);

                var result = await _Services.AdminInsertDynamicdataAsync(Json);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Adminadvancedynamicdata
        /// </summary>
        /// <param name="Json"></param>
        /// <returns></returns>
        [Route("/api/adminadvancedynamicdata")]
        [HttpPost]
        public async Task<IActionResult> Adminadvancedynamicdata([FromBody] Adminadvancedynamicconfig Json)
        {
            try
            {
                ////if (Json.clientid == "")
                ////{
                ////    return BadRequest("clientid is required.");
                ////}
                ////Dynamicadminmodel Model = new Dynamicadminmodel();
                var result = await _Services.AdminInsertAdvanceDynamicdataAsync(Json);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// adminupdatedynamicdata
        /// </summary>
        /// <param name="Json"></param>
        /// <returns></returns>
        [Route("/api/adminupdatedynamicdata")]
        [HttpPut]
        public async Task<IActionResult> Adminupdatedynamicdata([FromBody] DynamicAdminUpdate Json)
        {
            try
            {
                if (String.IsNullOrEmpty(Json.formid))
                {
                    return BadRequest("formid is required.");
                }
                else if (String.IsNullOrEmpty(Json.id))
                {
                    return BadRequest("id is required.");
                }
                else if (String.IsNullOrEmpty(Json.clientid))
                {
                    return BadRequest("clientid is required.");
                }
                var formidexists = await _Crudservice.GetDyanmicdatabyformid(Convert.ToInt32(Json.formid));
                if (formidexists == null || formidexists == "")
                    return NotFound("Record not found for the formid  " + Json.formid);

                var result = await _Services.AdminupdatetDynamicdataAsync(Json);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Adminaupdatedvancedynamicdata
        /// </summary>
        /// <param name="Json"></param>
        /// <returns></returns>
        [Route("/api/adminaupdatedvancedynamicdata")]
        [HttpPut]
        public async Task<IActionResult> Adminaupdatedvancedynamicdata([FromBody] Adminadvancedynamicconfig Json)
        {
            try
            {
                if (String.IsNullOrEmpty(Json.formid))
                {
                    return BadRequest("id is required.");
                }
                else if (String.IsNullOrEmpty(Json.id))
                {
                    return BadRequest("id is required.");
                }
                else if (String.IsNullOrEmpty(Json.clientid))
                {
                    return BadRequest("clientid is required.");
                }

                var formidexists = await _Crudservice.GetDyanmicdatabyformid(Convert.ToInt32(Json.formid));
                if (formidexists == null || formidexists == "")
                    return NotFound("Record not found for the formid  " + Json.formid);

                var result = await _Services.AdminUpdateAdvanceDynamicdataAsync(Json);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

    }
}
