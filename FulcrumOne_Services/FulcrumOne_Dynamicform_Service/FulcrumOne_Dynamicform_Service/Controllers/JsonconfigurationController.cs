﻿using FulcrumOne_Dynamicform_Service.Core.IServices;
using FulcrumOne_Dynamicform_Service.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NLog;

namespace FulcrumOne_Dynamicform_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JsonconfigurationController : ControllerBase
    {
        private readonly IJsonconfigurationService _srvc;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        public JsonconfigurationController(IJsonconfigurationService Services)
        {
            _srvc = Services;
        }


        [Route("/api/addappsettingsjson")]
        [HttpPost]
        public async Task<IActionResult> Addnotificationjson([FromBody] JsonconfigModel Json)
        {
            try
            {
                var result = await _srvc.Addjsonconfiguration(Json);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [Route("/api/updateappsettingsjson")]
        ////[NonAction]
        [HttpPut]
        public async Task<IActionResult> Updateappsettingsjson([FromBody] JsonconfigModel Json)
        {
            try
            {
                var result = await _srvc.Updateappsettingsjson(Json);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [Route("/api/getjson/{formid}")]
        [HttpGet]
        public async Task<IActionResult> Getjson(int formid)
        {
            try
            {
                var result = await _srvc.Getjsonconfig(formid);
                Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(result);               
                var data = myDeserializedClass.Jsondata![0];
                dynamic? jsondata = JsonConvert.DeserializeObject(myDeserializedClass.Jsondata[0].ToString());
                Roots? json = JsonConvert.DeserializeObject<Roots>(data);
                return Ok(data);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public class ServiceBus
        {
            public string? ConnectionString { get; set; }
            public string? MainTopicName { get; set; }
            public string? SubscriptionName { get; set; }
        }

        public class TokenOptions
        {
            public string? Key { get; set; }
            public string? Audience { get; set; }
            public string? Issuer { get; set; }
            public int AccessTokenExpiration { get; set; }
            public int RefreshTokenExpiration { get; set; }
        }

        public class ConnectionStrings
        {
            public string? MySqlConnectionString { get; set; }
        }

        public class EmailConfiguration
        {
            public string? SavedEmailTemplatesPath { get; set; }
            public string? SenderEmail { get; set; }
            public string? SenderEmailPassword { get; set; }
            public string? EmailSenderHost { get; set; }
            public int EmailSenderPort { get; set; }
            public bool IsEmailEnabled { get; set; }
            public string? TestMail { get; set; }
            public bool EmailIsSSL { get; set; }
            public string? Extensions { get; set; }
            public string? EmailAttachments { get; set; }
        }

        public class LicenseAppSettings
        {
            public string? ServiceregistrationAPI { get; set; }
            public string? Hostnameofservice { get; set; }
            public string? LicenseKey { get; set; }
        }

        public class SendGridConfiguration
        {
            public string? Apikey { get; set; }
            public string? FromAddress { get; set; }
            public bool IsSendGridEmail { get; set; }
        }

        public class NexmoSmsConfiguration
        {
            public string? ApiKey { get; set; }
            public string? ApiSecret { get; set; }
            public string? SenderNO { get; set; }
        }

        public class TwilioSmsConfiguration
        {
            public string? AccountSid { get; set; }
            public string? AuthToken { get; set; }
            public string? SenderNO { get; set; }
        }

        public class LogLevel
        {
            public string? Default { get; set; }
            public string? Microsoft { get; set; }

            [JsonProperty("Microsoft.Hosting.Lifetime")]
            public string? MicrosoftHostingLifetime { get; set; }
        }

        public class Logging
        {
            public LogLevel? LogLevel { get; set; }
        }
        public class Roots
        {
            public ServiceBus? ServiceBus { get; set; }
            public TokenOptions? TokenOptions { get; set; }
            public ConnectionStrings? ConnectionStrings { get; set; }
            public EmailConfiguration? EmailConfiguration { get; set; }
            public LicenseAppSettings? LicenseAppSettings { get; set; }
            public SendGridConfiguration? SendGridConfiguration { get; set; }
            public NexmoSmsConfiguration? NexmoSmsConfiguration { get; set; }
            public TwilioSmsConfiguration? TwilioSmsConfiguration { get; set; }
            public Logging? Logging { get; set; }
            public string? AllowedHosts { get; set; }
            public string? Extensions { get; set; }
            public string? EmailAttachments { get; set; }
        }

        public class Root
        {
            public List<string>? Jsondata { get; set; }
            public string? Jsonfor { get; set; }
            public string? formid { get; set; }
            public string? id { get; set; }
            public string? Isdeleted { get; set; }
            public string? Responsemessage { get; set; }
            public string? _rid { get; set; }
            public string? _self { get; set; }
            public string? _etag { get; set; }
            public string? _attachments { get; set; }
            public int _ts { get; set; }
        }
    }
}
