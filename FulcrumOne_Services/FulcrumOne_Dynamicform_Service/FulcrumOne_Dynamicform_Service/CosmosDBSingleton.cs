﻿using FulcrumOne_Dynamicform_Service.Models;
using Microsoft.Azure.Cosmos;

namespace FulcrumOne_Dynamicform_Service
{
    /// <summary>
    /// CosmosDBSingleton
    /// </summary>
    public sealed class CosmosDBSingleton
    {
        /// <summary>
        /// The database options
        /// </summary>
        private readonly DBOptions _dbOptions;

        /// <summary>
        /// The cosmos client
        /// </summary>
        private CosmosClient cosmosClient;

        /// <summary>
        /// database
        /// </summary>
        public Database database { get; set; }

        /// <summary>
        /// container
        /// </summary>
        public Container container { get; set; }

        /// <summary>
        /// CosmosDBSingleton
        /// </summary>
        private CosmosDBSingleton()
        { 
            CreateDatabaseAsync().Wait();
        }

        private static CosmosDBSingleton instance = null;
        /// <summary>
        /// Instance
        /// </summary>
        public static CosmosDBSingleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CosmosDBSingleton();
                }
                return instance;
            }
        }

        /// <summary>
        /// CreateDatabaseAsync
        /// </summary>
        /// <returns></returns>
        public async Task<Database> CreateDatabaseAsync()
        {
            // Create a new database
            this.cosmosClient = new CosmosClient(_dbOptions.EndpointUri, _dbOptions.PrimaryKey);
            database = await this.cosmosClient.CreateDatabaseIfNotExistsAsync(_dbOptions.DatabaseId);
            container = await this.database.CreateContainerIfNotExistsAsync(_dbOptions.ContainerId, "/formid");
            return database;
        }

    }

}
