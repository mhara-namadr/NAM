﻿namespace FulcrumOne_Dynamicform_Service
{
    public sealed class ConnectionString
    {
        public ConnectionString(string value) => Value = value;

        public string Value { get; }
    }
}
