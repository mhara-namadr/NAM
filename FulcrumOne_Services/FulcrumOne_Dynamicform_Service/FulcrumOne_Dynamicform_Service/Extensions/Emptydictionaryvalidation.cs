﻿namespace FulcrumOne_Dynamicform_Service.Extensions
{
    public static class Emptydictionaryvalidation
    {
        public static bool ContainsPair<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
                                                  TKey key,
                                                  TValue value)
        {
            return dictionary.TryGetValue(key, out var found) && found.Equals(value);
        }
    }
}
