﻿namespace FulcrumOne_Dynamicform_Service.Core.IRepository
{
    /// <summary>
    /// Interface ICrudoperationRepository
    /// </summary>
    public interface ICrudoperationRepository
    {
        /// <summary>
        /// AddnewDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <returns></returns>
        Task<dynamic> AddnewDynamicdata(Dictionary<string, string> Jsondata);


        /// <summary>
        /// UpdateDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <param name="formid"></param>
        /// <returns></returns>
        Task<dynamic> UpdateDynamicdata(Dictionary<string, string> Jsondata);//, int formid);


        /// <summary>
        /// GetDyanmicdatabyformid
        /// </summary>
        /// <returns></returns>
        //Task<List<dynamic>> GetDyanmicdatabyformid(int formid);

        Task<string> GetDyanmicdatabyformid(int loggerid);

        /// <summary>
        /// Getdynamicdatabyclientid
        /// </summary>
        /// <param name="clientid"></param>
        /// <returns></returns>
        Task<List<dynamic>> Getdynamicdatabyclientid(string clientid);

        /// <summary>
        /// GetAllDyanmicdata
        /// </summary>
        /// <returns></returns>
        Task<List<dynamic>> GetAllDyanmicdata();


        /// <summary>
        /// DeleteDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        Task<Dictionary<string, object>> DeleteDyanmicdatabyformid(int formid);


        /// <summary>
        /// GetAlldynamicdatabystatus
        /// </summary>
        /// <param name="IsDeleted"></param>
        /// <returns></returns>
        Task<List<dynamic>> GetAlldynamicdatabystatus(string IsDeleted);

        /// <summary>
        /// Getdynamicdatabydate
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        Task<List<dynamic>> Getdynamicdatabydate(string fromdate, string todate);
   
        /// <summary>
        /// Getdynamicdatabydynamicfield
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <returns></returns> 
        Task<List<dynamic>> Getdynamicdatabydynamicfields(Dictionary<string, string> jsondata);
    }
}
