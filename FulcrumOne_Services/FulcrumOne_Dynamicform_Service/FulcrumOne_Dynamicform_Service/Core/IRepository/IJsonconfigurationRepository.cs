﻿using FulcrumOne_Dynamicform_Service.Models;

namespace FulcrumOne_Dynamicform_Service.Core.IRepository
{
    public interface IJsonconfigurationRepository
    {
        Task<dynamic> Addjsonconfiguration(JsonconfigModel Model);
        Task<string> GetDyanmicdatabyformid(int formid);
        Task<dynamic> Updateappsettingsjson(JsonconfigModel Model);
    }
}
