﻿using FulcrumOne_Dynamicform_Service.Models;

namespace FulcrumOne_Dynamicform_Service.Core.IRepository
{
    public interface IAdmindynamicformRepository
    {
        Task<dynamic> AdminInsertDynamicdataAsync(Dynamicadminmodel Model);
        Task<dynamic> AdminInsertAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model);
        Task<DynamicAdminUpdate> AdminupdatetDynamicdataAsync(DynamicAdminUpdate Model);
        Task<dynamic> AdminUpdateAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model);

    }
}
