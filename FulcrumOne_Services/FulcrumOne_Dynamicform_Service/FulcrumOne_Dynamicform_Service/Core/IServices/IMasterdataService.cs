﻿using System.Data;

namespace FulcrumOne_Dynamicform_Service.Core.IServices
{
    public interface IMasterdataService
    {
        Task<dynamic> Getdatafordropdownlist(string tablename);
        Task<DataSet> Getmultipledropdownlist(string tablename);
        Task<dynamic> Getsingledropdowndatabychannel(string tablename, int Channelid);
        Task<DataSet> Getmultipledropdowndatabychannel(string tablename, int Channelid);
        Task<dynamic> Gettransactiontabledropdown(string tablename);
    }
}
