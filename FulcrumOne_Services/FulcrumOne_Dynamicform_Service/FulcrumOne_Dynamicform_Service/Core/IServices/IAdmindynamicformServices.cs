﻿using FulcrumOne_Dynamicform_Service.Models;

namespace FulcrumOne_Dynamicform_Service.Core.IServices
{
    public interface IAdmindynamicformServices
    {
        Task<dynamic> AdminInsertDynamicdataAsync(Dynamicadminmodel Model);
        Task<DynamicAdminUpdate> AdminupdatetDynamicdataAsync(DynamicAdminUpdate Model);
        Task<dynamic> AdminInsertAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model);
        Task<dynamic> AdminUpdateAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model);
    }
}
