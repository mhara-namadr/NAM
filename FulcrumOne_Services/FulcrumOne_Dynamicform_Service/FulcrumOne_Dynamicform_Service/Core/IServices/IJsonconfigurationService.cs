﻿using FulcrumOne_Dynamicform_Service.Models;

namespace FulcrumOne_Dynamicform_Service.Core.IServices
{
    public interface IJsonconfigurationService
    {
        Task<dynamic> Addjsonconfiguration(JsonconfigModel Model);
        Task<dynamic> Getjsonconfig(int formid);
        Task<dynamic> Updateappsettingsjson(JsonconfigModel Model);
    }
}
