﻿namespace FulcrumOne_Dynamicform_Service.Core.IServices
{
    /// <summary>
    /// ICrudoperationServices
    /// </summary>
    public interface ICrudoperationServices
    {
        /// <summary>
        /// AddnewDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <returns></returns>
        Task<dynamic> AddnewDynamicdata(Dictionary<string, string> Jsondata);


        /// <summary>
        /// UpdateDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <param name="formid"></param>
        /// <returns></returns>
        Task<dynamic> UpdateDynamicdata(Dictionary<string, string> Jsondata);//, int formid);


        /// <summary>
        /// GetDyanmicdatabyformid
        /// </summary>
        /// <returns></returns>
        //Task<List<dynamic>> GetDyanmicdatabyformid(int formid);
        Task<string> GetDyanmicdatabyformid(int loggerid);


        /// <summary>
        /// Getdynamicdatabydate
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        Task<List<dynamic>> Getdynamicdatabydate(string fromdate, string todate);

        /// <summary>
        /// GetAllDyanmicdata
        /// </summary>
        /// <returns></returns>
        Task<List<dynamic>> GetAllDyanmicdata();


        /// <summary>
        /// DeleteDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        Task<Dictionary<string, object>> DeleteDyanmicdatabyformid(int formid);

        /// <summary>
        /// GetAlldynamicdatabystatus
        /// </summary>
        /// <param name="IsDeleted"></param>
        /// <returns></returns>
        Task<List<dynamic>> GetAlldynamicdatabystatus(Boolean IsDeleted);
        /// <summary>
        /// Getdynamicdatabyclientid
        /// </summary>
        /// <param name="clientid"></param>
        /// <returns></returns>
        Task<List<dynamic>> Getdynamicdatabyclientid(string clientid);

        Task<List<dynamic>> Getdynamicdatabydynamicfields(Dictionary<string, string> Jsondata);

    }
}
