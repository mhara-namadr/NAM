﻿using FulcrumOne_Dynamicform_Service.Core.IRepository;
using FulcrumOne_Dynamicform_Service.Models;
using Microsoft.Azure.Cosmos;
using NLog;

namespace FulcrumOne_Dynamicform_Service.Core.Repository
{
    public class AdmindynamicformRepository: IAdmindynamicformRepository
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly Container container;
        private readonly CosmosConnector _cosmosConnector;
        private readonly Database database;
        private readonly string Cosmos_GetMaxFormid;
        private readonly int Reseed_Formid;
        private readonly string Cosmos_GetDyanmicdatabyformid;
        private readonly string Cosmos_GetAlldynamicdata;
        private readonly string Cosmos_GetAlldynamicdatabystatus;
        private readonly string Cosmos_Getdynamicdatabyclientid;

        public AdmindynamicformRepository(IConfiguration config, CosmosConnector cosmosConnector)
        {
            _cosmosConnector = cosmosConnector;
            database = _cosmosConnector.database;
            container = _cosmosConnector.container;
            Cosmos_GetMaxFormid = config.GetValue<string>("Cosmos_GetMaxFormid");
            Reseed_Formid = config.GetValue<int>("Reseed_Formid");
            Cosmos_GetDyanmicdatabyformid = config.GetValue<string>("Cosmos_GetDyanmicdatabyformid");
            Cosmos_GetAlldynamicdata = config.GetValue<string>("Cosmos_GetAlldynamicdata");
            Cosmos_GetAlldynamicdatabystatus = config.GetValue<string>("Cosmos_GetAlldynamicdatabystatus");
            Cosmos_Getdynamicdatabyclientid = config.GetValue<string>("Cosmos_Getdynamicdatabyclientid");
        }

        public async Task<dynamic> AdminInsertAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model)
        {
            try
            {
                int CountFormID = await GetMaxFormID();
                if (CountFormID == 0)
                {
                    Model.formid = Convert.ToString(Reseed_Formid);
                }
                else
                {
                    Model.formid = Convert.ToString(Convert.ToInt32(CountFormID) + 1);
                }

                System.Guid guid = System.Guid.NewGuid();
                Model.id = guid.ToString();
                Model.Isdeleted = "0";
                Model.Responsemessage = "";
                ItemResponse<dynamic>? response = null;
                //// response = await container.ReadItemAsync<ServiceRegistrationModel>("id", new PartitionKey(Model.registrationid));
                if (response == null)
                {
                    await container.CreateItemAsync<Adminadvancedynamicconfig>(Model, new PartitionKey(Model.formid));
                    Model.Responsemessage = "Record inserted successfully";
                }
                return Model;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public async Task<dynamic> AdminInsertDynamicdataAsync(Dynamicadminmodel Model)
        {
            try
            {
                int CountFormID = await GetMaxFormID();
                if (CountFormID == 0)
                {
                    Model.formid = Convert.ToString(Reseed_Formid);
                }
                else
                {
                    Model.formid = Convert.ToString(Convert.ToInt32(CountFormID) + 1);
                }

                System.Guid guid = System.Guid.NewGuid();
                Model.id = guid.ToString();
                Model.Isdeleted = "0";
                Model.Responsemessage = "";
                ItemResponse<dynamic>? response = null;
                //// response = await container.ReadItemAsync<ServiceRegistrationModel>("id", new PartitionKey(Model.registrationid));
                if (response == null)
                {
                    await container.CreateItemAsync<Dynamicadminmodel>(Model, new PartitionKey(Model.formid));
                    Model.Responsemessage = "Record inserted successfully";
                }
                return Model;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public async Task<dynamic> AdminUpdateAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model)
        {
            try
            {
                ItemResponse<Adminadvancedynamicconfig>? response = null;
                try
                {
                    response = await container.ReadItemAsync<Adminadvancedynamicconfig>(Model.id, new PartitionKey(Model.formid));
                }
                catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    logger.Error(ex);
                    throw;
                }
                if (response != null)
                {
                    await container.ReplaceItemAsync<Adminadvancedynamicconfig>(Model, Model.id, new PartitionKey(Model.formid));
                    Model.Responsemessage = "Record updated successfully";
                }
                else
                {
                    Model.Responsemessage = "Record not found";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            return Model;
        }


        public async Task<DynamicAdminUpdate> AdminupdatetDynamicdataAsync(DynamicAdminUpdate Model)
        {
            try
            {
                ItemResponse<DynamicAdminUpdate>? response = null;
                try
                {
                    response = await container.ReadItemAsync<DynamicAdminUpdate>(Model.id, new PartitionKey(Model.formid));
                }
                catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    logger.Error(ex);
                    throw;
                }
                if (response != null)
                {
                    await container.ReplaceItemAsync<DynamicAdminUpdate>(Model, Model.id, new PartitionKey(Model.formid));
                    Model.Responsemessage = "Record updated successfully";
                }
                else
                {
                    Model.Responsemessage = "Record not found";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            return Model;
        }

        /// <summary>
        /// GetMaxRegistrationid
        /// </summary>
        /// <returns></returns>
        private async Task<int> GetMaxFormID()
        {
            try
            {
                QueryDefinition queryDefinition = new QueryDefinition(Cosmos_GetMaxFormid);
                FeedIterator<int> queryResultSetIterator = this.container.GetItemQueryIterator<int>(queryDefinition);

                int formid = 0;
                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        Microsoft.Azure.Cosmos.FeedResponse<int> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        foreach (var item in currentResultSet)
                        {
                            formid = item;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                return formid;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }



    }
}
