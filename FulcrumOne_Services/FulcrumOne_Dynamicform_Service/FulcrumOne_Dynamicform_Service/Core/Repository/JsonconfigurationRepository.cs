﻿using FulcrumOne_Dynamicform_Service.Core.IRepository; 
using FulcrumOne_Dynamicform_Service.Models;
using Microsoft.Azure.Cosmos;
using NLog;

namespace FulcrumOne_Dynamicform_Service.Core.Repository
{
    public class JsonconfigurationRepository : IJsonconfigurationRepository
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly Container container;
        private readonly CosmosConnector _cosmosConnector;
        public Database database;
        private readonly string? Cosmos_GetMaxFormid;
        private readonly int Reseed_Formid;
        private readonly string? Cosmos_GetDyanmicdatabyformid;
        ////private readonly string? Cosmos_GetAlldynamicdata;
        //// private readonly string? Cosmos_GetAlldynamicdatabystatus;
        //// private readonly string? Cosmos_Getdynamicdatabyclientid;

        public JsonconfigurationRepository(IConfiguration config, CosmosConnector cosmosConnector)
        {
            _cosmosConnector = cosmosConnector;
            database = _cosmosConnector.database;
            container = _cosmosConnector.container;
            Cosmos_GetMaxFormid = config.GetValue<string>("Cosmos_GetMaxFormid");
            Reseed_Formid = config.GetValue<int>("Reseed_Formid");
            Cosmos_GetDyanmicdatabyformid = config.GetValue<string>("Cosmos_GetDyanmicdatabyformid");
        }

        public async Task<dynamic> Addjsonconfiguration(JsonconfigModel Model)
        {
            try
            {
                int CountFormID = await GetMaxFormID();
                if (CountFormID == 0)
                {
                    Model.formid = Convert.ToString(Reseed_Formid);
                }
                else
                {
                    Model.formid = Convert.ToString(Convert.ToInt32(CountFormID) + 1);
                }
                System.Guid guid = System.Guid.NewGuid();
                Model.id = guid.ToString();
                Model.Isdeleted = "0";
                Model.Responsemessage = "";
                ItemResponse<dynamic>? response = null;
                if (response == null)
                {
                    await container.CreateItemAsync<JsonconfigModel>(Model, new PartitionKey(Model.formid));
                    Model.Responsemessage = "Record inserted successfully";
                }
                return Model;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// GetMaxRegistrationid
        /// </summary>
        /// <returns></returns>
        private async Task<int> GetMaxFormID()
        {
            try
            {
                QueryDefinition queryDefinition = new(Cosmos_GetMaxFormid);
                FeedIterator<int> queryResultSetIterator = this.container.GetItemQueryIterator<int>(queryDefinition);

                int formid = 0;
                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        FeedResponse<int> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        foreach (var item in currentResultSet)
                        {
                            formid = item;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        throw;
                    }
                }
                return formid;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// GetDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        public async Task<string> GetDyanmicdatabyformid(int formid)
        {
            try
            {
                string newquery = Cosmos_GetDyanmicdatabyformid!.Replace("filterid", formid.ToString());
                var query = new QueryDefinition(newquery);

                List<dynamic> result = new();
                Dictionary<string, string> values = new();

                FeedIterator<dynamic> queryResultSetIterator = this.container.GetItemQueryIterator<dynamic>(query);
                string itemstring = string.Empty;
                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<dynamic> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (dynamic item in currentResultSet)
                    {
                        itemstring = item.ToString();
                    }
                }
                return itemstring;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        public async Task<dynamic> Updateappsettingsjson(JsonconfigModel Model)
        {
            try
            {
                ItemResponse<JsonconfigModel>? response = null;
                try
                {
                    response = await container.ReadItemAsync<JsonconfigModel>(Model.id, new PartitionKey(Model.formid));
                }
                catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    logger.Error(ex);
                    throw;
                }
                if (response != null)
                {
                    await container.ReplaceItemAsync<JsonconfigModel>(Model, Model.id, new PartitionKey(Model.formid));
                    Model.Responsemessage = "Record updated successfully";
                }
                else
                {
                    Model.Responsemessage = "Record not found";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            return Model;
        }


    }
}
