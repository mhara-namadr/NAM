﻿using FulcrumOne_Dynamicform_Service.Core.IRepository;
using FulcrumOne_Dynamicform_Service.Models;
using Microsoft.Azure.Cosmos;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System.Collections;
using System.ComponentModel;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace FulcrumOne_Dynamicform_Service.Core.Repository
{
    /// <summary>
    /// CrudoperationRepository
    /// </summary>
    public class CrudoperationRepository : ICrudoperationRepository
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly Microsoft.Azure.Cosmos.Container container;
        private readonly CosmosConnector _cosmosConnector;
        private readonly Database database;
        private readonly string Cosmos_GetMaxFormid;
        private readonly int Reseed_Formid;
        private readonly string Cosmos_GetDyanmicdatabyformid;
        private readonly string Cosmos_GetAlldynamicdata;
        private readonly string Cosmos_GetAlldynamicdatabystatus;
        private readonly string Cosmos_Getdynamicdatabyclientid;
        private readonly string Cosmos_Getdatabycreateddate;
        private readonly string Cosmos_Getdynamicdatabydynamicfield;


        /// <summary>
        /// CrudoperationRepository
        /// </summary>
        /// <param name="config"></param>
        /// <param name="cosmosConnector"></param>
        public CrudoperationRepository(IConfiguration config, CosmosConnector cosmosConnector)
        {
            _cosmosConnector = cosmosConnector;
            database = _cosmosConnector.database;
            container = _cosmosConnector.container;
            Cosmos_GetMaxFormid = config.GetValue<string>("Cosmos_GetMaxFormid");
            Reseed_Formid = config.GetValue<int>("Reseed_Formid");
            Cosmos_GetDyanmicdatabyformid = config.GetValue<string>("Cosmos_GetDyanmicdatabyformid");
            Cosmos_GetAlldynamicdata = config.GetValue<string>("Cosmos_GetAlldynamicdata");
            Cosmos_GetAlldynamicdatabystatus = config.GetValue<string>("Cosmos_GetAlldynamicdatabystatus");
            Cosmos_Getdynamicdatabyclientid = config.GetValue<string>("Cosmos_Getdynamicdatabyclientid");
            Cosmos_Getdatabycreateddate = config.GetValue<string>("Cosmos_Getdatabycreateddate");
            Cosmos_Getdynamicdatabydynamicfield = config.GetValue<string>("Cosmos_Getdynamicdatabydynamicfield");
        }

        /// <summary>
        /// AddnewDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param> 
        /// <returns></returns>
        public async Task<dynamic> AddnewDynamicdata(Dictionary<string, string> Jsondata)
        {
            try
            {
                dynamic expando = new ExpandoObject();
                var DynamicModel = expando as IDictionary<string, object>;

                #region Get Key value pairs from Dynamic dictionary                 
                foreach (KeyValuePair<string, string> item in Jsondata)
                {
                    string Field = item.Key;
                    string Value = item.Value;
                    DynamicModel.Add(Field, Value);
                }
                DynamicModel.Add("id", "");
                DynamicModel.Add("Isdeleted", "0");
                #endregion

                var result = await InsertDynamicdataAsync(DynamicModel);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// InsertServiceRegistrationAsync
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        private async Task<dynamic> InsertDynamicdataAsync(dynamic Model)
        {
            try
            {
                int CountFormID = await GetMaxFormID();
                if (CountFormID == 0)
                {
                    Model.loggerid = Convert.ToString(Reseed_Formid);
                }
                else
                {
                    Model.loggerid = Convert.ToString(Convert.ToInt32(CountFormID) + 1);
                }

                System.Guid guid = System.Guid.NewGuid();
                Model.id = guid.ToString();

                ItemResponse<dynamic> response = null;
                //// response = await container.ReadItemAsync<ServiceRegistrationModel>("id", new PartitionKey(Model.registrationid));
                if (response == null)
                {
                    await container.CreateItemAsync<dynamic>(Model, new PartitionKey(Model.loggerid));
                    Model.Responsemessage = "Record inserted successfully";
                }
                return Model;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// GetMaxRegistrationid
        /// </summary>
        /// <returns></returns>
        private async Task<int> GetMaxFormID()
        {
            try
            {
                QueryDefinition queryDefinition = new QueryDefinition(Cosmos_GetMaxFormid);
                FeedIterator<int> queryResultSetIterator = this.container.GetItemQueryIterator<int>(queryDefinition);

                int formid = 0;
                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        Microsoft.Azure.Cosmos.FeedResponse<int> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        foreach (var item in currentResultSet)
                        {
                            formid = item;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                return formid;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// UpdateDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param> 
        /// <returns></returns>
        public async Task<dynamic> UpdateDynamicdata(Dictionary<string, string> Jsondata)
        {
            try
            {
                dynamic expando = new ExpandoObject();
                var DynamicModel = expando as IDictionary<string, object>;
                foreach (KeyValuePair<string, string> item in Jsondata)
                {
                    string Field = item.Key;
                    string Value = item.Value;
                    DynamicModel.Add(Field, Value);
                }
                var result = await UpdateData(DynamicModel);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// UpdateData
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        private async Task<dynamic> UpdateData(dynamic Model)
        {
            try
            {
                ItemResponse<dynamic> response = null;
                try
                {
                    response = await container.ReadItemAsync<dynamic>(Model.id, new PartitionKey(Model.loggerid));
                }
                catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    logger.Error(ex);
                    throw;
                }
                if (response != null)
                {
                    await container.ReplaceItemAsync<dynamic>(Model, Model.id, new PartitionKey(Model.loggerid));
                    Model.Responsemessage = "Record updated successfully";
                }
                else
                {
                    Model.Responsemessage = "Record not found";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            return Model;
        }

        /// <summary>
        /// GetDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        public async Task<string> GetDyanmicdatabyformid(int loggerid)
        {
            try
            {
                string newquery = Cosmos_GetDyanmicdatabyformid.Replace("filterid", loggerid.ToString());
                var query = new QueryDefinition(newquery);

                List<dynamic> result = new();
                Dictionary<string, string> values = new();

                FeedIterator<dynamic> queryResultSetIterator = this.container.GetItemQueryIterator<dynamic>(query);
                string itemstring = string.Empty;
                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<dynamic> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (dynamic item in currentResultSet)
                    {
                        itemstring = item.ToString();
                        ////values = JsonConvert.DeserializeObject<Dictionary<string, string>>(itemstring);
                        ////var data = JsonConvert.DeserializeObject<ExpandoObject>(itemstring);
                        ////result.Add(data);
                    }
                }
                return itemstring;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Getdynamicdatabydate
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public async Task<List<dynamic>> Getdynamicdatabydate(string fromdate, string todate)
        {
            try
            {
                string newquery1 = Cosmos_Getdatabycreateddate.Replace("fromdate", fromdate.ToString());
                string newquery = newquery1.Replace("todate", todate.ToString());
                var query = new QueryDefinition(newquery);

                List<dynamic> result = new List<dynamic>();
                //// Dictionary<string, string> values = new Dictionary<string, string>();

                FeedIterator<dynamic> queryResultSetIterator = this.container.GetItemQueryIterator<dynamic>(query);

                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<dynamic> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (dynamic item in currentResultSet)
                    {
                        string itemstring = item.ToString();
                        ////values = JsonConvert.DeserializeObject<Dictionary<string, string>>(itemstring);

                        var data = JsonConvert.DeserializeObject<ExpandoObject>(itemstring);
                        result.Add(data);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        public async Task<List<dynamic>> Getdynamicdatabyclientid(string clientid)
        {
            try
            {
                string newquery = Cosmos_Getdynamicdatabyclientid.Replace("filterid", clientid);
                var query = new QueryDefinition(newquery);

                List<dynamic> result = new List<dynamic>();
                //// Dictionary<string, string> values = new Dictionary<string, string>();

                FeedIterator<dynamic> queryResultSetIterator = this.container.GetItemQueryIterator<dynamic>(query);

                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<dynamic> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (dynamic item in currentResultSet)
                    {
                        string itemstring = item.ToString();
                        ////values = JsonConvert.DeserializeObject<Dictionary<string, string>>(itemstring);

                        var data = JsonConvert.DeserializeObject<ExpandoObject>(itemstring);
                        result.Add(data);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// GetAllDyanmicdata
        /// </summary>
        /// <returns></returns>
        public async Task<List<dynamic>> GetAllDyanmicdata()
        {
            try
            {
                var query = new QueryDefinition(Cosmos_GetAlldynamicdata);

                //// FeedIterator<Dictionary<string, string>> queryResultSetIterator = this.container.GetItemQueryIterator<Dictionary<string, string>>(query);
                var results = new List<dynamic>();

                var resultSetIterator = container.GetItemQueryIterator<dynamic>(query);
                while (resultSetIterator.HasMoreResults)
                {
                    var response = await resultSetIterator.ReadNextAsync();
                    results.AddRange(response);
                    if (response.Diagnostics != null)
                    {
                        Console.WriteLine($"\nQueryWithSqlParameters Diagnostics: {response.Diagnostics}");
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// GetAlldynamicdatabystatus
        /// </summary>
        /// <param name="IsDeleted"></param>
        /// <returns></returns>
        public async Task<List<dynamic>> GetAlldynamicdatabystatus(string IsDeleted)
        {
            try
            {
                string newquery = Cosmos_GetAlldynamicdatabystatus.Replace("filterid", IsDeleted);
                var query = new QueryDefinition(newquery);

                List<dynamic> result = new List<dynamic>();
                //// Dictionary<string, string> values = new Dictionary<string, string>();

                FeedIterator<dynamic> queryResultSetIterator = this.container.GetItemQueryIterator<dynamic>(query);

                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<dynamic> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (dynamic item in currentResultSet)
                    {
                        string itemstring = item.ToString();
                        ////values = JsonConvert.DeserializeObject<Dictionary<string, string>>(itemstring);

                        var data = JsonConvert.DeserializeObject<ExpandoObject>(itemstring);
                        result.Add(data);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// DynamicToDictionaryConverter
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Dictionary<string, dynamic> DynamicToDictionaryConverter(dynamic item)
        {
            if (IsDictionary(item))
            {
                return (Dictionary<string, dynamic>)item;
            }

            Dictionary<string, dynamic> newItem = new Dictionary<string, dynamic>();
            PropertyInfo[] props = item.GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {
                newItem.Add(prop.Name, prop.GetValue(item, null));

            }
            return newItem;
        }


        /// <summary>
        /// IsDictionary
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool IsDictionary(object o)
        {
            if (o == null) return false;
            return o is IDictionary &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>));
        }


        /// <summary>
        /// deserializeToDictionary
        /// </summary>
        /// <param name="jo"></param>
        /// <returns></returns>
        private Dictionary<string, object> deserializeToDictionary(string jo)
        {
            var values = JsonConvert.DeserializeObject<Dictionary<string, object>>(jo);
            var values2 = new Dictionary<string, object>();
            foreach (KeyValuePair<string, object> d in values)
            {
                // if (d.Value.GetType().FullName.Contains("Newtonsoft.Json.Linq.JObject"))
                if (d.Value is JObject)
                {
                    values2.Add(d.Key, deserializeToDictionary(d.Value.ToString()));
                }
                else
                {
                    values2.Add(d.Key, d.Value);
                }
            }
            return values2;
        }


        /// <summary>
        /// DeleteDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> DeleteDyanmicdatabyformid(int formid)
        {
            try
            {
                dynamic Model = await GetDyanmicdatabyformid(formid);
                if (Model != "")
                {
                    //Model["Isdeleted"] = "1";
                    var dic = Dyn2Dict(Model);

                    var updateResult = await UpdateDynamicdata(Model);
                    var dictionary = new Dictionary<string, object>(updateResult);
                    dictionary["Responsemessage"] = "Record Deleted Successfully";
                    return dictionary;
                }
                else
                {
                    //var result = Model;
                    //return result;
                    var dictionary = new Dictionary<string, object>();
                    dictionary.Add("Responsemessage", "Record not found");
                    return dictionary;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Dyn2Dict
        /// </summary>
        /// <param name="dynObj"></param>
        /// <returns></returns>
        public Dictionary<String, Object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }

        public async Task<List<dynamic>> Getdynamicdatabydynamicfields(Dictionary<string, string> jsondata)
        {
            try
            {
                //string newquery = Cosmos_Getdynamicdatabydynamicfield + " where ";
                //StringBuilder sb = new StringBuilder();
                //int count = 0;
                //foreach (var item in jsondata)
                //{
                //    string key = item.Key;
                //    string value = item.Value;
                //    if (count == 0)
                //    {
                //        sb.Append(newquery + "S." + key + " = ");
                //        sb.Append("'");
                //        sb.Append(value);
                //        sb.Append("'");
                //    }
                //    else
                //    {
                //        sb.Append(" and " + "S." + key + " = ");
                //        sb.Append("'");
                //        sb.Append(value);
                //        sb.Append("'");
                //    }
                //    count++;
                //}
                string dynamicquery = await Getdynamicquery(jsondata);
                var query = new QueryDefinition(dynamicquery.ToString());

                List<dynamic> result = new List<dynamic>();
                //// Dictionary<string, string> values = new Dictionary<string, string>();

                FeedIterator<dynamic> queryResultSetIterator = this.container.GetItemQueryIterator<dynamic>(query);

                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<dynamic> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (dynamic item in currentResultSet)
                    {
                        string itemstring = item.ToString();
                        ////values = JsonConvert.DeserializeObject<Dictionary<string, string>>(itemstring);

                        var data = JsonConvert.DeserializeObject<ExpandoObject>(itemstring);
                        result.Add(data);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Getdynamicquery
        /// </summary>
        /// <param name="jsondata"></param>
        /// <returns></returns>
        public async Task<string> Getdynamicquery(Dictionary<string, string> jsondata)
        {
            try
            {
                string newquery = Cosmos_Getdynamicdatabydynamicfield + " where ";
                StringBuilder sb = new StringBuilder();
                int count = 0;
                await Task.Run(() =>
                {
                    foreach (var item in jsondata)
                    {
                        string key = item.Key;
                        string value = item.Value;
                        if (count == 0)
                        {
                            sb.Append(newquery + "S." + key + " = ");
                            sb.Append("'");
                            sb.Append(value);
                            sb.Append("'");
                        }
                        else
                        {
                            sb.Append(" and " + "S." + key + " = ");
                            sb.Append("'");
                            sb.Append(value);
                            sb.Append("'");
                        }
                        count++;
                    }
                });
                return sb.ToString();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }
    }
}
