﻿using Dapper;
using FulcrumOne_Dynamicform_Service.Core.IRepository;
using NLog;
using System.Data;
using System.Data.SqlClient;

namespace FulcrumOne_Dynamicform_Service.Core.Repository
{
    public class MasterdataRepository : IMasterdataRepository
    {
        private readonly ConnectionString _connectionString;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        public MasterdataRepository(ConnectionString connectionStrings)
        {
            _connectionString = connectionStrings;
        }

        public IDbConnection CreateConnection()
        {
            return new SqlConnection(_connectionString.Value);
        }

        public async Task<dynamic> Getdatafordropdownlist(string tablename)
        {
            using (var conn = new SqlConnection(_connectionString.Value))
            {
                conn.Open();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("Tablename", tablename);
                    param.Add("ErrorCode", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                    param.Add("ErrorDescription", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                    string procedurename = "Find_keyvalues";
                    var result = await conn.QueryAsync<dynamic>(procedurename, param,
                      commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public Task<DataSet> Getmultipledropdownlist(string tablename)
        {
            List<dynamic> lst = new();
            using (var con = new SqlConnection(_connectionString.Value))
            {
                con.Open();
                try
                {
                    var param = new DynamicParameters();
                    ////param.Add("ErrorCode", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                    ////param.Add("ErrorDescription", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                    string procedurename = "GET_MasterData";
                    param.Add("Tablename", tablename);

                    DataSet ds = new();
                    SqlCommand cmd = new(procedurename, con);
                    SqlDataAdapter da = new(cmd);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Tablename", SqlDbType.VarChar, 5000));
                    da.SelectCommand.Parameters["@Tablename"].Value = tablename;

                    da.Fill(ds);
                    ////Console.WriteLine(ds.ToString());
                    ////for (int i = 0; i < ds.Tables.Count; i++)
                    ////{
                    ////    DataTable dt = ds.Tables[i];
                    ////    var d = DatatableConvert.ConvertDataTable<dynamic>(dt);
                    ////    lst.AddRange(d);
                    ////}
                    return Task.FromResult(ds);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public async Task<dynamic> Getsingledropdowndatabychannel(string tablename, int Channelid)
        {
            using (var conn = new SqlConnection(_connectionString.Value))
            {
                conn.Open();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("Tablename", tablename);
                    param.Add("ChannelID", Channelid);
                    param.Add("ErrorCode", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                    param.Add("ErrorDescription", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                    string procedurename = "Get_SingleDropdownList";
                    var result = await conn.QueryAsync<dynamic>(procedurename, param,
                      commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public Task<DataSet> Getmultipledropdowndatabychannel(string tablename, int Channelid)
        {
            using (var con = new SqlConnection(_connectionString.Value))
            {
                con.Open();
                try
                {
                    var param = new DynamicParameters();
                    string procedurename = "Get_MultipleDropdownData";
                    DataSet ds = new();
                    SqlCommand cmd = new(procedurename, con);
                    SqlDataAdapter da = new(cmd);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Tablename", SqlDbType.VarChar, 5000));
                    da.SelectCommand.Parameters["@Tablename"].Value = tablename;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@ChannelID", SqlDbType.VarChar, 5000));
                    da.SelectCommand.Parameters["@ChannelID"].Value = Channelid;

                    da.Fill(ds);
                    return Task.FromResult(ds);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public async Task<dynamic> Gettransactiontabledropdown(string tablename)
        {
            using (var conn = new SqlConnection(_connectionString.Value))
            {
                try
                {
                    conn.Open();
                    string procedurename = "Select * from " + tablename + ";";
                    var result = await conn.QueryAsync<dynamic>(procedurename, commandType: CommandType.Text);
                    return result.ToList();
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}
