﻿
using FulcrumOne_Dynamicform_Service.Core.IRepository;
using FulcrumOne_Dynamicform_Service.Core.IServices;
using NLog;

namespace FulcrumOne_Dynamicform_Service.Core.Services
{

    /// <summary>
    /// CrudoperationServices
    /// </summary>
    public class CrudoperationServices : ICrudoperationServices
    {

        private readonly ICrudoperationRepository repository;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();



        /// <summary>
        /// CrudoperationServices
        /// </summary>
        /// <param name="_Repo"></param>
        public CrudoperationServices(ICrudoperationRepository _Repo)
        {
            repository = _Repo;
        }


        /// <summary>
        /// AddnewDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param> 
        /// <returns></returns>
        public async Task<dynamic> AddnewDynamicdata(Dictionary<string, string> Jsondata)
        {
            try
            {
                var result = await repository.AddnewDynamicdata(Jsondata);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// UpdateDynamicdata
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <param name="formid"></param>
        /// <returns></returns>
        public async Task<dynamic> UpdateDynamicdata(Dictionary<string, string> Jsondata)
        {
            try
            {
                var result = await repository.UpdateDynamicdata(Jsondata);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// GetDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        public async Task<string> GetDyanmicdatabyformid(int loggerid)
        {
            try
            {
                //var result = await repository.GetDyanmicdatabyformid(formid);
                //result.Remove("_self");
                //result.Remove("_etag");
                //result.Remove("_rid");
                //result.Remove("_attachments");
                //result.Remove("_ts");
                //return result;
                var data = await repository.GetDyanmicdatabyformid(loggerid);
                return data;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Getdynamicdatabydate
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public async Task<List<dynamic>> Getdynamicdatabydate(string fromdate, string todate)
        {
            try
            {
                var data = await repository.Getdynamicdatabydate(fromdate, todate);
                return data;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// DeleteDyanmicdatabyformid
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> DeleteDyanmicdatabyformid(int formid)
        {
            try
            {
                var result = await repository.DeleteDyanmicdatabyformid(formid);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// GetAllDyanmicdata
        /// </summary>
        /// <returns></returns>
        public async Task<List<dynamic>> GetAllDyanmicdata()
        {
            try
            {
                var result = await repository.GetAllDyanmicdata();
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        private Dictionary<string, string> RemoveUnwantedString(Dictionary<string, string> M)
        {
            M.Remove("_self");
            M.Remove("_etag");
            M.Remove("_rid");
            M.Remove("_attachments");
            M.Remove("_ts");
            return M;
        }


        /// <summary>
        /// GetAlldynamicdatabystatus
        /// </summary>
        /// <param name="IsDeleted"></param>
        /// <returns></returns>
        public async Task<List<dynamic>> GetAlldynamicdatabystatus(bool IsDeleted)
        {
            try
            {
                if (IsDeleted)
                {
                    var data = await repository.GetAlldynamicdatabystatus("1");
                    //var result = RemoveUnwantedString(data);
                    return data;
                }
                else
                {
                    var data = await repository.GetAlldynamicdatabystatus("0");
                    //var result = RemoveUnwantedString(data);
                    return data;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Getdynamicdatabyclientid
        /// </summary>
        /// <param name="clientid"></param>
        /// <returns></returns>
        public async Task<List<dynamic>> Getdynamicdatabyclientid(string clientid)
        {
            try
            {
                var result = await repository.Getdynamicdatabyclientid(clientid);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public async Task<List<dynamic>> Getdynamicdatabydynamicfields(Dictionary<string, string> Jsondata)
        {
            try
            {
                var result = await repository.Getdynamicdatabydynamicfields(Jsondata);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }
    }
}
