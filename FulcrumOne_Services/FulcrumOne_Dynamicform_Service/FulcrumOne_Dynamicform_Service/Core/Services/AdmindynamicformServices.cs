﻿using NLog;
using FulcrumOne_Dynamicform_Service.Core.IServices;
using FulcrumOne_Dynamicform_Service.Core.IRepository;
using FulcrumOne_Dynamicform_Service.Models; 

namespace FulcrumOne_Dynamicform_Service.Core.Services
{
    public class AdmindynamicformServices : IAdmindynamicformServices
    {
        private readonly IAdmindynamicformRepository repository;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        public AdmindynamicformServices(IAdmindynamicformRepository _Repo)
        {
            repository = _Repo;
        }
        public async Task<dynamic> AdminInsertDynamicdataAsync(Dynamicadminmodel Model)
        {
            try
            {
                var result = await repository.AdminInsertDynamicdataAsync(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public async Task<DynamicAdminUpdate> AdminupdatetDynamicdataAsync(DynamicAdminUpdate Model)
        {
            try
            {
                var result = await repository.AdminupdatetDynamicdataAsync(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public Task<dynamic> AdminInsertAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model)
        {
            try
            {
                var result = repository.AdminInsertAdvanceDynamicdataAsync(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public Task<dynamic> AdminUpdateAdvanceDynamicdataAsync(Adminadvancedynamicconfig Model)
        {
            try
            {
                var result = repository.AdminUpdateAdvanceDynamicdataAsync(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }
    }
}
