﻿using FulcrumOne_Dynamicform_Service.Core.IRepository;
using FulcrumOne_Dynamicform_Service.Core.IServices;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using NLog;
using System.Data;
using System.Text;

namespace FulcrumOne_Dynamicform_Service.Core.Services
{
    public class MasterdataService : IMasterdataService
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        public readonly IMasterdataRepository _repos;
        /// <summary>
        /// MasterdataService
        /// </summary>
        /// <param name="Repo"></param>
        public MasterdataService(IMasterdataRepository Repo)
        {
            _repos = Repo;
        }

        public async Task<dynamic> Getdatafordropdownlist(string tablename)
        {
            try
            {
                var result = await _repos.Getdatafordropdownlist(tablename);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<DataSet> Getmultipledropdowndatabychannel(string tablename, int Channelid)
        {
            try
            {
                var result = await _repos.Getmultipledropdowndatabychannel(tablename, Channelid);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<DataSet> Getmultipledropdownlist(string tablename)
        {
            try
            {
                var result = await _repos.Getmultipledropdownlist(tablename);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<dynamic> Getsingledropdowndatabychannel(string tablename, int Channelid)
        {
            try
            {
                var result = await _repos.Getsingledropdowndatabychannel(tablename, Channelid);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<dynamic> Gettransactiontabledropdown(string tablename)
        {
            try
            {
                var result = await _repos.Gettransactiontabledropdown(tablename);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
    }
}
