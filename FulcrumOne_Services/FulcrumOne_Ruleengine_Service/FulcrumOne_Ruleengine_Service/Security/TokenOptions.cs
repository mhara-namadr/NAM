namespace FulcrumOne_Ruleengine_Service.Security
{
    /// <summary>
    /// TokenOptions
    /// </summary>
    public class TokenOptions
    {
        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        /// <value>
        /// The audience.
        /// </value>
        public string? Audience { get; set; }
        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        /// <value>
        /// The issuer.
        /// </value>
        public string? Issuer { get; set; }
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string? Key { get; set; }
        /// <summary>
        /// Gets or sets the access token expiration.
        /// </summary>
        /// <value>
        /// The access token expiration.
        /// </value>
        public long AccessTokenExpiration { get; set; }
        /// <summary>
        /// Gets or sets the refresh token expiration.
        /// </summary>
        /// <value>
        /// The refresh token expiration.
        /// </value>
        public long RefreshTokenExpiration { get; set; }

        
    }
}