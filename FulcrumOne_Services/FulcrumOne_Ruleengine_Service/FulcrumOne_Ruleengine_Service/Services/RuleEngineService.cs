﻿using FulcrumOne_Ruleengine_Service.IRepository;
using FulcrumOne_Ruleengine_Service.IServices;
using FulcrumOne_Ruleengine_Service.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using RulesEngine.Models;
using System.Dynamic;
using static RulesEngine.Extensions.ListofRuleResultTreeExtension;


namespace FulcrumOne_Ruleengine_Service.Services
{
    /// <summary>DiscountService
    ///   <br />
    /// </summary>
    public class RuleEngineService : IRuleEngineService
    {
        private readonly ILogger<RuleEngineService> _logger;
        private readonly string _Pathforrulesfile;
        private readonly ICosmosdbaccessrepository repository;

        /// <summary>Initializes a new instance of the <see cref="RuleEngineService" /> class.</summary>
        /// <param name="logger">The logger.</param>
        /// <param name="config"></param>
        public RuleEngineService(ILogger<RuleEngineService> logger, IConfiguration config, ICosmosdbaccessrepository _repo)
        {
            _Pathforrulesfile = config.GetValue<string>("Pathforrulesfile");
            _logger = logger;
            repository = _repo;
        }

        /// <summary>Validates the discount.</summary>
        /// <returns>
        ///   <br />
        /// </returns>
        public string ValidateDiscount(DiscountModel Model)
        {
            try
            {
                return DemodiscountOffer(Model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        /// <summary>Runs this instance.</summary>
        /// <exception cref="System.Exception">Rules not found.</exception>
        public string DemodiscountOffer(DiscountModel Model)
        {
            var basicInfo = "{\"name\": \"hello\",\"email\": \"abcy@xyz.com\",\"creditHistory\": \"good\",\"country\": \"india\",\"loyalityFactor\": 3,\"totalPurchasesToDate\": " + Model.TotalPurchasesToDate + "}";
            var orderInfo = "{\"totalOrders\": " + Model.TotalOrders + ",\"recurringItems\": 2}";


            var telemetryInfo = "{\"noOfVisitsPerMonth\": " + Model.NoOfVisitsPerMonth + ",\"percentageOfBuyingToVisit\": 15}";

            var converter = new ExpandoObjectConverter();

            dynamic input1 = JsonConvert.DeserializeObject<ExpandoObject>(basicInfo, converter);
            dynamic input2 = JsonConvert.DeserializeObject<ExpandoObject>(orderInfo, converter);
            dynamic input3 = JsonConvert.DeserializeObject<ExpandoObject>(telemetryInfo, converter);

            var inputs = new dynamic[]
                {
                    input1,
                    input2,
                    input3
                };

            var files = Directory.GetFiles(_Pathforrulesfile, "Discount.json", SearchOption.AllDirectories);
            if (files == null || files.Length == 0)
            {
                Exception exception = new Exception("Rules not found.");
                throw exception;
            }

            var fileData = File.ReadAllText(files[0]);
            var workflowRules = JsonConvert.DeserializeObject<List<WorkflowRules>>(fileData);

            var bre = new RulesEngine.RulesEngine(workflowRules.ToArray(), null);

            string discountOffered = "No discount offered.";

            List<RuleResultTree> resultList = bre.ExecuteRule("Discount", inputs);

            resultList.OnSuccess((eventName) =>
                {
                    discountOffered = $"Discount offered is {eventName} % over MRP.";
                });

            resultList.OnFail(() =>
            {
                discountOffered = "The user is not eligible for any discount.";
            });

            _logger.LogInformation(discountOffered);
            return discountOffered;
        }


        /// <summary>Validates the custom rule.</summary>
        /// <param name="Jsondata">The model.</param>
        /// <param name="Rulefor"></param>
        /// <returns>
        ///   <br />
        /// </returns>
        public string ValidateSingleCustomRule(string Jsondata, string Rulefor)
        {
            string ruleapplied = "No matching rule found.";
            try
            {

                var converter = new ExpandoObjectConverter();
                dynamic inputjson = JsonConvert.DeserializeObject<ExpandoObject>(Jsondata, converter);

                var inputs = new dynamic[]
                    {
                    inputjson
                    };

                var files = Directory.GetFiles(_Pathforrulesfile, Rulefor + ".json", SearchOption.AllDirectories);
                if (files == null || files.Length == 0)
                {
                    return "Rule Not found";
                }

                var fileData = File.ReadAllText(files[0]);
                var workflowRules = JsonConvert.DeserializeObject<List<WorkflowRules>>(fileData);

                var bre = new RulesEngine.RulesEngine(workflowRules.ToArray(), null);

                List<RuleResultTree> resultList = bre.ExecuteRule(workflowRules[0].WorkflowName, inputs);

                resultList.OnSuccess((eventName) =>
                {
                    ruleapplied = $"After applying " + Rulefor + " rule, the ouput of Rule Engine is " + eventName + " .";
                });

                resultList.OnFail(() =>
                {
                    ruleapplied = "The user is not eligible for any rule.";
                });
                _logger.LogInformation(ruleapplied);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
            return ruleapplied;
        }

        /// <summary>Validates the muliple custom rule.</summary>
        /// <param name="Jsondata">The jsondata.</param>
        /// <param name="Rulefor">The rulefor.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public string ValidateMulipleCustomRule(string Jsondata, string Rulefor)
        {
            string ruleapplied = "No matching rule found.";
            try
            {

                var converter = new ExpandoObjectConverter();
                dynamic inputjson = JsonConvert.DeserializeObject<ExpandoObject>(Jsondata, converter);

                var inputs = new dynamic[]
                    {
                    inputjson
                    };

                var files = Directory.GetFiles(_Pathforrulesfile, Rulefor + ".json", SearchOption.AllDirectories);
                if (files == null || files.Length == 0)
                {
                    return "Rule Not found";
                }

                var fileData = File.ReadAllText(files[0]);
                var workflowRules = JsonConvert.DeserializeObject<List<WorkflowRules>>(fileData);

                var bre = new RulesEngine.RulesEngine(workflowRules.ToArray(), null);

                List<RuleResultTree> resultList = bre.ExecuteRule(workflowRules[0].WorkflowName, inputs);
                string resultset = string.Empty;

                if (resultList != null)
                {
                    foreach (var item in resultList)
                    {
                        if (item.IsSuccess)
                        {
                            if (resultset == string.Empty)
                            {
                                resultset = item.Rule.SuccessEvent;
                            }
                            else
                            {
                                resultset = resultset + "," + item.Rule.SuccessEvent;
                            }
                        }
                    }
                }
                List<string> uniqueValues = resultset.Split(',').Distinct().ToList();
                string UniqueString = string.Join(",", uniqueValues);

                resultList.OnSuccess((eventName) =>
                {
                    ruleapplied = UniqueString;   ////$"After applying " + Rulefor + " rule, the ouput of Rule Engine is " + resultset + " .";
                });

                resultList.OnFail(() =>
                {
                    ruleapplied = "The user is not eligible for any rule.";
                });
                _logger.LogInformation(ruleapplied);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
            return ruleapplied;
        }

        /// <summary>
        /// ValidateCustomRuleFromCosmos
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <param name="Ruleid"></param>
        /// <param name="Rulefor"></param>
        /// <returns></returns>
        public async Task<string> ValidateMultipleCustomRuleFromCosmos(string Jsondata, int Ruleid, string Rulefor)
        {
            string ruleapplied = "No matching rule found.";
            try
            {

                var converter = new ExpandoObjectConverter();
                dynamic inputjson = JsonConvert.DeserializeObject<ExpandoObject>(Jsondata, converter);

                var inputs = new dynamic[]
                    {
                    inputjson
                    };
                var result = await repository.Getjsondatabyid(Ruleid);
                if (String.IsNullOrEmpty(result))
                {
                    return ruleapplied;
                }
                JObject jo = JObject.Parse(result);
                jo.Property("id").Remove();
                jo.Property("ruleid").Remove();
                jo.Property("Clientid").Remove();
                jo.Property("Productcode").Remove();
                jo.Property("Responsemessage").Remove();
                jo.Property("Isdeleted").Remove();
                jo.Property("_rid").Remove();
                jo.Property("_self").Remove();
                jo.Property("_etag").Remove();
                jo.Property("_attachments").Remove();
                jo.Property("_ts").Remove();
                result = jo.ToString();
                ////var files = Directory.GetFiles(_Pathforrulesfile, Rulefor + ".json", SearchOption.AllDirectories);
                ////if (files == null || files.Length == 0)
                ////{
                ////    return "Rule Not found";
                ////}

                result = "[" + result + "]";

                var fileData = result;//// File.ReadAllText(files[0]);
                var workflowRules = JsonConvert.DeserializeObject<List<WorkflowRules>>(fileData);

                var bre = new RulesEngine.RulesEngine(workflowRules.ToArray(), null);

                List<RuleResultTree> resultList = bre.ExecuteRule(workflowRules[0].WorkflowName, inputs);
                string resultset = string.Empty;

                if (resultList != null)
                {
                    foreach (var item in resultList)
                    {
                        if (item.IsSuccess)
                        {
                            if (resultset == string.Empty)
                            {
                                resultset = item.Rule.SuccessEvent;
                            }
                            else
                            {
                                resultset = resultset + "," + item.Rule.SuccessEvent;
                            }
                        }
                    }
                }
                List<string> uniqueValues = resultset.Split(',').Distinct().ToList();
                string UniqueString = string.Join(",", uniqueValues);

                resultList.OnSuccess((eventName) =>
                {
                    ruleapplied = $"After applying " + Rulefor + " rule, the ouput of Rule Engine is " + UniqueString + " .";
                    ////ruleapplied = UniqueString;   //$"After applying " + Rulefor + " rule, the ouput of Rule Engine is " + resultset + " .";
                });

                resultList.OnFail(() =>
                {
                    ruleapplied = "The user is not eligible for any rule.";
                });
                _logger.LogInformation(ruleapplied);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
            return ruleapplied;
        }

        /// <summary>
        /// ValidateSingleCustomRuleFromCosmos
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <param name="Ruleid"></param>
        /// <param name="Rulefor"></param>
        /// <returns></returns>
        public async Task<string> ValidateSingleCustomRuleFromCosmos(string Jsondata, int Ruleid, string Rulefor)
        {
            string ruleapplied = "No matching rule found.";
            try
            {

                var converter = new ExpandoObjectConverter();
                dynamic inputjson = JsonConvert.DeserializeObject<ExpandoObject>(Jsondata, converter);

                var inputs = new dynamic[]
                    {
                    inputjson
                    };
                var result = await repository.Getjsondatabyid(Ruleid);
                if (String.IsNullOrEmpty(result))
                {
                    return ruleapplied;
                }
                JObject jo = JObject.Parse(result);
                jo.Property("id").Remove();
                jo.Property("ruleid").Remove();
                jo.Property("Clientid").Remove();
                jo.Property("Productcode").Remove();
                jo.Property("Responsemessage").Remove();
                jo.Property("Isdeleted").Remove();
                jo.Property("_rid").Remove();
                jo.Property("_self").Remove();
                jo.Property("_etag").Remove();
                jo.Property("_attachments").Remove();
                jo.Property("_ts").Remove();
                result = jo.ToString();
             

                result = "[" + result + "]";

                var fileData = result;//// File.ReadAllText(files[0]);
                var workflowRules = JsonConvert.DeserializeObject<List<WorkflowRules>>(fileData);

                var bre = new RulesEngine.RulesEngine(workflowRules.ToArray(), null);

                List<RuleResultTree> resultList = bre.ExecuteRule(workflowRules[0].WorkflowName, inputs);
                string resultset = string.Empty;


                List<string> uniqueValues = resultset.Split(',').Distinct().ToList();
                string UniqueString = string.Join(",", uniqueValues);

                resultList.OnSuccess((eventName) =>
                {
                    ruleapplied = $"After applying " + Rulefor + " rule, the ouput of Rule Engine is " + eventName + " .";
                });

                resultList.OnFail(() =>
                {
                    ruleapplied = "The user is not eligible for any rule.";
                });
                _logger.LogInformation(ruleapplied);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
            return ruleapplied;
        }



    }

}
