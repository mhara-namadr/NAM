﻿using FulcrumOne_Ruleengine_Service.IRepository;
using FulcrumOne_Ruleengine_Service.IServices;
using FulcrumOne_Ruleengine_Service.Models;
using NLog;
using ILogger = NLog.ILogger;

namespace FulcrumOne_Ruleengine_Service.Services
{
    /// <summary>
    /// Cosmosdbaccessservice
    /// </summary>
    public class Cosmosdbaccessservice : ICosmosdbaccessservice
    {
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly string _Pathforrulesfile;
        private readonly ICosmosdbaccessrepository repository;

        /// <summary>
        /// Cosmosdbaccessservice constructor
        /// </summary>
        /// <param name="config"></param>
        /// <param name="_repo"></param>
        public Cosmosdbaccessservice(IConfiguration config, ICosmosdbaccessrepository _repo)
        {
            _Pathforrulesfile = config.GetValue<string>("Pathforrulesfile");
            repository = _repo;
        }

        /// <summary>
        /// Addnewrule
        /// </summary>
        /// <returns></returns>
        public async Task<RuleFile> Addnewrule(RuleFile Model)
        {
            try
            {
                var result = await repository.Addnewrule(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Deletejsonrule
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        public async Task<string> Deletejsonrule(int ruleid)
        {
            try
            {
                var result = await repository.Deletejsonrule(ruleid);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Getjsondatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        public async Task<string> Getjsondatabyid(int ruleid)
        {
            try
            {
                var result = await repository.Getjsondatabyid(ruleid);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// GetrulefiledatabyCreateduser
        /// </summary>
        /// <param name="Createdby"></param>
        /// <returns></returns>
        public async Task<List<RuleFile>> GetrulefiledatabyCreateduser(string Createdby)
        {
            try
            {
                var result = await repository.GetrulefiledatabyCreateduser(Createdby);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Getrulefiledatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        public async Task<RuleFile> Getrulefiledatabyid(int ruleid)
        {
            try
            {
                var result = await repository.Getrulefiledatabyid(ruleid);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Updatejsonrule
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public async Task<RuleFile> Updatejsonrule(RuleFile Model)
        {
            try
            {
                var result = await repository.Updatejsonrule(Model, "Update");
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }
    }
}
