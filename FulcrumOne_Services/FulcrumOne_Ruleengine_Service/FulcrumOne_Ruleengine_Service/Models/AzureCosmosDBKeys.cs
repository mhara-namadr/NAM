﻿namespace FulcrumOne_Ruleengine_Service.Models
{
    /// <summary>
    /// AzureCosmosDBKeys
    /// </summary>
    public class AzureCosmosDBKeys
    {
        /// <summary>
        /// EndpointUri
        /// </summary>
        public String? EndpointUri { get; set; }

        /// <summary>
        /// PrimaryKey
        /// </summary>
        public String? PrimaryKey { get; set; }

        /// <summary>
        /// DatabaseId
        /// </summary>
        public String? DatabaseId { get; set; }

        /// <summary>
        /// ContainerId
        /// </summary>
        public String? ContainerId { get; set; }


    }
}
