﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Ruleengine_Service.Models
{
    /// <summary>DiscountModel
    ///   <br />
    /// </summary>
    public class DiscountModel
    { 

        /// <summary>Gets or sets the total purchases to date.</summary>
        /// <value>The total purchases to date.</value>
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int TotalPurchasesToDate { get; set; }

        /// <summary>Gets or sets the total orders.</summary>
        /// <value>The total orders.</value>
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int TotalOrders { get; set; }

        /// <summary>Gets or sets the no of visits per month.</summary>
        /// <value>The no of visits per month.</value>
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int NoOfVisitsPerMonth { get; set; }

    }
}
