﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Ruleengine_Service.Models
{
    /// <summary>
    /// RuleFile class
    /// </summary>
    public class RuleFile
    {
        public string id { get; set; }
        public string ruleid { get; set; }
        public string Clientid { get; set; }
        public string Productcode { get; set; }
        public string Responsemessage { get; set; }
        public bool Isdeleted { get; set; }

        [StringRange]
        public string WorkflowName { get; set; }

        [StringRange]
        public string Createduser { get; set; }

        [ValidateEachItem]
        public List<Rule> Rules { get; set; }



        ////public string _rid { get; set; }
        ////public string _self { get; set; }
        ////public string _etag { get; set; }
        ////public string _attachments { get; set; }
        ////public int _ts { get; set; }
    }

    public class Rule
    {
        public string RuleName { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorType { get; set; }
        public string RuleExpressionType { get; set; }
        public string Expression { get; set; }
        public string SuccessEvent { get; set; }
        public string Operator { get; set; }
        public string ActionName { get; set; }
        public string ActionValue { get; set; }
        public List<Rule> Rules { get; set; }
    }

    /// <summary>
    /// Validation to not accept NULL or EMPTY Values
    /// </summary>
    public class StringRangeAttribute : ValidationAttribute
    {
        /// <summary>
        /// IsValid
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Field  is missing.");
            }
            else if ((value.ToString().Trim().Length == 0) || (value.ToString() == "") || (value.ToString() == "string") || (value.ToString() == "String") || (value.ToString() == "STRING") || (value.ToString() == null))
            {
                return new ValidationResult("Please enter a correct value");
            }
            return ValidationResult.Success;
        }
    }

    /// <summary>
    /// ValidateEachItemAttribute
    /// </summary>
    public class ValidateEachItemAttribute : ValidationAttribute
    {
        /// <summary>
        /// validationResults
        /// </summary>
        protected readonly List<ValidationResult> validationResults = new List<ValidationResult>();

        /// <summary>
        /// IsValid
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            var list = value as IEnumerable;
            if (list == null) return true;

            var isValid = true;

            foreach (var item in list)
            {
                var validationContext = new ValidationContext(item);
                var isItemValid = Validator.TryValidateObject(item, validationContext, validationResults, true);
                isValid &= isItemValid;
            }
            return isValid;
        }

    }
}
