﻿using FulcrumOne_Ruleengine_Service.Models;

namespace FulcrumOne_Ruleengine_Service.IRepository
{
    /// <summary>
    /// ICosmosdbaccessrepository
    /// </summary>
    public interface ICosmosdbaccessrepository
    {
        /// <summary>
        /// Getjsondatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        Task<string> Getjsondatabyid(int ruleid);
      
        /// <summary>
        /// Addnewrule
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        Task<RuleFile> Addnewrule(RuleFile Model);

        /// <summary>
        /// Getrulefiledatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        Task<RuleFile> Getrulefiledatabyid(int ruleid);

        /// <summary>
        /// Deleterule
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        Task<string> Deletejsonrule(int ruleid);

        /// <summary>
        /// Updatejsonrule
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="Action"></param>
        /// <returns></returns>
        Task<RuleFile> Updatejsonrule(RuleFile Model, string Action);

        /// <summary>
        /// GetrulefiledatabyCreateduser
        /// </summary>
        /// <param name="Createduser"></param>
        /// <returns></returns>
        Task<List<RuleFile>> GetrulefiledatabyCreateduser(string Createduser);

    }
}
