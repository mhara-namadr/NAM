﻿
using FulcrumOne_Ruleengine_Service.Models;

namespace FulcrumOne_Ruleengine_Service.IServices
{
    /// <summary>
    /// ICosmosdbaccessservice
    /// </summary>
    public interface ICosmosdbaccessservice
    {
        /// <summary>
        /// Addnewrule
        /// </summary>
        /// <returns></returns>
        Task<RuleFile> Addnewrule(RuleFile Model);
        /// <summary>
        /// Getjsondatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        Task<string> Getjsondatabyid(int ruleid);
        /// <summary>
        /// Getrulefiledatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        Task<RuleFile> Getrulefiledatabyid(int ruleid);
        /// <summary>
        /// Deletejsonrule
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        Task<string> Deletejsonrule(int ruleid);
        /// <summary>
        /// Updatejsonrule
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        Task<RuleFile> Updatejsonrule(RuleFile Model);

        /// <summary>
        /// GetrulefiledatabyCreateduser
        /// </summary>
        /// <param name="Createdby"></param>
        /// <returns></returns>
        Task<List<RuleFile>> GetrulefiledatabyCreateduser(string Createdby);
    }
}
