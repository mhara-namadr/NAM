﻿using FulcrumOne_Ruleengine_Service.Models;

namespace FulcrumOne_Ruleengine_Service.IServices
{
    /// <summary>IDiscountService
    ///   <br />
    /// </summary>
    public interface IRuleEngineService
    {
        /// <summary>Validates the discount.</summary>
        /// <returns>
        ///   <br />
        /// </returns>
        string ValidateDiscount(DiscountModel Model);


        /// <summary>Validates the custom rule.</summary>
        /// <param name="Jsondata">The model.</param>
        /// <param name="Rulefor"></param>
        /// <returns>
        ///   <br />
        /// </returns>
        string ValidateSingleCustomRule(string Jsondata, string Rulefor);


        /// <summary>Validates the muliple custom rule.</summary>
        /// <param name="Jsondata">The jsondata.</param>
        /// <param name="Rulefor">The rulefor.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        string ValidateMulipleCustomRule(string Jsondata, string Rulefor);


        /// <summary>
        /// ValidateMultipleCustomRuleFromCosmos
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <param name="Ruleid"></param>
        /// <param name="Rulefor"></param>
        /// <returns></returns>
        Task<string> ValidateMultipleCustomRuleFromCosmos(string Jsondata, int Ruleid, string Rulefor);


        /// <summary>
        /// ValidateSingleCustomRuleFromCosmos
        /// </summary>
        /// <param name="Jsondata"></param>
        /// <param name="Ruleid"></param>
        /// <param name="Rulefor"></param>
        /// <returns></returns>
        Task<string> ValidateSingleCustomRuleFromCosmos(string Jsondata, int Ruleid, string Rulefor);


    }
}
