﻿using FulcrumOne_Ruleengine_Service.IRepository;
using FulcrumOne_Ruleengine_Service.Models;
using Microsoft.Azure.Cosmos;
using NLog;

namespace FulcrumOne_Ruleengine_Service.Repository
{
    /// <summary>
    /// Cosmosdbaccessrepository
    /// </summary>
    public class Cosmosdbaccessrepository : ICosmosdbaccessrepository
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly Microsoft.Azure.Cosmos.Container container;
        private readonly CosmosConnector _cosmosConnector;
        private readonly Database database;
        private readonly string Cosmos_Getmaxruleid;
        private readonly int Reseed_ruleid;
        private readonly string Cosmos_getdatabyruleid;
        private readonly string Cosmos_getdatabycreateduser;
        ////private readonly string Cosmos_GetAlldynamicdata;

        /// <summary>
        /// Cosmosdbaccessrepository Constructor
        /// </summary>
        /// <param name="config"></param>
        /// <param name="cosmosConnector"></param>
        public Cosmosdbaccessrepository(IConfiguration config, CosmosConnector cosmosConnector)
        {
            _cosmosConnector = cosmosConnector;
            database = _cosmosConnector.database;
            container = _cosmosConnector.container;
            Cosmos_Getmaxruleid = config.GetValue<string>("Cosmos_Getmaxruleid");
            Reseed_ruleid = config.GetValue<int>("Reseed_ruleid");
            Cosmos_getdatabyruleid = config.GetValue<string>("Cosmos_getdatabyruleid");
            Cosmos_getdatabycreateduser = config.GetValue<string>("Cosmos_getdatabycreateduser");
        }

        /// <summary>
        /// Getjsondatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        public async Task<string> Getjsondatabyid(int ruleid)
        {
            try
            {
                string newquery = Cosmos_getdatabyruleid.Replace("filterid", ruleid.ToString());
                var query = new QueryDefinition(newquery);

                List<dynamic> result = new List<dynamic>();
                Dictionary<string, string> values = new Dictionary<string, string>();

                FeedIterator<dynamic> queryResultSetIterator = this.container.GetItemQueryIterator<dynamic>(query);
                string itemstring = string.Empty;
                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<dynamic> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (dynamic item in currentResultSet)
                    {
                        itemstring = item.ToString();
                    }
                }
                return itemstring;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Addnewrule
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public async Task<RuleFile> Addnewrule(RuleFile Model)
        {
            try
            {
                int Countruleid = await Getmaxruleid();
                if (Countruleid == 0)
                {
                    Model.ruleid = Convert.ToString(Reseed_ruleid);
                }
                else
                {
                    Model.ruleid = Convert.ToString(Convert.ToInt32(Countruleid) + 1);
                }
                System.Guid guid = System.Guid.NewGuid();
                Model.id = guid.ToString();
                Model.Isdeleted = false;

                ItemResponse<dynamic>? response = null;
                //// response = await container.ReadItemAsync<ServiceRegistrationModel>("id", new PartitionKey(Model.registrationid));
                if (response == null)
                {
                    await container.CreateItemAsync<RuleFile>(Model, new PartitionKey(Model.ruleid));
                    Model.Responsemessage = "Json data Inserted Successfully";
                }
                return Model;

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        private async Task<int> Getmaxruleid()
        {
            try
            {
                QueryDefinition queryDefinition = new QueryDefinition(Cosmos_Getmaxruleid);
                FeedIterator<int> queryResultSetIterator = this.container.GetItemQueryIterator<int>(queryDefinition);

                int formid = 0;
                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        Microsoft.Azure.Cosmos.FeedResponse<int> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        foreach (var item in currentResultSet)
                        {
                            formid = item;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                return formid;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Getrulefiledatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        public async Task<RuleFile> Getrulefiledatabyid(int ruleid)
        {
            try
            {
                string newquery = Cosmos_getdatabyruleid.Replace("filterid", ruleid.ToString());
                var query = new QueryDefinition(newquery);

                FeedIterator<RuleFile> queryResultSetIterator = this.container.GetItemQueryIterator<RuleFile>(query);

                RuleFile Model = new();

                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        FeedResponse<RuleFile> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        foreach (RuleFile item in currentResultSet)
                        {
                            Model = item;
                        }

                        if (Model.ruleid is null)
                        {
                            Model.Responsemessage = "Json data Not Found";
                            return Model;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                return Model;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// GetrulefiledatabyCreateduser
        /// </summary>
        /// <param name="Createduser"></param>
        /// <param name="Createdby"></param>
        /// <returns></returns>
        public async Task<List<RuleFile>> GetrulefiledatabyCreateduser(string Createduser)
        {
            try
            {
                string newquery = Cosmos_getdatabycreateduser.Replace("filterid", Createduser.ToString());
                var query = new QueryDefinition(newquery);

                FeedIterator<RuleFile> queryResultSetIterator = this.container.GetItemQueryIterator<RuleFile>(query);

                List<RuleFile> Modellist = new List<RuleFile>();

                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        FeedResponse<RuleFile> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        if (currentResultSet.Count > 0)
                        {
                            foreach (RuleFile item in currentResultSet)
                            {
                                Modellist.Add(item);
                            }
                        } 
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                return Modellist;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Deleterule
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        public async Task<string> Deletejsonrule(int ruleid)
        {
            RuleFile Model = await Getrulefiledatabyid(ruleid);
            string Responsemessage = string.Empty;
            try
            {
                if (Model.Responsemessage == "Json data Not Found")
                {
                    Responsemessage = Model.Responsemessage;
                }
                else
                {
                    await Updatejsonrule(Model, "Delete");
                    Responsemessage = "Successfully deleted the rule json.";
                }
                return Responsemessage;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// Updatejsonrule
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="Action"></param>
        /// <returns></returns>
        public async Task<RuleFile> Updatejsonrule(RuleFile Model, string Action)
        {
            try
            {
                if (Action == "Delete")
                {
                    Model.Isdeleted = true;
                }
                else
                {
                    Model.Isdeleted = false;
                }
                ItemResponse<RuleFile>? response = null;
                try
                {
                    response = await container.ReadItemAsync<RuleFile>(Model.id, new PartitionKey(Model.ruleid));
                }
                catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    Console.WriteLine(ex);
                    throw;
                }
                if (response != null)
                {
                    await container.ReplaceItemAsync<RuleFile>(Model, Model.id, new PartitionKey(Model.ruleid));
                    Model.Responsemessage = "Json data updated successfully";
                }
                else
                {
                    Model.Responsemessage = "Json data not found";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return Model;
        }

    }
}
