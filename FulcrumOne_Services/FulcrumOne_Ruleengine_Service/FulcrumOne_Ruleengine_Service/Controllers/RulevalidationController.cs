﻿using FulcrumOne_Ruleengine_Service.Extensions;
using FulcrumOne_Ruleengine_Service.IServices;
using FulcrumOne_Ruleengine_Service.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Ruleengine_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RulevalidationController : ControllerBase
    {


        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IRuleEngineService _servc;

        /// <summary>Initializes a new instance of the <see cref="RulevalidationController" /> class.</summary>
        /// <param name="logger">The logger.</param>
        /// <param name="Ruleservice"></param>
        public RulevalidationController(ILogger<RulevalidationController> logger, IRuleEngineService Ruleservice)
        {

            _servc = Ruleservice;
        }

        /// <summary>Validates the rule.</summary>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpPost]
        [Route("/api/demodiscountrule")]
        public IActionResult demodiscountrule(DiscountModel Model)
        {
            logger.Info("Started to validate rule.");
            var discountoffered = _servc.ValidateDiscount(Model);
            return Ok(discountoffered);
        }

        /// <summary>Validates the Single custom rule.</summary>
        /// <param name="Inputjsondata"></param>
        /// <param name="Rulefilename">The Rulefilename.</param>
        /// <returns>
        ///   <br />
        ///   
        /// </returns>
        [HttpPost]
        [Route("/api/validatesinglecustomrule")]
        public IActionResult validatecustomrule([Required] string Inputjsondata, [Required] string Rulefilename)
        {
            try
            {
                logger.Info("Started to validate rule.");
                if (!Validatejsonata.IsValidJson(Inputjsondata))
                {
                    logger.Info("Invalid JSON data");
                    return BadRequest("Invalid JSON data");
                }

                var ruleapplied = _servc.ValidateSingleCustomRule(Inputjsondata, Rulefilename.Trim());
                return Ok(ruleapplied);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }




        /// <summary>Validates the multiple custom rule.</summary>
        /// <param name="Inputjsondata">The inputjsondata.</param>
        /// <param name="Rulefilename">The rulefilename.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpPost]
        [Route("/api/validatemultiplecustomrule")]
        public IActionResult validatemultiplecustomrule([Required] string Inputjsondata, [Required] string Rulefilename)
        {
            try
            {
                logger.Info("Started to validate rule.");
                if (!Validatejsonata.IsValidJson(Inputjsondata))
                {
                    logger.Info("Invalid JSON data");
                    return BadRequest("Invalid JSON data");
                }

                var ruleapplied = _servc.ValidateMulipleCustomRule(Inputjsondata, Rulefilename.Trim());
                return Ok(ruleapplied);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
    }
}