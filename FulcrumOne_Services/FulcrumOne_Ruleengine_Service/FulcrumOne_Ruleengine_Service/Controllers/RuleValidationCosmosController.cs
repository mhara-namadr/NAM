﻿using FulcrumOne_Ruleengine_Service.Extensions;
using FulcrumOne_Ruleengine_Service.IServices;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Ruleengine_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RuleValidationCosmosController : ControllerBase
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IRuleEngineService _servc;

        /// <summary>
        /// RuleValidationCosmosController
        /// </summary>
        /// <param name="Ruleservice"></param>
        public RuleValidationCosmosController(IRuleEngineService Ruleservice)
        {
            _servc = Ruleservice;
        }

        /// <summary>
        /// validatecosmoscustomrule
        /// </summary>
        /// <param name="Inputjsondata"></param>
        /// <param name="Ruleid"></param>
        /// <param name="Rulefilename"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/validatesingleruleforjsondata")]
        public async Task<IActionResult> Validatesingleruleforjsondata([Required] string Inputjsondata, [Required] int Ruleid, [Required] string Rulefilename)
        {
            try
            {
                logger.Info("Started to validate rule.");
                if (!Validatejsonata.IsValidJson(Inputjsondata))
                {
                    logger.Info("Invalid JSON data");
                    return BadRequest("Invalid JSON data");
                }

                var ruleapplied = await _servc.ValidateSingleCustomRuleFromCosmos(Inputjsondata, Ruleid, Rulefilename.Trim());
                return Ok(ruleapplied);
            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString());
                throw;
            }
        }


        /// <summary>
        /// Validatemultipleruleforjsondata
        /// </summary>
        /// <param name="Inputjsondata"></param>
        /// <param name="Ruleid"></param>
        /// <param name="Rulefilename"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/validatemultipleruleforjsondata")]
        public async Task<IActionResult> Validatemultipleruleforjsondata([Required] string Inputjsondata, [Required] int Ruleid, [Required] string Rulefilename)
        {
            try
            {
                logger.Info("Started to validate rule.");
                if (!Validatejsonata.IsValidJson(Inputjsondata))
                {
                    logger.Info("Invalid JSON data");
                    return BadRequest("Invalid JSON data");
                }

                var ruleapplied = await _servc.ValidateMultipleCustomRuleFromCosmos(Inputjsondata, Ruleid, Rulefilename.Trim());
                return Ok(ruleapplied);
            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString());
                throw;
            }
        }

    }
}
