﻿using FulcrumOne_Ruleengine_Service.IServices;
using FulcrumOne_Ruleengine_Service.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Ruleengine_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RulescrudoperationController : ControllerBase
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly ICosmosdbaccessservice _servc;

        /// <summary>
        /// RulescrudoperationController
        /// </summary>
        /// <param name="srvc"></param>
        /// <param name="Ruleservice"></param>
        public RulescrudoperationController(ICosmosdbaccessservice srvc)
        {
            _servc = srvc;
        }

        /// <summary>
        /// Addnewrule
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        [Route("/api/addnewrulefile")]
        [HttpPost]
        public async Task<IActionResult> Addnewrule([FromBody] RuleFile json)
        {
            try
            {
                if (String.IsNullOrEmpty(json.Clientid))
                {
                    return BadRequest("Clientid is required.");
                }
                var Result = await _servc.Addnewrule(json);
                return Ok(Result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Getrulefiledatabyid
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/getjsonrulefiledata")]
        public async Task<IActionResult> Getrulefiledatabyid([Required] int ruleid)
        {
            try
            {
                var result = await _servc.Getrulefiledatabyid(ruleid);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Deletejsonrule
        /// </summary>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/deletejsonrule")]
        public async Task<IActionResult> Deletejsonrule([Required] int ruleid)
        {
            try
            {
                var result = await _servc.Deletejsonrule(ruleid);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Updatejsonrule
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/updatejsonrule")]
        public async Task<IActionResult> Updatejsonrule([FromBody] RuleFile Model)
        {
            try
            {
                if (String.IsNullOrEmpty(Model.Clientid))
                {
                    return BadRequest("Clientid is required.");
                }
                var result = await _servc.Updatejsonrule(Model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        /// <summary>
        /// Getruledatabycreateduser
        /// </summary>
        /// <param name="Createduser"></param>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/getruledatabycreateduser")]
        public async Task<IActionResult> Getruledatabycreateduser([Required] string Createduser)
        {
            try
            {
                var result = await _servc.GetrulefiledatabyCreateduser(Createduser);
                if (result.Count > 0)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("No data found.");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


    }
}
