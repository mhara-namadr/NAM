﻿using Ingress.Producer.Models;

namespace Ingress.Producer.Notifications.Models
{
    public class NewUserCreatedEvent : Event
    {
        public string Emailmessage { get; set; }
        public string Receiveremail { get; set; }
        public string Emailsubject { get; set; } 
    }
}
