﻿using FulcrumOne_SSO_Service.Core.Security.Hashing;
using FulcrumOne_SSO_Service.Core.Security.Tokens;
using FulcrumOne_SSO_Service.Core.Services;
using FulcrumOne_SSO_Service.Core.Services.Communication;
using Microsoft.IdentityModel.Tokens;
using NLog;

namespace FulcrumOne_SSO_Service.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserService _userService;
        private readonly IUserV2Service _userV2Service;
        private readonly IPasswordHasher _passwordHasher;
        private readonly ITokenHandler _tokenHandler;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();


        public AuthenticationService(IUserV2Service userV2Service, IUserService userService, IPasswordHasher passwordHasher, ITokenHandler tokenHandler)
        {
            _userV2Service = userV2Service;
            _tokenHandler = tokenHandler;
            _passwordHasher = passwordHasher;
            _userService = userService;
        }

        public async Task<TokenResponse> CreateAccessTokenAsync(string email, string password, string ClientID)
        {
            try
            {
                var existingclientid = await _userV2Service.GetClientDetail(ClientID);
                if (existingclientid == null)
                {
                    return new TokenResponse(false, "ClientID not found.", null);
                }
                var user = await _userV2Service.FindByEmailClientIDAsync(email, ClientID);

                if (user == null || !_passwordHasher.PasswordMatches(password, user.Password))
                {
                    return new TokenResponse(false, "Invalid credentials.", null);
                }
                var token = _tokenHandler.CreateAccessToken(user);
                return new TokenResponse(true, null, token);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<TokenResponse> CreateFaceAccessTokenAsync(string email, string ClientID)
        {
            try
            {
                var user = await _userV2Service.FindByEmailClientIDAsync(email, ClientID);
                if (user == null)
                {
                    return new TokenResponse(false, "Invalid credentials.", null);
                }
                var token = _tokenHandler.CreateAccessToken(user);
                return new TokenResponse(true, null, token);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<TokenResponse> RefreshTokenAsync(string refreshToken, string userEmail)
        {
            try
            {
                var token = _tokenHandler.TakeRefreshToken(refreshToken);
                if (token == null)
                {
                    return new TokenResponse(false, "Invalid refresh token.", null);
                }
                if (token.IsExpired())
                {
                    return new TokenResponse(false, "Expired refresh token.", null);
                }
                var user = await _userService.FindByEmailAsync(userEmail);
                if (user == null)
                {
                    return new TokenResponse(false, "Invalid refresh token.", null);
                }
                var accessToken = _tokenHandler.CreateAccessToken(user);
                return new TokenResponse(true, null, accessToken);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public void RevokeRefreshToken(string refreshToken)
        {
            try
            {
                _tokenHandler.RevokeRefreshToken(refreshToken);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public RefreshToken RevokeRefreshTokens(string refreshToken)
        {
            try
            {
                var re = _tokenHandler.RevokeRefreshToken(refreshToken);
                return re;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public TokenValidationParameters GetValidationParameters()
        {
            try
            {
                return GetValidationParameters();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
    }
}
