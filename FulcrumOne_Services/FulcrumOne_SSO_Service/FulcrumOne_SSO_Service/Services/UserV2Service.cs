﻿using System.Dynamic;
using FulcrumOne_SSO_Service.Core.Repositories;
using FulcrumOne_SSO_Service.Core.Security.Hashing;
using FulcrumOne_SSO_Service.Core.Services;
using FulcrumOne_SSO_Service.Core.Services.Communication;
using FulcrumOne_SSO_Service.Models;
using NLog;

namespace FulcrumOne_SSO_Service.Services
{
    public class UserV2Service : IUserV2Service
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserV2Repository _userV2Repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPasswordHasher _passwordHasher;
        private readonly IEmailNotificationService _emailNotificationService;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        public UserV2Service(IUserV2Repository userV2Repository, IUserRepository userRepository, IUnitOfWork unitOfWork, IPasswordHasher passwordHasher, IEmailNotificationService emailNotificationService)
        {
            _userV2Repository = userV2Repository;
            _passwordHasher = passwordHasher;
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _emailNotificationService = emailNotificationService;
        }


        public async Task<ChangePasswordWithCode> ChangePasswordWithCode(string verifyCode, string newPassword)
        {
            try
            {
                string password = CreateRandomPassword(8);
                password = _passwordHasher.HashPassword(newPassword);
                return await _userV2Repository.ChangePasswordWithCode(verifyCode, password);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        private static string CreateRandomPassword(int length)
        {
            //// Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_-";
            Random random = new Random();

            //// Select one random character at a time from the string  
            //// and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }

            return new string(chars);
        }

        public async Task<CreateUserResponse> CreateUserFaceAsync(AuthUser user, string ClientID, List<string> base64string, params ERole[] userRoles)
        {
            try
            {
                var existingUser = await _userRepository.FindByEmailAsync(user.Email);
                ////var existingUser = await _userRepository.FindByEmailClientIDAsync(user.Email, user.ClientID);
                if (existingUser != null)
                {
                    return new CreateUserResponse(false, "Email already in use.", null);
                    ////return new CreateUserResponse(false, "Email & ClientID already in use.", null);
                }
                user.Password = _passwordHasher.HashPassword(user.Password);

                await _userV2Repository.AddFaceAsync(user, ClientID, base64string, userRoles);
                await _unitOfWork.CompleteAsync();
                return new CreateUserResponse(true, null, user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<CreateUserResponse> CreateUserWithCodeAsync(AuthUser user, string ClientID, params ERole[] userRoles)
        {
            try
            {
                var existingUser = await _userRepository.FindByEmailAsync(user.Email);
                if (existingUser != null)
                {
                    return new CreateUserResponse(false, "Existing user, Please login with your registered Email Id and Password", null);
                }
                ////var result = await SendMailUserRequest(user);

                ////await _userRepository.SendUserRegistrationNotification(user);
                user.Password = _passwordHasher.HashPassword(user.Password);
                user.VerifyCode = _userRepository.Create(10, 0);
                await _userV2Repository.AddWithCodeAsync(user, ClientID, userRoles);
                await _unitOfWork.CompleteAsync();
                return new CreateUserResponse(true, null, user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<AuthUser> FindByEmailClientIDAsync(string email, string ClientID)
        {
            try
            {
                return await _userV2Repository.FindByEmailClientIDAsync(email, ClientID);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



        public async Task<ForgotPassword> ForgotPasswordWithCode(string Email)
        {
            ForgotPassword model = new();

            AuthUser user = new();
            user.Email = Email;
            try
            {
                if (!String.IsNullOrEmpty(Email))
                {
                    var ValidEmail = await _userRepository.FindByEmailAsync(Email);
                    if (ValidEmail != null)
                    {
                        if (user != null)
                        {
                            model = await _userV2Repository.ChangepasswordafterresetWithCode(Email);
                            if (model.UpdatePassword)
                            {
                                model.Passresetstatus = "Code Created";
                            }
                        }
                        else
                        {
                            model.Passresetstatus = "Error occured";
                        }
                    }
                    else
                    {
                        model.Passresetstatus = "Not a valid Email";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
            return model;
        }



        public async Task<string> VerifyAccountWithCode(string verifyCode)
        {
            try
            {
                return await _userV2Repository.VerifyAccountWithCode(verifyCode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



        public async Task<string> Addnewclient(AuthClientDetail Model)
        {
            try
            {
                var result = await _userV2Repository.Addnewclient(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<AuthClientDetail> GetClientDetail(string ClientID)
        {
            try
            {
                var result = await _userV2Repository.FindClient(ClientID);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<int> Findbyemailclientidroleid(int RoleId, string ClientID, string Email)
        {
            try
            {
                var result = await _userV2Repository.Findbyemailclientidroleid(RoleId, ClientID, Email);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



        public async Task<AuthUser> ValidateTwoFactorCode(string Email, string Twofactorcode)
        {
            try
            {
                return await _userV2Repository.ValidateTwoFactorCode(Email, Twofactorcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



        public async Task<bool> UpdateTwoFactorCode(string Email, string Twofactorcode, int Count)
        {
            try
            {
                return await _userV2Repository.UpdateTwoFactorCode(Email, Twofactorcode, Count);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

    }
}
