﻿using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using FulcrumOne_SSO_Service.Core.Services;
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Services
{
    public class EmailNotificationService : IEmailNotificationService
    {
        private readonly EmailConfiguration _emailConfigurations;
        private readonly IWebHostEnvironment _env;

        ///// <summary>
        ///// Email Notification Service Constructor
        ///// </summary>
        public EmailNotificationService(IOptions<EmailConfiguration> EmailConfigSnapshot, IWebHostEnvironment env)
        {
            _emailConfigurations = EmailConfigSnapshot.Value;
            _env = env;
        }


        #region Send Customised Email Notification
        public async Task<string> SendCustomEmailNotification(CustomNotificationResources Json)
        {
            string EmailResult;
            try
            {
                var EmailMessage = new MailMessage();

                EmailNotificationResources _EMModel = new EmailNotificationResources();

                ///Settings.  
                EmailMessage.From = new MailAddress(_emailConfigurations.SenderEmail);
                EmailMessage.Subject = Json.EmailSubject;
                EmailMessage.Body = Json.EmailTemplate;
                EmailMessage.IsBodyHtml = true;

                #region Populatse Email Body Dynamic Fields
                string Jsondata = JsonConvert.SerializeObject(Json.EmailBodyVariables);
                List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Jsondata);
                foreach (Dictionary<string, string> lst in obj)
                {
                    foreach (KeyValuePair<string, string> item in lst)
                    {
                        string Field = item.Key;
                        string Value = item.Value;
                        EmailMessage.Body = EmailMessage.Body.Replace(Field, Value);
                    }
                }
                #endregion

                foreach (var item in Json.ListEmailReceivers)
                {
                    _EMModel.ReceiverName = item.ReceiverName;

                    if (_emailConfigurations.IsEmailEnabled)
                    {
                        _EMModel.ReceiverEmail = item.ReceiverEmail;
                    }
                    else
                    {
                        _EMModel.ReceiverEmail = _emailConfigurations.TestMail;
                    }

                    EmailMessage.To.Add(new MailAddress(_EMModel.ReceiverEmail));
                }
                await SendMailAsync(EmailMessage);
                EmailResult = "Email Notification Sent Successfully";
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return EmailResult;
        }
        #endregion


        #region Send Mail
        private Task SendMailAsync(MailMessage Email)
        {
            try
            {
                var smtp = new SmtpClient
                {
                    UseDefaultCredentials = false
                };
                var credential = new NetworkCredential
                {
                    UserName = _emailConfigurations.SenderEmail,
                    Password = _emailConfigurations.SenderEmailPassoword
                };

                smtp.Credentials = credential;
                smtp.Host = _emailConfigurations.EmailSenderHost;
                smtp.Port = Convert.ToInt32(_emailConfigurations.EmailSenderPort);
                smtp.EnableSsl = Convert.ToBoolean(_emailConfigurations.EmailIsSSL);

                var EmailSendTask = smtp.SendMailAsync(Email);
                return EmailSendTask;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }
        #endregion

    }
}



