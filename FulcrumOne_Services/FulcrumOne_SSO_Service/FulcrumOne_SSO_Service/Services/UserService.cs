﻿using System.Dynamic;
using FulcrumOne_SSO_Service.Core.Repositories;
using FulcrumOne_SSO_Service.Core.Security.Hashing;
using FulcrumOne_SSO_Service.Core.Services;
using FulcrumOne_SSO_Service.Core.Services.Communication;
//using FulcrumOne_SSO_Service.Infrastructure.Notifications.Publishers.Dispatchers;
using FulcrumOne_SSO_Service.Models;
using NLog;

namespace FulcrumOne_SSO_Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserV2Repository _userV2Repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPasswordHasher _passwordHasher;
        private readonly IEmailNotificationService _emailNotificationService;
        //private readonly NewUserCreatedEventDispatcher _newUserCreatedEventDispatcher;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// UserService
        /// </summary>
        /// <param name="userV2Repository"></param>
        /// <param name="userRepository"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="passwordHasher"></param>
        /// <param name="emailNotificationService"></param>
        public UserService(IUserV2Repository userV2Repository, IUserRepository userRepository, IUnitOfWork unitOfWork, IPasswordHasher passwordHasher, IEmailNotificationService emailNotificationService)
        {
            _userV2Repository = userV2Repository;
            _passwordHasher = passwordHasher;
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _emailNotificationService = emailNotificationService;
           // _newUserCreatedEventDispatcher = newUserCreatedEventDispatcher ?? throw new ArgumentNullException(nameof(newUserCreatedEventDispatcher));

        }

        public async Task<CreateUserResponse> CreateUserAsync(AuthUser user, string ClientID, params ERole[] userRoles)
        {
            try
            {
                var existingUser = await _userRepository.FindByEmailAsync(user.Email);
                var existingclientid = await _userV2Repository.FindClient(ClientID);
                if (existingUser != null)
                {
                    return new CreateUserResponse(false, "Email already in use.", null);
                }
                if (existingclientid == null)
                {
                    return new CreateUserResponse(false, "ClientID not found.", null);
                }

                int Userexists = 0;
                foreach (var item in userRoles)
                {
                    Userexists = await _userV2Repository.Findbyemailclientidroleid(Convert.ToInt32(item), ClientID, user.Email);
                }
                if (Userexists != 0)
                {
                    return new CreateUserResponse(false, "Email, RoleID and ClientID already in use.", null);
                }
                ////{
                ////    return new CreateUserResponse(false, "Email already in use.", null);
                ////    ////return new CreateUserResponse(false, "Email & ClientID already in use.", null);
                ////}//if (existingUser != null)

                user.Password = _passwordHasher.HashPassword(user.Password);

                await _userRepository.AddAsync(user, ClientID, userRoles);
                await _unitOfWork.CompleteAsync();
                return new CreateUserResponse(true, null, user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<AuthUser> FindByEmailAsync(string email)
        {
            try
            {
                return await _userRepository.FindByEmailAsync(email);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<bool> UpdateSigninAttempt(string Email)
        {
            try
            {
                return await _userRepository.UpdateSigninAttempt(Email);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw;
            }
        }

        public int? GetSignInAttempt(string Email)
        {
            try
            {
                return _userRepository.GetSignInAttempt(Email);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets the user role.
        /// </summary>
        /// <param name="email">The email.</param> 
        /// <returns></returns>
        public async Task<AuthRole> GetUserRole(string email)
        {
            try
            {
                return await _userRepository.GetUserRole(email);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        public async Task<bool> ChangePassword(string email, string oldPassword, string newPassword)
        {
            try
            {
                bool result = false;
                string fromDb = await _userRepository.GetPassword(email);
                if (_passwordHasher.PasswordMatches(oldPassword, fromDb))
                {
                    newPassword = _passwordHasher.HashPassword(newPassword);
                    result = await _userRepository.ChangePassword(email, oldPassword, newPassword);
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw;
            }
        }

        public async Task<List<AuthUser>> GetAllUser(string role)
        {
            try
            {
                return await _userRepository.GetAllUser(role);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<bool> UpdateUserAsync(AuthUser user, string ClientID, ERole[] userRoles)
        {
            try
            {
                user.Password = _passwordHasher.HashPassword(user.Password);
                return await _userRepository.UpdateUserAsync(user, ClientID, userRoles);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<string> DeleteUser(string userEmail)
        {
            try
            {
                await _userRepository.DeleteUser(userEmail);
                return "User Deleted Successfully";
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<string> ForgotPassword(string Email)
        {
            string Newpassword = CreateRandomPassword(8);

            AuthUser user = new();
            user.Password = Newpassword;
            user.Email = Email;

            Newpassword = _passwordHasher.HashPassword(user.Password);
            string Passresetstatus;
            try
            {
                Passresetstatus = "";
                if (!String.IsNullOrEmpty(Email))
                {
                    var ValidEmail = await _userRepository.FindByEmailAsync(Email);
                    ////var ValidEmail = await _userRepository.FindByEmailClientIDAsync(Email, ClientID);

                    if (ValidEmail != null)
                    {
                        ////if (Send_New_Password(user))
                        if (await Sendnewpasswordasync(user))
                        {
                            bool Changepasswordresult = await _userRepository.Changepasswordafterreset(Email, Newpassword);
                            if (Changepasswordresult)
                            {
                                Passresetstatus = "New password sent successfully";
                            }
                        }
                        else
                        {
                            Passresetstatus = "Error occured while sending new password";
                        }
                    }
                    else
                    {
                        Passresetstatus = "Not a valid Email";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
            return Passresetstatus;
        }

        private async Task<bool> Sendnewpasswordasync(AuthUser model)
        {
            bool sentstatus;
            try
            {
               // await _newUserCreatedEventDispatcher.DispatchnewpasswordAsync(model);
                sentstatus = true;
            }
            catch (Exception ex)
            {
                sentstatus = false;
                logger.Error(ex.ToString());
                throw;
            }
            return sentstatus;
        }

        private bool Send_New_Password(AuthUser user)
        {
            bool SendMailStatus = false;
            try
            {
                if (SendPassword(user).Result == "Email Notification Sent Successfully")
                {
                    SendMailStatus = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
            return SendMailStatus;
        }

        private static string CreateRandomPassword(int length)
        {
            //// Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_-";
            Random random = new Random();

            //// Select one random character at a time from the string  
            //// and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }

            return new string(chars);
        }

        public async Task<string> SendMailUserRequest(AuthUser user)
        {
            try
            {
                CustomNotificationResources Model = new CustomNotificationResources();

                #region add email body variables

                List<dynamic> EmailBodyVariables = new List<dynamic>();
                dynamic expando = new ExpandoObject();

                expando.EmailId = user.Email;
                expando.PasswordInDb = user.Password;

                EmailBodyVariables.Add(expando);

                #endregion

                #region Custom Email Notification Service Parameters

                List<EmailNotificationResources> Lst = new List<EmailNotificationResources>
            {
                new EmailNotificationResources()
                {
                    ReceiverName = "User",
                    ReceiverEmail = user.Email
                }
            };

                Model.ListEmailReceivers = Lst;
                Model.EmailSubject = "User Registration Successful";
                ////Model.EmailTemplate = "Dear UserName, <br /> <br /> Your registration is successful in booking class. <br /> <br/> Email Address : EmailId <br/> Password : PasswordInDb <br /> <p>This is a system generated e-mail and please do not reply. If any help is required please call on the mobile number mention Mobile Number: +1999999999.<p/> <br /> Please login <a href=swimquestLogin>here</a>. <br /><br /> Thank you for registering with us! <br /> <br /> Regards, <br/> Administrator, <br /> Swim-Quest Team";
                Model.EmailTemplate = "Dear UserName,<br /><br /> Your registration is successful for the SwimQuest booking class. Please find the details below to login.<br/><br/><br/>Email Address : EmailId <br/>Password : PasswordInDb <br/> <br/> <br/> To login click <a href=swimquestLogin>here</a> <br/><br/>This is a system generated e-mail and please do not reply. In case of any assistance please call on the mobile number +1999999999.<br/> <br/>Regards,  <br/>Administrator,  <br/>Swim-Quest Team";


                Model.EmailBodyVariables = EmailBodyVariables;

                #endregion
                return await _emailNotificationService.SendCustomEmailNotification(Model);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<string> SendPassword(AuthUser user)
        {
            try
            {
                CustomNotificationResources Model = new CustomNotificationResources();

                #region add email body variables

                List<dynamic> EmailBodyVariables = new();
                dynamic expando = new ExpandoObject();
                expando.EmailId = user.Email;
                expando.PasswordInDb = user.Password;

                EmailBodyVariables.Add(expando);
                #endregion

                List<EmailNotificationResources> Lst = new()
                {
                    new EmailNotificationResources()
                    {
                        ReceiverName = "User",
                        ReceiverEmail = user.Email
                    }
                };

                Model.ListEmailReceivers = Lst;
                Model.EmailSubject = "F1 Forgot Password";
                ////Model.EmailTemplate = "Dear UserName,<br /><br /> Your forgot password is given below. Please use the same to login. <br /> <br/> Email Address : EmailId <br/> Password : PasswordInDb <br /> <p>This is a system generated e-mail and please do not reply. If any help is required please call on the mobile number mention Mobile Number: +1999999999.<p/> <br /> Please login <a href=swimquestLogin>here</a>. <br /><br /> Thank you for registering with us! <br /> <br /> Regards, <br/> Administrator, <br /> Swim-Quest Team";
                Model.EmailTemplate = "Dear UserName,<br/> <br/> Your new password is given below. Please use the below details  to login. <br/> <br/> Email Address : EmailId  <br/> Password : PasswordInDb <br/> <br/> <br/>To login click <a href=swimquestLogin>here</a> <br/> <br/> This is a system generated e-mail and please do not reply. In case of any assistance please call on the mobile number +1999999999. <br/> <br/> Regards,  <br/> Administrator,  <br/> Swim-Quest Team";
                Model.EmailBodyVariables = EmailBodyVariables;
                return await _emailNotificationService.SendCustomEmailNotification(Model);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



        public async Task<string> AddUserrole(AuthUserRole Role)
        {
            try
            {
                var result = await _userRepository.AddUserrole(Role);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<string> Addroleasync(Mst_Role Model)
        {
            try
            {
                var result = await _userRepository.Addroleasync(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


    }
}
