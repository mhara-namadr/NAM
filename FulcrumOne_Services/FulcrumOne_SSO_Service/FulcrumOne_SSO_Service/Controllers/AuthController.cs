﻿using System.Dynamic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using AutoMapper;
using FulcrumOne_SSO_Service.Core.Security.Tokens;
using FulcrumOne_SSO_Service.Core.Services;
using FulcrumOne_SSO_Service.Extensions;
using FulcrumOne_SSO_Service.Models;
using FulcrumOne_SSO_Service.Security.Tokens;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

/// <summary>
/// The controller to generate the Token, Refresh token and Revoke the token by passing argument into it.
/// </summary>
namespace FulcrumOne_SSO_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserService _userService;
        private readonly IUserV2Service _userV2Service;
        private readonly TwoFactorServiceInitiator _TwoFactor;
        private readonly IEmailNotificationService _emailNotificationService;
        public readonly AzureFaceDetection _azureFaceDetection;
        private readonly SigningConfigurations _signingConfigurations;



        public AuthController(IUserV2Service userV2Service, IMapper mapper, IEmailNotificationService EmailService, IAuthenticationService authenticationService, IOptions<TwoFactorServiceInitiator> TwoFactor, IUserService userService, IOptions<AzureFaceDetection> AzureFaceDetection, SigningConfigurations signingConfigurations)
        {
            _authenticationService = authenticationService;
            _emailNotificationService = EmailService;
            _userV2Service = userV2Service;
            _userService = userService;
            _mapper = mapper;
            _TwoFactor = TwoFactor.Value;
            _azureFaceDetection = AzureFaceDetection.Value;
            _signingConfigurations = signingConfigurations;
        }

        [Route("/api/generatetoken")]
        [HttpPost]
        public async Task<IActionResult> generatetokenasync([FromBody] UserCredentialsResource userCredentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            UserTokenModel utk = new UserTokenModel();
            var response = await _authenticationService.CreateAccessTokenAsync(userCredentials.Email, userCredentials.Password, userCredentials.ClientID);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            }
            //// var user = await _userService.FindByEmailAsync(userCredentials.Email);
            var user = await _userV2Service.FindByEmailClientIDAsync(userCredentials.Email, userCredentials.ClientID);
            var accessTokenResource = _mapper.Map<AccessToken, AccessTokenResource>(response.Token);

            utk.Id = user.Id;
            utk.Email = user.Email;
            utk.IsTwoFactorEnabled = user.IsTwoFactorEnabled;
            utk.Twofactorcode = user.Twofactorcode;
            utk.TwoFactorEnabledBy = user.TwoFactorEnabledBy;
            utk.SigninAttempt = user.SigninAttempt;
            utk.GroupName = user.GroupName;
            utk.PersonId = user.PersonId;

            foreach (var item in user.UserRoles)
            {
                utk.RoleID = Convert.ToString(item.RoleId);
                utk.Rolename = Convert.ToString(item.Role.Name);
            }

            if (!_TwoFactor.IsTwoFactorServiceEnabled)
            {
                return Ok(accessTokenResource);
            }
            else
            {

                TokenGeneration Model = new()
                {
                    AccessToken = accessTokenResource.AccessToken,
                    Expiration = accessTokenResource.Expiration,
                    RefreshToken = accessTokenResource.RefreshToken,
                    IsTwoFactorEnabled = user.IsTwoFactorEnabled,
                    Twofactorcode = "",
                    TwoFactorEnabledBy = user.TwoFactorEnabledBy,
                    Email = userCredentials.Email

                };
                await TwoFactorServiceInitiator(Model);
                utk.TokenGeneration = Model;
                return Ok(utk);
            }
        }

        #region ValidateToken

        [Route("/api/validatetoken")]
        [HttpPost]
        public async Task<IActionResult> ValidateToken(string authToken, string email)
        {
            UserTokenModel utk = new UserTokenModel();
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = _authenticationService.GetValidationParameters();
            var user = await _userService.FindByEmailAsync(email);

            SecurityToken validatedToken;
            try
            {
                IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
                utk.Id = user.Id;
                utk.Email = user.Email;
                utk.IsTwoFactorEnabled = user.IsTwoFactorEnabled;
                utk.Twofactorcode = user.Twofactorcode;
                utk.TwoFactorEnabledBy = user.TwoFactorEnabledBy;
                utk.SigninAttempt = user.SigninAttempt;
                utk.GroupName = user.GroupName;
                utk.PersonId = user.PersonId;

                foreach (var item in user.UserRoles)
                {
                    utk.RoleID = Convert.ToString(item.RoleId);
                    utk.Rolename = Convert.ToString(item.Role.Name);
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
            ////var res = new { message = "Token validated successfully!" };
            return Ok(utk);
        }


        #endregion

        #region Two Factor Service
        private async Task TwoFactorServiceInitiator(TokenGeneration Model)
        {
            if (Model.IsTwoFactorEnabled == true)
            {
                string AuthenticationCode = TwoFactorCodeGenerator(_TwoFactor.TwoFactorCodeLength);

                if (Model.TwoFactorEnabledBy == "Email")
                {
                    var result = await SendMail(AuthenticationCode, Model.Email);
                    if (result == "Email Notification Sent Successfully")
                    {
                        // Update the Two Factor code in DB
                        await _userV2Service.UpdateTwoFactorCode(Model.Email, AuthenticationCode, 0);
                    }
                }
                else if (Model.TwoFactorEnabledBy == "Phone")
                {
                    Console.WriteLine("Send SMS");
                }
            }
        }
        #endregion

        #region Generate Authentication Code
        private static Random random = new Random();
        public static string TwoFactorCodeGenerator(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion

        #region Send Two Factor Authentication code
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<string> SendMail(string TwoFactorCode, string ReceiverEmail)
        {
            CustomNotificationResources Model = new();

            #region add email body variables

            List<dynamic> EmailBodyVariables = new List<dynamic>();
            dynamic expando = new ExpandoObject();
            expando.userField = "User";
            expando.TwoFactorField = TwoFactorCode;
            EmailBodyVariables.Add(expando);

            #endregion

            #region Custom Email Notification Service Parameters

            List<EmailNotificationResources> Lst = new List<EmailNotificationResources>
            {
                new EmailNotificationResources()
                {
                    ReceiverName = "User",
                    ReceiverEmail = ReceiverEmail
                }
            };

            Model.ListEmailReceivers = Lst;
            Model.EmailSubject = "Your Authentication Code";

            Model.EmailTemplate = "<h1>Two Factor Authentication Code</h1><br/>Welcome userField <br/><br/>Thanks For joining us<br/>The Two factor Authentication code is: <br/><span id='myspanid'><div id='div2factorcode'> TwoFactorField  </div> </span><br/><br/> Use this code for signing-in <br/><br/> This code is valid for next 30 mins.";
            Model.EmailBodyVariables = EmailBodyVariables;

            #endregion


            if (!_TwoFactor.ConsumeEmailService)
            {
                return await _emailNotificationService.SendCustomEmailNotification(Model);
            }
            else
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("Key", "Secret@123");
                    var user = _TwoFactor.EmailServiceUsername;
                    var password = _TwoFactor.EmailServicePassword;
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    StringContent content = new StringContent(JsonConvert.SerializeObject(Model), Encoding.UTF8, "application/json");

                    using (var response = await httpClient.PostAsync(_TwoFactor.EmailNotificationService, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        return apiResponse;
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Validates the two factor code.
        /// </summary>
        /// <returns></returns>
        [Route("validatetwofactorauthentication")]
        [HttpPost]
        public async Task<IActionResult> validatetwofactorcode(string Email, string Twofactorcode)
        {
            if (Email == null || Email == "")
            {
                return BadRequest("Invalid Email");
            }
            if (string.IsNullOrEmpty(Twofactorcode))
            {
                return BadRequest("Two Factor code required");
            }

            var result = await _userV2Service.ValidateTwoFactorCode(Email, Twofactorcode);
            int? SigninAttempt = _userService.GetSignInAttempt(Email);

            if (SigninAttempt >= _TwoFactor.MaximumAttempts)
            {
                await _userV2Service.UpdateTwoFactorCode(Email, "", 0);
                return BadRequest("Reached maximum attempts. Please generate new code");
            }

            else if (result != null)
            {
                await _userV2Service.UpdateTwoFactorCode(Email, "", 0);
                return Ok("Successfully validated the Two factor code");
            }
            else
            {
                int? leftattempts = (_TwoFactor.MaximumAttempts - (SigninAttempt == null ? 0 : SigninAttempt));

                await _userService.UpdateSigninAttempt(Email);
                return BadRequest("Failure while validating the code. You have left " + (leftattempts - 1) + " attempts to login.");
            }
        }




        [Route("/api/token/refresh")]
        [HttpPost]
        public async Task<IActionResult> refreshtokenasync([FromBody] RefreshTokenResource refreshTokenResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = await _authenticationService.RefreshTokenAsync(refreshTokenResource.Token, refreshTokenResource.UserEmail);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            }

            var tokenResource = _mapper.Map<AccessToken, AccessTokenResource>(response.Token);
            return Ok(tokenResource);
        }


        [Route("/api/token/revoke")]
        [HttpPost]
        public IActionResult revoketoken([FromBody] RevokeTokenResource revokeTokenResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _authenticationService.RevokeRefreshTokens(revokeTokenResource.Token);
            if (result == null)
            {
                return BadRequest("Invalid Revoke Token");
            }
            return Ok("Success");
        }


        #region FaceDetection        

        //// [Route("/api/generatefaceapitoken")]
        //// [HttpPost]
        //// public async Task<IActionResult> generatefaceapitoken([FromBody] GenerateTokenFaciAPIResources userCredentials)
        //// {
        ////     if (!ModelState.IsValid)
        ////     {
        ////         return BadRequest(ModelState);
        ////     }
        ////     UserTokenModel utk = new UserTokenModel();
        ////     TokenResponse tokenResponse = null;
        ////     string email = string.Empty;
        ////     TokenGeneration Model;
        ////     User user;


        ////     Tuple<string, double>[] tupleArray = new Tuple<string, double>[10];
        ////     ImageStream imageStream = new ImageStream();
        ////     foreach (var item in userCredentials.Base64string)
        ////     {
        ////         imageStream.Base64string = item.ToString();
        ////     }
        ////     if (userCredentials.GroupName != null && userCredentials.GroupName != "" && userCredentials.GroupName != "string")
        ////     {
        ////         imageStream.GroupName = userCredentials.GroupName;
        ////     }
        ////     else
        ////     {
        ////         imageStream.GroupName = "fulcrumone";
        ////     }

        ////     tupleArray = await _userService.FaceDetection(imageStream);

        ////     foreach (var item in tupleArray)
        ////     {
        ////         if (item.Item1 != "User Not Found" && item.Item1 != null && item.Item1 != "More than 1 faces detected, please enter another image")
        ////         {
        ////             email = item.Item1.ToString();
        ////         }

        ////     }

        ////     if (email != string.Empty || email != null)
        ////     {
        ////         tokenResponse = await _authenticationService.CreateFaceAccessTokenAsync(email, userCredentials.ClientID);
        ////     }

        ////     if (!tokenResponse.Success)
        ////     {
        ////         return BadRequest(tokenResponse.Message);
        ////     }

        ////     ////user = await _userService.FindByEmailAsync(email);
        ////     user = await _userService.FindByEmailClientIDAsync(email, userCredentials.ClientID);

        ////     utk.Id = user.Id;
        ////     utk.Email = user.Email;
        ////     utk.IsTwoFactorEnabled = user.IsTwoFactorEnabled;
        ////     utk.Twofactorcode = user.Twofactorcode;
        ////     utk.TwoFactorEnabledBy = user.TwoFactorEnabledBy;
        ////     utk.SigninAttempt = user.SigninAttempt;
        ////     utk.GroupName = user.GroupName;
        ////     utk.PersonId = user.PersonId;

        ////     foreach (var item in user.UserRoles)
        ////     {
        ////         utk.RoleID = Convert.ToString(item.RoleId);
        ////         utk.Rolename = Convert.ToString(item.Role.Name);
        ////     }

        ////     var accessTokenResource = _mapper.Map<AccessToken, AccessTokenResource>(tokenResponse.Token);

        ////     if (!_TwoFactor.IsTwoFactorServiceEnabled)
        ////     {
        ////         return Ok(accessTokenResource);
        ////     }
        ////     else
        ////     {
        ////         Model = new TokenGeneration
        ////         {
        ////             AccessToken = accessTokenResource.AccessToken,
        ////             Expiration = accessTokenResource.Expiration,
        ////             RefreshToken = accessTokenResource.RefreshToken,
        ////             IsTwoFactorEnabled = user.IsTwoFactorEnabled,
        ////             Twofactorcode = "",
        ////             TwoFactorEnabledBy = user.TwoFactorEnabledBy,
        ////             Email = email

        ////         };


        ////         await TwoFactorServiceInitiator(Model);
        ////         utk.TokenGeneration = Model;
        ////         return Ok(utk);
        ////         //return Ok(Model);
        ////     }
        //// }

        ////[Route("/api/token/FaceDetection")]
        ////[HttpPost]
        ////public async void FaceDetect(UserCredentialsResource userCredentials)
        ////{
        ////    try
        ////    {
        ////        string email = string.Empty;
        ////        Tuple<string, double>[] tupleArray = new Tuple<string, double>[10];
        ////        ImageStream imageStream = new ImageStream();
        ////        foreach (var item in userCredentials.Base64string)
        ////        {
        ////            imageStream.Base64string = item.ToString();
        ////        }
        ////        if (userCredentials.GroupName != null && userCredentials.GroupName != "" && userCredentials.GroupName != "string")
        ////        {
        ////            imageStream.GroupName = userCredentials.GroupName;
        ////        }
        ////        else
        ////        {
        ////            imageStream.GroupName = "fulcrumone";
        ////        }

        ////        tupleArray = await _userService.FaceDetection(imageStream);

        ////        foreach (var item in tupleArray)
        ////        {
        ////            if (item.Item1 != "User Not Found")
        ////            {
        ////                email = item.Item1.ToString();
        ////            }
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        throw ex;
        ////    }
        ////}

        #endregion

        [Route("/api/addnewclientdetails")]
        [HttpPost]
        public async Task<IActionResult> Addnewclient([FromBody] AuthClientDetail Model)
        {
            try
            {
                //// var containerObject = new JsonObject(Model);

                if (!Keyexistinmodel.HasProperty(Model, "ClientID"))
                {
                    return BadRequest("ClientID field is required.");
                }
                Model.CreatedBy = "1";
                Model.Createddate = DateTime.Now;
                Model.IsActive = true;
                Model.IsDeleted = false;
                var result = await _userV2Service.Addnewclient(Model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }


    }
}