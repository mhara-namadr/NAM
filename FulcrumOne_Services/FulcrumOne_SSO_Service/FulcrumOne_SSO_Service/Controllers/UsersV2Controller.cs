﻿using AutoMapper;
using FulcrumOne_SSO_Service.Core.Services;
using FulcrumOne_SSO_Service.Models;
using Microsoft.AspNetCore.Mvc;
using NLog;


namespace FulcrumOne_SSO_Service.Controllers
{
    [ApiController]
    ////[ApiVersion("2.0")]
    [Route("api/[controller]")]
    public class UsersV2Controller : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IUserV2Service _userV2Service;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        public UsersV2Controller(IUserService userService, IUserV2Service userV2Service, IMapper mapper)
        {
            _userService = userService;
            _userV2Service = userV2Service;
            _mapper = mapper;
        }


        ////[HttpPost]
        ////[ApiVersion("2.0")]
        ////[Route("/api/{v:apiVersion}/createnewuser")]
        ////public async Task<IActionResult> createnewuser([FromBody] UserCredentialsResource userCredentials)
        ////{
        ////    if (!ModelState.IsValid)
        ////    {
        ////        return BadRequest(ModelState);
        ////    }

        ////    var user = _mapper.Map<UserCredentialsResource, User>(userCredentials);
        ////    if (userCredentials.ClientID == string.Empty || userCredentials.ClientID == "")
        ////    {
        ////        userCredentials.ClientID = "F1";
        ////    }

        ////    var response = await _userService.CreateUserAsync(user, userCredentials.ClientID, ERole.Common);
        ////    if (!response.Success)
        ////    {
        ////        return BadRequest(response.Message);
        ////    }

        ////    var userResource = _mapper.Map<User, UserResource>(response.User);
        ////    return Ok(userResource);
        ////}


        [HttpPost]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("/api/createcommonuserwithcode")]
        public async Task<IActionResult> CreateCommonUserWithCodeAsync([FromBody] UserCredentialsResource userCredentials)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var user = _mapper.Map<UserCredentialsResource, AuthUser>(userCredentials);

                if (userCredentials.ClientID == string.Empty || userCredentials.ClientID == "")
                {
                    userCredentials.ClientID = "F1";
                }

                var response = await _userV2Service.CreateUserWithCodeAsync(user, userCredentials.ClientID, ERole.Common);
                if (!response.Success)
                {
                    var res = new { message = response.Message };
                    return BadRequest(res);
                }
                var userResource = _mapper.Map<AuthUser, UserResource>(response.User);
                return Ok(userResource);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



        [HttpPost]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("/api/createadminuserwithcode")]
        public async Task<IActionResult> CreateAdminUserWithCodeAsync([FromBody] UserCredentialsResource userCredentials)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var user = _mapper.Map<UserCredentialsResource, AuthUser>(userCredentials);
                if (userCredentials.ClientID == string.Empty || userCredentials.ClientID == "")
                {
                    userCredentials.ClientID = "F1";
                }

                var response = await _userV2Service.CreateUserWithCodeAsync(user, userCredentials.ClientID, ERole.Administrator);
                if (!response.Success)
                {
                    var res = new { message = response.Message };
                    return BadRequest(res);
                }

                var userResource = _mapper.Map<AuthUser, UserResource>(response.User);
                ////var users = _mapper.Map<UserCredentialsResource, User>(userCredentials);
                return Ok(userResource);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("/api/forgotpasswordwithcode")]
        public async Task<IActionResult> ForgotPasswordWithCode([FromBody] ForgotPasswordResource forgotPasswordResource)
        {
            try
            {
                if (((forgotPasswordResource.Email == "") || (forgotPasswordResource.Email == null)))
                {
                    var result = new { ErrorMsg = "Not a valid Email" };
                    return BadRequest(result);
                }
                else
                {
                    var result = await _userV2Service.ForgotPasswordWithCode(forgotPasswordResource.Email);
                    var res = new { Message = result };
                    return Ok(res);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [Route("/api/verifyaccountwithcode")]
        public async Task<string> VerifyAccountWithCode(string verifyCode)
        {
            try
            {
                var result = await _userV2Service.VerifyAccountWithCode(verifyCode);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("/api/changepasswordwithcode")]
        public async Task<IActionResult> ChangePasswordWithCode(string verifyCode, string newPassword)
        {
            try
            {
                if (!(verifyCode == null || newPassword == null))
                {
                    var userRole = await _userV2Service.ChangePasswordWithCode(verifyCode, newPassword);
                    return Ok(userRole);
                }
                else
                {
                    var result = new { ErrorMsg = "Verify Password or New Password cannot be null" };
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



    }
}
