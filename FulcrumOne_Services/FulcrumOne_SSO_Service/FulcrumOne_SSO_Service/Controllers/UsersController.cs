using AutoMapper;
using FulcrumOne_SSO_Service.Core.Services;
//using FulcrumOne_SSO_Service.Infrastructure.Notifications.Publishers.Dispatchers;
using FulcrumOne_SSO_Service.Models;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace FulcrumOne_SSO_Service.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    //[ApiVersion("1.0")]
    //[Route("api/v{version:apiVersion}/[controller]")]
    public class UsersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IUserV2Service _userV2Service;
        //private readonly NewUserCreatedEventDispatcher _newUserCreatedEventDispatcher;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();


        public UsersController(IUserV2Service userV2Service, IUserService userService, IMapper mapper)
        {
            _userV2Service = userV2Service;
            _userService = userService;
            _mapper = mapper;
           // _newUserCreatedEventDispatcher = newUserCreatedEventDispatcher ?? throw new ArgumentNullException(nameof(newUserCreatedEventDispatcher));
        }


        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/createnewuser")]
        public async Task<IActionResult> Createnewuser([FromBody] UserCredentialsResource userCredentials)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var user = _mapper.Map<UserCredentialsResource, AuthUser>(userCredentials);
                if (userCredentials.ClientID == string.Empty || userCredentials.ClientID == "")
                {
                    userCredentials.ClientID = "F1";
                }
                var response = await _userService.CreateUserAsync(user, userCredentials.ClientID, ERole.Common);
                if (!response.Success)
                {
                    return BadRequest(response.Message);
                }
                var userResource = _mapper.Map<AuthUser, UserResource>(response.User);
             //   await _newUserCreatedEventDispatcher.DispatchAsync(userCredentials);
                return Ok(userResource);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/createadminuser")]
        public async Task<IActionResult> Createadminuser([FromBody] UserCredentialsResource userCredentials)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var user = _mapper.Map<UserCredentialsResource, AuthUser>(userCredentials);

                var response = await _userService.CreateUserAsync(user, userCredentials.ClientID, ERole.Administrator);
                if (!response.Success)
                {
                    return BadRequest(response.Message);
                }
                var userResource = _mapper.Map<AuthUser, UserResource>(response.User);
                return Ok(userResource);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        /// <summary>
        /// Gets the user role.
        /// </summary>
        /// <param name="email">The email.</param> 
        /// <returns></returns>
        [HttpGet]
        [ApiVersion("1.0")]
        [Route("/api/getuserrole")]
        public async Task<IActionResult> Getuserrole([StringRange] string email)
        {
            try
            {
                var userRole = await _userService.GetUserRole(email);
                if (userRole != null)
                {
                    return Ok(userRole);
                }
                else
                {
                    return Ok("User not found");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/changepassword")]
        public async Task<IActionResult> Changepassword(string email, string oldPassword, string newPassword)
        {
            try
            {
                if (email != "" && email != string.Empty && email != null)
                {
                    if (oldPassword != null && newPassword != null)
                    {
                        var userRole = await _userService.ChangePassword(email, oldPassword, newPassword);
                        if (userRole)
                        {
                            return Ok(userRole);
                        }
                        else
                        {
                            return Ok("Old Password does not match");
                        }
                    }
                    else
                    {
                        return Ok("Enter valid oldpassword");
                    }
                }
                else
                {
                    return Ok("Please enter valid email id");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/getalluser")]
        public async Task<IActionResult> Getalluser(string role)
        {
            try
            {
                if (role != "" && role != string.Empty && role != null)
                {
                    var getAllUser = await _userService.GetAllUser(role);
                    if (getAllUser != null && getAllUser.Count != 0)
                    {
                        return Ok(getAllUser);
                    }
                    else
                    {
                        return Ok("Role not found");
                    }
                }
                else
                {
                    return Ok("Please enter valid role name");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/updateuserasync")]
        public async Task<IActionResult> Updateuserasync([FromBody] UserCredentialsResource userCredentials)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var Clientidexists = await _userV2Service.GetClientDetail(userCredentials.ClientID);
                if (Clientidexists == null)
                {
                    return BadRequest("ClientID not found.");
                }
                var user = _mapper.Map<UserCredentialsResource, AuthUser>(userCredentials);
                bool response;
                ////int Userexists = await _userService.Findbyemailclientidroleid(Convert.ToInt32(ERole.Administrator), userCredentials.ClientID, user.Email);
                response = await _userService.UpdateUserAsync(user, userCredentials.ClientID, ERole.Administrator);
                return Ok(response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/deleteuser")]
        public async Task<IActionResult> Deleteuser(string userEmail)
        {
            try
            {
                string result = string.Empty;
                if (userEmail != null)
                {
                    var existingUser = await _userService.FindByEmailAsync(userEmail);
                    if (existingUser != null)
                    {
                        var response = await _userService.DeleteUser(userEmail);
                        result = response;
                    }
                    else
                    {
                        result = "Not a valid user";
                    }
                }
                else
                {
                    result = "User Email is null";
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [ApiVersion("1.0")]
        ////[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("/api/forgotpassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordResource forgotPasswordResource)
        {
            try
            {
                var result = await _userService.ForgotPassword(forgotPasswordResource.Email);
                var res = new { Message = result };
                return Ok(res);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [NonAction]
        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/addnewuserrole")]
        public async Task<IActionResult> Adduserroles([FromBody] Adduserrole Model)
        {
            try
            {
                var Emailresult = await _userService.FindByEmailAsync(Model.Email);
                var userroleexist = await _userV2Service.Findbyemailclientidroleid(Convert.ToInt32(Model.RoleId), Model.ClientID, Model.Email);
                if (userroleexist != 0)
                {
                    return Ok("User role already exists.");
                }
                if (Emailresult == null)
                {
                    return BadRequest("Email Does not exist.");
                }
                else
                {
                    AuthUserRole Ur = new()
                    {
                        ClientID = Model.ClientID,
                        RoleId = Convert.ToInt32(Model.RoleId),
                        UserId = Emailresult.Id
                    };
                    await _userService.AddUserrole(Ur);
                    return Ok("Success");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        [HttpPost]
        [ApiVersion("1.0")]
        [Route("/api/addrole")]
        public async Task<IActionResult> AddRole(Mst_Role Model)
        {
            try
            {
                var result = await _userService.Addroleasync(Model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

    }
}