﻿using Microsoft.AspNetCore.Mvc;

namespace FulcrumOne_SSO_Service.Controllers
{
    public class LoginController : Controller
    {
        // GET: LoginController
        public ActionResult Index()
        {
            return View();
        }

        // GET: LoginController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


        [HttpGet]
        //[Route("userlogin/{Clientid}/{callbackurl}")]
        [Route("userlogin")]
        public ActionResult Loginview(string Clientid, string callbackurl)
        
        {
            ViewBag.Clientid = Clientid;
            ViewBag.callbackurl = callbackurl;
            return View();
        }

        [HttpGet]
        [Route("forgotpassword")]
        public ActionResult Forgotpassword()
        {
            return View();
        }

        [HttpGet]
        [Route("registernewuser")]
        public ActionResult Registernewuser()
        {
            return View();
        }

        [HttpGet]
        [Route("adduserroles")]
        public ActionResult Adduserroles()
        {
            return View();
        }
    }
}
