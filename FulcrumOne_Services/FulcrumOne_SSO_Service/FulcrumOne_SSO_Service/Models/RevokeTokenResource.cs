﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{
    public class RevokeTokenResource
    {
        [Required]
        [StringRange]
        public  string Token { get; set; }
    }
}
