namespace FulcrumOne_SSO_Service.Models
{
    public class UserResource
    {
        public int Id { get; set; }
        public string? Email { get; set; }
        public IEnumerable<string>? Roles { get; set; }
        public string? VerifyCode { get; set; }
    }
}