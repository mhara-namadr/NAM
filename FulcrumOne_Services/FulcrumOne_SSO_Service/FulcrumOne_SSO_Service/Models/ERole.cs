﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Models
{
    public enum ERole
    {
        Common = 1,
        Administrator = 2
    }
}
