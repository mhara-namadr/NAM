﻿ 
using System.ComponentModel.DataAnnotations; 

namespace FulcrumOne_SSO_Service.Models
{
    public class ForgotPasswordResource
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(255)]
        public  string Email { get; set; }

    }
}
