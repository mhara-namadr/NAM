﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Models
{
    public class ChangePasswordWithCode
    {

        public bool ChangePassword { get; set; }
        public  string? Message { get; set; }
    }
}
