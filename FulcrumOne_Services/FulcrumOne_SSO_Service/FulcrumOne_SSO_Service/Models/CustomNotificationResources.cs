﻿ 

namespace FulcrumOne_SSO_Service.Models
{
    public class CustomNotificationResources
    {
        public List<EmailNotificationResources>? ListEmailReceivers { get; set; }
         
        public  string? EmailSubject { get; set; }
         
        public  string? EmailTemplate { get; set; }
          
        public List<dynamic>? EmailBodyVariables { get; set; }
    }
}
