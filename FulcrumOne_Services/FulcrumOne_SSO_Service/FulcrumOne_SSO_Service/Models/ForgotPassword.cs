﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Models
{
    public class ForgotPassword
    {
        public string? Name { get; set; }
        public string? Email { get; set; }
        public bool UpdatePassword { get; set; }
        public string? Passresetstatus { get; set; }
        public string? VerifyCode { get; set; }
    }
}
