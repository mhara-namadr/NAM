﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Models
{
    public class Adduserrole
    {
        public  string Email { get; set; }
        public  string ClientID { get; set; }
        public  string? RoleId { get; set; }
    }
}
