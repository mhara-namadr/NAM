﻿namespace FulcrumOne_SSO_Service.Models
{
    public class EmailNotificationResources
    {
        public  string? ReceiverName { get; set; }
        public  string? ReceiverEmail { get; set; }

    }
}