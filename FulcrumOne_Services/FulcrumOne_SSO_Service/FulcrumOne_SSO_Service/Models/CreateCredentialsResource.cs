﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Models
{
    public class CreateCredentialsResource
    {

        [Required]       
        public  string? Email { get; set; }

        
        public  string? Password { get; set; }
        public bool IsTwoFactorEnabled { get; set; }
        public  string? Twofactorcode { get; set; }
        public  string? TwoFactorEnabledBy { get; set; }
        public int SigninAttempt { get; set; }
    }
}
