﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{
    public class ImageStream
    {

        [Required]
        public string? Base64string { get; set; }

        public string? GroupName { get; set; }

        public  string? Email { get; set; }
    }
}
