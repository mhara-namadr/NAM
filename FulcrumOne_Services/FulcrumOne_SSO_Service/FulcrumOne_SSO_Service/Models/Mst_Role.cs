﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{
    public class Mst_Role
    {
        [Required]
        public  string? Id { get; set; }

        [Required]
        [StringLength(50)]
        public  string? Name { get; set; }

    }
}
