﻿
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{
    public class AuthRole
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public  string? Name { get; set; }

        public ICollection<AuthUserRole> UsersRole { get; set; } = new Collection<AuthUserRole>();
    }
}
