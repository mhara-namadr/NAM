﻿namespace FulcrumOne_SSO_Service.Models
{
    public class UserTokenModel
    {
        public int Id { get; set; }
        public  string? Email { get; set; }

        public  string? Password { get; set; }

        public bool? IsTwoFactorEnabled { get; set; }

        public  string? Twofactorcode { get; set; }

        public  string? TwoFactorEnabledBy { get; set; }

        public int? SigninAttempt { get; set; }
        public  string? GroupName { get; set; }
        public  string? PersonId { get; set; }
        public  string? RoleID { get; set; }
        public TokenGeneration? TokenGeneration { get; set; }

        public  string? Rolename { get; set; }


    }
}
