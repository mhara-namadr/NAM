﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Models
{
    public class EmailConfiguration
    {
        public  string? SavedEmailTemplatesPath { get; set; }
        public  string? SenderEmail { get; set; }
        public  string? SenderEmailPassoword { get; set; }
        public  string? EmailSenderHost { get; set; }
        public int EmailSenderPort { get; set; }
        public bool EmailIsSSL { get; set; }
        public bool IsEmailEnabled { get; set; }
        public  string? TestMail { get; set; }

    }
}
