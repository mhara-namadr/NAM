﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{
    public class GenerateTokenFaciAPIResources
    {

        [Required]
        [StringRange]
        public List<string>? Base64string { get; set; }

        [Required]
        [StringRange]
        public  string? GroupName { get; set; }

        [StringRange]
        public  string? ClientID { get; set; }

    }


}
