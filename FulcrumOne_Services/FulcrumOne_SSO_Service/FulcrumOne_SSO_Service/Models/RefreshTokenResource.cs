﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{
    public class RefreshTokenResource
    {
        [Required]
        public  string Token { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(255)]
        public  string UserEmail { get; set; }
        
    }
}
