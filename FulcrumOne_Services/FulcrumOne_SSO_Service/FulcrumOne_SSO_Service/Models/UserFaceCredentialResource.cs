﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{
    public class UserFaceCredentialResource
    {

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(255)]
        public string? Email { get; set; }

        [Required]
        [StringLength(32)]
        public string? Password { get; set; }

        [Required]
        public List<string>? Base64string { get; set; }

        [Required]
        public string? GroupName { get; set; }

        [Required]
        public string? ClientID { get; set; }

        //[Required]
        //public bool isFaceEnabled { get; set; }
    }

    public class StringRangeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Field  is missing.");
            }
            else if ((value.ToString() == "") || (value.ToString() == "string") || (value.ToString() == "String") || (value.ToString() == "STRING") || (value.ToString() == null))
            {
                return new ValidationResult("Please enter a correct value");
            }

            return ValidationResult.Success;

        }
    }

}
