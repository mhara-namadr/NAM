﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_SSO_Service.Models
{

    public class AuthUser
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(255)]
        public string? Email { get; set; }

        [Required]
        public string? Password { get; set; }

        public bool? IsTwoFactorEnabled { get; set; }

        public string? Twofactorcode { get; set; }

        public string? TwoFactorEnabledBy { get; set; }

        public int? SigninAttempt { get; set; }
        public string? GroupName { get; set; }
        public string? PersonId { get; set; }
        ////public string ClientID { get; set; }
        public string? VerifyCode { get; set; }

        public ICollection<AuthUserRole> UserRoles { get; set; } = new Collection<AuthUserRole>();
    }
}