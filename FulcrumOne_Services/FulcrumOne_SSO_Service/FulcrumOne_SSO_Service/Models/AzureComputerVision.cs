﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Models
{
    public class AzureComputerVision
    {
        public  string? Endpoint { get; set; }
        public  string? Key { get; set; }
    }
}
