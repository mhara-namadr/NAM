﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FulcrumOne_SSO_Service.Models
{
    [Table("AuthUserRoles")]
    public class AuthUserRole
    {
        public int UserId { get; set; }
        public AuthUser? User { get; set; }

        public int RoleId { get; set; }
        public AuthRole? Role { get; set; }
        public string? ClientID { get; set; }

    }
}
