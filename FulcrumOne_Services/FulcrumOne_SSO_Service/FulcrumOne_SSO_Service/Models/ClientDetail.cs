﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FulcrumOne_SSO_Service.Models
{
    [Table("AuthClientDetail")]
    public class AuthClientDetail
    {
        public int ID { get; set; }

        [Required]
        [StringRange]
        public  string? ClientID { get; set; }

        [StringRange]
        public  string? ClientSecret { get; set; }

        [Required]
        [StringRange]
        public  string? Clientname { get; set; }
        public DateTime Createddate { get; set; }
        public  string? CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }
}
