﻿namespace FulcrumOne_SSO_Service.Models
{
    public class TokenGeneration : AccessTokenResource
    { 
        public bool? IsTwoFactorEnabled { get; set; }

        public  string? Twofactorcode { get; set; }

        public  string? TwoFactorEnabledBy { get; set; }

        public  string Email { get; set; }

    }
}
