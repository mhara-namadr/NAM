﻿namespace FulcrumOne_SSO_Service.Models
{
    public class AccessTokenResource
    {
        public string? AccessToken { get; set; }
        public string? RefreshToken { get; set; }
        public long Expiration { get; set; }
    }
}
