using AutoMapper;
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Mapping
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<UserCredentialsResource, AuthUser>();
            CreateMap<UserFaceCredentialResource, AuthUser>();
        }
    }
}