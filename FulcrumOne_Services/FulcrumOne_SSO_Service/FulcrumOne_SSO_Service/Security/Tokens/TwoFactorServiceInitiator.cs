﻿
namespace FulcrumOne_SSO_Service.Security.Tokens
{
    public class TwoFactorServiceInitiator
    {
        public int TwoFactorCodeLength { get; set; }

        public int MaximumAttempts { get; set; }
        public string? EmailNotificationService { get; set; }

        public string? EmailServiceUsername { get; set; }

        public string? EmailServicePassword { get; set; }

        public bool ConsumeEmailService { get; set; }

        public bool IsTwoFactorServiceEnabled { get; set; }



    }
}
