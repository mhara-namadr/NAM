
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace FulcrumOne_SSO_Service.Security.Tokens
{
    public class SigningConfigurations
    {
        public SecurityKey Key { get; }
        public SigningCredentials SigningCredentials { get; }

        public SigningConfigurations()
        {

            Key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("MIIBCgKCAQEAu6xUNXEN5ckPaMzfJstXsgZ8a4qUs0IwjAig4jAxyvWhBIwcj49pbD2AVhTHA+lRm1ti5PNwAv8JRUe6Ay9zMforPtFQv7sN7SC+qabDghHBhXxqZDpk/GBHsCcowKVFWzYdMQt1N1G9BOQCN3nRAzeM0EAUyWM4cMKyXoK+7+Y6Gt4cIPXEyLjyMcGFjLpNJYBqtnluOi3xA1x5ILd5SEDk0tDdBVRA87pvfUfW9sGs62A1KByGXM3sQY9OQeA1wbVBprDMEddlCoFZpYDErJq1BDTC+/F5DnyZfSM7wTAisCXmiCyWPn7ASEYZqhT9KAh9B/0lfZ9e8X7ru/P4vQIDAQAB"));


            SigningCredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256Signature);
        }
    }
}