﻿using Microsoft.EntityFrameworkCore; 
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Persistence
{
    public class AppDbContext : DbContext
    {

        public DbSet<AuthUser> AuthUsers { get; set; }
        public DbSet<AuthRole> AuthRoles { get; set; }

        public DbSet<AuthUserRole> AuthUserRoles { get; set; }
        public DbSet<AuthClientDetail> AuthClientDetail { get; set; }


        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        // => options.UseSqlite("Data Source=Token_Generator.db3");
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AuthUserRole>().HasKey(ur => new { ur.UserId, ur.RoleId });
        }
    }
}
