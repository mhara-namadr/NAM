﻿using FulcrumOne_SSO_Service.Core.Repositories;
using FulcrumOne_SSO_Service.Models;
using NLog;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Security.Cryptography;

namespace FulcrumOne_SSO_Service.Persistence
{
    public class UserV2Repository : IUserV2Repository
    {
        private readonly AppDbContext _context;
        public readonly AzureFaceDetection _azureFaceDetection;
        public readonly string _imagepath;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private static readonly char[] Punctuations = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

        /// <summary>
        /// UserV2Repository
        /// </summary>
        /// <param name="config"></param>
        /// <param name="context"></param>
        /// <param name="AzureFaceDetection"></param>
        public UserV2Repository(IConfiguration config, AppDbContext context, IOptions<AzureFaceDetection> AzureFaceDetection)
        {
            _imagepath = config.GetValue<string>("Imagepath");
            _context = context;
            _azureFaceDetection = AzureFaceDetection.Value;
        }


        public async Task AddWithCodeAsync(AuthUser user, string ClientID, ERole[] userRoles)
        {
            try
            {

                var roleNames = userRoles.Select(r => r.ToString()).ToList();
                var roles = await _context.AuthRoles.Where(r => roleNames.Contains(r.Name)).ToListAsync();

                foreach (var role in roles)
                {
                    user.UserRoles.Add(new AuthUserRole { RoleId = role.Id, ClientID = ClientID });
                }
                ////var userCode = _mapper.Map<UserWithCode, User>(user);

                _context.AuthUsers.Add(user);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public async Task AddFaceAsync(AuthUser user, string ClientID, List<string> base64string, ERole[] userRoles)
        {
            try
            {
                var roleNames = userRoles.Select(r => r.ToString()).ToList();
                var roles = await _context.AuthRoles.Where(r => roleNames.Contains(r.Name)).ToListAsync();

                foreach (var role in roles)
                {
                    user.UserRoles.Add(new AuthUserRole { RoleId = role.Id, ClientID = ClientID });
                }
                _context.AuthUsers.Add(user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<string> Addnewclient(AuthClientDetail Model)
        {
            string Returnmsg = string.Empty;
            try
            {
                ////var Clientidexist = _context.ClientDetail.Where(i => i.ClientID == Model.ClientID).FirstOrDefault();
                var Clientidexist = await FindClient(Model.ClientID);
                if (Clientidexist != null)
                {
                    Returnmsg = "ClientID already exists.";
                    return Returnmsg;
                }
                var result = _context.AuthClientDetail.Add(Model);
                await _context.SaveChangesAsync();
                Returnmsg = "Successfully added new Client.";
            }
            catch (Exception ex)
            {
                Returnmsg = "Error occured whhile adding new Client.";
                logger.Error(ex.ToString());
                throw;
            }
            return Returnmsg;
        }


        public async Task<AuthClientDetail> FindClient(string ClientID)
        {
            try
            {
                var result = await _context.AuthClientDetail.Where(c => c.ClientID == ClientID).SingleOrDefaultAsync();
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


        public async Task<AuthUser> FindByEmailClientIDAsync(string Email, string ClientID)
        {
            try
            {

                var result = await _context.AuthUsers.Include(u => u.UserRoles)
                                         .ThenInclude(ur => ur.Role)
                                         .SingleOrDefaultAsync(u => u.Email == Email);// && u.ClientID == ClientID);
                return result;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }



        public async Task<string> VerifyAccountWithCode(string verifyCode)
        {
            var user = _context.AuthUsers.Where(f => verifyCode == f.VerifyCode).FirstOrDefault();
            string resultString = string.Empty;

            if (user == null)
            {
                return "Something went wrong".ToString();
            }

            if (user.VerifyCode == null)
            {
                return "Code not Valid.".ToString();
            }
            else
            {
                user.VerifyCode = "";
                _context.AuthUsers.Update(user);
                var result = _context.SaveChanges();
                return "Account Verified Successfully".ToString();
            }
        }

        public async Task<ForgotPassword> ChangepasswordafterresetWithCode(string Email)
        {
            ForgotPassword model = new ForgotPassword();
            ////bool UpdatePassword = false;

            try
            {

                if (Email != "" || Email != null)
                {
                    var result = _context.AuthUsers.Where(u => u.Email == Email)
                                                     .FirstOrDefault();
                    if (result != null)
                    {


                        result.VerifyCode = Create(10, 0);
                        _context.AuthUsers.Update(result);
                        await _context.SaveChangesAsync();
                        model.UpdatePassword = true;
                        model.VerifyCode = result.VerifyCode;

                    }
                    return model;
                }
                else
                {
                    model.UpdatePassword = false;
                    model.Name = "";
                    return model;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        private string Create(int length, int numberOfNonAlphanumericCharacters)
        {
            if (length <= 0 || length > 128)
                throw new ArgumentException("Length must be 0 to 128.");

            if (numberOfNonAlphanumericCharacters > 0)
                return GeneratePassword(length, numberOfNonAlphanumericCharacters);

            const string alphaChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(alphaChars[rnd.Next(alphaChars.Length)]);
            }
            return res.ToString();
        }

        private static string GeneratePassword(int length, int numberOfNonAlphanumericCharacters)
        {
            if (length < 1 || length > 128)
            {
                throw new ArgumentException(nameof(length));
            }

            if (numberOfNonAlphanumericCharacters > length || numberOfNonAlphanumericCharacters < 0)
            {
                throw new ArgumentException(nameof(numberOfNonAlphanumericCharacters));
            }

            using (var rng = RandomNumberGenerator.Create())
            {
                var byteBuffer = new byte[length];

                rng.GetBytes(byteBuffer);

                var count = 0;
                var characterBuffer = new char[length];

                for (var iter = 0; iter < length; iter++)
                {
                    var i = byteBuffer[iter] % 87;

                    if (i < 10)
                    {
                        characterBuffer[iter] = (char)('0' + i);
                    }
                    else if (i < 36)
                    {
                        characterBuffer[iter] = (char)('A' + i - 10);
                    }
                    else if (i < 62)
                    {
                        characterBuffer[iter] = (char)('a' + i - 36);
                    }
                    else
                    {
                        characterBuffer[iter] = Punctuations[i - 62];
                        count++;
                    }
                }

                if (count >= numberOfNonAlphanumericCharacters)
                {
                    return new string(characterBuffer);
                }

                int j;
                var rand = new Random();

                for (j = 0; j < numberOfNonAlphanumericCharacters - count; j++)
                {
                    int k;
                    do
                    {
                        k = rand.Next(0, length);
                    }
                    while (!char.IsLetterOrDigit(characterBuffer[k]));

                    characterBuffer[k] = Punctuations[rand.Next(0, Punctuations.Length)];
                }

                return new string(characterBuffer);
            }
        }


        public async Task<ChangePasswordWithCode> ChangePasswordWithCode(string verifyCode, string password)
        {
            ChangePasswordWithCode model = new ChangePasswordWithCode();
            var user = _context.AuthUsers.Where(f => verifyCode == f.VerifyCode).FirstOrDefault();
            if (user == null)
            {
                model.ChangePassword = false;
                model.Message = "Code Not Valid.";
                return model;
            }
            else
            {
                model.ChangePassword = true;
                user.VerifyCode = "";
                user.Password = password;
                _context.AuthUsers.Update(user);
                await _context.SaveChangesAsync();
                model.Message = "Password Changed Successfully";
                return model;
            }
        }



        public async Task<AuthUser> ValidateTwoFactorCode(string Email, string Twofactorcode)
        {
            try
            {
                return await _context.AuthUsers.Where(u => u.Email == Email
                                                    && u.Twofactorcode == Twofactorcode)
                                            .SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        public async Task<bool> UpdateTwoFactorCode(string Email, string Twofactorcode, int Count)
        {
            bool UpdateStatus = false;
            try
            {
                var result = _context.AuthUsers.Where(u => u.Email == Email)
                                                       .SingleOrDefault();
                if (result != null)
                {
                    result.Twofactorcode = Twofactorcode;
                    result.SigninAttempt = Count;
                    await _context.SaveChangesAsync();
                    UpdateStatus = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return UpdateStatus;
        }


        /// <summary>
        /// Findbyclientuserid
        /// </summary> 
        /// <param name="RoleId"></param>
        /// <param name="ClientID"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        public async Task<int> Findbyemailclientidroleid(int RoleId, string ClientID, string Email)
        {
            try
            {
                //var result = await _context.UserRoles.Where(c => (c.UserId == UserId) && (c.RoleId == RoleId) && (c.ClientID == ClientID)).SingleOrDefaultAsync();
                var result = await (from U in _context.AuthUsers
                                    join UR in _context.AuthUserRoles on U.Id equals UR.UserId
                                    where U.Email == Email && UR.RoleId == RoleId && UR.ClientID == ClientID
                                    select U.Id)
              .FirstOrDefaultAsync();
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

    }
}
