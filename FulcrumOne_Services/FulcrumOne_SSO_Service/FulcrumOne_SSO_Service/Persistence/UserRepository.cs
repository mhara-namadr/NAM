﻿using System.Security.Cryptography;
using System.Text;
using FulcrumOne_SSO_Service.Core.Repositories;
using FulcrumOne_SSO_Service.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NLog;

namespace FulcrumOne_SSO_Service.Persistence
{
    /// <summary>Repository class to create the CRUD operation on the User table</summary>
    /// <seealso cref="IUserRepository" />
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _context;
        public readonly AzureFaceDetection _azureFaceDetection;
        public readonly string _imagepath;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private static readonly char[] Punctuations = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

        public UserRepository(IConfiguration config, AppDbContext context, IOptions<AzureFaceDetection> AzureFaceDetection)
        {
            _imagepath = config.GetValue<string>("Imagepath");
            _context = context;
            _azureFaceDetection = AzureFaceDetection.Value;
        }

        public async Task AddAsync(AuthUser user, string ClientID, ERole[] userRoles)
        {
            try
            {
                var roleNames = userRoles.Select(r => r.ToString()).ToList();
                var roles = await _context.AuthRoles.Where(r => roleNames.Contains(r.Name)).ToListAsync();
                foreach (var role in roles)
                {
                    user.UserRoles.Add(new AuthUserRole { RoleId = role.Id, ClientID = ClientID });
                }
                _context.AuthUsers.Add(user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<string> AddUserrole(AuthUserRole Role)
        {
            string returnmsg = string.Empty;
            try
            {
                var result = await _context.AuthUserRoles.AddAsync(Role);
                await _context.SaveChangesAsync();
                returnmsg = "Successfully added new Userrole.";
            }
            catch (Exception ex)
            {
                returnmsg = "Error occured whhile adding new Userrole.";
                logger.Error(ex);
                throw;
            }
            return returnmsg;
        }
        public async Task<string> Addroleasync(Mst_Role Model)
        {
            string returnmsg = string.Empty;
            try
            {
                AuthRole Rolemodel = new AuthRole();
                Rolemodel.Id = Convert.ToInt32(Model.Id);
                Rolemodel.Name = Model.Name;
                var entity = await _context.AuthRoles.FirstOrDefaultAsync(i => i.Id == Convert.ToInt32(Model.Id));
                if (entity == null)
                {
                    var result = await _context.AuthRoles.AddAsync(Rolemodel);
                    await _context.SaveChangesAsync();
                    returnmsg = "Successfully added new Role.";
                }
                else
                {
                    entity.Id = Convert.ToInt32(Model.Id);
                    entity.Name = Model.Name;
                    _context.SaveChanges();
                    returnmsg = "Successfully updated Role.";
                }
            }
            catch (Exception ex)
            {
                returnmsg = "Error occured whhile adding new Role.";
                logger.Error(ex);
                throw;
            }
            return returnmsg;
        }
        public async Task<AuthUser> FindByEmailAsync(string email)
        {
            try
            {

                ////var result = await _context.Users.Include(u => u.UserRoles)
                ////                           .ThenInclude(ur => ur.Role)
                ////                           .SingleOrDefaultAsync(u => u.Email == email);
                var result = await _context.AuthUsers.Where(c => c.Email == email).SingleOrDefaultAsync();
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }
        public async Task<string> GetPassword(string email)
        {
            try
            {
                string password = string.Empty;
                password = await (from U in _context.AuthUsers
                                  where U.Email == email
                                  select U.Password).FirstOrDefaultAsync();

                return password;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }
        public string Create(int length, int numberOfNonAlphanumericCharacters)
        {
            if (length <= 0 || length > 128)
                throw new ArgumentException("Length must be 0 to 128.");

            if (numberOfNonAlphanumericCharacters > 0)
                return GeneratePassword(length, numberOfNonAlphanumericCharacters);

            const string alphaChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(alphaChars[rnd.Next(alphaChars.Length)]);
            }
            return res.ToString();
        }

        public async Task<bool> Changepasswordafterreset(string Email, string Newpassword)
        {
            bool UpdatePassword = false;

            try
            {

                if (Email != "" || Email != null)
                {
                    var result = _context.AuthUsers.Where(u => u.Email == Email)
                                                     .SingleOrDefault();
                    if (result != null)
                    {
                        result.Password = Newpassword;
                        await _context.SaveChangesAsync();
                        UpdatePassword = true;
                    }
                    return UpdatePassword;
                }
                else
                {
                    UpdatePassword = false;
                    return UpdatePassword;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public static string GeneratePassword(int length, int numberOfNonAlphanumericCharacters)
        {
            if (length < 1 || length > 128)
            {
                throw new ArgumentException(nameof(length));
            }

            if (numberOfNonAlphanumericCharacters > length || numberOfNonAlphanumericCharacters < 0)
            {
                throw new ArgumentException(nameof(numberOfNonAlphanumericCharacters));
            }

            using (var rng = RandomNumberGenerator.Create())
            {
                var byteBuffer = new byte[length];

                rng.GetBytes(byteBuffer);

                var count = 0;
                var characterBuffer = new char[length];

                for (var iter = 0; iter < length; iter++)
                {
                    var i = byteBuffer[iter] % 87;

                    if (i < 10)
                    {
                        characterBuffer[iter] = (char)('0' + i);
                    }
                    else if (i < 36)
                    {
                        characterBuffer[iter] = (char)('A' + i - 10);
                    }
                    else if (i < 62)
                    {
                        characterBuffer[iter] = (char)('a' + i - 36);
                    }
                    else
                    {
                        characterBuffer[iter] = Punctuations[i - 62];
                        count++;
                    }
                }

                if (count >= numberOfNonAlphanumericCharacters)
                {
                    return new string(characterBuffer);
                }

                int j;
                var rand = new Random();

                for (j = 0; j < numberOfNonAlphanumericCharacters - count; j++)
                {
                    int k;
                    do
                    {
                        k = rand.Next(0, length);
                    }
                    while (!char.IsLetterOrDigit(characterBuffer[k]));

                    characterBuffer[k] = Punctuations[rand.Next(0, Punctuations.Length)];
                }

                return new string(characterBuffer);
            }
        }


        public async Task<bool> UpdateSigninAttempt(string Email)
        {
            bool UpdateStatus = false;
            try
            {
                var result = _context.AuthUsers.Where(u => u.Email == Email)
                                                       .SingleOrDefault();
                if (result != null)
                {
                    result.SigninAttempt = ((result.SigninAttempt == null || result.SigninAttempt == 0) ? 1 : result.SigninAttempt + 1);
                    await _context.SaveChangesAsync();
                    UpdateStatus = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return UpdateStatus;
        }

        public int? GetSignInAttempt(string Email)
        {
            int? result;
            try
            {
                result = _context.AuthUsers.Where(u => u.Email == Email)
                      .Select(u => u.SigninAttempt).SingleOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return result;
        }

        /// <summary>
        /// Gets the user role.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        public async Task<AuthRole> GetUserRole(string email)
        {
            try
            {
                var result = await (from u in _context.AuthUsers
                                    join ur in _context.AuthUserRoles on u.Id equals ur.UserId
                                    join r in _context.AuthRoles on ur.RoleId equals r.Id
                                    where u.Email == email
                                    select new AuthRole { Id = r.Id, Name = r.Name }).FirstOrDefaultAsync();
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public async Task<bool> ChangePassword(string email, string oldPassword, string newPassword)
        {
            bool UpdatePassword = false;
            try
            {
                var result = _context.AuthUsers.Where(u => u.Email == email)
                                                       .SingleOrDefault();
                if (result != null)
                {

                    result.Password = newPassword;
                    await _context.SaveChangesAsync();
                    UpdatePassword = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return UpdatePassword;
        }


        public async Task<List<AuthUser>> GetAllUser(string role)
        {
            try
            {
                var user = await (from u in _context.AuthUsers
                                  join ur in _context.AuthUserRoles on u.Id equals ur.UserId
                                  join r in _context.AuthRoles on ur.RoleId equals r.Id
                                  where r.Name == role
                                  select new AuthUser() { Id = u.Id, Email = u.Email }).ToListAsync();

                return user;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public async Task<bool> UpdateUserAsync(AuthUser user, string ClientID, ERole[] userRoles)
        {
            bool UpdateUser = false;

            try
            {
                var roleNames = userRoles.Select(r => r.ToString()).ToList();
                var roles = await _context.AuthRoles.Where(r => roleNames.Contains(r.Name)).ToListAsync();
                var userId = _context.AuthUsers.Where(u => u.Email == user.Email)
                      .Select(u => u.Id).SingleOrDefault();

                user.Id = userId;
                var admin = (from u in _context.AuthUsers
                             join ur in _context.AuthUserRoles on u.Id equals ur.UserId
                             join r in _context.AuthRoles on ur.RoleId equals r.Id
                             where r.Name == "Administrator"
                             select new AuthUser { Email = u.Email }).FirstOrDefaultAsync();

                #region DeleteUserRoles
                var userRoleDelete = _context.AuthUserRoles.Where(u => u.UserId == user.Id).SingleOrDefault();
                _context.AuthUserRoles.Remove(userRoleDelete);
                await _context.SaveChangesAsync();
                #endregion
                #region DeleteUser
                var userDelete = _context.AuthUsers.Where(u => u.Id == user.Id).SingleOrDefault();
                _context.AuthUsers.Remove(userDelete);
                await _context.SaveChangesAsync();
                #endregion
                #region AddUser

                foreach (var newrole in roles)
                {
                    user.UserRoles.Add(new AuthUserRole { RoleId = newrole.Id, ClientID = ClientID });
                }
                _context.AuthUsers.Add(user);
                await _context.SaveChangesAsync();
                #endregion

                UpdateUser = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UpdateUser;
        }

        public async Task DeleteUser(string userEmail)
        {
            try
            {
                var userDelete = _context.AuthUsers.Where(u => u.Email == userEmail).SingleOrDefault();
                _context.AuthUsers.Remove(userDelete);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
