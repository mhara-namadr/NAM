﻿using FulcrumOne_SSO_Service.Core.Security.Hashing;
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Persistence
{
    /// <summary>
    /// EF Core already supports database seeding throught overriding "OnModelCreating", but I decided to create a separate seed class to avoid 
    /// injecting IPasswordHasher into AppDbContext.
    /// To understand how to use database seeding into DbContext classes, check this link: https://docs.microsoft.com/en-us/ef/core/modeling/data-seeding
    /// </summary>
    public class DatabaseSeed
    {
        public static void Seed(AppDbContext context, IPasswordHasher passwordHasher)
        {
            context.Database.EnsureCreated();

            if (context.AuthRoles.Count() == 0)
            {

                var roles = new List<AuthRole>
                {
                new AuthRole { Name = ERole.Common.ToString() },
                new AuthRole { Name = ERole.Administrator.ToString() }
                };

                context.AuthRoles.AddRange(roles);
                context.SaveChanges();
            }

            if (context.AuthUsers.Count() == 0)
            {
                var users = new List<AuthUser>
                {
                    new AuthUser { Email = "admin@admin.com", Password = passwordHasher.HashPassword("12345678") },
                    new AuthUser { Email = "common@common.com", Password = passwordHasher.HashPassword("12345678") },
                };

                users[0].UserRoles.Add(new AuthUserRole
                {
                    RoleId = context.AuthRoles.SingleOrDefault(r => r.Name == ERole.Administrator.ToString()).Id
                });

                users[1].UserRoles.Add(new AuthUserRole
                {
                    RoleId = context.AuthRoles.SingleOrDefault(r => r.Name == ERole.Common.ToString()).Id
                });

                context.AuthUsers.AddRange(users);
                context.SaveChanges();
            }
        }
    }
}
