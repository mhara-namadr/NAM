﻿using FulcrumOne_SSO_Service.Core.Repositories;

namespace FulcrumOne_SSO_Service.Persistence
{
    /// <summary>This is Unit of Work Class to maintain the Context</summary>
    /// <seealso cref="IUnitOfWork" />
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
