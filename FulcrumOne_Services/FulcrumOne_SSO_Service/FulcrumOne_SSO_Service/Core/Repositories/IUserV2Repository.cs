﻿
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Core.Repositories
{
    public interface IUserV2Repository
    {
        Task AddWithCodeAsync(AuthUser user, string ClientID, ERole[] userRoles);
        Task AddFaceAsync(AuthUser user, string ClientID, List<string> base64string, ERole[] userRoles);
        Task<string> Addnewclient(AuthClientDetail Model);
        Task<AuthClientDetail> FindClient(string ClientID);
        Task<AuthUser> FindByEmailClientIDAsync(string Email, string ClientID);
        Task<string> VerifyAccountWithCode(string verifyCode);
        Task<ForgotPassword> ChangepasswordafterresetWithCode(string Email);
        Task<ChangePasswordWithCode> ChangePasswordWithCode(string verifyCode, string password);
        Task<AuthUser> ValidateTwoFactorCode(string Email, string Twofactorcode);
        Task<bool> UpdateTwoFactorCode(string Email, string Twofactorcode, int Count);
        Task<int> Findbyemailclientidroleid(int RoleId, string ClientID, string Email);
    }
}
