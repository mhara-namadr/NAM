﻿
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Core.Repositories
{
    public interface IUserRepository
    {
        Task AddAsync(AuthUser user, string ClientID, ERole[] userRoles);
        Task<AuthUser> FindByEmailAsync(string email);
        Task<bool> UpdateSigninAttempt(string Email);
        int? GetSignInAttempt(string Email);
        Task<AuthRole> GetUserRole(string email);
        Task<List<AuthUser>> GetAllUser(string role);
        Task<bool> ChangePassword(string email, string oldPassword, string newPassword);
        Task<bool> UpdateUserAsync(AuthUser user, string ClientID, ERole[] userRoles);
        Task DeleteUser(string userEmail);
        Task<bool> Changepasswordafterreset(string Email, string Newpassword);
        Task<string> AddUserrole(AuthUserRole Role);
        Task<string> Addroleasync(Mst_Role Model);
        string Create(int length, int numberOfNonAlphanumericCharacters);
        Task<string> GetPassword(string email);

    }
}
