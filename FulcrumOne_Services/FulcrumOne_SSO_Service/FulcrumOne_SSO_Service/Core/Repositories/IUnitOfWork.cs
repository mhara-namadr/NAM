﻿using System.Threading.Tasks;

namespace FulcrumOne_SSO_Service.Core.Repositories
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}
