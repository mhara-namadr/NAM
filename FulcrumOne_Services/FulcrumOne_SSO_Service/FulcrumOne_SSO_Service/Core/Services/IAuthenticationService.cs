 
using FulcrumOne_SSO_Service.Core.Security.Tokens;
using FulcrumOne_SSO_Service.Core.Services.Communication;
using Microsoft.IdentityModel.Tokens;

namespace FulcrumOne_SSO_Service.Core.Services
{
    public interface IAuthenticationService
    {
        Task<TokenResponse> CreateAccessTokenAsync(string email, string password,string ClientID);
        Task<TokenResponse> CreateFaceAccessTokenAsync(string email, string ClientID);
        Task<TokenResponse> RefreshTokenAsync(string refreshToken, string userEmail);
        void RevokeRefreshToken(string refreshToken);
        RefreshToken RevokeRefreshTokens(string refreshToken);

        TokenValidationParameters GetValidationParameters();
    }
}