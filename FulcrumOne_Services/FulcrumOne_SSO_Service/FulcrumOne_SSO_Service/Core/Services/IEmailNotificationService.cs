﻿
using FulcrumOne_SSO_Service.Models; 

namespace FulcrumOne_SSO_Service.Core.Services
{
    public interface IEmailNotificationService
    {  

        Task<string> SendCustomEmailNotification(CustomNotificationResources Json);

    }
}
