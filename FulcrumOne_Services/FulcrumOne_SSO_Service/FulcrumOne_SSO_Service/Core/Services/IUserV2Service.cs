﻿
using FulcrumOne_SSO_Service.Core.Services.Communication;
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Core.Services
{
    public interface IUserV2Service
    {
        Task<AuthUser> FindByEmailClientIDAsync(string email, string ClientID);
        Task<CreateUserResponse> CreateUserFaceAsync(AuthUser user, string ClientID, List<string> base64string, params ERole[] userRoles);
        Task<CreateUserResponse> CreateUserWithCodeAsync(AuthUser user, string ClientID, params ERole[] userRoles);
        Task<string> VerifyAccountWithCode(string verifyCode);
        Task<ChangePasswordWithCode> ChangePasswordWithCode(string verifyCode, string newPassword);
        Task<ForgotPassword> ForgotPasswordWithCode(string Email);
        Task<string> Addnewclient(AuthClientDetail Model);
        Task<AuthClientDetail> GetClientDetail(string ClientID);
        Task<int> Findbyemailclientidroleid(int RoleId, string ClientID, string Email);
        Task<AuthUser> ValidateTwoFactorCode(string Email, string Twofactorcode);
        Task<bool> UpdateTwoFactorCode(string Email, string Twofactorcode, int Count);

    }
}
