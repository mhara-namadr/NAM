using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Core.Services.Communication
{
    public class CreateUserResponse : BaseResponse
    {
        public AuthUser User { get; private set; }

        public CreateUserResponse(bool success, string message, AuthUser user) : base(success, message)
        {
            User = user;
        }
    }
}