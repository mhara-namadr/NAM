

using FulcrumOne_SSO_Service.Core.Security.Tokens;

namespace FulcrumOne_SSO_Service.Core.Services.Communication
{
    public class TokenResponse : BaseResponse
    {
        public AccessToken Token { get; set; }

        public TokenResponse(bool success, string message, AccessToken token) : base(success, message)
        {
            Token = token;
        }
    }
}