
using FulcrumOne_SSO_Service.Core.Services.Communication;
using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Core.Services
{
    public interface IUserService
    {
        Task<CreateUserResponse> CreateUserAsync(AuthUser user, string ClientID, params ERole[] userRoles);
        Task<AuthUser> FindByEmailAsync(string email);
        Task<bool> UpdateSigninAttempt(string Email);
        int? GetSignInAttempt(string Email);
        Task<AuthRole> GetUserRole(string email);
        Task<List<AuthUser>> GetAllUser(string role);
        Task<bool> ChangePassword(string email, string oldPassword, string newPassword);
        Task<bool> UpdateUserAsync(AuthUser user, string ClientID, params ERole[] userRoles);
        Task<string> DeleteUser(string userEmail);
        Task<string> ForgotPassword(string Email);
        Task<string> AddUserrole(AuthUserRole Role);
        Task<string> Addroleasync(Mst_Role Model);

    }
}