using FulcrumOne_SSO_Service.Models;

namespace FulcrumOne_SSO_Service.Core.Security.Tokens
{
    public interface ITokenHandler
    {
        AccessToken CreateAccessToken(AuthUser user);
        RefreshToken TakeRefreshToken(string token);
        RefreshToken RevokeRefreshToken(string token);
        ////TokenValidationParameters GetValidationParameters();
    }
}