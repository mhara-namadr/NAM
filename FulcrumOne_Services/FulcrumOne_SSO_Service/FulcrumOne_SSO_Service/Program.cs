using FulcrumOne_SSO_Service.Persistence;
using FulcrumOne_SSO_Service.Core.Repositories;
using FulcrumOne_SSO_Service.Core.Security.Hashing;
using FulcrumOne_SSO_Service.Core.Security.Tokens;
using FulcrumOne_SSO_Service.Core.Services;
using FulcrumOne_SSO_Service.Models;
using FulcrumOne_SSO_Service.Security.Hashing;
using FulcrumOne_SSO_Service.Security.Tokens;
using FulcrumOne_SSO_Service.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
//using Ingress.Producer.Notifications.Extensions;
//using FulcrumOne_SSO_Service.Infrastructure.Notifications.Publishers.Dispatchers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddApiVersioning(x =>
{
    x.DefaultApiVersion = new ApiVersion(1, 0);
    x.AssumeDefaultVersionWhenUnspecified = true;
    x.ReportApiVersions = true;
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

////Add Automapper Dependency
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddApiVersioning();

#region  RegisterServiceBusDependencies

//builder.Services.AddServiceBusNotifications(builder.Configuration);
//builder.Services.AddScoped<NewUserCreatedEventDispatcher>();

#endregion

////Add COnnection String Dependency
//var connection = builder.Configuration.GetConnectionString("SqliteConnectionString");
////var serverVersion = new sqlserverve(new Version(5, 7, 0));
//builder.Services.AddDbContext<AppDbContext>(options =>
//    options.UseSqlServer(connection)
//);


////Enable CORS 
builder.Services.AddCors(options =>
          {
              options.AddPolicy("AllowOrigin",
                  builder => builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod());
          });

#region  Dependencies for Repository and Services

builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

builder.Services.AddSingleton<IPasswordHasher, PasswordHasher>();
builder.Services.AddSingleton<ITokenHandler, FulcrumOne_SSO_Service.Security.Tokens.TokenHandler>();

builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IUserV2Service, UserV2Service>();
builder.Services.AddScoped<IUserV2Repository, UserV2Repository>();
builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();
builder.Services.AddScoped<IEmailNotificationService, EmailNotificationService>();

#endregion

#region Get Settings from appsettings.json file

builder.Services.Configure<EmailConfiguration>(builder.Configuration.GetSection("EmailConfiguration"));
var emailconfiguration = builder.Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>();

builder.Services.Configure<TokenOptions>(builder.Configuration.GetSection("TokenOptions"));
var tokenOptions = builder.Configuration.GetSection("TokenOptions").Get<TokenOptions>();

builder.Services.Configure<TwoFactorServiceInitiator>(builder.Configuration.GetSection("TwoFactorServiceInitiator"));
var TwoFactorServiceInitiator = builder.Configuration.GetSection("TwoFactorServiceInitiator").Get<TwoFactorServiceInitiator>();

builder.Services.Configure<AzureFaceDetection>(builder.Configuration.GetSection("AzureFaceAPI"));
var azureFaceAPI = builder.Configuration.GetSection("AzureFaceAPI").Get<AzureFaceDetection>();

builder.Services.Configure<AzureComputerVision>(builder.Configuration.GetSection("AzureComputerVisionAPI"));
var azuerComputerVision = builder.Configuration.GetSection("AzureComputerVisionAPI").Get<AzureFaceDetection>();

#endregion

var signingConfigurations = new SigningConfigurations();
builder.Services.AddSingleton(signingConfigurations);

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(jwtBearerOptions =>
               {
                   jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                   {
                       ValidateAudience = true,
                       ValidateLifetime = true,
                       ValidateIssuerSigningKey = true,
                       ValidIssuer = tokenOptions.Issuer,
                       ValidAudience = tokenOptions.Audience,
                       IssuerSigningKey = signingConfigurations.Key,
                       ClockSkew = TimeSpan.Zero
                   };
               });
var connection = builder.Configuration.GetConnectionString("SqlConnectionString");
//var serverVersion = new sqlserverve(new Version(5, 7, 0));
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseSqlServer(connection)
);
var connectionString = builder.Configuration.GetConnectionString("SqlConnectionString");

builder.Services.AddSingleton(connectionString);

var app = builder.Build();
app.UseSwagger();
app.UseSwaggerUI();
// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseCors("AllowOrigin");

app.Run();


