﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Major Code Smell", "S2589:Boolean expressions should not be gratuitous", Justification = "<Pending>", Scope = "member", Target = "~M:FulcrumOne_Dynamicform_Service.Core.Repository.AdmindynamicformRepository.AdminInsertAdvanceDynamicdataAsync(FulcrumOne_Dynamicform_Service.Models.Adminadvancedynamicconfig)~System.Threading.Tasks.Task{System.Object}")]
[assembly: SuppressMessage("Major Code Smell", "S2589:Boolean expressions should not be gratuitous", Justification = "<Pending>", Scope = "member", Target = "~M:FulcrumOne_Dynamicform_Service.Core.Repository.AdmindynamicformRepository.AdminInsertDynamicdataAsync(FulcrumOne_Dynamicform_Service.Models.Dynamicadminmodel)~System.Threading.Tasks.Task{System.Object}")]
[assembly: SuppressMessage("Minor Code Smell", "S3459:Unassigned members should be removed", Justification = "<Pending>", Scope = "member", Target = "~F:FulcrumOne_Dynamicform_Service.CosmosDBSingleton._dbOptions")]
[assembly: SuppressMessage("Minor Code Smell", "S1450:Private fields only used as local variables in methods should become local variables", Justification = "<Pending>", Scope = "member", Target = "~F:FulcrumOne_Dynamicform_Service.CosmosDBSingleton.cosmosClient")]
[assembly: SuppressMessage("Minor Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:FulcrumOne_SSO_Service.Persistence.UserV2Repository.VerifyAccountWithCode(System.String)~System.Threading.Tasks.Task{System.String}")]
