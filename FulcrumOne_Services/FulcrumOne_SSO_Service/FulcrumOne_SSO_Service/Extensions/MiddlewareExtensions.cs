﻿// using Microsoft.OpenApi.Models;
// using System.Reflection;

// namespace FulcrumOne_SSO_Service.Extensions
// {
//     public static class MiddlewareExtensions
// 	{
//         private const string Url = "/swagger/v1/swagger.json";
//         private const string FduriString = "https://fulcrumdigital.com/";

//         public static IServiceCollection AddCustomSwagger(this IServiceCollection services)
// 		{
// 			services.AddSwaggerGen(cfg =>
// 			{
// 				cfg.SwaggerDoc("v1", new OpenApiInfo
// 				{
// 					Title = "F1 Token Generator",
// 					Version = "v3",
// 					Description = "Example API that shows how to implement JSON Web Token authentication and authorization with ASP.NET Core 3.1, built from scratch.",
// 					Contact = new OpenApiContact
// 					{
// 						Name = "Fulcrum Digital",
// 						Url = new Uri(FduriString)
// 					},
// 					License = new OpenApiLicense
// 					{
// 						Name = "MIT",
// 					},
// 				});

// 				cfg.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
// 				{
// 					In = ParameterLocation.Header,
// 					Description = "JSON Web Token to access resources. Example: Bearer {token}",
// 					Name = "Authorization",
// 					Type = SecuritySchemeType.ApiKey
// 				});

// 				cfg.AddSecurityRequirement(new OpenApiSecurityRequirement
// 				{
// 					{
// 						new OpenApiSecurityScheme
// 						{
// 							Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
// 						},
// 						new [] { string.Empty }
// 					}
// 				});

// 				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
// 				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
// 				cfg.IncludeXmlComments(xmlPath);
// 			});

// 			return services;
// 		}

// 		public static IApplicationBuilder UseCustomSwagger(this IApplicationBuilder app)
// 		{
// 			app.UseSwagger().UseSwaggerUI(options =>
// 			{
// 				options.SwaggerEndpoint(Url, "F1 Token Generator API");
// 				options.DocumentTitle = "F1 Token Generator API";
// 			});

// 			return app;
// 		}
// 	}
// }
