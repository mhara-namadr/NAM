﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace FulcrumOne_SSO_Service.Extensions
{
    public static class DbSetExtensions
    {
        public static void AddIfNotExists<T>(this DbSet<T> dbSet, Func<T, object> predicate, params T[] entities) where T : class, new()
        {
            foreach (var entity in entities)
            {
                var newValues = predicate.Invoke(entity);
                Expression<Func<T, bool>> compare = arg => predicate(arg).Equals(newValues);
                var compiled = compare.Compile();
                var existing = dbSet.FirstOrDefault(compiled);
                if (existing == null)
                {
                    dbSet.Add(entity);
                }
            }
        }

        public static void AddRangeIfNotExists<TEnt, TKey>(this DbSet<TEnt> dbSet, IEnumerable<TEnt> entities, Func<TEnt, TKey> predicate) where TEnt : class
        {
            var entitiesExist = from ent in dbSet
                                where entities.Any(add => predicate(ent).Equals(predicate(add)))
                                select ent;

            dbSet.AddRange(entities.Except(entitiesExist));
        }
    }
}
