﻿namespace FulcrumOne_SSO_Service.Extensions
{
    public static class Keyexistinmodel
    {
        public static bool HasProperty(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }
    }
}
