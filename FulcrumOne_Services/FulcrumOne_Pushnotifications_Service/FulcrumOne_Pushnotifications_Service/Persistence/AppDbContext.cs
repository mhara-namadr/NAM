﻿using Microsoft.EntityFrameworkCore;

namespace FulcrumOne_Pushnotifications_Service.Persistence
{
    public class AppDbContext : DbContext
    {

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        { }
    }
}
