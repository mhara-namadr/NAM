﻿using FulcrumOne_Pushnotifications_Service.Core.IServices;
using FulcrumOne_Pushnotifications_Service.Core.Repository;
//using FulcrumOne_Pushnotifications_Service.CosmosDB_Models;
using FulcrumOne_Pushnotifications_Service.Models;
using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using DatabaseTables = FulcrumOne_Pushnotifications_Service.CosmosDB_Models.DatabaseTables;

namespace FulcrumOne_Pushnotifications_Service.Services
{
    public class PushnotificationService : IPushnotificationService
    {
       
        private readonly DBConfigurationDetails _DBConfiguration;
        private readonly IPushnotificationRepository _repository;

        #region Constructor
        public PushnotificationService(IOptions<DBConfigurationDetails> DBConfig, IPushnotificationRepository IDBTranObj)
        {
            _DBConfiguration = DBConfig.Value;
            _repository = IDBTranObj;
        }
        #endregion

        public async Task<Getinstructortransactions> Getinstructortransactions(int Instuctorid, int count)
        {
            try
            {
                var list = await _repository.Getinstructortransactions(Instuctorid, count);
                return list;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<Getstudenttransactions> Getstudenttransactions(int Studentid, int Count)
        {
            try
            {
                var list = await _repository.Getstudenttransactions(Studentid, Count);
                return list;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<ResponseModel> ReadallInstructortransactions(int Instructorid)
        {
            try
            {
                var result = await _repository.ReadallInstructortransactions(Instructorid);
                return result;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<ResponseModel> ReadallStudenttransactions(int Studentid)
        {
            try
            {
                var result = await _repository.ReadallStudenttransactions(Studentid);
                return result;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<ResponseModel> Readinstructortransaction(int id)
        {
            try
            {
                var result = await _repository.Readinstructortransaction(id);
                return result;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<ResponseModel> Readstudenttransaction(int id)
        {
            try
            {
                var result = await _repository.Readstudenttransaction(id);
                return result;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

    }
}
