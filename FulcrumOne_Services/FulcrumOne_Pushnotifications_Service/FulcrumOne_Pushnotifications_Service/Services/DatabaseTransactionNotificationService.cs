﻿using FulcrumOne_Pushnotifications_Service.Core.Repository;
using FulcrumOne_Pushnotifications_Service.Models;
using FulcrumOne_Pushnotifications_Service.Core.IServices;
using FulcrumOne_Pushnotifications_Service.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FulcrumOne_Pushnotifications_Service.Services
{
    public class DatabaseTransactionNotificationService : IDatabaseTransactionNotificationService
    {
        private readonly DBConfigurationDetails _DBTables;
        private readonly IDBTransactionRepository _DBTransRepo;

        #region Constructor
        public DatabaseTransactionNotificationService(IOptions<DBConfigurationDetails> DBTables, IDBTransactionRepository IDBTranObj)
        {
            _DBTables = DBTables.Value;
            _DBTransRepo = IDBTranObj;
        }
        #endregion

        #region GetDatabaseTransactionNotification 
        public List<ListDBTransactionCount> GetDatabaseTransactionNotification(DatabaseTransactionCommand Model)
        {
            List<ListDBTransactionCount> List = new List<ListDBTransactionCount>();
            try
            {
                if (Model.TypeOfCommand == "Insert" || Model.TypeOfCommand == "insert" || Model.TypeOfCommand == "INSERT")
                {
                    List = GetInsertedRecordCount(Model);
                }
                else if (Model.TypeOfCommand == "Update" || Model.TypeOfCommand == "update" || Model.TypeOfCommand == "UPDATE")
                {
                    List = GetUpdatedRecordCount(Model);
                }
                else if (Model.TypeOfCommand == "Delete" || Model.TypeOfCommand == "delete" || Model.TypeOfCommand == "DELETE")
                {
                    List = GetDeletedRecordCount(Model);
                }
            }
            catch (Exception)
            {
                List = null;
            }
            return List;
        }
        #endregion

        #region Get Inserted Record Count
        public List<ListDBTransactionCount> GetInsertedRecordCount(DatabaseTransactionCommand Model)
        {
            List<ListDBTransactionCount> List = new List<ListDBTransactionCount>();
            try
            {
                string[] DatabasetablesForUpdate = _DBTables.DBTables;
                int DatabaseUpdateDays = _DBTables.DatabaseUpdateDays;  // No of Days for specifying the DB change data
                String InsertColumnSort = _DBTables.InsertColumnSort;

                foreach (var item in DatabasetablesForUpdate)
                {
                    int InsertedRecords = _DBTransRepo.GetInsertedRecordCount(item.Trim(), DatabaseUpdateDays, InsertColumnSort.Trim());
                    List.Add(new ListDBTransactionCount { Count = InsertedRecords, TableName = item.Trim() });
                }
            }
            catch (Exception)
            {
                List = null;
            }
            return List;
        }
        #endregion

        #region Get Updated Record Count
        public List<ListDBTransactionCount> GetUpdatedRecordCount(DatabaseTransactionCommand Model)
        {
            List<ListDBTransactionCount> List = new List<ListDBTransactionCount>();

            try
            {
                string[] DatabasetablesForUpdate = _DBTables.DBTables;
                int DatabaseUpdateDays = _DBTables.DatabaseUpdateDays;  // No of Days for specifying the DB change data

                String UpdateColumnSort = _DBTables.UpdateColumnSort;

                foreach (var item in DatabasetablesForUpdate)
                {
                    int Updatedrecords = _DBTransRepo.GetUpdatedRecordCount(item.Trim(), DatabaseUpdateDays, UpdateColumnSort.Trim());
                    List.Add(new ListDBTransactionCount { Count = Updatedrecords, TableName = item.Trim() });
                }
            }
            catch (Exception)
            {
                List = null;
            }
            return List;
        }
        #endregion

        #region Get Deleted Record Count
        public List<ListDBTransactionCount> GetDeletedRecordCount(DatabaseTransactionCommand Model)
        {
            List<ListDBTransactionCount> List = new List<ListDBTransactionCount>();

            try
            {
                string[] DatabasetablesForUpdate = _DBTables.DBTables;
                int DatabaseUpdateDays = _DBTables.DatabaseUpdateDays;  // No of Days for specifying the DB change data

                String DeleteColumnSort = _DBTables.DeleteColumnSort;

                foreach (var item in DatabasetablesForUpdate)
                {
                    int DeletedRecords = _DBTransRepo.GetDeletedRecordCount(item.Trim(), DatabaseUpdateDays, DeleteColumnSort.Trim(), _DBTables.UpdateColumnSort.Trim());
                    List.Add(new ListDBTransactionCount { Count = DeletedRecords, TableName = item.Trim() });
                }
            }
            catch (Exception)
            {
                List = null;
            }
            return List;
        }
        #endregion

        #region Get DB Transaction Details
        public List<dynamic> GetDatabaseTransactionDetails(DBTransactionDetails Model)
        {
            List<dynamic> list = new List<dynamic>();
            try
            {
                bool IsValidtable = _DBTables.DBTables.Contains(Model.TableName);

                if (!IsValidtable)
                {
                    list.Add(new
                    {
                        Message = "Invalid Table Name"
                    });
                    return list;
                }
                else if (Model.DatabaseTransactionCommand == "Insert" || Model.DatabaseTransactionCommand == "insert" || Model.DatabaseTransactionCommand == "INSERT")
                {
                    list = _DBTransRepo.GetInsertTransactionDetails(_DBTables.InsertColumnSort, Model.TableName, _DBTables.DatabaseUpdateDays).ToList();
                }
                else if (Model.DatabaseTransactionCommand == "Delete" || Model.DatabaseTransactionCommand == "delete" || Model.DatabaseTransactionCommand == "DELETE")
                {
                    list = _DBTransRepo.GetDeleteTransactionDetails(_DBTables.DeleteColumnSort, Model.TableName, _DBTables.DatabaseUpdateDays, _DBTables.UpdateColumnSort.Trim()).ToList();
                }
                else if (Model.DatabaseTransactionCommand == "Update" || Model.DatabaseTransactionCommand == "update" || Model.DatabaseTransactionCommand == "UPDATE")
                {
                    list = _DBTransRepo.GetUpdateTransactionDetails(_DBTables.UpdateColumnSort, Model.TableName, _DBTables.DatabaseUpdateDays).ToList();
                }
            }
            catch (Exception)
            {
                list = null;
            }

            return list;
        }
        #endregion
    }
}
