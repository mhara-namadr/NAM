﻿using Dapper;
using FulcrumOne_Pushnotifications_Service.Core.Repository;
using FulcrumOne_Pushnotifications_Service.Models;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace FulcrumOne_Pushnotifications_Service.Repository
{
    public class PushnotificationRepository : IPushnotificationRepository
    {

     
        private readonly string  _connectionString;
        private readonly DBConfigurationDetails _DBConfiguration;

        public PushnotificationRepository(ConnectionString connectionStrings, IOptions<DBConfigurationDetails> DBConfig)
        {
            _connectionString = "Data Source=f1mysql.mysql.database.azure.com;Port=3306;Database=skillsengine;User=f1admin@f1mysql;Password=Fulcrum0ne001;";
            _DBConfiguration = DBConfig.Value;
        }


        public async Task<Getinstructortransactions> Getinstructortransactions(int Instuctorid, int count)
        {
            Getinstructortransactions model = new Getinstructortransactions();

            string countquery = "Select count(id) from txn_dbtransactionnotification where Instructorid = " + Instuctorid + " and Istransactionreadbyinstrucotor = 0 and Istransactionforinstructor = 1 order by Createddate desc;";
            string query;
            if (count == 0)
            {
                query = "Select * from txn_dbtransactionnotification where Instructorid = " + Instuctorid + " and Istransactionreadbyinstrucotor = 0 and Istransactionforinstructor = 1 order by Createddate desc;";
            }
            else
            {
                query = "SELECT * from txn_dbtransactionnotification where Instructorid = " + Instuctorid + " and Istransactionreadbyinstrucotor = 0 and Istransactionforinstructor = 1 order by Createddate desc LIMIT " + count + ";";
            }
            try
            {
                using (IDbConnection db = new MySqlConnection(_connectionString))
                {
                    model.Transactions = (List<TxnDbtransactionnotification>)await db.QueryAsync<TxnDbtransactionnotification>(query);
                    model.Unreadmessagecount = await db.ExecuteScalarAsync<int>(countquery);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return model;
        }

        public async Task<Getstudenttransactions> Getstudenttransactions(int Studentid, int count)
        {
            Getstudenttransactions Model = new Getstudenttransactions();
            string countquery = "Select count(*) from txn_dbtransactionnotification where Studentid = " + Studentid + " and Istransactionreadbystudent = 0 and Istransactionforstudent = 1;";
            string query;

            if (count == 0)
            {
                query = "Select * from txn_dbtransactionnotification where Studentid = " + Studentid + " and Istransactionreadbystudent = 0 and Istransactionforstudent = 1 order by Createddate desc;";
            }
            else
            {
                query = "SELECT * from txn_dbtransactionnotification where Studentid = " + Studentid + " and Istransactionreadbystudent = 0 and Istransactionforstudent = 1 order by Createddate desc LIMIT " + count + ";";
            }
            try
            {
                using (IDbConnection db = new MySqlConnection(_connectionString))
                {
                    Model.Transactions = (List<TxnDbtransactionnotification>)await db.QueryAsync<TxnDbtransactionnotification>(query);
                    Model.Unreadmessagecount = await db.ExecuteScalarAsync<int>(countquery);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return Model;
        }

        public async Task<ResponseModel> Readstudenttransaction(int id)
        {
            ResponseModel Model = new ResponseModel
            {
                Responsestatus = false
            };

            try
            {
                string query = "UPDATE  txn_dbtransactionnotification SET Istransactionreadbystudent = 1 where id=" + id + ";";
                using (IDbConnection db = new MySqlConnection(_connectionString))
                {
                    int result = await db.ExecuteAsync(query);
                    if (result > 0)
                    {
                        Model.Responsestatus = true;
                        Model.Responsemessage = "Read the transaction.";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return Model;
        }


        public async Task<ResponseModel> Readinstructortransaction(int id)
        {
            ResponseModel Model = new ResponseModel
            {
                Responsestatus = false
            };
            try
            {
                string query = "UPDATE  txn_dbtransactionnotification SET Istransactionreadbyinstrucotor = 1 where id=" + id + ";";
                using (IDbConnection db = new MySqlConnection(_connectionString))
                {
                    int result = await db.ExecuteAsync(query);
                    if (result > 0)
                    {
                        Model.Responsestatus = true;
                        Model.Responsemessage = "Read the transaction.";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return Model;
        }


        public async Task<ResponseModel> ReadallInstructortransactions(int Instructorid)
        {
            ResponseModel Model = new ResponseModel
            {
                Responsestatus = false
            };
            try
            {
                string query = "UPDATE  txn_dbtransactionnotification SET Istransactionreadbyinstrucotor = 1 where Instructorid=" + Instructorid + ";";
                using (IDbConnection db = new MySqlConnection(_connectionString))
                {
                    int result = await db.ExecuteAsync(query);
                    if (result > 0)
                    {
                        Model.Responsestatus = true;
                        Model.Responsemessage = "Read the transaction.";
                    }
                    else
                    {
                        Model.Responsestatus = false;
                        Model.Responsemessage = result.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return Model;
        }


        public async Task<ResponseModel> ReadallStudenttransactions(int Studentid)
        {
            ResponseModel Model = new ResponseModel
            {
                Responsestatus = false
            };
            try
            {
                string query = "UPDATE txn_dbtransactionnotification SET Istransactionreadbystudent = 1 where Studentid=" + Studentid + ";";
                using (IDbConnection db = new MySqlConnection(_connectionString))
                {
                    int result = await db.ExecuteAsync(query);
                    if (result > 0)
                    {
                        Model.Responsestatus = true;
                        Model.Responsemessage = "Read the transaction.";
                    }
                    else
                    {
                        Model.Responsestatus = false;
                        Model.Responsemessage = result.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return Model;
        }


    }
}
