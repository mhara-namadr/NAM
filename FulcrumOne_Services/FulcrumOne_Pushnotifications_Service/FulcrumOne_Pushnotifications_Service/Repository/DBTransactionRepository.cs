﻿using Dapper; 
using FulcrumOne_Pushnotifications_Service.Core.Repository; 
using System.Data;
using System.Data.SQLite; 

namespace FulcrumOne_Pushnotifications_Service.Repository
{
    public class DBTransactionRepository : IDBTransactionRepository
    {
        private readonly ConnectionString _connectionString;
        public DBTransactionRepository(ConnectionString connectionStrings)
        {
            _connectionString = connectionStrings;
        }

        public IDbConnection CreateConnection()
        {
            return new SQLiteConnection(_connectionString.Value);
        }


        /// <summary>
        /// Use this method to get count of deleted records
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="DatabaseUpdateDays"></param>
        /// <param name="DeleteColumnSort"></param>
        /// <returns></returns>
        #region Get Delete Record Count
        public int GetDeletedRecordCount(string TableName, int DatabaseUpdateDays, string DeleteColumnSort, string UpdateColumnSort)
        {
            int DeletedRecordCount = 0;

            string Query = "Select COUNT(1) from  " + TableName + "  " + " where " + DeleteColumnSort + " = 1 " + " and " + UpdateColumnSort + " > " + "(SELECT DATETIME('now', " + ("'-") + DatabaseUpdateDays + " day'))";

            try
            {
                using IDbConnection db = new SQLiteConnection(_connectionString.Value);
                DeletedRecordCount = db.ExecuteScalar<int>(Query);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return DeletedRecordCount;
        }
        #endregion

        /// <summary>
        /// Use this query to get no of Inserted records in the database
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="DatabaseUpdateDays"></param>
        /// <param name="InsertColumnSort"></param>
        /// <returns></returns>
        #region Get Inserted Records Counts 
        public int GetInsertedRecordCount(string TableName, int DatabaseUpdateDays, string InsertColumnSort)
        {
            int InsertedRecordCount = 0;

            string Query = "Select COUNT(1) from  " + TableName + "  " + " where " + InsertColumnSort + " > " + "(SELECT DATETIME('now', " + ("'-") + DatabaseUpdateDays + " day'))";
            try
            {
                using IDbConnection db = new SQLiteConnection(_connectionString.Value);
                InsertedRecordCount = db.ExecuteScalar<int>(Query);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return InsertedRecordCount;
        }
        #endregion

        /// <summary>
        /// Used this query to get no of updated records in Database
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="DatabaseUpdateDays"></param>
        /// <param name="UpdateColumnSort"></param>
        /// <returns></returns>
        #region Get Updated Records Count
        public int GetUpdatedRecordCount(string TableName, int DatabaseUpdateDays, string UpdateColumnSort)
        {
            int UpdatedRecordCount = 0;
            string Query = "Select COUNT(1) from  " + TableName + "  " + " where " + UpdateColumnSort + " > " + "(SELECT DATETIME('now', " + ("'-") + DatabaseUpdateDays + " day'))";
            try
            {
                using IDbConnection db = new SQLiteConnection(_connectionString.Value);
                UpdatedRecordCount = db.ExecuteScalar<int>(Query);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return UpdatedRecordCount;
        }
        #endregion

        /// <summary>
        /// Use this method to get inserted row details
        /// </summary>
        /// <param name="InsertColumnSort"></param>
        /// <param name="TableName"></param>
        /// <param name="DatabaseUpdateDays"></param>
        /// <returns></returns>
        #region Get Inserted Records details
        public List<dynamic> GetInsertTransactionDetails(string InsertColumnSort, string TableName, int DatabaseUpdateDays)
        {
            List<dynamic> list;
            string Query = "Select * from  " + TableName + "  " + " where " + InsertColumnSort + " > " + "(SELECT DATETIME('now', " + ("'-") + DatabaseUpdateDays + " day'))";

            try
            {
                using IDbConnection db = new SQLiteConnection(_connectionString.Value);
                list = db.Query<dynamic>(Query).ToList();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw; 
            }
            return list;
        }
        #endregion

        /// <summary>
        /// Use this method to get updated row details
        /// </summary>
        /// <param name="UpdateColumnSort"></param>
        /// <param name="TableName"></param>
        /// <param name="DatabaseUpdateDays"></param>
        /// <returns></returns>
        #region Get Details for Updated Rows
        public List<dynamic> GetUpdateTransactionDetails(string UpdateColumnSort, string TableName, int DatabaseUpdateDays)
        {
            List<dynamic> list;
            string Query = "Select * from  " + TableName + "  " + " where " + UpdateColumnSort + " > " + "(SELECT DATETIME('now', " + ("'-") + DatabaseUpdateDays + " day'))";

            try
            {
                using IDbConnection db = new SQLiteConnection(_connectionString.Value);
                list = db.Query<dynamic>(Query).ToList();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return list;
        }
        #endregion

        /// <summary>
        /// Use this method to get the deleted row details
        /// </summary>
        /// <param name="DeleteColumnSort"></param>
        /// <param name="TableName"></param>
        /// <param name="DatabaseUpdateDays"></param>
        /// <returns></returns>
        #region Get Details for Deleted Rows
        public List<dynamic> GetDeleteTransactionDetails(string DeleteColumnSort, string TableName, int DatabaseUpdateDays, string UpdateColumnSort)
        {
            List<dynamic> list;
            string Query = "Select * from  " + TableName + "  " + " where " + DeleteColumnSort + " = 1 " + " and " + UpdateColumnSort + " > " + "(SELECT DATETIME('now', " + ("'-") + DatabaseUpdateDays + " day'))";

            try
            {
                using IDbConnection db = new SQLiteConnection(_connectionString.Value);
                list = db.Query<dynamic>(Query).ToList();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return list;
        }
        #endregion
    }
}
