using F1_DatabaseUpdateNotification.Security;
using FulcrumOne_Pushnotifications_Service;
using FulcrumOne_Pushnotifications_Service.Core.IServices;
using FulcrumOne_Pushnotifications_Service.Core.Repository;
using FulcrumOne_Pushnotifications_Service.Models;
using FulcrumOne_Pushnotifications_Service.Repository;
using FulcrumOne_Pushnotifications_Service.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

#region Enable CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowOrigin",
        builder => builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod());
});
#endregion

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//new change
builder.Services.Configure<DBConfigurationDetails>(builder.Configuration.GetSection("DBConfigurationDetails"));


#region DB Connection
var connectionString = new ConnectionString(builder.Configuration.GetConnectionString("DefaultConnection"));
builder.Services.AddSingleton(connectionString);
builder.Services.Configure<DBConfigurationDetails>(builder.Configuration.GetSection("DatabaseTables"));

#endregion

#region Bearer Token Authentication
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."
    });
    options.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement {
        {
            new Microsoft.OpenApi.Models.OpenApiSecurityScheme {
                    Reference = new Microsoft.OpenApi.Models.OpenApiReference {
                        Type = Microsoft.OpenApi.Models.ReferenceType.SecurityScheme,
                            Id = "Bearer"
                    }
                },
                new string[] {}
        }
    });
});
var tokenOptions = builder.Configuration.GetSection("TokenOptions").Get<TokenOptions>();
builder.Services.Configure<TokenOptions>(builder.Configuration.GetSection("TokenOptions"));
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(jwtBearerOptions =>
{
    var symmetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenOptions.Key));
    jwtBearerOptions.IncludeErrorDetails = true;
    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = false,
        ValidIssuer = tokenOptions.Issuer,
        ValidAudience = tokenOptions.Audience,
        IssuerSigningKey = symmetricKey,
        ClockSkew = TimeSpan.Zero
    };
});
#endregion

#region Service Dependencies
builder.Services.AddScoped<IDatabaseTransactionNotificationService, DatabaseTransactionNotificationService>();
builder.Services.AddScoped<IDBTransactionRepository, DBTransactionRepository>();
builder.Services.AddScoped<IPushnotificationRepository, PushnotificationRepository>();
builder.Services.AddScoped<IPushnotificationService, PushnotificationService>();
#endregion


var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    
//}

app.UseSwagger();
app.UseSwaggerUI();


app.UseAuthorization();

app.MapControllers();

/// <summary>
/// Enable CORS
/// </summary>

app.UseCors("AllowOrigin");
app.Run();
