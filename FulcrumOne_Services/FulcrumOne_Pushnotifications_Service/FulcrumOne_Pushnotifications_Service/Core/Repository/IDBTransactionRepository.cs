﻿using System.Collections.Generic;

namespace FulcrumOne_Pushnotifications_Service.Core.Repository
{
    public interface IDBTransactionRepository
    {

        int GetInsertedRecordCount(string TableName, int DatabaseUpdateDays, string InsertColumnSort);

        int GetUpdatedRecordCount(string TableName, int DatabaseUpdateDays, string UpdateColumnSort);
        
        int GetDeletedRecordCount(string TableName, int DatabaseUpdateDays, string DeleteColumnSort, string UpdateColumnSort);
         
        List<dynamic> GetInsertTransactionDetails(string InsertColumnSort, string TableName, int DatabaseUpdateDays);
        List<dynamic> GetUpdateTransactionDetails(string UpdateColumnSort, string TableName, int DatabaseUpdateDays);
        List<dynamic> GetDeleteTransactionDetails(string DeleteColumnSort, string TableName, int DatabaseUpdateDays,string UpdateColumnSort);

    }
}
