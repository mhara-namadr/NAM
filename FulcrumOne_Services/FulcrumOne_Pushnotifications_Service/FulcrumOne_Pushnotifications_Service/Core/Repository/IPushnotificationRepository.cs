﻿using FulcrumOne_Pushnotifications_Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace FulcrumOne_Pushnotifications_Service.Core.Repository
{
    public interface IPushnotificationRepository
    {
        Task<Getinstructortransactions> Getinstructortransactions(int Instuctorid, int count);
        Task<Getstudenttransactions> Getstudenttransactions(int Studentid, int count);
        Task<ResponseModel> Readstudenttransaction(int id);
        Task<ResponseModel> Readinstructortransaction(int id);
        Task<ResponseModel> ReadallInstructortransactions(int Instructorid);
        Task<ResponseModel> ReadallStudenttransactions(int Studentid);
    }
}
