﻿ 
using FulcrumOne_Pushnotifications_Service.Models;
using System.Collections.Generic;

namespace FulcrumOne_Pushnotifications_Service.Core.IServices
{
    public interface IDatabaseTransactionNotificationService
    {
        List<ListDBTransactionCount> GetDatabaseTransactionNotification(DatabaseTransactionCommand Model);
        List<ListDBTransactionCount> GetInsertedRecordCount(DatabaseTransactionCommand Model);
        List<ListDBTransactionCount> GetUpdatedRecordCount(DatabaseTransactionCommand Model);
        List<ListDBTransactionCount> GetDeletedRecordCount(DatabaseTransactionCommand Model);
        List<dynamic> GetDatabaseTransactionDetails(DBTransactionDetails Model);
    }
}
