﻿
using FulcrumOne_Pushnotifications_Service.Models;
using System;
using System.Threading.Tasks;
namespace FulcrumOne_Pushnotifications_Service.Core.IServices
{
    public interface IPushnotificationService
    {
        Task<Getinstructortransactions> Getinstructortransactions(int Instuctorid, int count);
        Task<Getstudenttransactions> Getstudenttransactions(int Studentid, int Count);
        Task<ResponseModel> Readstudenttransaction(int id);
        Task<ResponseModel> Readinstructortransaction(int id);
        Task<ResponseModel> ReadallInstructortransactions(int Instructorid);
        Task<ResponseModel> ReadallStudenttransactions(int Studentid);
    }
}
