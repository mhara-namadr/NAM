﻿
using FulcrumOne_Pushnotifications_Service.Models;
using System;

namespace FulcrumOne_Pushnotifications_Service.Core.IServices
{
    public interface IDatabaseUpdateNotificationService
    {
        String GetDatabaseUpdateCount(DatabaseUpdateParameters Model);
    }
}
