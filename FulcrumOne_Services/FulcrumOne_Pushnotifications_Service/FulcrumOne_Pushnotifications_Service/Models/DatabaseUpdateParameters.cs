﻿namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class DatabaseUpdateParameters
    {
        public string TypeOfUpdate { get; set; }
    }
}
