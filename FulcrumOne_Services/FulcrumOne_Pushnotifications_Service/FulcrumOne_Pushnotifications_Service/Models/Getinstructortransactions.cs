﻿using System.Collections.Generic;
namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class Getinstructortransactions
    {
        public int Unreadmessagecount { get; set; }
        public List<TxnDbtransactionnotification> Transactions { get; set; }
    }
}
