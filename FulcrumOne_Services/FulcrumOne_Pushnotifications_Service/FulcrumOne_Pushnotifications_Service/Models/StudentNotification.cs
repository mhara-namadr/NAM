﻿using FulcrumOne_Pushnotifications_Service.Models.DBModels;
using System.Collections.Generic;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class StudentNotification
    {
        public List<Mstcourses> Courses { get; set; }
        public List<Txncoursequestions> Questions { get; set; }
        public List<Txncoursequestionanswers> Questionanswers { get; set; }
        public List<Txncourseqacomments> Comments { get; set; }
    }
}
