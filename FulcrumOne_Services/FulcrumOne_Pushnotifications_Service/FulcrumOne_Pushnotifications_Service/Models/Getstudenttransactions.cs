﻿using FulcrumOne_Pushnotifications_Service.Models;
using System.Collections.Generic;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class Getstudenttransactions
    {
        public int Unreadmessagecount { get; set; } 
        public  List<TxnDbtransactionnotification> Transactions { get; set;}
    }
}
