﻿using System;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class TxnDbtransactionnotification
    {
        public int Id { get; set; }
        public string Transactionname { get; set; }
        public int Instructorid { get; set; }
        public int Studentid { get; set; }
        public string Tablename { get; set; }
        public DateTime Createddate { get; set; }
        public DateTime Modifieddate { get; set; }
        public byte Isdeletedtransaction { get; set; }
        public byte Istransactionreadbystudent { get; set; }
        public byte Istransactionreadbyinstrucotor { get; set; }
        public byte Istransactionforinstructor { get; set; }
        public byte Istransactionforstudent { get; set; }
        public byte Istransactionforadmin { get; set; }


    }
}
