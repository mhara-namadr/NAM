﻿using System;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class ListDBTransactionCount
    {
        public String TableName { get; set; }

        public int Count { get; set; }
    }
}
