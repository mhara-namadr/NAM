﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Pushnotifications_Service.Models.DBModels
{
    /// <summary>
    /// Txncoursequestionanswers
    /// </summary>
    public class Txncoursequestionanswers
    {
        [Key]
        public int Answerid { get; set; }
        public int Courseid { get; set; }
        public string Coursename { get; set; }
        public int InstructorId { get; set; }
        public int Questionid { get; set; }
        public string Answer { get; set; }
        public DateTime Createddate { get; set; }
        public string Createdby { get; set; }
        public DateTime Modifieddate { get; set; }
        public string Modifiedby { get; set; }
        public int Isactive { get; set; }
        public int Isdeleted { get; set; }
        public string Qaattachment { get; set; }
    }
}
