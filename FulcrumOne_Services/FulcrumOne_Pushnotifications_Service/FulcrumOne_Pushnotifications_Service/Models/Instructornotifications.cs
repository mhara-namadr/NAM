﻿using System.Collections.Generic;

namespace FulcrumOne_Pushnotifications_Service.Models.DBModels
{
    public class Instructornotifications
    {
        public List<Studentcourses> Studentcourses { get; set; }
        public List<Txncoursequestions> Questions { get; set; }
        public List<Txncoursequestionanswers> Questionanswers { get; set; }
        public List<Txncourseqacomments> Comments { get; set; }

    }
}
