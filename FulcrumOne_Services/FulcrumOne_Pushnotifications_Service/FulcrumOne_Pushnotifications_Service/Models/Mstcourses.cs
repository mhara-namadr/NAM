﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Pushnotifications_Service.Models.DBModels
{
    /// <summary>
    /// Mstcourses
    /// </summary>
    public class Mstcourses
    {
        [Key]
        public int Courseid { get; set; }
        public string Coursename { get; set; }
        public int Instructorid { get; set; }
        public string Coursecreatedby { get; set; }
        public DateTime? Createddate { get; set; }
        public DateTime? Modifieddate { get; set; }
        public string Coursemodifiedby { get; set; }
        public string Coursecategory { get; set; }
        public int Courseduration { get; set; }
        public string Coursecontent { get; set; }
        public string Coursetitle { get; set; }
        public string Coursesummary { get; set; }
        public string Courseoverview { get; set; }
        public byte Isactive { get; set; }
        public byte Isdeleted { get; set; }
        public string Coursecoverimageurl { get; set; }
        public string Coursetags { get; set; }
        public string Courselevel { get; set; }
    }
}
