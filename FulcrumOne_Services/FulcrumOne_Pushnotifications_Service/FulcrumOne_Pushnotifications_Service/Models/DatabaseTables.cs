﻿using System;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class DatabaseTables
    { 
        public String[] DBTables { get; set; }
        public String InsertColumnSort { get; set; }
        public String UpdateColumnSort { get; set; }
        public int DatabaseUpdateDays { get; set; }
    }
}
