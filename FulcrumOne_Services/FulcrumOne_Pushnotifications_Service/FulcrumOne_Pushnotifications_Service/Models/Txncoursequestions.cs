﻿using System;

namespace FulcrumOne_Pushnotifications_Service.Models.DBModels
{
    /// <summary>
    /// Txncoursequestions
    /// </summary>
    public class Txncoursequestions
    {
        public int Questionid { get; set; }
        public int Courseid { get; set; }
        public int InstructorId { get; set; }
        public string Coursename { get; set; }
        public string Question { get; set; }
        public DateTime Createddate { get; set; }
        public string Createdby { get; set; }
        public DateTime Modifieddate { get; set; }
        public string Modifiedby { get; set; }
        public int Isactive { get; set; }
        public int Isdeleted { get; set; }
        public string Questionattachment { get; set; }
    }
}
