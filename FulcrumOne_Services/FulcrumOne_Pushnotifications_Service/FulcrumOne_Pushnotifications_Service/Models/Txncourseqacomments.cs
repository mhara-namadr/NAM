﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Pushnotifications_Service.Models.DBModels
{
    /// <summary>
    /// Txncourseqacomments
    /// </summary>
    public class Txncourseqacomments
    {
        [Key]
        public int Idtxncourseqacomments { get; set; }
        public string Comment { get; set; }
        public int Answerid { get; set; }
        public int Questionid { get; set; }
        public int InstructorId { get; set; }
        public string Createdby { get; set; }
        public DateTime Createddate { get; set; }
        public DateTime Modifieddate { get; set; }
        public string Modifiedby { get; set; }
        public int Isactive { get; set; }
        public int Isdeleted { get; set; }
        public string Commentattachment { get; set; }
    }
}
