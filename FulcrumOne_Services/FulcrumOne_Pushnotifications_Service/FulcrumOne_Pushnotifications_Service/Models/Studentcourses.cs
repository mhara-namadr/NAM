﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Pushnotifications_Service.Models.DBModels
{
    /// <summary>
    /// Studentcourses
    /// </summary>
    public class Studentcourses
    {
        [Key]
        public int Studentcoursesid { get; set; }
        public int Studentid { get; set; }
        public int Courseid { get; set; }
        public int Instructorid { get; set; }
        public int Sectionid { get; set; }
        public int Assignmentid { get; set; }
        public string Coursecategory { get; set; }
        public int Courseduration { get; set; }

        public DateTime? Createddate { get; set; }
        public DateTime? Modifieddate { get; set; }
        public byte Isactive { get; set; }
        public byte Isdeleted { get; set; }
        public int Sectionstatus { get; set; }
        public int Coursestatus { get; set; }
        public int Assignmentstatus { get; set; }


    }
}
