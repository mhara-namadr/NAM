﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class DBTransactionDetails
    {
        [Required(ErrorMessage ="Table Name is required")]
        public String TableName { get; set; }


        [Required(ErrorMessage = "This type of Command is required")]
        [DatabaseCommandValidator(ErrorMessage = "This is not a valid command type")]
        public String DatabaseTransactionCommand { get; set; }


    }

    public class DatabaseCommandValidatorAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string[] AcceptedCommands = { "insert", "Insert", "INSERT", "update", "Update", "UPDATE", "delete", "Delete", "DELETE" };

            if (AcceptedCommands.Contains(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Please Enter a valid database command.");
            }
        }
    }
}
