﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class DatabaseTransactionCommand
    {
        [Required(ErrorMessage = "The type of Command is required")]
        [CustomCommandValidator(ErrorMessage = "This is not a valid command type")]
        public string TypeOfCommand { get; set; }

    }

    public class CustomCommandValidatorAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string[] AcceptedCommands = { "insert", "Insert", "INSERT", "update", "Update", "UPDATE", "delete", "Delete", "DELETE" };

            if (AcceptedCommands.Contains(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Please Enter a valid database command.");
            }
        }
    }

}
