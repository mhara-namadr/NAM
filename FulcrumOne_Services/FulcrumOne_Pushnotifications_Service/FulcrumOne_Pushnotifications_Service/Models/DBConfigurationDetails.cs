﻿using System;

namespace FulcrumOne_Pushnotifications_Service.Models
{
    public class DBConfigurationDetails
    { 
        public String[] DBTables { get; set; }
        public String InsertColumnSort { get; set; }
        public String UpdateColumnSort { get; set; }
        public String DeleteColumnSort { get; set; }
        public int DatabaseUpdateDays { get; set; }
    }
}
