﻿using FulcrumOne_Pushnotifications_Service.Core.IServices;
using FulcrumOne_Pushnotifications_Service.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FulcrumOne_Pushnotifications_Service.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PushNotificationController : ControllerBase
    {
      
        private readonly IPushnotificationService _Servc;
        private readonly DBConfigurationDetails _DBTables;

        public PushNotificationController(IOptions<DBConfigurationDetails> DBTables, IPushnotificationService servc)
        {
            _DBTables = DBTables.Value;
            _Servc = servc;
        }

        [Route("/api/getinstructorpushnotifications/{instructorId}/{count}")]
        [HttpGet]
        public async Task<IActionResult> Getinstructorpushnotifications(int instructorId, int count)
        {
            try
            {
                var result = await _Servc.Getinstructortransactions(instructorId, count);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }


        [Route("/api/getstudentpushnotifications/{studentId}/{count}")]
        [HttpGet]
        public async Task<IActionResult> Getstudentpushnotifications(int studentId, int count)
        {
            try
            {
                var result = await _Servc.Getstudenttransactions(studentId, count);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        [Route("/api/readinstructornotification/{id}")]
        [HttpPut]
        public async Task<IActionResult> Readinstructortransaction(int id)
        {
            try
            {
                var result = await _Servc.Readinstructortransaction(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }


        [Route("/api/readstudentnotification/{id}")]
        [HttpPut]
        public async Task<IActionResult> Readstudenttransaction(int id)
        {
            try
            {
                var result = await _Servc.Readstudenttransaction(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }


        [Route("/api/readallinstructornotifications/{instructorid}")]
        [HttpPut]
        public async Task<IActionResult> Readallinstructornotifications(int instructorid)
        {
            try
            {
                var result = await _Servc.ReadallInstructortransactions(instructorid);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }


        [Route("/api/readallstudentnotifications/{studentid}")]
        [HttpPut]
        public async Task<IActionResult> Readallstudentnotifications(int studentid)
        {
            try
            {
                var result = await _Servc.ReadallStudenttransactions(studentid);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

    }
}
