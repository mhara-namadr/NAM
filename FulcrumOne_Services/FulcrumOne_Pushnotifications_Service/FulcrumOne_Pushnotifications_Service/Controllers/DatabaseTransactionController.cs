﻿using FulcrumOne_Pushnotifications_Service.Core.IServices;
using FulcrumOne_Pushnotifications_Service.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FulcrumOne_Pushnotifications_Service.Controllers
{
    /// <summary>
    /// DatabaseTransactionController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    ////[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DatabaseTransactionController : ControllerBase
    {
        private readonly IDatabaseTransactionNotificationService _DatabaseUpdateNotification;

        /// <summary>
        /// DatabaseTransactionController
        /// </summary>
        /// <param name="databaseupdateNotificationServ"></param>
        public DatabaseTransactionController(IDatabaseTransactionNotificationService databaseupdateNotificationServ)
        {
            _DatabaseUpdateNotification = databaseupdateNotificationServ;
        }

        /// <summary>
        /// GetCountOfTotalDatabaseUpdates
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [Route("/api/databasetransactioncount")]
        [HttpPost]
        public IActionResult GetCountOfTotalDatabaseUpdates([FromBody] DatabaseTransactionCommand Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Result = _DatabaseUpdateNotification.GetDatabaseTransactionNotification(Model);

            return Ok(Result);
        }

        /// <summary>
        /// databasetransactiondetails
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [Route("/api/databasetransactiondetails")]
        [HttpPost]
        public IActionResult GetDatabaseTransactionDetails([FromBody] DBTransactionDetails Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Result = _DatabaseUpdateNotification.GetDatabaseTransactionDetails(Model);
            return Ok(Result);
        }

    }
}
