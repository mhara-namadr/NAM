﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.InputModel
{
    /// <summary>
    /// 
    /// </summary>
    public class InputHtml
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        [StringRange]
        public string? filename { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        [StringRange]
        public string? htmlInput { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class StringRangeAttribute : ValidationAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value != null)
            {
                if (value.ToString() != "string" || value.ToString() == "String" || value.ToString() == "STRING")
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Please enter a correct value");
                }
            }
            else
            {
                return new ValidationResult("Model not valid");
            }
           
        }
    }

}
