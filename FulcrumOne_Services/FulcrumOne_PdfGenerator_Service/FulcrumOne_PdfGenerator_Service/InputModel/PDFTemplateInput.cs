﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.InputModel
{
    /// <summary>
    /// 
    /// </summary>
    public class PDFTemplateInput
    {      
        /// <summary>
        /// 
        /// </summary>
        public string? Key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string? Value { get; set; }
        
    }
}
