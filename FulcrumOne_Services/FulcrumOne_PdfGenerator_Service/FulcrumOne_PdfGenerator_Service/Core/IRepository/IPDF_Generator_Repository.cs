﻿using FulcrumOne_PdfGenerator_Service.InputModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.Core.IRepository
{
    /// <summary>
    /// IPDF_Generator_Repository
    /// </summary>
    public interface IPDF_Generator_Repository
    {
        /// <summary>
        /// Generates the PDF.
        /// </summary>
        void GeneratePDF(List<PDFTemplateInput> pDFInputTemplate);


        /// <summary>
        /// GeneratePDFByTemplatename
        /// </summary>
        /// <param name="pDFTemplateInput"></param>
        /// <param name="Pdffilename"></param>
        /// <param name="Templatefilename"></param>
        /// <returns></returns>
        string GenerateCustomPDF(List<PDFTemplateInput> pDFTemplateInput, string Pdffilename, string Templatefilename);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        void GeneratePdfFromTiff(IFormFile files);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        void GeneratePdfFromHtml(InputHtml files);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        void GenerateExcelFromPdf(IFormFile files);
    }
}
