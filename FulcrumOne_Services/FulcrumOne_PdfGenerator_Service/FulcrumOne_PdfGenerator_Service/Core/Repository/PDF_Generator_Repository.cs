﻿using DinkToPdf;
using DinkToPdf.Contracts;
using FulcrumOne_PdfGenerator_Service.Core.IRepository;
using FulcrumOne_PdfGenerator_Service.InputModel;
using FulcrumOne_PdfGenerator_Service.OutputModel;
using System.Text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Web;
using System.Collections.Generic;
using System.Dynamic;
using ColorMode = DinkToPdf.ColorMode;
using Path = System.IO.Path;
using System.Drawing;
using BitMiracle.Docotic.Pdf;

namespace FulcrumOne_PdfGenerator_Service.Core.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="FulcrumOne_PdfGenerator_Service.Core.IRepository.IPDF_Generator_Repository" />
    public class PDF_Generator_Repository : IPDF_Generator_Repository
    {
        private readonly string _pdfTempalte;
        private readonly string _pdfReader;
        private readonly string _Templatefilepath;
        private readonly string _Downloadfilepath;
        private readonly string _pdffilepath;
        private IConverter _converter;

        /// <summary>
        /// Initializes a new instance of the <see cref="PDF_Generator_Repository"/> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="converter"></param>
        public PDF_Generator_Repository(IConfiguration config, IConverter converter)
        {
            _pdfTempalte = config.GetValue<string>("PdfTemplateFile");
            _pdfReader = config.GetValue<string>("PdfReader");
            _pdffilepath = config.GetValue<string>("Pdffilepath");
            _Templatefilepath = config.GetValue<string>("Templatefilepath");
            _Downloadfilepath = config.GetValue<string>("Downloadfilepath");
            _converter = converter;
        }


        /// <summary>
        /// Generates the PDF.
        /// </summary>
        public void GeneratePDF(List<PDFTemplateInput> pDFInputTemplate)
        {
            #region DynamicJsonFOrPosition

            List<PDFTemplateOutput> LstPDFOutputTemplates = new List<PDFTemplateOutput>();

            var myJsonStringOutput = File.ReadAllText("OuputTemplate.json");

            string pDFTemplates = myJsonStringOutput.ToString();
            dynamic config = JsonConvert.DeserializeObject<ExpandoObject>(pDFTemplates, new ExpandoObjectConverter());
            foreach (var enabledEndpoint in ((IEnumerable<dynamic>)config.PDFTemplatesMainOutputModel))
            {
                PDFTemplateOutput pdfs = new PDFTemplateOutput();
                pdfs.Key = enabledEndpoint.Label;
                pdfs.Value = enabledEndpoint.Position;
                LstPDFOutputTemplates.Add(pdfs);
            }

            #endregion


            string newFile = _pdfTempalte;

            PdfReader pdfReader = new PdfReader(_pdfReader);
            using (PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create)))
            {
                AcroFields pdfFormFields = pdfStamper.AcroFields;


                for (int i = 0; i < LstPDFOutputTemplates.Count; i++)
                {
                    for (int j = 0; j < pDFInputTemplate.Count; j++)
                    {
                        if (LstPDFOutputTemplates[i].Key == pDFInputTemplate[j].Key)
                        {
                            pdfFormFields.SetField(LstPDFOutputTemplates[i].Value, pDFInputTemplate[j].Value.ToString());
                        }
                    }
                }

                pdfStamper.FormFlattening = true;
                pdfStamper.Close();
            }
        }


        /// <summary>
        /// GeneratePDFByTemplatename
        /// </summary>
        /// <param name="pDFTemplateInput"></param>
        /// <param name="Pdffilename"></param>
        /// <param name="Templatefilename"></param>
        public string GenerateCustomPDF(List<PDFTemplateInput> pDFTemplateInput, string Pdffilename, string Templatefilename)
        {
            try
            {
                #region DynamicJsonFOrPosition
                List<PDFTemplateOutput> LstPDFOutputTemplates = new List<PDFTemplateOutput>();
                if (!File.Exists(Templatefilename + ".json"))
                {
                    return "Template File does not exist.";
                }
                var myJsonStringOutput = File.ReadAllText(Templatefilename + ".json");
                string pDFTemplates = myJsonStringOutput.ToString();
                //// string Templatefile = "InputTemplate\\" + Pdffilename + ".pdf";

                string Templatefile = _Templatefilepath + Pdffilename + ".pdf";

                if ((pDFTemplates == "") || (Templatefile == ""))
                {
                    return "Template JSON file cannot be blank";
                }
                if (!File.Exists(Templatefile))
                {
                    return "The Sample PDF file not found.";
                }
                dynamic config = JsonConvert.DeserializeObject<ExpandoObject>(pDFTemplates, new ExpandoObjectConverter());
                foreach (var enabledEndpoint in ((IEnumerable<dynamic>)config.PDFTemplatesMainOutputModel))
                {
                    PDFTemplateOutput pdfs = new PDFTemplateOutput();
                    pdfs.Key = enabledEndpoint.Label;
                    pdfs.Value = enabledEndpoint.Position;
                    LstPDFOutputTemplates.Add(pdfs);
                }
                #endregion

                //// string newFile = "DownloadedFiles\\IncomeProPlanOnlineProposalFormCustomer.pdf";  //// _pdfTempalte;

                ////string newFile = "DownloadedFiles\\" + Pdffilename + ".pdf";
                string newFile = _Downloadfilepath + Pdffilename + ".pdf";

                PdfReader pdfReader = new PdfReader(Templatefile);  //// new PdfReader(_pdfReader);
                using (PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create)))
                {
                    AcroFields pdfFormFields = pdfStamper.AcroFields;
                    for (int i = 0; i < LstPDFOutputTemplates.Count; i++)
                    {
                        for (int j = 0; j < pDFTemplateInput.Count; j++)
                        {
                            if (LstPDFOutputTemplates[i].Key == pDFTemplateInput[j].Key)
                            {
                                pdfFormFields.SetField(LstPDFOutputTemplates[i].Value, pDFTemplateInput[j].Value.ToString());
                            }
                        }
                    }
                    pdfStamper.Writer.CompressionLevel = 5;
                    //pdfStamper.SetFullCompression();
                    pdfStamper.Writer.SetFullCompression();
                    pdfStamper.FormFlattening = true;
                    pdfStamper.Close();
                    return "Success";
                }
            }
            catch (System.Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void GeneratePdfFromTiff(IFormFile files)
        {
            string fileuploadpath = _pdffilepath + files.FileName.Split('.')[0] + ".pdf";
            FileInfo file = new FileInfo(fileuploadpath);

            String folderPath = Path.Combine(_pdffilepath);
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using var fileContentStream = new MemoryStream();
            files.CopyToAsync(fileContentStream);
            System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, files.FileName), fileContentStream.ToArray());
            fileContentStream.Position = 0;
            iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 0, 0, 0, 0);

            ////string outputPath = _pdffilepath + "SaveAsTiff.tiff";
            //////string outputPath = 
            ////using (BitMiracle.Docotic.Pdf.PdfDocument pdf = new BitMiracle.Docotic.Pdf.PdfDocument(fileuploadpath))
            ////{
            ////    PdfDrawOptions options = PdfDrawOptions.Create();
            ////    options.BackgroundColor = new PdfRgbColor(255, 255, 255);
            ////    //options.

            ////    for (int i = 0; i < pdf.PageCount; ++i)
            ////        pdf.Pages[i].Save($"page_{i}.png", options);

            ////    pdf.SaveAsTiff(outputPath, options);
            ////}


            #region Shantanu

            ////PDFConvert converter = null;
            ////try
            ////{
            ////    bool converted = false;

            ////    converter = new PDFConvert();
            ////    converter.OutputToMultipleFile = true;
            ////    converter.FirstPageToConvert = -1;
            ////    converter.LastPageToConvert = -1;
            ////    converter.FitPage = false;
            ////    converter.JPEGQuality = 50;
            ////    converter.OutputFormat = "tifflzw";

            ////    PdfReader reader = new PdfReader();
            ////    int noofpages = reader.NumberOfPages;

            ////    converted = converter.Convert(@"c:/Shantanu.pdf", "c:/Export.tiff");


            ////}
            ////catch (Exception ex)
            ////{
            ////    string error = ex.Message;
            ////}
            ////finally
            ////{
            ////    converter = null;
            ////}

            #endregion

            // creation of the different writers
            iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, new System.IO.FileStream(fileuploadpath, System.IO.FileMode.Create));

            // load the tiff image and count the total pages
            ////System.Drawing.Bitmap bm = new System.Drawing.Bitmap(fileContentStream,true);
             Bitmap bm = new Bitmap("C:/QH002176-IdvConsent-20211101141713042.pdf",true);


            int total = bm.GetFrameCount(System.Drawing.Imaging.FrameDimension.Page);

            document.Open();
            iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;
            for (int k = 0; k < total; ++k)
            {
                bm.SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Page, k);

                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bm, System.Drawing.Imaging.ImageFormat.Tiff);
                img.SetAbsolutePosition(0, 0);
                img.ScaleAbsoluteHeight(document.PageSize.Height);
                img.ScaleAbsoluteWidth(document.PageSize.Width);
                cb.AddImage(img);

                document.NewPage();
            }
            document.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        public void GeneratePdfFromHtml(InputHtml files)
        {
            #region HTMLtoPDF

            // change in appsettings.json
            string fileName = files.filename + ".pdf";
            string htmlasString = files.htmlInput.ToString();           

            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                Out = _pdffilepath + fileName,
                UseCompression = true,
                ImageDPI = 300
            };
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = htmlasString,
                WebSettings = { DefaultEncoding = "utf-8" }
                ////HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                ////FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" }
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            _converter.Convert(pdf);

            #endregion
        }

        private void ExportPDFToExcel(IFormFile files)
        {
            ////HttpResponse Response = new HttpResponse();
            StringBuilder text = new StringBuilder();

            string fileuploadpath = _pdffilepath + files.FileName.Split('.')[0] + ".pdf";
            FileInfo file = new FileInfo(fileuploadpath);

            String folderPath = Path.Combine(_pdffilepath);
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using var fileContentStream = new MemoryStream();
            files.CopyToAsync(fileContentStream);
            System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, files.FileName), fileContentStream.ToArray());

            PdfReader pdfReader = new PdfReader(fileuploadpath);
            for (int page = 1; page <= pdfReader.NumberOfPages; page++)
            {
                ITextExtractionStrategy strategy = new LocationTextExtractionStrategy();
                string currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);
                currentText = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.UTF8.GetBytes(currentText)));
                text.Append(currentText);
                pdfReader.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        public void GenerateExcelFromPdf(IFormFile files)
        {
            ExportPDFToExcel(files);
        }
    }
}
