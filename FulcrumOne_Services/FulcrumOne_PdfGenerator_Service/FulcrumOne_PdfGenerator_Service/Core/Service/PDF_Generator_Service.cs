﻿using FulcrumOne_PdfGenerator_Service.Core.IRepository;
using FulcrumOne_PdfGenerator_Service.Core.IService;
using FulcrumOne_PdfGenerator_Service.InputModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace FulcrumOne_PdfGenerator_Service.Core.Service
{
    /// <summary>
    /// PDF_Generator_Service
    /// </summary>
    public class PDF_Generator_Service : IPDF_Generator_Service
    {
        private readonly IPDF_Generator_Repository _PdfGeneratorRepository;


        /// <summary>
        ///  PDF_Generator_Service Constructor
        /// </summary>
        /// <param name="pdfRepository"></param>
        public PDF_Generator_Service(IPDF_Generator_Repository pdfRepository)
        {
            _PdfGeneratorRepository = pdfRepository;
        }

        /// <summary>
        /// GeneratePDF
        /// </summary>
        /// <param name="pDFInputTemplate"></param>
        public void GeneratePDF(List<PDFTemplateInput> pDFInputTemplate)
        {
            _PdfGeneratorRepository.GeneratePDF(pDFInputTemplate);
        }

        /// <summary>
        /// GeneratePDFByTemplatename
        /// </summary>
        /// <param name="pDFTemplateInput"></param>
        /// <param name="Templatefilename"></param>
        /// <param name="Pdffilename"></param>
        /// <returns></returns>
        public string GenerateCustomPDF(List<PDFTemplateInput> pDFTemplateInput, string Pdffilename, string Templatefilename)
        {
            try
            {
                var result = _PdfGeneratorRepository.GenerateCustomPDF(pDFTemplateInput, Pdffilename, Templatefilename);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        public void GeneratePdfFromTiff(IFormFile files)
        {
            _PdfGeneratorRepository.GeneratePdfFromTiff(files);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        public void GeneratePdfFromHtml(InputHtml files)
        {
            _PdfGeneratorRepository.GeneratePdfFromHtml(files);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        public void GenerateExcelFromPdf(IFormFile files)
        {
            _PdfGeneratorRepository.GenerateExcelFromPdf(files);
        }
    }
}
