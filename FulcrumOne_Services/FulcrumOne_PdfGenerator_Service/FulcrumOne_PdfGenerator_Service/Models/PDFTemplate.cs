﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.Models
{
    public class PDFTemplate
    {
        public PDFTemplatesMainModel PDFTemplatesMainModel { get; set; }
     
    }

    public class PDFTemplatesMainModel
    {
        public ProposalNumber ProposalNumber { get; set; }
        public LifeAgent LifeAgent { get; set; }
        public Name Name { get; set; }
        public SourceCode SourceCode { get; set; }
        public LeadGenerationCode LeadGenerationCode { get; set; }
        public TypeOfBuisnessRural TypeOfBuisness_Rural { get; set; }
        public TypeOfBuisnessUrban TypeOfBuisness_Urban { get; set; }
        public SalesManagerCode SalesManagerCode { get; set; }
        public SalesManagerName SalesManagerName { get; set; }
        public CustomerId CustomerId { get; set; }
        public PolicyNumber PolicyNumber { get; set; }
        public InsuredTitleMr InsuredTitle_Mr { get; set; }
        public InsuredTitleMrs InsuredTitle_Mrs { get; set; }
        public InsuredTitleMs InsuredTitle_Ms { get; set; }
        public InsuredFirstName InsuredFirstName { get; set; }
        public InsuredMiddleName InsuredMiddleName { get; set; }
        public InsuredLastName InsuredLastName { get; set; }
        public ProposerTitleMr ProposerTitle_Mr { get; set; }
        public ProposerTitleMrs ProposerTitle_Mrs { get; set; }
        public ProposerTitleMs ProposerTitle_Ms { get; set; }
        public ProposerTitleMss ProposerTitle_Mss { get; set; }
        public ProposerFirstName ProposerFirstName { get; set; }
        public ProposerMiddleName ProposerMiddleName { get; set; }
        public ProposerLastName ProposerLastName { get; set; }
        public InsuredDateOfBirth InsuredDateOfBirth { get; set; }
        public InsuredPlaceOfBirth InsuredPlaceOfBirth { get; set; }
        public ProposerDateOfBirth ProposerDateOfBirth { get; set; }
        public ProposerPlaceOfBirth ProposerPlaceOfBirth { get; set; }
        public InsuredFatherName InsuredFatherName { get; set; }
        public ProposerFatherName ProposerFatherName { get; set; }
        public InsuredAddress InsuredAddress { get; set; }
        public ProposerAddress ProposerAddress { get; set; }
    }

    public class ProposalNumber
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class LifeAgent
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class Name
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class SourceCode
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class LeadGenerationCode
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class TypeOfBuisnessRural
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class TypeOfBuisnessUrban
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class SalesManagerCode
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class SalesManagerName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class CustomerId
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class PolicyNumber
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredTitleMr
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredTitleMrs
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredTitleMs
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredFirstName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredMiddleName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredLastName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerTitleMr
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerTitleMrs
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerTitleMs
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerTitleMss
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerFirstName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerMiddleName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerLastName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredDateOfBirth
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredPlaceOfBirth
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerDateOfBirth
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerPlaceOfBirth
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredFatherName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerFatherName
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class InsuredAddress
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

    public class ProposerAddress
    {
        public string Label { get; set; }
        public string Position { get; set; }
    }

  


}
