﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.Models
{
    public class PDFTemplatesMainModel
    {
        public PDFTemplate ProposalNumber { get; set; }
        public PDFTemplate LifeAgent { get; set; }
        public PDFTemplate Name { get; set; }
        public PDFTemplate SourceCode { get; set; }
        public PDFTemplate LeadGenerationCode { get; set; }
        public PDFTemplate TypeOfBuisness_Rural { get; set; }
        public PDFTemplate TypeOfBuisness_Urban { get; set; }
        public PDFTemplate SalesManagerCode { get; set; }
        public PDFTemplate SalesManagerName { get; set; }
        public PDFTemplate CustomerId { get; set; }
        public PDFTemplate PolicyNumber { get; set; }
        public PDFTemplate InsuredTitle_Mr { get; set; }
        public PDFTemplate InsuredTitle_Mrs { get; set; }
        public PDFTemplate InsuredTitle_Ms { get; set; }
        public PDFTemplate InsuredFirstName { get; set; }
        public PDFTemplate InsuredMiddleName { get; set; }
        public PDFTemplate InsuredLtName { get; set; }
        public PDFTemplate ProposerTitle_Mr { get; set; }
        public PDFTemplate ProposerTitle_Mrs { get; set; }
        public PDFTemplate ProposerTitle_Ms { get; set; }
        public PDFTemplate ProposerTitle_Mss { get; set; }
        public PDFTemplate ProposerFirstName { get; set; }
        public PDFTemplate ProposerMiddleName { get; set; }
        public PDFTemplate ProposerLtName { get; set; }
        public PDFTemplate InsuredDateOfBirth { get; set; }
        public PDFTemplate InsuredPlaceOfBirth { get; set; }
        public PDFTemplate ProposerDateOfBirth { get; set; }
        public PDFTemplate ProposerPlaceOfBirth { get; set; }
        public PDFTemplate InsuredFatherName { get; set; }
        public PDFTemplate ProposerFatherName { get; set; }
        public PDFTemplate InsuredAddress { get; set; }
        public PDFTemplate ProposerAddress { get; set; }

    }
}
