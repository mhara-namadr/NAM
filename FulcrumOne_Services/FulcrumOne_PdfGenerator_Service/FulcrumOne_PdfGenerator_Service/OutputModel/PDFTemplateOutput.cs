﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.OutputModel
{
    public class PDFTemplateOutput
    {
        public string Key { get; set; }
        public string Value { get; set; }

    }
}
