﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.OutputModel
{

    /// <summary>
    /// BaseSixtyFourModel
    /// </summary>
    public class BaseSixtyFourModel
    {
        /// <summary>
        /// Filename
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Base64string
        /// </summary>
        [Required]
        [StringRange]
        public string Base64string { get; set; }


    }

    /// <summary>
    /// StringRangeAttribute
    /// </summary>
    public class StringRangeAttribute : ValidationAttribute
    {
        /// <summary>
        /// IsValid
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.ToString() == "string" || value.ToString() == "String" || value.ToString() == "STRING")
            {
                return new ValidationResult("Please enter a correct value");
            }
            return ValidationResult.Success;
        }
    }

}
