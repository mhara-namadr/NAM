﻿using CoreHtmlToImage;
using CsvHelper;
using DinkToPdf;
using DinkToPdf.Contracts;
using FulcrumOne_PdfGenerator_Service.Core.IService;
using FulcrumOne_PdfGenerator_Service.Extensions;
using FulcrumOne_PdfGenerator_Service.InputModel;
using FulcrumOne_PdfGenerator_Service.OutputModel;
using iTextSharp.text;
using PDFtoImage;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Text;
using ILogger = NLog.ILogger;

namespace FulcrumOne_PdfGenerator_Service.Controllers
{
    /// <summary>
    /// PdfGeneratorController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PdfGeneratorController : ControllerBase
    {
        private IConverter _converter;
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IPDF_Generator_Service _PdfService;
        private readonly string _pdfTempalte;
        private readonly string _pdffilepath;
        private readonly string _Downloadfilepath;

        /// <summary>
        /// PdfGeneratorController
        /// </summary>
        /// <param name="pdfService"></param>
        /// <param name="converter"></param>
        /// <param name="config"></param>
        public PdfGeneratorController(IPDF_Generator_Service pdfService, IConverter converter, IConfiguration config)
        {
            _pdfTempalte = config.GetValue<string>("PdfTemplateFile");
            _pdffilepath = config.GetValue<string>("Pdffilepath");
            _Downloadfilepath = config.GetValue<string>("Downloadfilepath");
            _PdfService = pdfService;
            _converter = converter;
        }


        /// <summary>
        /// GeneratorPdf
        /// </summary>
        /// <param name="pDFTemplate"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/generatorpdf")]
        public IActionResult generatorpdf(object pDFTemplate)
        {
            try
            {
                List<PDFTemplateInput> LstPDFTemplates = new List<PDFTemplateInput>();
                string pDFTemplates = pDFTemplate.ToString();
                dynamic config = JsonConvert.DeserializeObject<ExpandoObject>(pDFTemplates, new ExpandoObjectConverter());
                foreach (var enabledEndpoint in ((IEnumerable<dynamic>)config.PDFTemplate))
                {
                    PDFTemplateInput pdfs = new PDFTemplateInput();
                    pdfs.Key = enabledEndpoint.Label;
                    pdfs.Value = enabledEndpoint.Value;
                    LstPDFTemplates.Add(pdfs);
                }

                _PdfService.GeneratePDF(LstPDFTemplates);

                var memory = new MemoryStream();

                using (var stream = new FileStream(_pdfTempalte, FileMode.Open))
                {
                    stream.CopyTo(memory);
                }
                memory.Position = 0;

                string file_name = _pdfTempalte;
                FileInfo file = new FileInfo(file_name);

                if (file.Exists)//check file exsit or not  
                {
                    file.Delete();
                }
                return File(memory, "application/pdf", Path.GetFileName(_pdfTempalte));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// GeneratorCustomPdf for Generating PDF based on file name and Template JSON file name
        /// </summary>
        /// <param name="pDFTemplate"></param>
        /// <param name="Pdffilename"></param>
        /// <param name="templatename"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/generatorcustompdf")]
        public IActionResult generatorcustompdf([Required] object pDFTemplate, [Required] string Pdffilename, [Required] string templatename)
        {
            try
            {
                List<PDFTemplateInput> LstPDFTemplates = new List<PDFTemplateInput>();
                string pDFTemplates = pDFTemplate.ToString();
                dynamic config = JsonConvert.DeserializeObject<ExpandoObject>(pDFTemplates, new ExpandoObjectConverter());
                foreach (var enabledEndpoint in ((IEnumerable<dynamic>)config.PDFTemplate))
                {
                    PDFTemplateInput pdfs = new PDFTemplateInput();
                    pdfs.Key = enabledEndpoint.Label;
                    pdfs.Value = enabledEndpoint.Value;
                    LstPDFTemplates.Add(pdfs);
                }

                var Result = _PdfService.GenerateCustomPDF(LstPDFTemplates, Pdffilename, templatename);

                if (Result == "Success")
                {
                    var memory = new MemoryStream();
                    string Newfile = _Downloadfilepath + Pdffilename + ".pdf";
                    using (var stream = new FileStream(Newfile, FileMode.Open))
                    {
                        stream.CopyTo(memory);
                    }
                    memory.Position = 0;
                    string file_name = Pdffilename + ".pdf";
                    FileInfo file = new FileInfo(file_name);
                    if (file.Exists)//check file exsit or not  
                    {
                        file.Delete();
                    }
                    return File(memory, "application/pdf", Path.GetFileName(Newfile));
                }
                else
                {
                    return BadRequest(Result);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }


        /// <summary>
        /// GeneratePdfwithBase64string
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/generatepdfwithbase64string")]
        public IActionResult generatepdfwithbase64string([FromBody] BaseSixtyFourModel Model)
        {
            try
            {
                if (!Base64Validstring.IsBase64(Model.Base64string))
                {
                    return BadRequest("Invalid base64string");
                }
                #region Generate and Save attachment
                if (String.IsNullOrEmpty(Model.Filename))
                {
                    Model.Filename = "File_" + Guid.NewGuid();
                }
                byte[] bytes = Convert.FromBase64String(Model.Base64string);
                string fileuploadpath = _pdffilepath + Model.Filename + ".pdf";
                FileInfo file = new FileInfo(fileuploadpath);
                if (file.Exists)//check file exsit or not  
                {
                    file.Delete();
                }
                FileStream stream = new FileStream(fileuploadpath, FileMode.CreateNew);
                BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();
                #endregion

                var memory = new MemoryStream();
                using (var streams = new FileStream(fileuploadpath, FileMode.Open))
                {
                    streams.CopyTo(memory);
                }
                memory.Position = 0;

                string file_name = Model.Filename;
                FileInfo files = new FileInfo(file_name);
                if (file.Exists)//check file exsit or not  
                {
                    file.Delete();
                }
                return File(memory, "application/pdf", Path.GetFileName(Model.Filename));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/uploadtifffile")]
        public IActionResult uploadtifffile([Required] IFormFile files)
        {
            try
            {                
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                #region MyRegion

                //PDFDocument pdf = new PDFDocument();

                //// Create a file stream with PDF message.
                //FileStream stream = new FileStream("sample.pdf", FileMode.Open);

                //// Load PDF from file stream.
                //pdf.LoadPDF(stream);

                //// Set output tif image width.
                //int width = pdf.GetPageWidth(0) / 2;

                //// Set output tif image height.
                //int height = pdf.GetPageHeight(0) / 2;

                //// Convert the first PDF page to image with the desired size.
                //Bitmap tif = pdf.ToImage(0, width, height);

                //// Save image to tif format.
                //tif.Save("result.tiff", ImageFormat.Tiff);

                #endregion

                //_PdfService.GeneratePdfFromTiff(files);

                string fileuploadpath = _pdffilepath + files.FileName.Split('.')[0] + ".pdf";
                FileInfo file = new FileInfo(fileuploadpath);

                #region MyRegion
                //String folderPath = Path.Combine(_pdffilepath);
                //if (!Directory.Exists(folderPath))
                //{
                //    Directory.CreateDirectory(folderPath);
                //}
                //using var fileContentStream = new MemoryStream();
                //files.CopyToAsync(fileContentStream);
                //System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, files.FileName), fileContentStream.ToArray());

                //iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 0, 0, 0, 0);

                //// creation of the different writers
                //iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, new System.IO.FileStream(fileuploadpath, System.IO.FileMode.Create));

                //// load the tiff image and count the total pages
                //System.Drawing.Bitmap bm = new System.Drawing.Bitmap(fileContentStream);
                //int total = bm.GetFrameCount(System.Drawing.Imaging.FrameDimension.Page);

                //document.Open();
                //iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;
                //for (int k = 0; k < total; ++k)
                //{
                //    bm.SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Page, k);

                //    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bm, System.Drawing.Imaging.ImageFormat.Tiff);
                //    img.SetAbsolutePosition(0, 0);
                //    img.ScaleAbsoluteHeight(document.PageSize.Height);
                //    img.ScaleAbsoluteWidth(document.PageSize.Width);
                //    cb.AddImage(img);

                //    document.NewPage();
                //}
                //document.Close();
                #endregion

                var memory = new MemoryStream();
                using (var streams = new FileStream(fileuploadpath, FileMode.Open))
                {
                    streams.CopyTo(memory);
                }
                memory.Position = 0;

                FileInfo file1 = new FileInfo(_pdffilepath + files.FileName);
                if(file1.Exists)
                {
                    file1.Delete();
                }
                if (file.Exists)//check file exsit or not  
                {
                    file.Delete();
                }
                return File(memory, "application/pdf", Path.GetFileName(fileuploadpath));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }


        }


       /// <summary>
       /// Generates pdf file when a string is input.
       /// </summary>
       /// <param name="files"></param>
       /// <returns></returns>
        [HttpPost]
        [Route("/api/convertstringtohtml")]       
        public IActionResult stringToPdf([FromBody] InputHtml files)
        {            
            try
            {
                logger.Info("In html method");
                string filename = files.filename + ".pdf";                
                _PdfService.GeneratePdfFromHtml(files);

                string fileuploadpath = _pdffilepath + filename;
                var memory = new MemoryStream();
                using (var streams = new FileStream(fileuploadpath, FileMode.Open))
                {
                    streams.CopyTo(memory);
                }
                memory.Position = 0;

                FileInfo file1 = new FileInfo(_pdffilepath + filename);
                if (file1.Exists)
                {
                    file1.Delete();
                }
                return File(memory, "application/pdf", Path.GetFileName(fileuploadpath));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            
        }

        #region JsonToCsv
        ////[HttpPost]
        ////[Route("/api/jsontoCsv")]
        ////public IActionResult jsontoCsv([Required] IFormFile files)
        ////{

        ////    string filename = files.FileName.Split('.')[0] + ".json";
        ////    string htm = GetHTMLString(files);

        ////    string tab = jsonToCSV(htm, ",");
        ////    string asd = tab.Replace('\"', '"');

        ////    string tesin = asd;
        ////    return Ok("success");

        ////}
        ////private string jsonToCSV(string jsonContent, string delimiter)
        ////{
        ////    StringWriter csvString = new StringWriter();

        ////    using (var csv = new CsvWriter(csvString, System.Globalization.CultureInfo.InvariantCulture))
        ////    {
        ////        //csv.Configuration. = delimiter;// .SkipEmptyRecords = true;
        ////        //csv.Configuration.WillThrowOnMissingField = false;
        ////        //csv.Configuration.Delimiter = delimiter;

        ////        using (var dt = jsonStringToTable(jsonContent))
        ////        {
        ////            foreach (DataColumn column in dt.Columns)
        ////            {
        ////                csv.WriteField(column.ColumnName);
        ////            }
        ////            csv.NextRecord();

        ////            foreach (DataRow row in dt.Rows)
        ////            {
        ////                for (var i = 0; i < dt.Columns.Count; i++)
        ////                {
        ////                    csv.WriteField(row[i]);
        ////                }
        ////                csv.NextRecord();
        ////            }
        ////        }
        ////    }
        ////    return csvString.ToString();
        ////}
        ////private DataTable jsonStringToTable(string jsonContent)
        ////{
        ////    DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonContent);
        ////    return dt;
        ////}
        ////private string GetHTMLString(IFormFile files)
        ////{
        ////    string fileuploadpath = _pdffilepath + files.FileName.Split('.')[0] + ".html";

        ////    FileInfo file = new FileInfo(fileuploadpath);

        ////    String folderPath = Path.Combine(_pdffilepath);
        ////    if (!Directory.Exists(folderPath))
        ////    {
        ////        Directory.CreateDirectory(folderPath);
        ////    }
        ////    using var fileContentStream = new MemoryStream();
        ////    files.CopyToAsync(fileContentStream);
        ////    System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, files.FileName), fileContentStream.ToArray());

        ////    string htmlString = string.Empty;

        ////    using (FileStream fs = System.IO.File.Open(fileuploadpath, FileMode.Open, FileAccess.ReadWrite))
        ////    {
        ////        using (StreamReader srs = new StreamReader(fs))
        ////        {
        ////            htmlString = srs.ReadToEnd();
        ////        }
        ////    }

        ////    return htmlString;
        ////}
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/converttoexcel")]
        public IActionResult converttoexcel([Required] IFormFile files)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);


                _PdfService.GenerateExcelFromPdf(files);

                string fileuploadpath = _pdffilepath + files.FileName.Split('.')[0] + ".pdf";
                FileInfo file = new FileInfo(fileuploadpath);
                
                var memory = new MemoryStream();
                using (var streams = new FileStream(fileuploadpath, FileMode.Open))
                {
                    streams.CopyTo(memory);
                }
                memory.Position = 0;

                FileInfo file1 = new FileInfo(_pdffilepath + files.FileName);
                if (file1.Exists)
                {
                    file1.Delete();
                }
                if (file.Exists)//check file exsit or not  
                {
                    file.Delete();
                }
                return File(memory, "application/pdf", Path.GetFileName(fileuploadpath));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }


        }




    }
}
