﻿
using FulcrumOne_PdfGenerator_Service.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FulcrumOne_PdfGenerator_Service.Controllers
{

    /// <summary>
    /// FileconverterController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FileconverterController : ControllerBase
    {
        private readonly string _pdffilepath;
        private readonly string _Downloadfilepath;

        /// <summary>
        /// FileconverterController
        /// </summary>
        /// <param name="config"></param>
        public FileconverterController(IConfiguration config)
        {
            _pdffilepath = config.GetValue<string>("Pdffilepath");
            _Downloadfilepath = config.GetValue<string>("Downloadfilepath");
        }

        /// <summary>
        /// Jsontocsvconverter
        /// </summary>
        /// <param name="jsosn"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        [NonAction]
        [HttpGet]
        public IActionResult Jsontocsvconverter(string jsosn, string filename)
        {
            try
            {
                ////var json = @"{
                ////       ""employees"": [
                ////       { ""firstName"":""Yogesh"" , ""lastName"":""Doe"" },
                ////       { ""firstName"":""Aryan"" , ""lastName"":""Smith"" },
                ////       { ""firstName"":""Mangesh"" , ""lastName"":""Jones"" }
                ////       ]
                ////       }";
                var json = @"{
                       ""employees"": [
                       { ""firstName"":""Yogesh"" , ""lastName"":""Doe"", ""Address"":""Pune"" },
                       { ""firstName"":""Aryan"" , ""lastName"":""Smith"" , ""Address"":""Satara Road""},
                       { ""firstName"":""Mangesh"" , ""lastName"":""Jones"" , ""Address"":""PB Highway""}
                       ]
                       }";
                //used NewtonSoft json nuget package
                XmlNode xml = JsonConvert.DeserializeXmlNode("{records:{record:" + json + "}}");
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xml.InnerXml);
                XmlReader xmlReader = new XmlNodeReader(xml);
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(xmlReader);
                var dataTable = dataSet.Tables[1];
                ////dataTable.Columns.Remove("record_Id");

                //Datatable to CSV
                var lines = new List<string>();
                string[] columnNames = dataTable.Columns.Cast<DataColumn>().
                                                  Select(column => column.ColumnName).
                                                  ToArray();
                var header = string.Join(",", columnNames);
                lines.Add(header);
                var valueLines = dataTable.AsEnumerable()
                                   .Select(row => string.Join(",", row.ItemArray));
                lines.AddRange(valueLines);
                string file_name = filename + ".csv";
                FileInfo file = new FileInfo(file_name);
                if (file.Exists)//check file exsit or not  
                {
                    file.Delete();
                }
                ////System.IO.File.WriteAllLines(@"D:/Export.csv", lines);
                string filepath = _pdffilepath + file_name;
                System.IO.File.WriteAllLines(filepath, lines);
                //var dt = ConvertCSVtoDataTable(filepath);
                string sb = Get_CSV_Data(dataTable);
                return File(new System.Text.UTF8Encoding().GetBytes(sb), "text/csv", filepath);
                ////dt.Columns.Remove("record_Id"); 
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private static string Get_CSV_Data(DataTable dt)
        {
            try
            {
                var sb = new StringBuilder();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sb.Append(dt.Columns[i].ColumnName);
                    sb.Append(i == dt.Columns.Count - 1 ? "\n" : ",");
                }

                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        sb.Append(row[i].ToString());
                        sb.Append(i == dt.Columns.Count - 1 ? "\n" : ",");
                    }
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }


        /// <summary>
        /// Jsontocsv
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/jsontocsv")]
        public IActionResult Jsontocsv(string json)
        {
            DataTable dt = Datatable.JsonStringToDataTable(json);
            ////DataTable dt = Datatable.Tabulate(json); 

            var lines = new List<string>();
            string[] columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName).
                                              ToArray();
            var header = string.Join(",", columnNames);
            lines.Add(header);
            var valueLines = dt.AsEnumerable()
                               .Select(row => string.Join(",", row.ItemArray));
            lines.AddRange(valueLines);
            string file_name = "Samplecsv" + ".csv";
            FileInfo file = new FileInfo(file_name);
            if (file.Exists)//check file exsit or not  
            {
                file.Delete();
            }
            ////System.IO.File.WriteAllLines(@"D:/Export.csv", lines);
            string filepath = _pdffilepath + file_name;
            System.IO.File.WriteAllLines(filepath, lines);
            string sbi = Get_CSV_Data(dt);
            return File(new UTF8Encoding().GetBytes(sbi), "text/csv", filepath);
        }

        ////[NonAction]
        ////[HttpGet]
        ////[Route("/api/complexjsontocsv")]
        ////public IActionResult complexjsontocsv(string json)
        ////{
        ////    //string csv = jsonToCSV(json, ",");

        ////    //HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
        ////    //result.Content = new StringContent(csv);
        ////    //result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
        ////    //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "export.csv" };
        ////    //return Ok(result);
        ////} 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonContent"></param>
        /// <returns></returns>
        public static DataTable jsonStringToTable(string jsonContent)
        {
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonContent);
            return dt;
        }
    }
}
