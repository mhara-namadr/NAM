﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace F1_Notification.Extensions
{
    /// <summary>
    ///this class is used to add the custom swagger method
    /// </summary>
    public static class MiddlewareExtensions
    {
        /// <summary>
        /// Adds the custom swagger.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IServiceCollection AddCustomSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(cfg =>
            {
                cfg.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "FulcrumOne PDF Generation Service",
                    Version = "v3",
                    Description = "Following service is used to generate the BI/Products PDF's for Insurance Industry also includes following features such as String to PDF",
                    Contact = new OpenApiContact
                    {
                        Name = "Fulcrum",
                        Url = new Uri("https://fulcrumdigital.com/")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "FulcrumOne",
                    },
                });

                cfg.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "JSON Web Token to access resources. Example: Bearer {token}",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                cfg.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
                        },
                        new [] { string.Empty }
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                cfg.IncludeXmlComments(xmlPath);
            });

            return services;
        }

        /// <summary>
        /// Uses the custom swagger.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns></returns>
        public static IApplicationBuilder UseCustomSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger().UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "F1 PDF Generation Service");
                options.DocumentTitle = "FulcrumOne PDF Generation Service";
            });

            return app;
        }
    }
}