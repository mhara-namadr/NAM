﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_PdfGenerator_Service.Extensions
{
    /// <summary>
    /// Base64Validstring
    /// </summary>
    public static class Base64Validstring
    {
        /// <summary>
        /// IsBase64
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public static bool IsBase64(this string base64String)
        {
            // Credit: oybek https://stackoverflow.com/users/794764/oybek
            if (string.IsNullOrEmpty(base64String) || base64String.Length % 4 != 0
               || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r")
               || base64String.Contains("\n") || base64String.Contains("string") || base64String.Contains("String") || base64String.Contains("STRING"))
                return false;

            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                throw;
                // Handle the exception
            }
        }
    }
}
