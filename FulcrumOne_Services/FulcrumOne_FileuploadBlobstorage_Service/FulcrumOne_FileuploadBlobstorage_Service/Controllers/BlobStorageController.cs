﻿using FulcrumOne_FileuploadBlobstorage_Service.IServices;
using FulcrumOne_FileuploadBlobstorage_Service.Model;
using FulcrumOne_FileuploadBlobstorage_Service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Text.Json;

namespace FulcrumOne_FileuploadBlobstorage_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlobStorageController : ControllerBase
    {
        private readonly IOptions<MyConfig> config;

        private readonly IBlobStorageService _Service;
        /// <summary>Initializes a new instance of the <see cref="BlobStorageController" /> class.</summary>
        /// <param name="config">The configuration.</param>
        public BlobStorageController(IOptions<MyConfig> config, IBlobStorageService Service)
        {
            this.config = config;
            _Service = Service;
        }


        /// <summary>Lists the files.</summary>
        /// <returns>
        ///   <br />
        /// </returns>
        [Route("/api/listfiles")]
        [HttpGet]
        public async Task<IActionResult> Listfiles()
        {
            try
            {
                var result = await _Service.Listfiles();
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        /// <summary>Inserts the file.</summary>
        /// <param name="asset">The asset.</param>
        /// <returns>
        ///   <br />
        ///   
        /// </returns>
        [Route("/api/insertfile")]
        [HttpPost]
        public async Task<IActionResult> Insertfile(IFormFile asset)
        {
            try
            {
                var result = await _Service.Insertfile(asset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        /// <summary>Downloads the file.</summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [Route("/api/downloadfile/{fileName}")]
        [HttpGet]
        public async Task<IActionResult> Downloadfile(string fileName)
        {
            try
            {
                MemoryStream ms = new();
                if (CloudStorageAccount.TryParse(config.Value.StorageConnection, out CloudStorageAccount storageAccount))
                {
                    CloudBlobClient BlobClient = storageAccount.CreateCloudBlobClient();
                    CloudBlobContainer container = BlobClient.GetContainerReference(config.Value.Container);

                    if (await container.ExistsAsync())
                    {
                        CloudBlob file = container.GetBlobReference(fileName);

                        if (await file.ExistsAsync())
                        {
                            await file.DownloadToStreamAsync(ms);
                            Stream blobStream = file.OpenReadAsync().Result;
                            return File(blobStream, file.Properties.ContentType, file.Name);
                        }
                        else
                        {
                            return Content("File does not exist");
                        }
                    }
                    else
                    {
                        return Content("Container does not exist");
                    }
                }
                else
                {
                    return Content("Error opening storage");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        /// <summary>Deletes the file.</summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [Route("/deletefile/{fileName}")]
        [HttpGet]
        public async Task<IActionResult> Deletefile(string fileName)
        {
            try
            {
                var result = await _Service.Deletefile(fileName);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }


    }
}
