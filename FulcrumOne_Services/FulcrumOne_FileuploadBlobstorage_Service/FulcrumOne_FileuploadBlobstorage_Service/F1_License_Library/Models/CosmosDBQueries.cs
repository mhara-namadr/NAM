﻿using System;
using System.Collections.Generic;
using System.Text;

namespace F1_License_Library.Models
{
    public class CosmosDBQueries
    {
        public string Cosmos_GetMaxRegistrationid { get; set; }
        public string Reseed_RegistrationID { get; set; }
        public string Cosmos_GetAllServices { get; set; }
        public string Cosmons_GetServiceByRegistrationno { get; set; }
        public string Cosmons_GetServiceByHostname { get; set; }

    }
}
