﻿using System;
using System.Collections.Generic;
using System.Text;

namespace F1_License_Library.Models
{
    public class ServiceDetailsModel
    {
        public string LicenseKey { get; set; }
        public string Hostname { get; set; }
    }
}
