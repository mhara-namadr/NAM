﻿using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Database = Microsoft.Azure.Cosmos.Database;

namespace F1_License_Library.Models
{
    /// <summary>
    ///
    /// </summary>
    public class CosmosConnector
    {
        /// <summary>
        /// The database options
        /// </summary>
        private readonly DBOptions _dbOptions;

        /// <summary>
        /// The cosmos client
        /// </summary>
        private CosmosClient cosmosClient;

        public Database database { get; set; }

        public Container container { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CosmosConnector"/> class.
        /// </summary>
        /// <param name="dbOptions">The database options.</param>
        public CosmosConnector(IOptions<DBOptions> dbOptions)
        {
            _dbOptions = dbOptions.Value;
            CreateDatabaseAsync().Wait();
        }

        /*
        Create the database if it does not exist
        */

        /// <summary>
        /// Creates the database asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task<Database> CreateDatabaseAsync()
        {
            // Create a new database
            this.cosmosClient = new CosmosClient(_dbOptions.EndpointUri, _dbOptions.PrimaryKey);
            database = await this.cosmosClient.CreateDatabaseIfNotExistsAsync(_dbOptions.DatabaseId);
            container = await this.database.CreateContainerIfNotExistsAsync(_dbOptions.ContainerId, "/registrationid");
            return database;
        }

        /*
        Create the container if it does not exist.
        Specifiy "/LastName" as the partition key since we're storing family information, to ensure good distribution of requests and storage.
        */

        /// <summary>
        /// Creates the container asynchronous.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        //public async Task<Container> CreateContainerAsync(Database db)
        //{
        //    // Create a new container
        //    return await db.CreateContainerIfNotExistsAsync(_dbOptions.ContainerId, "/ProjectId");
        //}

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <returns></returns>
        //public int getStatus(string status)
        //{
        //    var query = new SqlQuerySpec
        //    {
        //        QueryText = "SELECT VALUE COUNT(1) FROM c as s WHERE s.Status = @status",
        //        Parameters = new SqlParameterCollection { new SqlParameter { Name = "@status", Value = status } }
        //    };
        //    try
        //    {
        //        int count = 0;
        //        DocumentClient client = new DocumentClient(new Uri(_dbOptions.EndpointUri), _dbOptions.PrimaryKey, new ConnectionPolicy { EnableEndpointDiscovery = false });
        //        List<int> list = client.CreateDocumentQuery<int>(UriFactory.CreateDocumentCollectionUri(_dbOptions.DatabaseId, _dbOptions.ContainerId).ToString(),
        //                      query, new FeedOptions { MaxItemCount = -1 }).ToList<int>();

        //        count = list[0];

        //        return count;
        //    }

        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        throw;
        //    }



        //}
    }

}
