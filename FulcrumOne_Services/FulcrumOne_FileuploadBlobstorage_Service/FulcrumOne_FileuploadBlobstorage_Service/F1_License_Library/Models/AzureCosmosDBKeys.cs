﻿using System;
using System.Collections.Generic;
using System.Text;

namespace F1_License_Library.Models
{
    public class AzureCosmosDBKeys
    {
        public String EndpointUri { get; set; }
        public String PrimaryKey { get; set; }
        public String DatabaseId { get; set; }
        public String ContainerId { get; set; }


    }
}
