﻿using F1_License_Library.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace F1_License_Library.IServices
{
    public interface ILicenseRegistrationService
    {
        Task<ServiceRegistrationModel> AddHitcount();
        Task<ServiceRegistrationModel> GetDetailsbyhostname();
     }
}
