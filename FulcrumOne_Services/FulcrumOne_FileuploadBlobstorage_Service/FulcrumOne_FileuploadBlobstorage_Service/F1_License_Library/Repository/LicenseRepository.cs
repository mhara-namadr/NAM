﻿using F1_License_Library.Models;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace F1_License_Library.Repository
{
    public class LicenseRepository
    {
        #region Variables
        private readonly Container container;
        private readonly CosmosConnector _cosmosConnector;
        private readonly Database database;
        private readonly string Cosmos_GetMaxRegistrationid;
        private readonly int Reseed_RegistrationID;
        private readonly string Cosmos_GetAllServices;
        private readonly string Cosmons_GetServiceByRegistrationno;
        private readonly string Cosmons_GetServiceByHostname;
        #endregion

        #region Contructor
        public LicenseRepository(IConfiguration config, CosmosConnector cosmosConnector)
        {
            _cosmosConnector = cosmosConnector;
            database = _cosmosConnector.database;
            container = _cosmosConnector.container;
            Cosmos_GetMaxRegistrationid = config.GetValue<string>("Cosmos_GetMaxRegistrationid");
            Reseed_RegistrationID = config.GetValue<int>("Reseed_RegistrationID");
            Cosmos_GetAllServices = config.GetValue<string>("Cosmos_GetAllServices");
            Cosmons_GetServiceByRegistrationno = config.GetValue<string>("Cosmons_GetServiceByRegistrationno");
            Cosmons_GetServiceByHostname = config.GetValue<string>("Cosmons_GetServiceByHostname");
        }
        #endregion

        public async Task<ServiceRegistrationModel> GetserviceDetails(string registrationid)
        {
            try
            {
                var query = new QueryDefinition(Cosmons_GetServiceByRegistrationno);
                query.WithParameter("@registrationid", registrationid);

                FeedIterator<ServiceRegistrationModel> queryResultSetIterator = this.container.GetItemQueryIterator<ServiceRegistrationModel>(query);

                ServiceRegistrationModel projectModel = new ServiceRegistrationModel();

                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        FeedResponse<ServiceRegistrationModel> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        foreach (ServiceRegistrationModel projetMod in currentResultSet)
                        {
                            projectModel = projetMod;
                        }

                        if (projectModel.registrationid is null)
                        {
                            projectModel.Responsermessage = "Records Not Found";
                            return projectModel;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                return projectModel;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public async Task<ServiceRegistrationModel> GetserviceDetailsByhostname(ServiceDetailsModel Model)
        {
            try
            {
                var query = new QueryDefinition(Cosmons_GetServiceByHostname);
                query.WithParameter("@Hostname", Model.Hostname);
                query.WithParameter("@LicenseKey", Model.LicenseKey);

                FeedIterator<ServiceRegistrationModel> queryResultSetIterator = this.container.GetItemQueryIterator<ServiceRegistrationModel>(query);

                ServiceRegistrationModel projectModel = new ServiceRegistrationModel();

                while (queryResultSetIterator.HasMoreResults)
                {
                    try
                    {
                        FeedResponse<ServiceRegistrationModel> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                        foreach (ServiceRegistrationModel projetMod in currentResultSet)
                        {
                            projectModel = projetMod;
                        }

                        if (projectModel.registrationid is null)
                        {
                            projectModel.Responsermessage = "Records Not Found";
                            return projectModel;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                return projectModel;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public async Task<ServiceRegistrationModel> AddHitcount(ServiceDetailsModel ModelObj)
        {
            try
            {
                ServiceRegistrationModel Model = await GetserviceDetailsByhostname(ModelObj);
                if (Model.Responsermessage != "Records Not Found")
                {
                    Model.Hitcount += 1;
                    var result = await UpdateService(Model);
                    return result;
                }
                else
                {
                    var result = Model;
                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<ServiceRegistrationModel> UpdateService(ServiceRegistrationModel Model)
        {
            try
            {
                ItemResponse<ServiceRegistrationModel> response = null;
                try
                {
                    response = await container.ReadItemAsync<ServiceRegistrationModel>(Model.ID, new PartitionKey(Model.registrationid));
                }
                catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    Console.WriteLine(ex);
                    throw;
                }
                if (response != null)
                {
                    await container.ReplaceItemAsync<ServiceRegistrationModel>(Model, Model.ID, new PartitionKey(Model.registrationid));
                    Model.Responsermessage = "Record updated successfully";
                }
                else
                {
                    Model.Responsermessage = "Record not found";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return Model;
        }

    }
}
