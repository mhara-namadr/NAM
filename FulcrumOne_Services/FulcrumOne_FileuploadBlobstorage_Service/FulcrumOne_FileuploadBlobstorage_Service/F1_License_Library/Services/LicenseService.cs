﻿using F1_License_Library.IServices;
using F1_License_Library.Models;
using F1_License_Library.Repository;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace F1_License_Library.Services
{
    public class LicenseService : ILicenseRegistrationService
    {
        public IConfiguration Configuration { get; }
        private readonly CosmosConnector _cosmosConnector;
        private readonly string _Hostname;
        private readonly string _LicenseKey;

        public LicenseService(IConfiguration configuration, CosmosConnector cosmosConnector)
        {
            Configuration = configuration;
            _cosmosConnector = cosmosConnector;
            _Hostname = configuration.GetValue<string>("Hostnameofservice");
            _LicenseKey = configuration.GetValue<string>("LicenseKey");
        }

        #region Add 1 to Hit Count by using the API of the Service Registration APP
        public async Task<ServiceRegistrationModel> AddHitcount()
        {
            ServiceDetailsModel Model = new ServiceDetailsModel
            {
                Hostname = _Hostname,
                LicenseKey = _LicenseKey
            };

            try
            {
                LicenseRepository obj = new LicenseRepository(Configuration, _cosmosConnector);
                var result = await obj.AddHitcount(Model);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        #endregion


        public async Task<ServiceRegistrationModel> GetDetailsbyhostname()
        {
            ServiceDetailsModel Model = new ServiceDetailsModel
            {
                Hostname = _Hostname,
                LicenseKey = _LicenseKey
            };

            try
            {
                LicenseRepository obj = new LicenseRepository(Configuration, _cosmosConnector);
                var result = await obj.GetserviceDetailsByhostname(Model);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
