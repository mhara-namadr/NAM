﻿using FulcrumOne_FileuploadBlobstorage_Service.IServices;
using FulcrumOne_FileuploadBlobstorage_Service.Model;
using FulcrumOne_FileuploadBlobstorage_Service.Models;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace FulcrumOne_FileuploadBlobstorage_Service.Services
{
    public class BlobStorageService : IBlobStorageService
    {
        private readonly IOptions<MyConfig> config;
        public BlobStorageService(IOptions<MyConfig> config)
        {
            this.config = config;
        }

        /// <summary>
        /// Deletefile
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> Deletefile(string fileName)
        {
            ResponseModel model = new ResponseModel
            {
                Responsestatus = false
            };
            try
            {
                if (CloudStorageAccount.TryParse(config.Value.StorageConnection, out CloudStorageAccount storageAccount))
                {
                    CloudBlobClient BlobClient = storageAccount.CreateCloudBlobClient();
                    CloudBlobContainer container = BlobClient.GetContainerReference(config.Value.Container);

                    if (await container.ExistsAsync())
                    {
                        CloudBlob file = container.GetBlobReference(fileName);
                        if (await file.ExistsAsync())
                        {
                            await file.DeleteAsync();
                            model.Responsemessage = Constants.Filedeletesuccess;
                            model.Responsestatus = true;
                        }
                        else
                        {
                            model.Responsemessage = Constants.Filenotexists;
                            model.Responsestatus = false;
                        }
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Insert New File
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        public async Task<ResponseModel> Insertfile(IFormFile asset)
        {
            ResponseModel model = new ResponseModel
            {
                Responsestatus = false
            };
            try
            {
                if (CloudStorageAccount.TryParse(config.Value.StorageConnection, out CloudStorageAccount storageAccount))
                {
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    CloudBlobContainer container = blobClient.GetContainerReference(config.Value.Container);

                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(asset.FileName);

                    await blockBlob.UploadFromStreamAsync(asset.OpenReadStream());

                    model.Responsestatus = true;
                    model.Responsemessage = Constants.Fileuploadsuccess;
                }
                else
                {
                    model.Responsestatus = false;
                    model.Responsemessage = Constants.Fileuploadfailure;
                }
                return model;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// List all Files
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> Listfiles()
        {
            List<string> blobs = new List<string>();
            try
            {
                if (CloudStorageAccount.TryParse(config.Value.StorageConnection, out CloudStorageAccount storageAccount))
                {
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    CloudBlobContainer container = blobClient.GetContainerReference(config.Value.Container);

                    BlobResultSegment resultSegment = await container.ListBlobsSegmentedAsync(null);
                    foreach (IListBlobItem item in resultSegment.Results)
                    {
                        if (item.GetType() == typeof(CloudBlockBlob))
                        {
                            CloudBlockBlob blob = (CloudBlockBlob)item;
                            blobs.Add(blob.Name);
                        }
                        else if (item.GetType() == typeof(CloudPageBlob))
                        {
                            CloudPageBlob blob = (CloudPageBlob)item;
                            blobs.Add(blob.Name);
                        }
                        else if (item.GetType() == typeof(CloudBlobDirectory))
                        {
                            CloudBlobDirectory dir = (CloudBlobDirectory)item;
                            blobs.Add(dir.Uri.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
            return blobs;
        }

        
    }
}
