using F1_License_Library.Models;
using FulcrumOne_FileuploadBlobstorage_Service.Extensions;
using FulcrumOne_FileuploadBlobstorage_Service.IServices;
using FulcrumOne_FileuploadBlobstorage_Service.Model;
using FulcrumOne_FileuploadBlobstorage_Service.Services;

var builder = WebApplication.CreateBuilder(args);

#region CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowOrigin",
        builder => builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod());
});
#endregion


builder.Services.Configure<MyConfig>(builder.Configuration.GetSection("MyConfig"));
var emailconfiguration = builder.Configuration.GetSection("MyConfig").Get<MyConfig>();

#region Dependencies for Licensing
builder.Services.Configure<DBOptions>(builder.Configuration.GetSection("AzureCosmosDBKeys"));
builder.Services.AddSingleton<CosmosConnector>();
builder.Services.AddSingleton<DBOptions>();
#endregion

bool _IsLicensingenabled = builder.Configuration.GetValue<bool>("IsLicensingenabled");

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IBlobStorageService, BlobStorageService>();


var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
   
//}

app.UseSwagger();
app.UseSwaggerUI();

if (_IsLicensingenabled)
{
    app.UseLicenseMiddleware();
    ////app.UseLicensingMiddleware();

}
app.UseAuthorization();

app.MapControllers();

app.UseCors("AllowOrigin");

app.Run();
