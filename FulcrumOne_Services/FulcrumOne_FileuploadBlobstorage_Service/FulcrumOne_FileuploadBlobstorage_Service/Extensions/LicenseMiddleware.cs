﻿using F1_License_Library.Models;

namespace FulcrumOne_FileuploadBlobstorage_Service.Extensions
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class LicenseMiddleware
    {
        private readonly RequestDelegate _next;
        public IConfiguration Configuration { get; }
        private readonly CosmosConnector _cosmosConnector;
        private int Counter;

        public LicenseMiddleware(RequestDelegate next, IConfiguration configuration, CosmosConnector cosmosConnector)
        {
            _next = next;
            Configuration = configuration;
            _cosmosConnector = cosmosConnector;
        }

        public async Task InvokeAsync(HttpContext httpContext, IHostApplicationLifetime appLifetime)
        {

            #region Check License for Service if Expired then Stop applcation else continue

            ////Counter++;
            //////if ((Counter % 3) == 0)
            ////if ((Counter % 2) == 0)
            ////{
            LicenseValidation lc = new(Configuration, _cosmosConnector);
            bool Isvalidlicense = await lc.CheckLicenseandValidity();
            if (!Isvalidlicense)
            {
                appLifetime.StopApplication();
                appLifetime.StopApplication();
            }
            ////}

            await this._next(httpContext);
            #endregion
            ////return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class LicenseMiddlewareExtensions
    {
        public static IApplicationBuilder UseLicenseMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LicenseMiddleware>();
        }
    }
}
