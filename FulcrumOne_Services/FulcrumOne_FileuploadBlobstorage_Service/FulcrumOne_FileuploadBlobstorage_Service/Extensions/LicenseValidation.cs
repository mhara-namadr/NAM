﻿using F1_License_Library.Models;
using F1_License_Library.Services;

namespace FulcrumOne_FileuploadBlobstorage_Service.Extensions
{
    public class LicenseValidation
    {
        public IConfiguration Configuration { get; }
        private readonly CosmosConnector _cosmosConnector;
        public LicenseValidation(IConfiguration configuration, CosmosConnector cosmosConnector)
        {
            Configuration = configuration;
            _cosmosConnector = cosmosConnector;
        }

        /// <summary>
        /// CheckLicenseandValidity
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckLicenseandValidity()
        {
            try
            {
                LicenseService srvc = new LicenseService(Configuration, _cosmosConnector);

                var result = await srvc.GetDetailsbyhostname();
                if ((DateTime.Now > result.EndDate) || (result.Responsermessage == "Records Not Found"))
                {
                    /// Condition if License is expired
                    await srvc.AddHitcount();
                    return false;
                }
                else
                {
                    /// Condition   License still their valid
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
