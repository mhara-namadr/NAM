﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace F1_FileUploadBlobStorage_Service.Extensions
{
    /// <summary>
    /// MiddlewareExtensions  <br />
    /// </summary>
    public static class MiddlewareExtensions
    {
        private const string Url = "/swagger/v1/swagger.json";
        private const string FduriString = "https://fulcrumdigital.com/";

        /// <summary>Adds the custom swagger.</summary>
        /// <param name="services">The services.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public static IServiceCollection AddCustomSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(cfg =>
            {
                cfg.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "F1 File Upload Blobstorage API",
                    Version = "v1",
                    Description = "Example API that shows how to implement File Upload on to the blob storage container with ASP.NET Core 3.1, built from scratch.",
                    Contact = new OpenApiContact
                    {
                        Name = "Fulcrum Digital",
                        Url = new Uri(FduriString)
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT",
                    },
                });



                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                cfg.IncludeXmlComments(xmlPath);
            });

            return services;
        }

        /// <summary>Uses the custom swagger.</summary>
        /// <param name="app">The application.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public static IApplicationBuilder UseCustomSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger().UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(Url, "F1 File Upload Blobstorage API");
                options.DocumentTitle = "F1 File Upload Blobstorage API";
            });
            return app;

        }
    }
}
