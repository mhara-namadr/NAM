﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FulcrumOne_FileuploadBlobstorage_Service.Extensions
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class LicensingMiddleware
    {
        private readonly RequestDelegate _next;

        public LicensingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            Console.WriteLine("YS");
            await _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class LicensingMiddlewareExtensions
    {
        public static IApplicationBuilder UseLicensingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LicensingMiddleware>();
        }
    }
}
