﻿using FulcrumOne_FileuploadBlobstorage_Service.Models;

namespace FulcrumOne_FileuploadBlobstorage_Service.IServices
{
    public interface IBlobStorageService
    {
        Task<List<string>> Listfiles();
        Task<ResponseModel> Deletefile(string fileName);
        Task<ResponseModel> Insertfile(IFormFile asset);
    }
}
