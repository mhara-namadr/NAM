﻿

namespace FulcrumOne_FileuploadBlobstorage_Service.Model
{
    /// <summary>
    ///   <br />
    /// </summary>
    public class MyConfig
    {
        /// <summary>Gets or sets the storage connection.</summary>
        /// <value>The storage connection.</value>
        public string StorageConnection { get; set; }

        /// <summary>Gets or sets the container.</summary>
        /// <value>The container.</value>
        public string Container { get; set; }
    }
}
