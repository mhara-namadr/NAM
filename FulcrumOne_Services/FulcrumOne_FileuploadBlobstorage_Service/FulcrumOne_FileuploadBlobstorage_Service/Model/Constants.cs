﻿namespace FulcrumOne_FileuploadBlobstorage_Service.Model
{
    public static class Constants
    {
        public const string Fileuploadsuccess = "File was successfully uploaded.";
        public const string Fileuploadfailure = "Error occured while uploading the file.";
        public const string Filedeletesuccess = "File Deleted Successfully!";
        public const string Filenotexists = "File Does not exist";
    }
}
