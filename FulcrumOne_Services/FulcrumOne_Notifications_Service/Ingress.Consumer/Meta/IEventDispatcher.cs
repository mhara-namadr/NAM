using System.Collections.Generic;
using System.Threading.Tasks;
using Ingress.Consumer.Models;

namespace Ingress.Consumer.Meta
{
    public interface IEventDispatcher<TEvent> where TEvent : Event
    {
        void Publish(TEvent @event);
        Task PublishAsync(TEvent @event);
        Task PublishManyAsync(ICollection<TEvent> @events);
    }
}