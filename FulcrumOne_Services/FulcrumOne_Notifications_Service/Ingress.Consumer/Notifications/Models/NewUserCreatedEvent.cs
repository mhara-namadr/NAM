﻿
using Ingress.Consumer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ingress.Consumer.Notifications.Models
{
    public class NewUserCreatedEvent : Event
    {
        public string Emailmessage { get; set; }
        public string Receiveremail { get; set; }
        public string Emailsubject { get; set; }
    }
}
