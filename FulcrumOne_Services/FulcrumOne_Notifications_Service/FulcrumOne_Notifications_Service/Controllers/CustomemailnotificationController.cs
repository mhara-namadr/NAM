﻿using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.Core.IServices;
using FulcrumOne_Notifications_Service.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
using NLog;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CustomemailnotificationController : ControllerBase
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly string _folderName;
        private readonly string _permittedextensions;
        private readonly IEmailNotificationService _EmailNotificationService;

        /// <summary>
        /// CustomemailnotificationController
        /// </summary>
        /// <param name="config"></param>
        /// <param name="emailnotificationservc"></param>
        public CustomemailnotificationController(IConfiguration config, IEmailNotificationService emailnotificationservc)
        {
            _folderName = config.GetValue<string>("EmailAttachments");
            _permittedextensions = config.GetValue<string>("Extensions");
            _EmailNotificationService = emailnotificationservc;
        }


        /// <summary>
        /// Send a customised Email Notification with a template body
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        #region Send Customised Email Notification
        [Route("/api/customemailnotification")]
        [HttpPost]
        public async Task<IActionResult> SendCustomisedEmailNotification([FromBody] CustomNotificationResources Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var json = new JavaScriptSerializer().Serialize(Model);
            if (!JsonValidation.IsValidJson(json))
            {
                return BadRequest("Invalid Json.");
            }
            var Result = await _EmailNotificationService.SendCustomEmailNotification(Model, "");
            if (Result.Responsestatus)
            {
                return Ok(Result);
            }
            else
            {
                return BadRequest(Result);
            }
        }
        #endregion


        /// <summary>
        ///Send  custom emailnotification with uploaded attachment 
        /// </summary>
        /// <param name="Json"></param>
        /// <returns></returns>
        #region Send Custom Email with Attachment
        [Route("/api/customemailnotificationwithattachment")]
        [HttpPost]
        public async Task<IActionResult> SendCustomisedEmailNotificationwithAttachment([FromBody] CustomNotificationResources Json)
        {
            if (String.IsNullOrEmpty(Json.Filename) || (Json.Filename.Equals("String")) || (Json.Filename.Equals("STRING")) || (Json.Filename.Equals("string")))
            {
                return BadRequest("Invalid Filename");
            }
            var jsondata = new JavaScriptSerializer().Serialize(Json);
            if (!JsonValidation.IsValidJson(jsondata))
            {
                return BadRequest("Invalid Json.");
            }
            string attachments = string.Empty;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string[] values = Json.Filename.Split(',').Select(sValue => sValue.Trim()).ToArray();
            foreach (var item in values)
            {
                string attachmentpath = _folderName + "//" + item.ToString().Trim();
                if (String.IsNullOrEmpty(attachments))
                {
                    attachments = attachmentpath;
                    if (!System.IO.File.Exists(attachmentpath))
                    {
                        return NotFound("File Not found. " + attachmentpath);
                    }
                }
                else
                {
                    if (!System.IO.File.Exists(attachmentpath))
                    {
                        return NotFound("File Not found. " + attachmentpath);
                    }
                    attachments = attachments + "," + attachmentpath;
                }
            }

            var Result = await _EmailNotificationService.SendCustomEmailNotification(Json, attachments);
            if (Result.Responsestatus)
            {
                return Ok(Result);
            }
            else
            {
                return BadRequest(Result);
            }
        }
        #endregion


        /// <summary>
        /// send customemail with 1 string attachment
        /// </summary>
        /// <param name="Json"></param>
        /// <returns></returns>
        [Route("/api/sendcustomemailstringattachment")]
        [HttpPost]
        public async Task<IActionResult> SendCustomEmailStringAttachment([FromBody] CustomNotificationStringAttachmentResource Json)
        {
            string base64string = Json.Base64string;
            try
            {
                var jsondata = new JavaScriptSerializer().Serialize(Json);
                if (!JsonValidation.IsValidJson(jsondata))
                {
                    return BadRequest("Invalid Json.");
                }
                if (!Base64Validstring.IsBase64(base64string))
                {
                    return BadRequest("Invalid base 64 string.");
                }
                bool IsAllowedfiletye = _permittedextensions.Split(',').Contains("." + Json.Attachmentfiletype);
                if (!IsAllowedfiletye)
                {
                    return BadRequest("File type not supported.");
                }
                #region Generate and Save attachment
                string filename = "Attachment_" + Guid.NewGuid();
                byte[] bytes = Convert.FromBase64String(base64string);
                string fileuploadpath = _folderName + "\\" + filename + "." + Json.Attachmentfiletype;
                FileStream stream = new(fileuploadpath, FileMode.CreateNew);
                BinaryWriter writer = new(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();
                #endregion

                CustomNotificationResources Model = new()
                {
                    EmailBodyVariables = Json.EmailBodyVariables,
                    EmailSubject = Json.EmailSubject,
                    EmailTemplate = Json.EmailTemplate,
                    ListEmailReceivers = Json.ListEmailReceivers,
                    Cc = Json.Cc,
                    Bcc = Json.Bcc
                };

                var Result = await _EmailNotificationService.SendCustomEmailNotification(Model, fileuploadpath);
                if (Result.Responsestatus)
                {
                    return Ok(Result);
                }
                else
                {
                    return BadRequest(Result);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }


        /// <summary>
        /// Send Custom Email Notification with Multiple String Attachment
        /// </summary>
        /// <returns></returns>
        [Route("/api/sendcustomemailmultiplestringattachment")]
        [HttpPost]
        public async Task<IActionResult> SendCustomEmailMultipleStringAttachment([FromBody] CustomEmailWithStringMultipleAttachment Json)
        {
            string listuploadpaths = string.Empty;
            var jsondata = new JavaScriptSerializer().Serialize(Json);
            if (!JsonValidation.IsValidJson(jsondata))
            {
                return BadRequest("Invalid Json.");
            }
            if (Json.Attachments.Count == 0)
            {
                return BadRequest("Atleast 1 attachment is required.");
            }
            var isUnique = Json.Attachments.GroupBy(i => i.filename).Count() == Json.Attachments.Count;
            if (!isUnique)
            {
                return BadRequest("Files names must be unique.");
            }

            try
            {
                foreach (var item in Json.Attachments)
                {
                    if (string.IsNullOrEmpty(item.filetype))
                    {
                        return BadRequest("File type is required");
                    }
                    bool IsAllowedfiletye = _permittedextensions.Split(',').Contains("." + item.filetype);
                    if (!IsAllowedfiletye)
                    {
                        return BadRequest("File type not supported.");
                    }
                    if (!Base64Validstring.IsBase64(item.base64string))
                    {
                        return BadRequest("Invalid base 64 string.");
                    }

                    #region Generate and Save attachment
                    string filename = item.filename;//// "Attachment_" + Guid.NewGuid();
                    byte[] bytes = Convert.FromBase64String(item.base64string);
                    string fileuploadpath = _folderName + "\\" + filename + "." + item.filetype;
                    if (System.IO.File.Exists(fileuploadpath))
                    {
                        System.IO.File.Delete(fileuploadpath);
                    }
                    using (var stream = new FileStream(fileuploadpath, FileMode.CreateNew))
                    {
                        BinaryWriter writer = new(stream);
                        writer.Write(bytes, 0, bytes.Length);
                        writer.Close();
                        writer.Dispose();
                    }
                    #endregion
                    if (String.IsNullOrEmpty(listuploadpaths))
                    {
                        listuploadpaths = fileuploadpath;
                    }
                    else
                    {
                        listuploadpaths = listuploadpaths + "," + fileuploadpath;
                    }
                }

                CustomNotificationResources Model = new()
                {
                    EmailBodyVariables = Json.EmailBodyVariables,
                    EmailSubject = Json.EmailSubject,
                    EmailTemplate = Json.EmailTemplate,
                    ListEmailReceivers = Json.ListEmailReceivers,
                    Cc = Json.Cc,
                    Bcc = Json.Bcc
                };

                var Result = await _EmailNotificationService.SendCustomEmailNotification(Model, listuploadpaths);
                if (Result.Responsestatus)
                {
                    return Ok(Result);
                }
                else
                {
                    return BadRequest(Result);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }



        /// <summary>
        /// UploadAttachment for sending into the EMail
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        #region Upload Attachment
        [Route("/api/uploadattachment")]
        [HttpPost]
        public IActionResult UploadAttachment([Required(ErrorMessage = "Please select a file.")] IFormFile file)
        {
            try
            {
                var fileextension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                bool IsAllowedfiletye = _permittedextensions.Split(',').Contains(fileextension);
                if (IsAllowedfiletye)
                {
                    string filename;
                    if (file.FileName.Contains(" "))
                    {
                        filename = file.FileName.Replace(" ", "_");
                    }
                    else
                    {
                        filename = file.FileName;
                    }

                    String folderPath = Path.Combine(Directory.GetCurrentDirectory(), _folderName);
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    using (var fileContentStream = new MemoryStream())
                    {
                        file.CopyToAsync(fileContentStream);
                        System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, filename), fileContentStream.ToArray());
                    }
                    return Ok(filename);
                }
                else
                {
                    return BadRequest("File type not allowed");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;

            }
        }
        #endregion


    }
}
