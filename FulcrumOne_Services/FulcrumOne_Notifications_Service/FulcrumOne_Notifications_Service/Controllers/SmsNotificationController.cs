﻿using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.Core.IServices;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FulcrumOne_Notifications_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class SmsNotificationController : ControllerBase
    {
        #region Constructor
        private readonly ISmsNotificationService _SMSNotificationService;
        public SmsNotificationController(ISmsNotificationService servc)
        {
            _SMSNotificationService = servc;
        }
        #endregion



        #region Send SMS Nexmo
        [Route("/api/sendsmsnexmo")]
        [HttpPost]
        public IActionResult SendSMSwithNexmo(SmsSenderResources Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _SMSNotificationService.SendSMSNexmo(Model);

            if (result == "0")
            {
                return Ok("SMS Notification Sent Successfully");
            }
            else if (result == "29")
            {
                return Ok("Non White-listed Destination - rejected (The no needs to be added into the list)");
            }
            else
            {
                return BadRequest("Error Occured while Sending the Notification");
            }
        }
        #endregion

        #region Send Custom SMS Nexmo
        [Route("/api/sendcustomsmsnexmo")]
        [HttpPost]
        public IActionResult SendCustomSMSNexmo(CustomSmsSenderResources Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _SMSNotificationService.SendCustomSMSNexmo(Model);

            if (result == "0")
            {
                return Ok("SMS Notification Sent Successfully");
            }
            else if (result == "29")
            {
                return Ok("Non White-listed Destination - rejected (The no needs to be added into the list)");
            }
            else
            {
                return BadRequest("Error Occured while Sending the Notification");
            }
        }
        #endregion

        #region Send SMS using Twillo
        [Route("/api/sendsmstwilio")]
        [HttpPost]
        public IActionResult SendSMSTwilio(SmsSenderResources Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _SMSNotificationService.SendSMSTwilio(Model);
            return Ok(result);
        }
        #endregion

        #region Send Custom SMS Twillo
        [Route("/api/sendcustomsmstwilio")]
        [HttpPost]
        public IActionResult SendCustomSMSTwilio(CustomSmsSenderResources Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _SMSNotificationService.SendCustomSMSTwilio(Model);

            return Ok(result);
        }
        #endregion
    }
}
