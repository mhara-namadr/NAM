﻿using FulcrumOne_Notifications_Service.DBModels;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using FulcrumOne_Notifications_Service.Core.IServices;
using FulcrumOne_Notifications_Service.AppModels;
using Nancy.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace FulcrumOne_Notifications_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BatchEmailController : ControllerBase
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        private readonly IMapper _mapper;
        private readonly IBatchEmailService _srvc;
        public BatchEmailController(IBatchEmailService servc, IMapper mapper)
        {
            _srvc = servc;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("/api/addnewemailbatchdata")]
        public async Task<IActionResult> Addnewemailbatchdata([FromBody] EmailbatchModel Model)
        {
            try
            {
                var batchnameexists = await _srvc.Getemailbatchdata(Model.Batchfor);
                if (batchnameexists.Batchfor == Model.Batchfor)
                {
                    return BadRequest("Batch Name Already Exists.");
                }

                EmailScheduleBatch _batchDTO = this._mapper.Map<EmailbatchModel, EmailScheduleBatch>(Model);
                var result = await _srvc.Addnewemailbatch(_batchDTO);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet]
        [Route("/api/getemailbatchdata")]
        public async Task<IActionResult> Getemailbatchdata([Required] string Batchname)
        {
            try
            {
                var result = await _srvc.Getemailbatchdata(Batchname);
                if (result.id == 0)
                {
                    Emailresponse response = new()
                    {
                        Responsemessage = Responsemessages.Deletebatcherror,
                        Responsestatus = false
                    };
                    return BadRequest(response);
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        [HttpGet]
        [Route("/api/getcustomemailbatchdata")]
        public async Task<IActionResult> Getcustomemailbatchdata([Required] string Batchname)
        {
            try
            {
                var result = await _srvc.Getcustomemailbatchdata(Batchname);
                if (result.id == 0)
                {
                    Emailresponse response = new()
                    {
                        Responsemessage = Responsemessages.Deletebatcherror,
                        Responsestatus = false
                    };
                    return BadRequest(response);
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        [HttpDelete]
        [Route("/api/deletebatchdata")]
        public async Task<IActionResult> Deletebatchdata([Required] string Batchname)
        {
            try
            {
                var result = await _srvc.Deleteemailbatchdata(Batchname);
                if (!result.Responsestatus)
                {
                    Emailresponse response = new()
                    {
                        Responsemessage = Responsemessages.Deletebatcherror,
                        Responsestatus = false
                    };
                    return BadRequest(response);
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Addcustomemailbatch
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/addcustomemailbatch")]
        public async Task<IActionResult> Addcustomemailbatch([FromBody] EmailbatchCustomEmail Model)
        {
            try
            {
                var batchnameexists = await _srvc.Getcustomemailbatchdata(Model.Batchfor);
                if (batchnameexists.Batchfor == Model.Batchfor)
                {
                    return BadRequest("Batch Name Already Exists.");
                }

                customemailschedulebatch _batchDTOs = this._mapper.Map<EmailbatchCustomEmail, customemailschedulebatch>(Model);
                var jsonBodyVariables = new JavaScriptSerializer().Serialize(Model.EmailBodyVariables.ToDictionary(item => item.Key.ToString(), item => item.Value.ToString()));
                ////_batchDTOs.EmailBodyVariables = Model.EmailBodyVariables[0].ToString();
                _batchDTOs.EmailBodyVariables = jsonBodyVariables;
                var result = await _srvc.Addcustomemailbatch(_batchDTOs);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
    }
}
