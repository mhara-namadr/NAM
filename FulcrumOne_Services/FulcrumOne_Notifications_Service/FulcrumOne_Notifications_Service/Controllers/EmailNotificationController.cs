﻿using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.Core.IServices;
using FulcrumOne_Notifications_Service.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
using NLog;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class EmailNotificationController : ControllerBase
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        private readonly string _folderName;
        private readonly string _permittedextensions;
        private readonly IEmailNotificationService _EmailNotificationService;


        #region Constructor
        public EmailNotificationController(IConfiguration config, IEmailNotificationService emailnotificationservc)
        {
            _folderName = config.GetValue<string>("EmailAttachments");
            _permittedextensions = config.GetValue<string>("Extensions");
            _EmailNotificationService = emailnotificationservc;
        }
        #endregion

        /// <summary>
        /// This end point is used to send an Email Notification using the Saved Email Tempalte in the Application itself.
        /// The user just needs to send the details like Receeiver Name, Receiver Email,Email Subject
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        #region EmailNotification with built in Template
        [Route("/api/emailnotification")]
        [HttpPost]
        public async Task<IActionResult> SendEmailNotification([FromBody] TemplateBasedResource Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var json = new JavaScriptSerializer().Serialize(Model);
            if (!JsonValidation.IsValidJson(json))
            {
                return BadRequest("Invalid Json.");
            }
            var Result = await _EmailNotificationService.SendEmailNotification(Model);
            if (Result.Responsestatus)
            {
                return Ok(Result);
            }
            else
            {
                return BadRequest(Result);
            }
        }
        #endregion



        #region Send static custom email
        [Route("/api/staticemailnotification")]
        [HttpPost]
        public async Task<IActionResult> SendStaticEmailNotification([FromBody] StaticNotificationResources json)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var jsondata = new JavaScriptSerializer().Serialize(json);
            if (!JsonValidation.IsValidJson(jsondata))
            {
                return BadRequest("Invalid Json.");
            }
            var Result = await _EmailNotificationService.SendStaticEmailNotification(json);
            if (Result.Responsestatus)
            {
                return Ok(Result);
            }
            else
            {
                return BadRequest(Result);
            }
        }
        #endregion



        /// <summary>
        /// Send Email Notification with Tabular data
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        #region Send Email Notification using Tabular Grid
        [Route("/api/sendemailnotificationwithtabulardata")]
        [HttpPost]
        public async Task<IActionResult> SendEmailNotificationWithTabulardata([FromBody] TemplateBasedResource Model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var json = new JavaScriptSerializer().Serialize(Model);
            if (!JsonValidation.IsValidJson(json))
            {
                return BadRequest("Invalid Json.");
            }
            var Result = await _EmailNotificationService.SendEmailNotificationWithTabulardata(Model);
            if (Result.Responsestatus)
            {
                return Ok(Result);
            }
            else
            {
                return BadRequest(Result);
            }
        }
        #endregion




    }
}
