﻿
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using SendGrid;
using NLog;
using SendGrid.Helpers.Mail;
using System.Text;
using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.Core.IServices;
using FulcrumOne_Notifications_Service.Extensions;
using FulcrumOne_Notifications_Service.Security;

namespace FulcrumOne_Notifications_Service.Services
{
    public class EmailNotificationService : IEmailNotificationService
    {
        private static readonly NLog.ILogger _logger = LogManager.GetCurrentClassLogger();

        private readonly EmailConfiguration _emailConfigurations;
        private readonly IWebHostEnvironment _env;
        private readonly SendGridConfiguration _sendGridConfiguraiton;

        ///// <summary>
        ///// Email Notification Service Constructor
        ///// </summary>
        public EmailNotificationService(IOptions<EmailConfiguration> EmailConfigSnapshot, IWebHostEnvironment env, IOptions<SendGridConfiguration> SendGridSanpShot)
        {
            _emailConfigurations = EmailConfigSnapshot.Value;
            _sendGridConfiguraiton = SendGridSanpShot.Value;
            _env = env;
        }

        #region Send Email Notification using saved template
        public async Task<Emailresponse> SendEmailNotification(TemplateBasedResource _Model)
        {
            Emailresponse response = new()
            {
                Responsestatus = false
            };
            try
            {
                EmailNotificationResources _EMModel = new();

                String pathToFile = string.Empty;
                if (String.IsNullOrEmpty(_Model.TemplateName) || (_Model.TemplateName == "string") || (_Model.TemplateName == "String"))
                {
                    pathToFile = _env.ContentRootPath
                      + Path.DirectorySeparatorChar.ToString()
                      + "Templates"
                      + Path.DirectorySeparatorChar.ToString()
                      + "SavedEmailTemplates"
                      + Path.DirectorySeparatorChar.ToString()
                      + "Welcome.html";
                }
                else
                {
                    pathToFile = _emailConfigurations.SavedEmailTemplatesPath + "//" + _Model.TemplateName.Trim() + ".html";
                }

                string FilePath = pathToFile;
                StreamReader str = new StreamReader(FilePath);
                string MailText = str.ReadToEnd();
                str.Close();

                #region Populatse Email Body Dynamic Fields
                foreach (var item in _Model.EmailBodyVariables)
                {
                    string Field = item.Key;
                    string Value = item.Value;
                    MailText = MailText.Replace(Field, Value);
                }
                #endregion

                MailMessage EmailMessage = new MailMessage
                {
                    //Make TRUE because our body text is html  
                    IsBodyHtml = true,
                    From = new MailAddress(_emailConfigurations.SenderEmail),
                    Subject = _Model.EmailSubject,
                    Body = MailText
                };

                #region Loop the Emailers list                
                foreach (var item in _Model.ListEmailReceivers)
                {
                    _EMModel.ReceiverName = item.ReceiverName;

                    if (_emailConfigurations.IsEmailEnabled)
                    {
                        _EMModel.ReceiverEmail = item.ReceiverEmail;
                    }
                    else
                    {
                        _EMModel.ReceiverEmail = _emailConfigurations.TestMail;
                    }

                    EmailMessage.To.Add(new MailAddress(_EMModel.ReceiverEmail));
                }
                #endregion

                #region Adding CC and Bcc Mailers

                List<EmailAddress> lstcc = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(_Model.Cc))
                {
                    EmailMessage.CC.Add(_Model.Cc);
                    foreach (string value in _Model.Cc.Split(','))
                    {
                        lstcc.Add(new EmailAddress(value));
                    }
                }

                List<EmailAddress> lstbcc = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(_Model.Bcc))
                {
                    EmailMessage.Bcc.Add(_Model.Bcc);
                    foreach (string value in _Model.Bcc.Split(','))
                    {
                        lstbcc.Add(new EmailAddress(value));
                    }
                }
                #endregion

                if (_sendGridConfiguraiton.IsSendGridEmail)
                {
                    bool status = await SendGridTemplatebasedEmail(_Model, EmailMessage, _EMModel, lstcc, lstbcc);
                    if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
                else
                {
                    bool status = await SendMailAsync(EmailMessage);
                    if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());

            }
            return response;
        }
        #endregion

        #region Send Email Notification with table using Template
        public async Task<Emailresponse> SendEmailNotificationWithTabulardata(TemplateBasedResource _Model)
        {
            Emailresponse response = new()
            {
                Responsestatus = false
            };
            try
            {
                EmailNotificationResources _EMModel = new EmailNotificationResources();

                String pathToFile = string.Empty;
                if (String.IsNullOrEmpty(_Model.TemplateName) || (_Model.TemplateName == "string") || (_Model.TemplateName == "String"))
                {
                    pathToFile = _env.ContentRootPath
                      + Path.DirectorySeparatorChar.ToString()
                      + "Templates"
                      + Path.DirectorySeparatorChar.ToString()
                      + "SavedEmailTemplates"
                      + Path.DirectorySeparatorChar.ToString()
                      + "Tabular.html";
                }
                else
                {
                    pathToFile = _emailConfigurations.SavedEmailTemplatesPath + "//" + _Model.TemplateName.Trim() + ".html";
                }

                string FilePath = pathToFile;
                StreamReader str = new(FilePath);
                string MailText = str.ReadToEnd();
                str.Close();
                string tablestring = string.Empty;
                #region Populatse Email Body Dynamic Fields
                string Jsondata = JsonConvert.SerializeObject(_Model.EmailBodyVariables);
                string Jsonarray = "[" + Jsondata + "]";
                List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Jsonarray);
                foreach (Dictionary<string, string> lst in obj)
                {
                    StringBuilder sb = new();
                    if (lst.ContainsKey("Organization"))
                    {
                        sb.Append("<tr><td>" + lst["Organization"] + "</td>");
                    }
                    else
                    {
                        sb.Append("<tr><td>" + "" + "</td>");
                    }
                    if (lst.ContainsKey("AircraftID"))
                    {
                        sb.Append("<td>" + lst["AircraftID"] + "</td>");
                    }
                    else
                    {
                        sb.Append("<td>" + "" + "</td>");
                    }
                    if (lst.ContainsKey("DepartureLocation"))
                    {
                        sb.Append("<td>" + lst["DepartureLocation"] + "</td>");
                    }
                    else
                    {
                        sb.Append("<td>" + "" + "</td>");
                    }
                    if (lst.ContainsKey("ArrivalLocation"))
                    {
                        sb.Append("<td>" + lst["ArrivalLocation"] + "</td>");
                    }
                    else
                    {
                        sb.Append("<td>" + "" + "</td>");
                    }
                    if (lst.ContainsKey("ArrivalDate"))
                    {
                        sb.Append("<td>" + lst["ArrivalDate"] + "</td>");
                    }
                    else
                    {
                        sb.Append("<td>" + "" + "</td>");
                    }
                    if (lst.ContainsKey("ViewFlight"))
                    {
                        sb.Append("<td>" + "<a href = " + lst["ViewFlight"] + "> Click here</a> " + "</td></tr>");
                    }
                    else
                    {
                        sb.Append("<td>" + "" + "</td></tr>");
                    }
                    tablestring += sb;
                    sb.Clear();
                }
                MailText = MailText.Replace("tablestring", tablestring);

                foreach (var item in _Model.ListEmailReceivers)
                {
                    _EMModel.ReceiverName = item.ReceiverName;

                    if (_emailConfigurations.IsEmailEnabled)
                    {
                        _EMModel.ReceiverEmail = item.ReceiverEmail;
                    }
                    else
                    {
                        _EMModel.ReceiverEmail = _emailConfigurations.TestMail;
                    }

                    MailText = MailText.Replace("EmailAddress", _EMModel.ReceiverEmail);
                }
                #endregion

                MailMessage EmailMessage = new MailMessage
                {
                    //Make TRUE because our body text is html  
                    IsBodyHtml = true,
                    //From = new MailAddress(_sendGridConfiguraiton.FromAddress),
                    From = new MailAddress(_emailConfigurations.SenderEmail),
                    Subject = _Model.EmailSubject,
                    Body = MailText
                };
                foreach (var item in _Model.ListEmailReceivers)
                {
                    _EMModel.ReceiverName = item.ReceiverName;

                    if (_emailConfigurations.IsEmailEnabled)
                    {
                        _EMModel.ReceiverEmail = item.ReceiverEmail;
                    }
                    else
                    {
                        _EMModel.ReceiverEmail = _emailConfigurations.TestMail;
                    }
                    EmailMessage.To.Add(new MailAddress(_EMModel.ReceiverEmail));
                }

                #region Adding CC and Bcc Mailers

                List<EmailAddress> lstcc = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(_Model.Cc))
                {
                    EmailMessage.CC.Add(_Model.Cc);
                    foreach (string value in _Model.Cc.Split(','))
                    {
                        lstcc.Add(new EmailAddress(value));
                    }
                }

                List<EmailAddress> lstbcc = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(_Model.Bcc))
                {
                    EmailMessage.Bcc.Add(_Model.Bcc);
                    foreach (string value in _Model.Bcc.Split(','))
                    {
                        lstbcc.Add(new EmailAddress(value));
                    }
                }
                #endregion

                if (_sendGridConfiguraiton.IsSendGridEmail)
                {
                    bool status = await SendGridTemplatebasedEmail(_Model, EmailMessage, _EMModel, lstcc, lstbcc);
                    if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
                else
                {
                    bool status = await SendMailAsync(EmailMessage);
                    if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Responsemessage = ex.ToString();
                _logger.Error(ex.ToString());
            }
            return response;
        }
        #endregion

        #region Send Customised Email Notification
        public async Task<Emailresponse> SendCustomEmailNotification(CustomNotificationResources Json, string fileuploadpath)
        {
            Emailresponse response = new Emailresponse
            {
                Responsestatus = false
            };
            try
            {
                var EmailMessage = new MailMessage();

                EmailNotificationResources _EMModel = new EmailNotificationResources();

                ////Settings.  
                EmailMessage.From = new MailAddress(_emailConfigurations.SenderEmail);
                EmailMessage.Subject = Json.EmailSubject;
                EmailMessage.Body = Json.EmailTemplate;
                EmailMessage.IsBodyHtml = true;
                List<SendGrid.Helpers.Mail.Attachment> lstattachmts = new();

                if (!String.IsNullOrEmpty(fileuploadpath))
                {
                    string[] attachments = fileuploadpath.Split(',');
                    foreach (var item in attachments)
                    {
                        System.Net.Mail.Attachment attachitem = new System.Net.Mail.Attachment(item);
                        EmailMessage.Attachments.Add(attachitem);
                        lstattachmts.Add(Sendgridextension.GetSendGridAttachment(attachitem));
                    }
                    ////System.Net.Mail.Attachment attachitem = new System.Net.Mail.Attachment(fileuploadpath);
                    ////EmailMessage.Attachments.Add(attachitem);
                }

                #region Populatse Email Body Dynamic Fields
                string Jsondata = JsonConvert.SerializeObject(Json.EmailBodyVariables);
                ////List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Jsondata);
                ////foreach (Dictionary<string, string> lst in obj)
                foreach (var item in Json.EmailBodyVariables)
                {
                    ////foreach (KeyValuePair<string, string> item in lst)
                    ////{
                    string Field = item.Key;
                    string Value = item.Value;
                    EmailMessage.Body = EmailMessage.Body.Replace(Field, Value);
                    ////}
                }
                #endregion

                #region Loop the Emailers list
                foreach (var item in Json.ListEmailReceivers)
                {
                    _EMModel.ReceiverName = item.ReceiverName;

                    if (_emailConfigurations.IsEmailEnabled)
                    {
                        _EMModel.ReceiverEmail = item.ReceiverEmail;
                    }
                    else
                    {
                        _EMModel.ReceiverEmail = _emailConfigurations.TestMail;
                    }

                    EmailMessage.To.Add(new MailAddress(_EMModel.ReceiverEmail));
                }
                #endregion

                #region Adding CC and Bcc Mailers

                List<EmailAddress> lstcc = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(Json.Cc))
                {
                    EmailMessage.CC.Add(Json.Cc);
                    foreach (string value in Json.Cc.Split(','))
                    {
                        lstcc.Add(new EmailAddress(value));
                    }
                }

                List<EmailAddress> lstbcc = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(Json.Bcc))
                {
                    EmailMessage.Bcc.Add(Json.Bcc);
                    foreach (string value in Json.Bcc.Split(','))
                    {
                        lstbcc.Add(new EmailAddress(value));
                    }
                }
                #endregion


                if (_sendGridConfiguraiton.IsSendGridEmail)
                {
                    bool status = await SendGridCustomEmail(Json, EmailMessage, _EMModel, lstcc, lstbcc, lstattachmts);
                    if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
                else
                {
                    bool status = await SendMailAsync(EmailMessage);
                    if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Responsemessage = ex.ToString();
                _logger.Error(ex.ToString());
            }
            return response;
        }
        #endregion

        #region Send Static Email
        public async Task<Emailresponse> SendStaticEmailNotification(StaticNotificationResources json)
        {
            Emailresponse response = new()
            {
                Responsestatus = false
            };
            try
            {
                var EmailMessage = new MailMessage();

                EmailNotificationResources _EMModel = new EmailNotificationResources();

                ////Settings.  
                EmailMessage.From = new MailAddress(_emailConfigurations.SenderEmail);
                EmailMessage.Body = json.EmailTemplate;

                EmailMessage.IsBodyHtml = true;
                EmailMessage.Subject = json.EmailSubject;

                ////_EMModel.ReceiverEmail = json.ListEmailReceivers;
                ////EmailMessage.To.Add(new MailAddress(_EMModel.ReceiverEmail));

                #region Loop the Emailers list
                foreach (string value in json.ListEmailReceivers.Split(','))
                {
                    if (_emailConfigurations.IsEmailEnabled)
                    {
                        _EMModel.ReceiverEmail = value.ToString();
                    }
                    else
                    {
                        _EMModel.ReceiverEmail = _emailConfigurations.TestMail;
                    }
                    EmailMessage.To.Add(new MailAddress(_EMModel.ReceiverEmail));
                }
                #endregion

                #region Adding CC and Bcc Mailers
                ////if (!String.IsNullOrEmpty(json.Cc))
                ////{
                ////    EmailMessage.CC.Add(json.Cc);
                ////}
                ////if (!String.IsNullOrEmpty(json.Bcc))
                ////{
                ////    EmailMessage.Bcc.Add(json.Bcc);
                ////}

                List<EmailAddress> lstcc = new();
                if (!String.IsNullOrEmpty(json.Cc))
                {
                    EmailMessage.CC.Add(json.Cc);
                    foreach (string value in json.Cc.Split(','))
                    {
                        lstcc.Add(new EmailAddress(value));
                    }
                }

                List<EmailAddress> lstbcc = new();
                if (!String.IsNullOrEmpty(json.Bcc))
                {
                    EmailMessage.Bcc.Add(json.Bcc);
                    foreach (string value in json.Bcc.Split(','))
                    {
                        lstbcc.Add(new EmailAddress(value));
                    }
                }
                #endregion

                if (_sendGridConfiguraiton.IsSendGridEmail)
                {
                    bool status = await SendGridStaticEmail(json, EmailMessage, lstcc, lstbcc);
                    if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
                else
                {
                    bool status = await SendMailAsync(EmailMessage); if (status)
                    {
                        response.Responsemessage = Responsemessages.Emailsentsuccess;
                        response.Responsestatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Responsemessage = ex.ToString();
                _logger.Error(ex.ToString());
            }
            return response;
        }
        #endregion

        #region SendGrid Mail with Send Grid Credentials
        private async Task<bool> SendGridCustomEmail(CustomNotificationResources Json, MailMessage EmailMessage, EmailNotificationResources _EMModel, List<SendGrid.Helpers.Mail.EmailAddress> CC, List<SendGrid.Helpers.Mail.EmailAddress> Bcc, List<SendGrid.Helpers.Mail.Attachment> lstattachmts)
        {
            bool emailsentstatus;
            try
            {
                var apiKey = _sendGridConfiguraiton.ApiKey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(_sendGridConfiguraiton.FromAddress, "");
                var to = new EmailAddress(_EMModel.ReceiverEmail, "");
                var emailMessage = MailHelper.CreateSingleEmail(from, to, Json.EmailSubject, "", EmailMessage.Body);
                if (lstattachmts.Count > 0)
                {
                    foreach (var item in lstattachmts)
                    {
                        emailMessage.AddAttachment(item);
                    }
                }
                if (CC.Count > 0)
                {
                    emailMessage.AddCcs(CC);
                }
                if (Bcc.Count > 0)
                {
                    emailMessage.AddBccs(Bcc);
                }
                await client.SendEmailAsync(emailMessage);
                emailsentstatus = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw;
            }
            return emailsentstatus;
        }

        private async Task<bool> SendGridTemplatebasedEmail(TemplateBasedResource _Model, MailMessage EmailMessage, EmailNotificationResources _EMModel, List<SendGrid.Helpers.Mail.EmailAddress> CC, List<SendGrid.Helpers.Mail.EmailAddress> Bcc)
        {
            bool emailsentstatus;
            try
            {

                var apiKey = _sendGridConfiguraiton.ApiKey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(_sendGridConfiguraiton.FromAddress, "");
                var to = new EmailAddress(_EMModel.ReceiverEmail, "");
                ////var emailMessage = MailHelper.CreateSingleEmail(from, to, _Model.EmailSubject, "", EmailMessage.Body);
                var emailMessage = MailHelper.CreateSingleEmail(from, to, _Model.EmailSubject, "", EmailMessage.Body);
                if (CC.Count > 0)
                {
                    emailMessage.AddCcs(CC);
                }
                if (Bcc.Count > 0)
                {
                    emailMessage.AddBccs(Bcc);
                }
                await client.SendEmailAsync(emailMessage);
                emailsentstatus = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw;
            }
            return emailsentstatus;
        }

        private async Task<bool> SendGridStaticEmail(StaticNotificationResources _Model, MailMessage EmailMessage, List<SendGrid.Helpers.Mail.EmailAddress> CC, List<SendGrid.Helpers.Mail.EmailAddress> Bcc)
        {
            bool emailsentstatus;
            try
            {
                var apiKey = _sendGridConfiguraiton.ApiKey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(_sendGridConfiguraiton.FromAddress, "");
                List<EmailAddress> ToemailAddresses = Getemaillist(_Model.ListEmailReceivers);
                var emailMessage = MailHelper.CreateSingleEmailToMultipleRecipients(from, ToemailAddresses, _Model.EmailSubject, "", EmailMessage.Body);

                if (CC.Count > 0)
                {
                    emailMessage.AddCcs(CC);
                }
                if (Bcc.Count > 0)
                {
                    emailMessage.AddBccs(Bcc);
                }
                await client.SendEmailAsync(emailMessage);
                emailsentstatus = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw;
            }
            return emailsentstatus;
        }
        #endregion

        #region Send Normal Mail
        private async Task<bool> SendMailAsync(MailMessage Email)
        {
            bool emailsentstatus = false;
            try
            {
                var smtp = new SmtpClient
                {
                    UseDefaultCredentials = false
                };
                var credential = new NetworkCredential
                {
                    UserName = _emailConfigurations.SenderEmail,
                    Password = _emailConfigurations.SenderEmailPassword
                };

                smtp.Credentials = credential;
                smtp.Host = _emailConfigurations.EmailSenderHost;
                smtp.Port = Convert.ToInt32(_emailConfigurations.EmailSenderPort);
                smtp.EnableSsl = Convert.ToBoolean(_emailConfigurations.EmailIsSSL);
                smtp.SendCompleted += (s, e) =>
                {
                    smtp.Dispose();
                    Email.Dispose();
                };

                await smtp.SendMailAsync(Email);
                emailsentstatus = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw;
            }
            finally
            {
                ////Email.Attachments.Dispose();
            }
            return emailsentstatus;
        }
        #endregion

        private static List<EmailAddress> Getemaillist(string Emails)
        {
            try
            {
                List<EmailAddress> email = new();
                foreach (string value in Emails.Split(','))
                {
                    email.Add(new EmailAddress() { Email = value });
                }
                return email;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }


    }
}



