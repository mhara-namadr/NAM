﻿
using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.Core.IServices;
using FulcrumOne_Notifications_Service.Security;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Nexmo.Api;
using Twilio;
using NLog;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace FulcrumOne_Notifications_Service.Services
{
    public class SmsNotificationService : ISmsNotificationService
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();

        #region Constructor
        private readonly NexmoSMSConfiguration _nexmoSMSConfiguration;
        private readonly TwilioSMSConfiguration _TwillSMSConfig;

        public SmsNotificationService(IOptions<NexmoSMSConfiguration> NexmoConfig, IOptions<TwilioSMSConfiguration> TwilioConfig)
        {
            _nexmoSMSConfiguration = NexmoConfig.Value;
            _TwillSMSConfig = TwilioConfig.Value;
        }
        #endregion

        #region Send SMS Nexmo
        public string SendSMSNexmo(SmsSenderResources Model)
        {
            try
            {
                var client = new Nexmo.Api.Client(creds: new Nexmo.Api.Request.Credentials
                {
                    ApiKey = _nexmoSMSConfiguration.ApiKey,
                    ApiSecret = _nexmoSMSConfiguration.ApiSecret
                });

                var results = client.SMS.Send(request: new SMS.SMSRequest
                {
                    from = _nexmoSMSConfiguration.SenderNO,
                    to = Model.ReceiverNo,
                    text = Model.Message
                });
                return results.messages[0].status;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
        #endregion

        #region Send Custom SMS Nexmo
        public String SendCustomSMSNexmo(CustomSmsSenderResources Model)
        {
            try
            {
                var client = new Nexmo.Api.Client(creds: new Nexmo.Api.Request.Credentials
                {
                    ApiKey = _nexmoSMSConfiguration.ApiKey,
                    ApiSecret = _nexmoSMSConfiguration.ApiSecret
                });


                string SMSBody = Model.Message;
                #region Populatse SMS Body Dynamic Fields
                string Jsondata = JsonConvert.SerializeObject(Model.SMSBodyVariables);
                List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Jsondata);
                foreach (Dictionary<string, string> lst in obj)
                {
                    foreach (KeyValuePair<string, string> item in lst)
                    {
                        string Field = item.Key;
                        string Value = item.Value;
                        SMSBody = SMSBody.Replace(Field, Value);
                    }
                }
                #endregion

                var results = client.SMS.Send(request: new SMS.SMSRequest
                {
                    from = _nexmoSMSConfiguration.SenderNO,
                    to = Model.ReceiverNo,
                    text = SMSBody
                });
                return results.messages[0].status;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
        #endregion

        #region Send SMS Twilio
        public String SendSMSTwilio(SmsSenderResources Model)
        {
            try
            {
                string accountSid = _TwillSMSConfig.AccountSid;
                string authToken = _TwillSMSConfig.AuthToken;
                TwilioClient.Init(accountSid, authToken);

                var to = new PhoneNumber(Model.ReceiverNo);
                var message = MessageResource.Create(
                    to,
                    from: new PhoneNumber(_TwillSMSConfig.SenderNO),
                     body: $"" + Model.Message);

                return message.Status.ToString();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
        #endregion

        #region Send Custom SMS Twilio
        public String SendCustomSMSTwilio(CustomSmsSenderResources Model)
        {
            try
            {
                string accountSid = _TwillSMSConfig.AccountSid;
                string authToken = _TwillSMSConfig.AuthToken;
                TwilioClient.Init(accountSid, authToken);
                string SMSBody = Model.Message;

                #region Populatse SMS Body Dynamic Fields
                string Jsondata = JsonConvert.SerializeObject(Model.SMSBodyVariables);
                List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Jsondata);
                foreach (Dictionary<string, string> lst in obj)
                {
                    foreach (KeyValuePair<string, string> item in lst)
                    {
                        string Field = item.Key;
                        string Value = item.Value;
                        SMSBody = SMSBody.Replace(Field, Value);
                    }
                }
                #endregion

                var to = new PhoneNumber(Model.ReceiverNo);
                var message = MessageResource.Create(to,
                                                     from: new PhoneNumber(_TwillSMSConfig.SenderNO),
                                                     body: SMSBody);

                return message.Status.ToString();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
        #endregion

    }
}
