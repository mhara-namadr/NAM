﻿
using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.Core.IServices;
using FulcrumOne_Notifications_Service.Core.Repository;
using FulcrumOne_Notifications_Service.DBModels;
using NLog;

namespace FulcrumOne_Notifications_Service.Services
{
    public class BatchEmailService : IBatchEmailService
    {
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IBatchEmailRepository _repository;

        public BatchEmailService(IBatchEmailRepository repo)
        {
            _repository = repo;
        }

        public async Task<Emailresponse> Addcustomemailbatch(customemailschedulebatch Model)
        {
            try
            {
                var result = await _repository.Addcustomemailbatch(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<Emailresponse> Addnewemailbatch(EmailScheduleBatch Model)
        {
            try
            {
                var result = await _repository.Addnewemailbatch(Model);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<Emailresponse> Deleteemailbatchdata(string Batchname)
        {
            try
            {
                var result = await _repository.Deleteemailbatchdata(Batchname);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<customemailschedulebatch> Getcustomemailbatchdata(string Batchname)
        {
            try
            {
                var result = await _repository.Getcustomemailbatchdata(Batchname);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        public async Task<EmailScheduleBatch> Getemailbatchdata(string Batchname)
        {
            try
            {
                var result = await _repository.Getemailbatchdata(Batchname);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

    }
}
