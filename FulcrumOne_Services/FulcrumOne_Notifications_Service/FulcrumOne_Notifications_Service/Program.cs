using AutoMapper;
using FulcrumOne_Notifications_Service.Core.IServices;
using FulcrumOne_Notifications_Service.Core.Repository;
using FulcrumOne_Notifications_Service.Persisitence;
using FulcrumOne_Notifications_Service.Repository;
using FulcrumOne_Notifications_Service.Security;
using FulcrumOne_Notifications_Service.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text; 
using Microsoft.OpenApi.Models;
using Ingress.Consumer.Notifications.Extensions;
using FulcrumOne_Notifications_Service.Infrastructure.Notifications.Subscribers.Handlers;
using Ingress.Consumer.Notifications.Models;
using Ingress.Consumer.Meta;

var builder = WebApplication.CreateBuilder(args);
 
// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region  RegisterServiceBusDependencies

builder.Services.AddServiceBusNotifications(builder.Configuration);
builder.Services.AddScoped<NewUserCreatedEventHandler>();

#endregion

#region Bearer Token Authentication
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."
    });
    options.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement {
        {
            new Microsoft.OpenApi.Models.OpenApiSecurityScheme {
                    Reference = new Microsoft.OpenApi.Models.OpenApiReference {
                        Type = Microsoft.OpenApi.Models.ReferenceType.SecurityScheme,
                            Id = "Bearer"
                    }
                },
                new string[] {}
        }
    });
});
var tokenOptions = builder.Configuration.GetSection("TokenOptions").Get<TokenOptions>();
builder.Services.Configure<TokenOptions>(builder.Configuration.GetSection("TokenOptions"));
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(jwtBearerOptions =>
{
    var symmetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenOptions.Key));
    jwtBearerOptions.IncludeErrorDetails = true;
    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = false,
        ValidIssuer = tokenOptions.Issuer,
        ValidAudience = tokenOptions.Audience,
        IssuerSigningKey = symmetricKey,
        ClockSkew = TimeSpan.Zero
    };
});
#endregion

////Add Automapper Dependency
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

#region Enable CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowOrigin",
        builder => builder.WithOrigins("*").AllowAnyHeader().AllowAnyMethod());
});
#endregion

#region DB Settings
var connection = builder.Configuration.GetConnectionString("MySqlConnectionString");
var serverVersion = new MySqlServerVersion(new Version(5, 7, 0));
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseMySql(connection, serverVersion)
);
#endregion

#region Service Dependencies
builder.Services.AddScoped<ISmsNotificationService, SmsNotificationService>();
builder.Services.AddScoped<IBatchEmailService, BatchEmailService>();
builder.Services.AddScoped<IEmailNotificationService, EmailNotificationService>();
builder.Services.AddScoped<IBatchEmailRepository, BatchEmailRepository>();
#endregion

#region Get Appsettings data
builder.Services.Configure<EmailConfiguration>(builder.Configuration.GetSection("EmailConfiguration"));
builder.Services.AddSingleton<EmailConfiguration>();
builder.Services.Configure<SendGridConfiguration>(builder.Configuration.GetSection("SendGridConfiguration"));
builder.Services.AddSingleton<SendGridConfiguration>();
builder.Services.Configure<NexmoSMSConfiguration>(builder.Configuration.GetSection("NexmoSMSConfiguration"));
builder.Services.AddSingleton<NexmoSMSConfiguration>();
builder.Services.Configure<TwilioSMSConfiguration>(builder.Configuration.GetSection("TwilioSMSConfiguration"));
builder.Services.AddSingleton<TwilioSMSConfiguration>();
#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
 
//}

app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();

app.MapControllers();

/// <summary>
/// Enable CORS
/// </summary>
app.UseCors("AllowOrigin");
ConfigureServiceBus(app);

static void ConfigureServiceBus(IApplicationBuilder applicationBuilder)
{
    var eventBus = applicationBuilder.ApplicationServices.GetRequiredService<IEventBus>();
    eventBus.Subscribe<NewUserCreatedEvent, NewUserCreatedEventHandler>();
}

app.Run();
