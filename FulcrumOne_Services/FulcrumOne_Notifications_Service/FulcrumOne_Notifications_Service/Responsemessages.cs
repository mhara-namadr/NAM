﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_Notifications_Service
{
    public static class Responsemessages
    {
        public const string Deletebatchsuccess = "Email Batch data deleted successfully.";
        public const string Deletebatcherror = "No data found for the Batch.";
        public const string Addednewbatchsuccess = "Successfully added new Email batch data.";
        public const string Emailsentsuccess = "Email Notification Sent Successfully.";


    }
}
