﻿using FulcrumOne_Notifications_Service.DBModels;
using Microsoft.EntityFrameworkCore;

namespace FulcrumOne_Notifications_Service.Persisitence
{
    public class AppDbContext : DbContext
    {
        public DbSet<EmailScheduleBatch> EmailScheduleBatch { get; set; }
        public DbSet<customemailschedulebatch> customemailschedulebatch { get; set; }

        //// protected override void OnConfiguring(DbContextOptionsBuilder options)
        ////  => options.UseSqlite("Data Source=Token_Generator.db3");
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EmailScheduleBatch>().HasKey(ur => new { ur.id });
            modelBuilder.Entity<customemailschedulebatch>().HasKey(ur => new { ur.id });
        }
    }
}
