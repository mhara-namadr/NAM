﻿using F1_Notification.Core.Repository;
using FulcrumOne_Notifications_Service.Persisitence;
using System.Threading.Tasks;

namespace F1_Notification.Persisitence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
