﻿
using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.DBModels; 

namespace FulcrumOne_Notifications_Service.Core.IServices
{
    public interface IBatchEmailService
    {
        Task<Emailresponse> Addnewemailbatch(EmailScheduleBatch Model);
        Task<EmailScheduleBatch> Getemailbatchdata(string Batchname);
        Task<Emailresponse> Deleteemailbatchdata(string Batchname);
        Task<Emailresponse> Addcustomemailbatch(customemailschedulebatch Model);

        Task<customemailschedulebatch> Getcustomemailbatchdata(string Batchname);
    }
}
