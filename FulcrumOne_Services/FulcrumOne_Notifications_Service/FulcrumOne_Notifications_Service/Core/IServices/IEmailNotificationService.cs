﻿  
using FulcrumOne_Notifications_Service.AppModels; 

namespace FulcrumOne_Notifications_Service.Core.IServices
{
    public interface IEmailNotificationService
    {

        Task<Emailresponse> SendEmailNotification(TemplateBasedResource _Model);
        Task<Emailresponse> SendCustomEmailNotification(CustomNotificationResources Json, string fileuploadpath);
        Task<Emailresponse> SendEmailNotificationWithTabulardata(TemplateBasedResource _Model);
        Task<Emailresponse> SendStaticEmailNotification(StaticNotificationResources json);
   
    
    }
}
