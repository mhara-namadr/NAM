﻿
using FulcrumOne_Notifications_Service.AppModels; 

namespace FulcrumOne_Notifications_Service.Core.IServices
{
    public interface ISmsNotificationService
    {
        string SendSMSNexmo(SmsSenderResources Model);
        String SendSMSTwilio(SmsSenderResources Model);
        String SendCustomSMSTwilio(CustomSmsSenderResources Model);
        String SendCustomSMSNexmo(CustomSmsSenderResources Model);
    }
}
