﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace F1_Notification.Core.Repository
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();

    }
}
