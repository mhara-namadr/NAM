﻿
using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.DBModels; 

namespace FulcrumOne_Notifications_Service.Core.Repository
{
    public interface IBatchEmailRepository
    {
        Task<Emailresponse> Addnewemailbatch(EmailScheduleBatch Model);
        Task<EmailScheduleBatch> Getemailbatchdata(string Batchname);
        Task<customemailschedulebatch> Getcustomemailbatchdata(string Batchname);
        Task<Emailresponse> Deleteemailbatchdata(string Batchname);
        Task<Emailresponse> Addcustomemailbatch(customemailschedulebatch Model);
    }
}
