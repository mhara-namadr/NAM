﻿using Newtonsoft.Json;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class TemplateBasedResource
    {
        //[EnsureOneElement(ErrorMessage = "At least a receiver is required")]
        public List<EmailNotificationResources> ListEmailReceivers { get; set; }
        
        ////[ValidateBodyVariables] // use this validation if user requires atleast 1 email body variable
        public Dictionary<string,string> EmailBodyVariables { get; set; }


        [Required]
        public string EmailSubject { get; set; }

        public String TemplateName { get; set; }

        [EmailBlank]
        public string Cc { get; set; }

        [EmailBlank]
        public string Bcc { get; set; }

    }
    public class ValidateBodyVariablesAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string Jsondata = JsonConvert.SerializeObject(value);
            List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Jsondata);
            foreach (Dictionary<string, string> lst in obj)
            {
                if (lst.Count == 0)
                {
                    return new ValidationResult("Atleast 1 email body variable is required");
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            return null;
        }
    }
    public class EnsureOneElementAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is IList list)
            {
                return list.Count > 0;
            }
            return false;
        }
    }
}
