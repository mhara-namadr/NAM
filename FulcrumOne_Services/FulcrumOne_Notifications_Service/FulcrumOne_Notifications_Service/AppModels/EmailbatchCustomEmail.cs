﻿using System.ComponentModel.DataAnnotations;
//using ValidateBodyVariablesAttribute = F1_Email_Notification.Controllers.Resources.ValidateBodyVariablesAttribute;


namespace FulcrumOne_Notifications_Service.AppModels
{
    public class EmailbatchCustomEmail
    {
        [ValidEmailWithField]
        public string ToEmail { get; set; }

        [EmailBlank]
        public string Bcc { get; set; }

        [EmailBlank]
        public string Cc { get; set; }

        [StringRange]
        public string Messagebody { get; set; }



        [Required]
        public bool RealtimeBatch { get; set; }

        [StringRange]
        public string Batchfor { get; set; }

        [StringRange]
        public string Emailsubject { get; set; }

        [Required]
        //[ValidateBodyVariables]
        //public List<dynamic> EmailBodyVariables { get; set; }
        public Dictionary<string, string> EmailBodyVariables { get; set; }
    }
}
