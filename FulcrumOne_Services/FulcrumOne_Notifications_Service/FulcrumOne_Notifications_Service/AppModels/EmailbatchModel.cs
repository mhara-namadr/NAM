﻿
using FulcrumOne_Notifications_Service.AppModels;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class EmailbatchModel
    {
        //[ValidEmail]
        //public string FromEmail { get; set; }
         
        [EmailBlank]
        public string ToEmail { get; set; }
         
        [EmailBlank]
        public string Bcc { get; set; }
         
        [EmailBlank]
        public string Cc { get; set; }

        [StringRange]
        public string Messagebody { get; set; }        
         
        [Required]
        public bool RealtimeBatch { get; set; }

        [StringRange]
        public string Batchfor { get; set; }

        [StringRange]
        public string Emailsubject { get; set; }

    }
}
