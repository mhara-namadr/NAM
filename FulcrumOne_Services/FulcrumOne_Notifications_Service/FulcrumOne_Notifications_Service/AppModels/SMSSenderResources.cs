﻿ 
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class SmsSenderResources
    {
        [Required(ErrorMessage = "The Receiver No is required")]
        public String ReceiverNo { get; set; }

        [Required(ErrorMessage ="Message body is required")]
        public String Message { get; set; }

    }
}
