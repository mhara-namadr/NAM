﻿ 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 
using System.Text.RegularExpressions; 

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class CustomcommonemailResources
    {
        public List<EmailNotificationResources> ListEmailReceivers { get; set; }

        [Required]
        public String EmailSubject { get; set; }

        [Required]
        public String EmailTemplate { get; set; }

        [Required]
        ////[ValidateBodyVariables]
        public Dictionary<string, string> EmailBodyVariables { get; set; }

        [EmailBlank]
        public string Cc { get; set; }

        [EmailBlank]
        public string Bcc { get; set; }

    }
    public class ValidEmailWithFieldAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string re = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
            if (value == null)
            {
                return new ValidationResult("Field  is missing.");
            }
            else if (String.IsNullOrEmpty(value.ToString()))
            {
                return new ValidationResult("Value cannot be null or Empty.");
            }
            else if (!String.IsNullOrEmpty(value.ToString()))
            {
                string Emails = value.ToString();
                string[] emailsArr = Emails.Split(',');
                foreach (string email in emailsArr)
                {
                    if (!Regex.IsMatch(email, re))
                    {
                        return new ValidationResult("Please enter a valid Email");
                    }
                }
                return ValidationResult.Success;
            }
            return ValidationResult.Success;
        }
    }

    public class ValidEmailOnlyAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string re = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

            if (String.IsNullOrEmpty(value.ToString()))
            {
                return new ValidationResult("Value cannot be null or Empty.");
            }
            else if (!String.IsNullOrEmpty(value.ToString()))
            {
                string Emails = value.ToString();
                string[] emailsArr = Emails.Split(',');
                foreach (string email in emailsArr)
                {
                    if (!Regex.IsMatch(email, re))
                    {
                        return new ValidationResult("Please enter a valid Email");
                    }
                }
                return ValidationResult.Success;
            }
            return ValidationResult.Success;
        }
    }

    public class EmailBlankAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string re = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
            if (value == null)
            {
                return new ValidationResult("Field  is missing.");
            }
            else if (!String.IsNullOrEmpty(value.ToString()))
            {
                string Emails = value.ToString();
                string[] emailsArr = Emails.Split(',');
                foreach (string email in emailsArr)
                {
                    if (!Regex.IsMatch(email, re))
                    {
                        return new ValidationResult("Please enter a valid Email");
                    }
                }
                return ValidationResult.Success;
            }
            return ValidationResult.Success;
        }
    }
}
