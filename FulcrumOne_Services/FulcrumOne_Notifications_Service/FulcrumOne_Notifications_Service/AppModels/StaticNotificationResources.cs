﻿ 
using System.ComponentModel.DataAnnotations; 

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class StaticNotificationResources
    {
        [Required]
        [StringRange]
        public String ListEmailReceivers { get; set; }

        [Required]
        [StringRange]
        public String EmailSubject { get; set; }

        [Required]
        [StringRange]
        public String EmailTemplate { get; set; }

        [EmailBlank]
        public string Cc { get; set; }

        [EmailBlank]
        public string Bcc { get; set; }


    }
    //public class StringRangeAttribute : ValidationAttribute
    //{
    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        if (value.ToString() != "string" || value.ToString() == "String" || value.ToString() == "STRING")
    //        {
    //            return ValidationResult.Success;
    //        }
    //        return new ValidationResult("Please enter a correct value");
    //    }
    //}
}
