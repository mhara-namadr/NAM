﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class ServiceRegistrationModel
    {
        public ServiceRegistrationModel()
        {
            EndDate = DateTime.Today.AddYears(1);
            LinceseKey = Guid.NewGuid().ToString();
            IsActive = true;
            CreatedDate = DateTime.Today.Date;
        }

        [Required(ErrorMessage = "Host Name is required")]
        public string Hostname { get; set; }

        [Required(ErrorMessage = "IP Address is required")]
        [RegularExpression(@"^(?:[0-9]{1,3}.){3}[0-9]{1,3}$")]
        [CustomIPAddressValidator]
        public string IPAddress { get; set; }

        [Required]
        [MaxLength(5)]
        [MinLength(1)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Port no should be numeric")]
        public string Port { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public Boolean IsActive { get; set; }

        public int Hitcount { get; set; }

        public string LinceseKey { get; set; }
        [JsonProperty(PropertyName = "id")]
        public String ID { get; set; }

        public String registrationid { get; set; }

        public String Responsermessage { get; set; }

    }

    public class CustomIPAddressValidatorAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string ip = Convert.ToString(value);
            string[] splitValues = ip.Split('.');

            if (String.IsNullOrWhiteSpace(ip))
            {
                return new ValidationResult("IP Address cannot be null.");
            }
            else if (splitValues.Length != 4)
            {
                return new ValidationResult("Please Enter a valid IP Address.");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
