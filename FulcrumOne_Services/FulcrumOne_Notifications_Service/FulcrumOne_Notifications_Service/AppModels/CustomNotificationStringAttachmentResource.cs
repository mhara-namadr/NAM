﻿
using FulcrumOne_Notifications_Service.Extensions;
using Newtonsoft.Json; 
using System.ComponentModel.DataAnnotations; 

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class CustomNotificationStringAttachmentResource
    {

        public List<EmailNotificationResources> ListEmailReceivers { get; set; }

        [Required]
        public String EmailSubject { get; set; }

        [Required]
        public String EmailTemplate { get; set; }

        [Required]
        [ValidateBodyVariables]
        public Dictionary<string, string> EmailBodyVariables { get; set; }

        [Required(ErrorMessage = "Please enter Attachmentfiletype.")]
        [StringRange(ErrorMessage = "Please pass a valid attachment file type")]
        public string Attachmentfiletype { get; set; }

        [Base64range(ErrorMessage = "Please pass a valid Base 64 file")]
        public string Base64string { get; set; }

        [EmailBlank]
        public string Cc { get; set; }

        [EmailBlank]
        public string Bcc { get; set; }

    }
    public class StringRangeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Field  is missing.");
            }
            else if ((value.ToString().Trim().Length == 0) || (value.ToString() == "") || (value.ToString() == "string") || (value.ToString() == "String") || (value.ToString() == "STRING") || (value.ToString() == null))
            {
                return new ValidationResult("Please enter a correct value");
            }
            return ValidationResult.Success;
        }
    }
    public class Base64rangeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!Base64Validstring.IsBase64(value.ToString()))
            {
                return new ValidationResult("Please enter a valid Base 64 string");
            }
            return ValidationResult.Success;
        }
    }
}
