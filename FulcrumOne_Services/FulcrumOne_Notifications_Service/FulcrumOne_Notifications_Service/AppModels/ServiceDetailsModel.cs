﻿ 

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class ServiceDetailsModel
    {
        public string LicenseKey { get; set; }
        public string Hostname { get; set; }
    }
}
