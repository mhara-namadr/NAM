﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class CustomSmsSenderResources
    {
        [Required(ErrorMessage = "The Receiver No is required")]
        public String ReceiverNo { get; set; }

        [Required(ErrorMessage = "Message body is required")]
        public String Message { get; set; }

        [Required(ErrorMessage = "SMS body variables are required")]
        [ValidateBodyVariableAttribute]
        public List<dynamic> SMSBodyVariables { get; set; }

    }
    public class ValidateBodyVariableAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string Jsondata = JsonConvert.SerializeObject(value);
            List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Jsondata);
            foreach (Dictionary<string, string> lst in obj)
            {
                if (lst.Count == 0)
                {
                    return new ValidationResult("Atleast 1  body variable is required");
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            return null;
        }
    }
}
