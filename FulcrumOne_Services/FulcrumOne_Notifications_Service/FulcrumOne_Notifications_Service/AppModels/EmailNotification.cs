﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class EmailNotification
    {
        public int NotificationId { get; set; }

        [Required]
        public DateTime DateSent { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(255)]
        public string ReceiverEmail { get; set; }

        [Required]
        public string EmailSubject { get; set; }

        [Required]
        public string SenderEmail { get; set; }


    }
}
