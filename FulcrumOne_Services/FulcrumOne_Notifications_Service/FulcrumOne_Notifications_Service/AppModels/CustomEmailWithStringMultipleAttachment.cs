﻿ 

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class CustomEmailWithStringMultipleAttachment : CustomcommonemailResources
    {
        /// <summary>
        /// Attachments
        /// </summary>
        ////public Dictionary<string, string> Attachments { get; set; }


        public List<Attachmentsdata> Attachments { get; set; }

        ////public List<KeyValuePair<string, string>> Attachments { get; set; }
    }

    public class Attachmentsdata
    {
        [StringRange]
        public string filename { get; set; }

        [StringRange]
        public string filetype { get; set; }

        [Base64range]
        public string base64string { get; set; }
    }
}
