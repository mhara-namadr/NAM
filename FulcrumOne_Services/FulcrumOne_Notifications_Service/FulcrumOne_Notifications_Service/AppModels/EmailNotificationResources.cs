﻿using System.ComponentModel.DataAnnotations;

namespace FulcrumOne_Notifications_Service.AppModels
{
    public class EmailNotificationResources
    {
        [Required]
        public string ReceiverName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "This is Not a valid email")]
        public string ReceiverEmail { get; set; }
        
    }

}
