﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_Notifications_Service.DBModels
{
    [Table("EmailScheduleBatch")]
    public class EmailScheduleBatch
    {

        public int id { get; set; }
        //public string FromEmail { get; set; }
        public string Bcc { get; set; }
        public string Cc { get; set; }
        public string Messagebody { get; set; }
        public bool RealtimeBatch { get; set; }
        public string ToEmail { get; set; }
        public string Batchfor { get; set; }
        public string Emailsubject { get; set; }


    }
}
