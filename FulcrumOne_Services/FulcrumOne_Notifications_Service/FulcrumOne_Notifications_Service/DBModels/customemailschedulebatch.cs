﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_Notifications_Service.DBModels
{
    public class customemailschedulebatch
    {
        public int id { get; set; }
        //public string FromEmail { get; set; }
        public string Bcc { get; set; }
        public string Cc { get; set; }
        public string Messagebody { get; set; }
        public bool RealtimeBatch { get; set; }
        public string ToEmail { get; set; }
        public string Batchfor { get; set; }
        public string Emailsubject { get; set; }
        public string EmailBodyVariables { get; set; }
    }
}
