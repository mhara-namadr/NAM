﻿using AutoMapper; 
using FulcrumOne_Notifications_Service.AppModels;

namespace FulcrumOne_Notifications_Service.DBModels
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<EmailbatchModel, EmailScheduleBatch>();
            CreateMap<EmailbatchCustomEmail, customemailschedulebatch>();
        }
    }
}
