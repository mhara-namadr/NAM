﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

namespace FulcrumOne_Notifications_Service.Extensions
{

    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class LicenseMiddleware
    {
        private readonly RequestDelegate _next;

        public LicenseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext httpContext, IHostApplicationLifetime appLifetime)
        {
            #region Check License for Service if Expired then Stop applcation else continue
            bool Licensestatus = LicenseChecker.CheckLicenseandValidity();
            if (Licensestatus)
            {
                appLifetime.StopApplication();
            }
            #endregion

            return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class LicenseMiddlewareExtensions
    {
        public static IApplicationBuilder UseLicenseMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LicenseMiddleware>();
        }
    }
}
