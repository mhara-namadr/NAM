﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_Notifications_Service.Extensions
{
    public static class Base64Validstring
    {
        public static bool IsBase64(this string base64String)
        {
            // Credit: oybek https://stackoverflow.com/users/794764/oybek
            if (string.IsNullOrEmpty(base64String) || base64String.Length % 4 != 0
               || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r")
               || base64String.Contains("\n") || base64String.Contains("string") || base64String.Contains("String") || base64String.Contains("STRING"))
                return false;

            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                throw;
                // Handle the exception
            }
        }
    }
}
