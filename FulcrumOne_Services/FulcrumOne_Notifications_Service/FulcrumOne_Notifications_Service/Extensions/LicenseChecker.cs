﻿
using FulcrumOne_Notifications_Service.AppModels;
using System.Text; 

namespace FulcrumOne_Notifications_Service.Extensions
{
    public static class LicenseChecker
    {
        public static bool CheckLicenseandValidity()
        {
            try
            {
                ApiServices Service = new ApiServices();
                ////var res = Service.AddHitcount();

                var result = Service.GetDetailsbyhostname();
                if ((DateTime.Now > result.EndDate) || (result.Responsermessage == "Records Not Found"))
                {

                    //// Condition if License is expired
                    return true;
                }
                else
                {
                    //// Condition   License still their valid
                    Service.AddHitcount();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }


    public class ApiServices
    {
        #region Get Service details using Host name and License No from Appsettings
        public ServiceRegistrationModel GetDetailsbyhostname()
        {
            ServiceDetailsModel Model = new ServiceDetailsModel();
            string ServiceregistrationAPI = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("LicenseAppSettings")["ServiceregistrationAPI"];
            Model.Hostname = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("LicenseAppSettings")["Hostnameofservice"];
            Model.LicenseKey = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("LicenseAppSettings")["LicenseKey"];

            ServiceRegistrationModel Modelobj = new ServiceRegistrationModel();
            try
            {
                string apiUrl = ServiceregistrationAPI + "api/GetDetailsbyhostnamelicense";
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(Model);
                    var httpContent = new StringContent(serializedObject, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = client.PostAsync(apiUrl, httpContent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync();
                        Modelobj = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceRegistrationModel>(data.Result);
                        //// Modelobj = table;
                    }
                }
                return Modelobj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        #endregion

        #region Add 1 to Hit Count by using the API of the Service Registration APP
        public ServiceRegistrationModel AddHitcount()
        {
            ServiceDetailsModel Model = new ServiceDetailsModel();
            string ServiceregistrationAPI = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("LicenseAppSettings")["ServiceregistrationAPI"];
            Model.Hostname = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("LicenseAppSettings")["Hostnameofservice"];
            Model.LicenseKey = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("LicenseAppSettings")["LicenseKey"];

            ServiceRegistrationModel Modelobj = new ServiceRegistrationModel();
            try
            {
                //// string apiUrls = "https://localhost:44300/api/GetDetailsbyhostname/F1Test";
                string apiUrl = ServiceregistrationAPI + "api/AddHitcount";
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    string serializedObject = Newtonsoft.Json.JsonConvert.SerializeObject(Model);
                    var httpContent = new StringContent(serializedObject, Encoding.UTF8, "application/json");

                    var httpResponse = client.PutAsync(apiUrl, httpContent).Result;

                    if (httpResponse != null)
                    {
                        var data = httpResponse.Content.ReadAsStringAsync();
                        var table = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceRegistrationModel>(data.Result);
                        Modelobj = table;
                    }
                }
                return Modelobj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        #endregion

    }
}
