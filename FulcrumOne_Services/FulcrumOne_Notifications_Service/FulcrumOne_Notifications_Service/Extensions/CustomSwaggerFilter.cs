﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FulcrumOne_Notifications_Service.Extensions
{
    public class CustomSwaggerFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var restrictedroutes = swaggerDoc.Paths
                .Where(x => !x.Key.ToLower().Contains("Custom"))
                .ToList();
            restrictedroutes.ForEach(x => { swaggerDoc.Paths.Remove(x.Key); });
        }
    }
}
