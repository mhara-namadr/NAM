﻿using Microsoft.Azure.ServiceBus;
using System.Text;

namespace FulcrumOne_Notifications_Service.Extensions
{
    public static class SubscriberClass
    {
        static ISubscriptionClient subscriptionClient;
        static string sbConnectionString = "Endpoint=sb://servicebusfulcrumone.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=5N2HtSf/uSfUj0qgA8qbwKnxArI9dDtf9lzaM7Oqwqk=";
        static string sbTopic = "mytopic";
        //string sbSubscription = "akki5677";
        static string sbSubscription = "Emailsub";

        public static void Consume()
        {
            subscriptionClient = new SubscriptionClient(sbConnectionString, sbTopic, sbSubscription);
            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };
            subscriptionClient.RegisterMessageHandler(ReceiveMessagesAsync, messageHandlerOptions);

        }

        static async Task<string> ReceiveMessagesAsync(Message message, CancellationToken token)
        {
            try
            {
                Console.WriteLine($"Subscribed message: {Encoding.UTF8.GetString(message.Body)}");
                var messg = Encoding.UTF8.GetString(message.Body);
                await subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
                return messg;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine(exceptionReceivedEventArgs.Exception);
            return Task.CompletedTask;
        }


    }
}
