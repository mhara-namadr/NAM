﻿
using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.Core.IServices;
using Ingress.Consumer.Meta;
using Ingress.Consumer.Notifications.Models;
using System;
using System.Threading.Tasks;

namespace FulcrumOne_Notifications_Service.Infrastructure.Notifications.Subscribers.Handlers
{
    public class NewUserCreatedEventHandler : IEventHandler<NewUserCreatedEvent>
    {
        ////private readonly ILogger<NewUserCreatedEventHandler> _logger;

        private const string LOGGING_PREFIX = "[NewUserCreatedEventHandler:F1EmailNotificationService]";

        private readonly IEmailNotificationService _EmailNotificationService;
        public NewUserCreatedEventHandler(IEmailNotificationService emailnotificationservc)
        {
            _EmailNotificationService = emailnotificationservc;
        }

        public async Task Handle(NewUserCreatedEvent @event)
        {
            ////_logger.LogInformation($"{LOGGING_PREFIX} Started handling event with id '{@event.Id}'");
            try
            {
                var productsIds = @event.Emailmessage;

                StaticNotificationResources res = new StaticNotificationResources();
                res.EmailTemplate = @event.Emailmessage;
                res.EmailSubject = @event.Emailsubject;
                res.ListEmailReceivers = @event.Receiveremail;
                var result = await _EmailNotificationService.SendStaticEmailNotification(res);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                ////_logger.LogError(e, $"{LOGGING_PREFIX} Error occured during handling event with id '{@event.Id}'");
                throw;
            }

            ////_logger.LogInformation($"{LOGGING_PREFIX} Successfully handled event with id '{@event.Id}'");
        }
    }
}
