﻿ 
using FulcrumOne_Notifications_Service.AppModels;
using FulcrumOne_Notifications_Service.DBModels;
using FulcrumOne_Notifications_Service.Persisitence; 
using Microsoft.EntityFrameworkCore;
using NLog;   
using FulcrumOne_Notifications_Service.Core.Repository;

namespace FulcrumOne_Notifications_Service.Repository
{
    public class BatchEmailRepository : IBatchEmailRepository
    {
        private readonly AppDbContext _context;
        private static readonly NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        public BatchEmailRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Emailresponse> Addnewemailbatch(EmailScheduleBatch Model)
        {
            Emailresponse response = new();
            try
            {
                _context.EmailScheduleBatch.Add(Model);
                await _context.SaveChangesAsync();
                response.Responsestatus = true;
                response.Responsemessage = Responsemessages.Addednewbatchsuccess;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response.Responsestatus = false;
                response.Responsemessage = ex.ToString();
            }
            return response;
        }

        public async Task<EmailScheduleBatch> Getemailbatchdata(string Batchname)
        {
            try
            {
                var result = await _context.EmailScheduleBatch.Where(c => c.Batchfor == Batchname).FirstOrDefaultAsync();

                if (result == null)
                {
                    return new EmailScheduleBatch();
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        public async Task<Emailresponse> Deleteemailbatchdata(string Batchname)
        {
            Emailresponse response = new Emailresponse();
            try
            {
                var batchdata = await _context.EmailScheduleBatch.Where(c => c.Batchfor == Batchname).FirstOrDefaultAsync();
                if (batchdata != null)
                {
                    _context.Remove(batchdata);
                    await _context.SaveChangesAsync();
                    response.Responsestatus = true;
                    response.Responsemessage = Responsemessages.Deletebatchsuccess;
                }
                else
                {
                    response.Responsestatus = false;
                    response.Responsemessage = Responsemessages.Deletebatcherror;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response.Responsestatus = false;
                response.Responsemessage = ex.ToString();
            }
            return response;
        }


        public async Task<Emailresponse> Addcustomemailbatch(customemailschedulebatch Model)
        {
            Emailresponse response = new Emailresponse();
            try
            {
                _context.customemailschedulebatch.Add(Model);
                await _context.SaveChangesAsync();
                response.Responsestatus = true;
                response.Responsemessage = Responsemessages.Addednewbatchsuccess;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response.Responsestatus = false;
                response.Responsemessage = ex.ToString();
            }
            return response;
        }

        public async Task<customemailschedulebatch> Getcustomemailbatchdata(string Batchname)
        {
            try
            {
                var result = await _context.customemailschedulebatch.Where(c => c.Batchfor == Batchname).FirstOrDefaultAsync();

                if (result == null)
                {
                    return new customemailschedulebatch();
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }


    }
}
