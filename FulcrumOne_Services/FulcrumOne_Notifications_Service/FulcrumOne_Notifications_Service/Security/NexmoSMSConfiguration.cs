﻿using System; 

namespace FulcrumOne_Notifications_Service.Security
{
    public class NexmoSMSConfiguration
    {
        public String ApiKey { get; set; }
        public String ApiSecret { get; set; }
        public String SenderNO { get; set; }

    }
}
