﻿using System; 

namespace FulcrumOne_Notifications_Service.Security
{
    public class TwilioSMSConfiguration
    {
        public String AccountSid { get; set; }
        public String AuthToken { get; set; }
        public String SenderNO { get; set; }


    }
}
