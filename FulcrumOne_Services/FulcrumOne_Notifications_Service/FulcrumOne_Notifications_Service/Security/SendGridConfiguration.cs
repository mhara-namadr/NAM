﻿namespace FulcrumOne_Notifications_Service.Security
{
    public class SendGridConfiguration
    {
        public string ApiKey { get; set; }

        public string FromAddress { get; set; }

        public bool IsSendGridEmail { get; set; }
    }
}
