﻿ 

namespace FulcrumOne_Notifications_Service.Security
{
    public class EmailConfiguration
    {
        public String SavedEmailTemplatesPath { get; set; }
        public String SenderEmail { get; set; }
        public String SenderEmailPassword { get; set; }
        public String EmailSenderHost { get; set; }
        public int EmailSenderPort { get; set; }
        public bool EmailIsSSL { get; set; }
        public bool IsEmailEnabled { get; set; }
        public String TestMail { get; set; }

    }
}
